*%
*%  TG2460-H.GPD  Copyright (c) CUSTOM ENGINEERING 
*%
*%

*GPDSpecVersion: "1.0"
*Include: "StdNames.gpd"
*GPDFileVersion: "1.03 INF 2.03"
*GPDFileName: "TG2460-H.GPD"
*ModelName: "CUSTOM TG2460-H"
*MasterUnits: PAIR(204, 204)
*ResourceDLL: "TG2460-H.Dll"
*PrinterType: SERIAL

*%******************ORIENTATION******************
*Feature: Orientation
{
    *rcNameID: =ORIENTATION_DISPLAY
    *DefaultOption: PORTRAIT
    *Option: PORTRAIT
    {
        *rcNameID: =PORTRAIT_DISPLAY
    }
    *Option: LANDSCAPE_CC270
    {
        *rcNameID: =LANDSCAPE_DISPLAY
    }
}

*%********************PAPER SOURCE******************
*Feature: InputBin
{
    *rcNameID: =PAPER_SOURCE_DISPLAY
}

*%********************INPUT WIDTH******************
*Feature: Input
{
  *Name: "Input"
  *FeatureType: PRINTER_PROPERTY
  *DefaultOption: InBin60

	*%****************** 56 mm ******************
    *Option: InBin56
    {
        *Name: "Width 56mm"
    }
	*%****************** 60 mm ******************
    *Option: InBin60
    {
        *Name: "Width 60mm"
    }
}


*%******************DATA COMPRESSION******************
*Feature: DataComp
{
  *Name: "Data Compression"
  *FeatureType: PRINTER_PROPERTY
  *DefaultOption: FE_RLE

  *Option: FE_RLE
  {
    *Name: "RLE Compression"
    *Command: CmdEnableFE_RLE { *Cmd : "<1B>*b2M" }
    *Command: CmdDisableCompression { *Cmd : "<1B>*b0M" }
  }
  *Option: NO_CMP
  {
    *Name: "NO Compression"
  }

}

*%******************PRINT RESOLUTION******************
*Feature: Resolution
{
    *rcNameID: =RESOLUTION_DISPLAY
    *DefaultOption: Option1
	*%****************** 204 x 204 ******************
    *Option: Option1
    {
        *Name: "204 x 204 " =DOTS_PER_INCH
        *DPI: PAIR(204, 204)
        *TextDPI: PAIR(204, 204)
        *MinStripBlankPixels: 1000
        EXTERN_GLOBAL: *StripBlanks: LIST(LEADING,ENCLOSED,TRAILING)
        *SpotDiameter: 140
        *Command: CmdSendBlockData { *Cmd : "<1B>*b" %d{NumOfDataBytes }"W" }
        *Command: CmdSelect
        {
            *Order: DOC_SETUP.7
            *Cmd: "<1B>*t200R"
        }
    }
}

*%****************** PAPER SIZE ******************
*Feature: PaperSize
{
    *rcNameID: =PAPER_SIZE_DISPLAY
    *FeatureType: DOC_PROPERTY
    *DefaultOption: RollShortPage


	*%****************** ROLL SHORT ******************
    *Option: RollShortPage
    {
        *Name: "TG2460-H Roll Short"
        *switch: Input
        {
			*case: InBin56
			{
				*PageDimensions: PAIR(448, 4176)
				*PrintableArea: PAIR(416,  4160)
	   			*PrintableOrigin: PAIR(0, 8)
	   			*Command: CmdSelect
				{
	    			*Order: PAGE_SETUP.9
	    			*Cmd: "<1B>&l" %d{PhysPaperLength}"R<1B>&l" %d{PhysPaperWidth-32}"T"
				}
			}

			*case: InBin60
			{
				*PageDimensions: PAIR(480, 4176)
				*PrintableArea: PAIR(448, 4160)
				*PrintableOrigin: PAIR(0, 8)
				*Command: CmdSelect
				{
	   				*Order: PAGE_SETUP.9
    				*Cmd: "<1B>&l" %d{PhysPaperLength}"R<1B>&l" %d{PhysPaperWidth-32}"T"
				}
			}
        }
    }


*%****************** ROLL ******************
    *Option: RollPage
    {
        *Name: "TG2460-H Roll"
        *switch: Input
        {
			*case: InBin56
			{
				*PageDimensions: PAIR(448, 8352)
	        	*PrintableArea: PAIR(416, 8336)
				*PrintableOrigin: PAIR(0, 8)
				*Command: CmdSelect
				{
	    			*Order: PAGE_SETUP.9
	    			*Cmd: "<1B>&l" %d{PhysPaperLength}"R<1B>&l" %d{PhysPaperWidth-32}"T"
				}
			}

	   		*case: InBin60
			{
				*PageDimensions: PAIR(480, 8352)
				*PrintableArea: PAIR(448, 8336)
				*PrintableOrigin: PAIR(0, 8)
				*Command: CmdSelect
				{
	    			*Order: PAGE_SETUP.9
	    			*Cmd: "<1B>&l" %d{PhysPaperLength}"R<1B>&l" %d{PhysPaperWidth-32}"T"
				}
			}
        }
    }

	*%****************** 100mm ******************
    *Option: Page100mm
    {
        *Name: "TG2460-H 100 mm"
        *switch: Input
        {
			*case: InBin56
			{
				*PageDimensions: PAIR(448, 816)
			   	*PrintableArea: PAIR(416, 800)
				*PrintableOrigin: PAIR(0, 8)
				*Command: CmdSelect
				{
	    			*Order: PAGE_SETUP.9
	    			*Cmd: "<1B>&l" %d{PhysPaperLength}"P<1B>&l" %d{PhysPaperWidth-32}"T"
				}
			}

	   		*case: InBin60
			{
				*PageDimensions: PAIR(480, 816)
				*PrintableArea: PAIR(448, 800)
				*PrintableOrigin: PAIR(0, 8)
				*Command: CmdSelect
				{
		    		*Order: PAGE_SETUP.9
	    			*Cmd: "<1B>&l" %d{PhysPaperLength}"P<1B>&l" %d{PhysPaperWidth-32}"T"
				}
			}
        }
    }


	*%****************** 120mm ******************
    *Option: Page120mm
    {
        *Name: "TG2460-H 120 mm"
        *switch: Input
        {
			*case: InBin56
			{
				*PageDimensions: PAIR(448, 984)
	   			*PrintableArea: PAIR(416, 968)
				*PrintableOrigin: PAIR(0, 8)
				*Command: CmdSelect
				{
	    			*Order: PAGE_SETUP.9
	    			*Cmd: "<1B>&l" %d{PhysPaperLength}"P<1B>&l" %d{PhysPaperWidth-32}"T"
				}
			}

			*case: InBin60
			{
				*PageDimensions: PAIR(480, 984)
	  			*PrintableArea: PAIR(448, 968)
	   			*PrintableOrigin: PAIR(0, 8)
				*Command: CmdSelect
				{
	    			*Order: PAGE_SETUP.9
	    			*Cmd: "<1B>&l" %d{PhysPaperLength}"P<1B>&l" %d{PhysPaperWidth-32}"T"
				}
			}
        }
    }


	*%****************** 180mm ******************
    *Option: Page180mm
    {
        *Name: "TG2460-H 180 mm"
        *switch: Input
        {
			*case: InBin56
			{
				*PageDimensions: PAIR(448, 1470)
				*PrintableArea: PAIR(416, 1456)
				*PrintableOrigin: PAIR(0, 8)
				*Command: CmdSelect
				{
	    			*Order: PAGE_SETUP.9
	    			*Cmd: "<1B>&l" %d{PhysPaperLength}"P<1B>&l" %d{PhysPaperWidth-32}"T"
				}
			}

			*case: InBin60
			{
				*PageDimensions: PAIR(480, 1470)
				*PrintableArea: PAIR(448, 1456)
				*PrintableOrigin: PAIR(0, 8)
				*Command: CmdSelect
				{
					*Order: PAGE_SETUP.9
					*Cmd: "<1B>&l" %d{PhysPaperLength}"P<1B>&l" %d{PhysPaperWidth-32}"T"
				}
			}
        }
    }


	*%****************** 240mm ******************
    *Option: Page240mm
    {
        *Name: "TG2460-H 240 mm"
        *switch: Input
        {
			*case: InBin56
			{
				*PageDimensions: PAIR(448, 1960)
				*PrintableArea: PAIR(416, 1944)
				*PrintableOrigin: PAIR(0, 8)
				*Command: CmdSelect
				{
	    			*Order: PAGE_SETUP.9
	    			*Cmd: "<1B>&l" %d{PhysPaperLength}"P<1B>&l" %d{PhysPaperWidth-32}"T"
				}
			}

			*case: InBin60
			{
				*PageDimensions: PAIR(480, 1960)
				*PrintableArea: PAIR(448, 1944)
				*PrintableOrigin: PAIR(0, 8)
				*Command: CmdSelect
				{
					*Order: PAGE_SETUP.9
					*Cmd: "<1B>&l" %d{PhysPaperLength}"P<1B>&l" %d{PhysPaperWidth-32}"T"
				}
			}
        }
    }

	*%****************** CUSTOM PAPER SIZE ******************
    *Option: CUSTOMSIZE
    {
        *rcNameID: =USER_DEFINED_SIZE_DISPLAY
        *MinSize: PAIR(448, 240)
        *MaxSize: PAIR(480, 9248)
        *MaxPrintableWidth: 448
        *CenterPrintable?: FALSE
        *CustCursorOriginX: %d{32 + (PhysPaperWidth - 480) / 2}
        *CustCursorOriginY: %d{0}
        *CustPrintableOriginX: %d{32} *% *CustPrintableOriginX: %d{PrintableOrigin.X}
        *CustPrintableOriginY: %d{0}  *% *CustPrintableOriginY: %d{PrintableOrigin.Y}
        *CustPrintableSizeX: %d{PhysPaperWidth - 32}	*% %d{PhysPaperWidth - PrintableOrigin.X}
        *CustPrintableSizeY: %d{PhysPaperLength - 16}   *% %d{PhysPaperLength - PrintableOrigin.X}
        *Command: CmdSelect 
        {
            *Order: PAGE_SETUP.9
            *Cmd: "<1B>&l" %d{PhysPaperLength}"P<1B>&l" %d{PhysPaperWidth}"T"
        }
    }
}

*%*******************PRINT QUALITY********************
*Feature: PrintQuality
{
    *rcNameID: =TEXT_QUALITY_DISPLAY
    *DefaultOption: Option2

	*%****************** NOT SET BY DRIVER ******************
    *Option: Option1
    {
        *Name: "Not set by driver"
        *Command: CmdSelect
        {
            *Order: DOC_SETUP.3
			*Cmd: ""
        }
    }

	*%****************** NORMAL ******************
    *Option: Option2
    {
        *Name: "Normal"
        *Command: CmdSelect
        {
            *Order: DOC_SETUP.3
			*Cmd: "<1B>*o0Q"
        }
    }

	*%****************** HIGH SPEED ******************
    *Option: Option3
    {
        *Name: "High Speed"
        *Command: CmdSelect
        {
            *Order: DOC_SETUP.3
            *Cmd: "<1B>*o1Q"
        }
    }

}

*%****************PRINTER FEATURES: Print Density**************
*Feature: PrintDensity
{
    *rcNameID: =PRINTDENSITY_DISPLAY
    *DefaultOption: Option5
    *Option: Option1
    {
        *Name: "- 50 <25>"
        *Command: CmdSelect
        {
            *Order: PAGE_SETUP.11
            *Cmd: "<1B>&k0W"
        }
    }
    *Option: Option2
    {
        *Name: "- 37 <25>"
        *Command: CmdSelect
        {
            *Order: PAGE_SETUP.11
            *Cmd: "<1B>&k1W"
        }
    }
    *Option: Option3
    {
        *Name: "- 25 <25>"
        *Command: CmdSelect
        {
            *Order: PAGE_SETUP.11
            *Cmd: "<1B>&k2W"
        }
    }
    *Option: Option4
    {
        *Name: "- 12 <25>"
        *Command: CmdSelect
        {
            *Order: PAGE_SETUP.11
            *Cmd: "<1B>&k3W"
        }
    }
    *Option: Option5
    {
        *Name: "0 <25>"
        *Command: CmdSelect
        {
            *Order: PAGE_SETUP.11
            *Cmd: "<1B>&k4W"
        }
    }
    *Option: Option6
    {
        *Name: "+ 12 <25>"
        *Command: CmdSelect
        {
            *Order: PAGE_SETUP.11
            *Cmd: "<1B>&k5W"
        }
    }
    *Option: Option7
    {
        *Name: "+ 25 <25>"
        *Command: CmdSelect
        {
            *Order: PAGE_SETUP.11
            *Cmd: "<1B>&k6W"
        }
    }
    *Option: Option8
    {
        *Name: "+ 37 <25>"
        *Command: CmdSelect
        {
            *Order: PAGE_SETUP.11
            *Cmd: "<1B>&k7W"
        }
    }
    *Option: Option9
    {
        *Name: "+ 50 <25>"
        *Command: CmdSelect
        {
            *Order: PAGE_SETUP.11
            *Cmd: "<1B>&k8W"
        }
    }
    *Option: Option10
    {
        *Name: "Not set by driver"
        *Command: CmdSelect
        {
            *Order: PAGE_SETUP.11
            *Cmd: ""
        }
    }
}

*%*******************HALFTONING********************
*Feature: Halftone
{
    *rcNameID: =HALFTONING_DISPLAY
    *DefaultOption: HT_PATSIZE_AUTO
    *Option: HT_PATSIZE_AUTO
    {
        *rcNameID: =HT_AUTO_SELECT_DISPLAY
    }
    *Option: HT_PATSIZE_SUPERCELL_M
    {
        *rcNameID: =HT_SUPERCELL_DISPLAY
    }
    *Option: HT_PATSIZE_6x6_M
    {
        *rcNameID: =HT_DITHER6X6_DISPLAY
    }
    *Option: HT_PATSIZE_8x8_M
    {
        *rcNameID: =HT_DITHER8X8_DISPLAY
    }
}

*%****************PRINTER FEATURES: Notch Alignment**************
*Feature: BlackMark
{
    *Name: "Notch Alignment"
    *FeatureType: DOC_PROPERTY
    *DefaultOption: Black_Mark_Alignement_Off
    *Option: Black_Mark_Alignement_Off
    {
        *Name: "Off"
        *Command: CmdEndPage
        {
           *Order: PAGE_FINISH.1
           *Cmd: "<1B>*rB"
        }

        *Name: "Off"
        *Command: CmdStartPage
		{
			*Order: PAGE_SETUP.1
			*Cmd: ""
		}
	}
    *Option: Black_Mark_Alignement_EP
    {
        *Name: "At end page"
        *Command: CmdEndPage
        {
           *Order: PAGE_FINISH.1
           *Cmd: "<1B>*rB<1DF8>"
        }

        *Name: "At end page"
        *Command: CmdStartPage
		{
			*Order: PAGE_SETUP.1
			*Cmd: ""
		}
    }
}

*%****************PRINTER FEATURES: Cut Mode**************
*Feature: CutMode
{
   *Name: "Cut Mode"
    *FeatureType: DOC_PROPERTY
    *DefaultOption: Cut_And_Recovery

    *Option: Tear_Off	*% To perform the tear off 
    {
       *Name: "Tear off"		
        *Command: CmdSelect
         {
             *Order: PAGE_FINISH.2
			 *Cmd: "<1CC0>3<0A0A0A0A>"	*% No cut command
         }
    }
    *Option: No_Cut		*% Printers with cutter but no cut and no tear off will be performed
    {
       *Name: "No cut"
        *Command: CmdSelect
         {
             *Order: PAGE_FINISH.2
			 *Cmd: "<1CC0>3"	*% No cut command
         }
    }
    *Option: Cut_And_Recovery
    {
       *Name: "Total cut and recovery"
        *Command: CmdSelect
        {
			*Order: PAGE_FINISH.2
			*Cmd: "<1CC0>4"		*% Cut and recovery command
        }
    }
    *Option: Cut_Without_Recovery
    {
       *Name: "Total cut without recovery"
        *Command: CmdSelect
        {
			*Order: PAGE_FINISH.2
			*Cmd: "<1CC0>5"		*% Cut without recovery command
        }
    }
}

*Command: CmdStartDoc
{
    *Order: DOC_SETUP.1
    *Cmd: "<181B>2"
}

*Command: CmdEndJob
{
    *Order: JOB_FINISH.1
    *Cmd: "<1D>P<0000>"
}

*IgnoreBlock
{

*Command: CmdEndPage
{
    *Order: PAGE_FINISH.1
    *Cmd: "<1B>*rB<1B>i"

}

} *% end *IgnoreBlock



*RotateCoordinate?: FALSE
*RotateRaster?: FALSE
*RotateFont?: FALSE
*TextCaps: LIST(TC_EA_DOUBLE,TC_IA_ABLE,TC_UA_ABLE,TC_RA_ABLE)
*CursorXAfterCR: AT_PRINTABLE_X_ORIGIN
*ReselectFont: LIST(AFTER_XMOVE)
*XMoveThreshold: 0
*YMoveThreshold: 0
*XMoveUnit: 204
*YMoveUnit: 204
*Command: CmdXMoveAbsolute { *Cmd : "<1B>*p" %d{DestX}"X" }
*Command: CmdYMoveAbsolute { *Cmd : "<1B>*p" %d{DestY}"Y" }
*Command: CmdCR { *Cmd : "<0D>" }
*Command: CmdLF { *Cmd : "<0A>" }
*Command: CmdFF { *Cmd : ""  }
*Command: CmdBackSpace { *Cmd : "<08>" }
*EjectPageWithFF?: TRUE
*OutputDataFormat: H_BYTE
*OptimizeLeftBound?: FALSE
*CursorXAfterSendBlockData: AT_GRXDATA_ORIGIN
*CursorYAfterSendBlockData: AUTO_INCREMENT
*DefaultCTT: 0

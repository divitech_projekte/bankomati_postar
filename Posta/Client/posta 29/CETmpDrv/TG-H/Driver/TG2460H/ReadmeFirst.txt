Driver package for CUSTOM ENGINEERING TG2460-H printer model

OS Supported:

Windows XP.
Windows Vista.
Windows 7.

Installation procedure:

a) Serial interface
For a serial interface printer you may execute the standard
printer installation procedure or follow the automatic wizard procedure.
For standard printer installation the needed file are stored into: "..\CeTmpDrv" directory.
b) USB interface
Follow the automatic wizard procedure.


This driver supports printer:
"CUSTOM TG2460-H"

The "CUSTOM TG2460-H" printer driver supports:
i)  paper width size of 60mm and printable region width is 56mm
ii) paper width size of 56mm and printable region width is 52mm

This printer supports some page format:

"TG2460-H Roll Short"
"TG2460-H Roll"
"TG2460-H 120 mm"
"TG2460-H 180 mm"
"TG2460-H 240 mm"

[ the defalut installation page is "TG2460-H Roll Short" ]


If the system driver properties section shows these format pages and other not here
specified, you must select only the above mentioned pages format.

Note about BlackMark Feature:
 we suggest to use "TG2460-H Roll" page format when You are using Notch Alignmemnt.





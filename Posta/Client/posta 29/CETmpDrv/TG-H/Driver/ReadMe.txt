============================================================================
Driver Installation Wizard

Copyright(C) 2013 Custom Engineering
All Rights Reserved
============================================================================

Please see the license agreement included in the license dialog for limitations on the duplication and distribution of this software and other important information.

============================================================================
System Requirements
============================================================================
 Operating System:    Windows� XP Home Edition
                      Windows� XP Professional Edition
                      Windows� Vista (User Account Control disabled) 32bit
                      Windows� Vista (User Account Control disabled) 64bit
                      Windows� 7 (User Account Control disabled) 32bit
                      Windows� 7 (User Account Control disabled) 64bit

                      TCP/IP protocol must be installed on the machine (for the Ethernet Port)

 Minimum Hardware:    
    CPU:              Pentium III PC or higher
    RAM:              128MB

 Disk Space:          12MB of space for the installed software

============================================================================
Kiosk & ATMs Printers
============================================================================

============================================================================
Drivers Package Feature 2.04
============================================================================

	Drivers Package:
	|
	|
	|
	+--- "CUSTOM TG2460-H" Printer
	|	|
	|	|
	|	+--- STANDARD driver (Package Rel. 2.03)
	|	|	|
	|	|	+--- GPD    	Rel. 1.03
	|	|
	|	|
	|	+--- FULL driver (Package Rel. 2.03)
	|		|
	|		+--- GPD    		Rel. 1.03
	|		+--- CeLm.dll  		Rel. 2.21
	|		+--- CeSmLm.dll		Rel. 3.25
	|		+--- TG2460-H_UI.dll	Rel. 1.00
	|		+--- TG2460-H_UNI.dll	Rel. 1.01
	|
	|
	|
	+--- "CUSTOM TG2480-H" Printer
		|
		|
		+--- STANDARD driver (Package Rel. 2.03)
		|	|
		|	+--- GPD    	Rel. 1.02
		|
		|
		+--- FULL driver (Package Rel. 2.04)
			|
			+--- GPD    		Rel. 1.02
			+--- CeLm.dll  		Rel. 2.21
			+--- CeSmLm.dll		Rel. 3.25
			+--- TG2480-H_UI.dll	Rel. 1.00
			+--- TG2480-H_UNI.dll	Rel. 1.01

		


============================================================================
Technical Support
============================================================================

More information and updates can be found on the following websites:

http://www.custom.it
http://www.custom.biz

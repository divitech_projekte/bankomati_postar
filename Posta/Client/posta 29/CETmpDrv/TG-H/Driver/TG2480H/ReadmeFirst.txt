Driver package for CUSTOM ENGINEERING TG2480-H printer model

OS Supported:

Windows 2000.
Windows XP.
Windows 2K3.
Windows Vista.
Windows 7.

Installation procedure:

Serial interface
For (W2K, XP, W2K3, Vista, 7) you must execute the standard printer installation procedure.

USB interface
Follow the automatic wizard procedure.

This driver supports printer:
"CUSTOM TG2480-H"

The "CUSTOM TG2480-H" printer driver supports:
i)   paper width size of 80mm and printable region width is 76mm
ii)  paper width size of 76mm and printable region width is 74mm
iii) paper width size of 66mm and printable region width is 64mm
iv)  paper width size of 56mm and printable region width is 54mm

This printer supports some page format:

"TG2480-H PTP" special page: see note below
"TG2480-H Roll Short"
"TG2480-H Roll"
"TG2480-H 140 mm"
"TG2480-H 180 mm"
"TG2480-H 240 mm"

[ the defalut installation page is "TG2480-H PTP" ]


If the system driver properties section shows these format pages and other not here
specified, you must select only the above mentioned pages format.

Note about BlackMark Feature:
 we suggest to use "TG2480-H Roll" page format when You are using Notch Alignmemnt.

Note for pages length:
 the minimum page length supported by printer hardware is 140mm.
 For pages shorter than 140mm (for example using Roll pages), the hardware automatically 
 inserts a 40mm paper shift at the end of the page.

Note for "TG2480-H PTP"
This pages must be used only for the "Printer Test Page" (PTP) operation.
Don't use this page to print out from applications.



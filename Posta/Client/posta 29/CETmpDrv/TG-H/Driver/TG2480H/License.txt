Read the following terms and conditions carefully. In downloading and utilizing Custom Engineering's files, 
the user agrees to accept and be bound by the said terms and conditions. If these terms and conditions 
are not acceptable, do not download any of the files in question.
This License grants the right to nonexclusive use of Custom Engineering Software and its respective 
documentation (<<Software">>)according to the terms and conditions set forth below. 

1. License Grant. The user may: (a) use and (b) copy the Software in legible form from a machine for
back-up purposes only or onto a single workstation, with the understanding that the Custom Engineering 
copyright and other proprietary notices be reproduced.

2. Restrictions. Distribution of copies of this Software is strictly forbidden. The Software contains 
trade secrets, and in order to protect said secrets it is forbidden to reverse-engineer, decompile, 
disassemble, modify, adapt, translate, lend, lease, rent, sell for profit, distribute or create product
spin-offs based on the Software or any part of it. 

3. Property rights. This Software is an industrial secret, property of Custom Engineering. Custom Engineering
holds the title, interest and intellectual property rigths to the Software and all its subsequent copies, 
independent of the form or manner in which they are made. The Software is protected by international 
copyright agreements. This License does not constitute the right to sell or market the Software. 

4. Termination. This License will remain in effect until terminated. The License will automatically 
terminate without prior notice if any of these conditions are not respected. With the termination of
the License, all copies of the Software, including partial copies, must be destroyed. 

5. Exclusion of Warranties. With this agreement, the user accepts all risks that could arise from 
the downloading of the Software, including, but not limited to, possible transmission errors or 
contamination of previously-stored data or programs. Custom Engineering does not grant any warranty,
either explicit or implicit, and in particular declines any warranty against the infrigement of
third party rights, warranties of merchantability or fitness for a particular purpose. 

6. Limitation of Damages. To the extent permitted by applicable law, Custom Engineering and its 
agents shall in no event be liable to the user for any damages, including, but no limited to, any 
direct, indirect, or consequential damages or loss of profits, data, or clientele, lost work time, 
computer damage or operating problems, or any other commercial loss or damages, even if Custom 
Engineering was advised in advance of the possibility of such damages. In addition, Custom Engineering
and its agents shall not be liable for any defects, errors or omissions in the Software that is used
and distributed. 

1. Export Control Laws. In downloading the Software, the user accept not to export or re-export said
Software. The user also agrees not to export, re-export or transship the Licensed Program or the direct
product thereof in any form without the express permission of Custom Engineering. 

2. Miscellaneous. This Agreement is the entire agreement between the parties as to subjets matter of 
this License. The terms of this License may be amended, waived or modified only in writing signed by 
both parties. The user agrees to assume sole responsibility for the legal and responsible use of this
Software. If one or more provisions of this License shall be held to be invalid, illegal or unenforceable
in any jurisdiction whatsoever, the validity, legality and enforceability of the remaining provisions
shall not be affected or impaired thereby.

3. We suggest that the user make a back-up copy of all important system and configuration data before 
using and installing the programs; we also advise against running the programs on a computer connected 
to a network. 

GOVERNING LAWS : 
Any decision regarding possible disputes falls jointly within the competence of Italian Judicial Law
under the authority of Parma (Italy) law-court- 

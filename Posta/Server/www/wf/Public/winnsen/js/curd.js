/* 用户业务操作页面的操作扩展 */


var baseurl = '/wf/Admin/';

/**
 * 数据添加操作
 * 定义控制器及具体方法
 * xucc
 * @param str
 * @param action
 * @param method
 */
function doAddByController(str, action, method) {
	$("#ffxAdd" + str).form('enableValidation');
	$("#ffxAdd" + str).form('submit', {
		url : baseurl + action + '/' + method,
		success : function(r_data) {
			var json = $.parseJSON(r_data);
			doJson(str, '新增操作', json);
		}
	});
}

/**
 * 获取编辑数据操作
 * 定义控制器
 * @param str
 * @param action
 * @param method
 */
function toEditByController(str, action) {
	var row = $('#dg'+str).datagrid('getSelected');
	if(row) {
		$("#ffxEdit"+str).form('disableValidation');
		$("#ffxEdit"+str).form('clear');
		$('#ffxEdit'+str).form('load', baseurl + action + '/ajaxGetDataById?id=' + row.id);
		$('#dlgEdit'+str).dialog('open');
	}
	else {
		$.messager.alert('提醒', '请选择一条数据进行编辑操作！');
	}
}

/**
 * 通过页面id跳转页面
 * 定义控制器和方法
 * @param str
 * @param action
 * @param method
 */
function toPageById(str, id, action, method) {
	var row = $('#dg'+str).datagrid('getSelected');
	if(row) {
		$("#" + id).form('disableValidation');
		$("#" + id).form('clear');
		$("#" + id).form('load', baseurl + action + '/' + method + '?id=' + row.id);
		$("#" + id).dialog('open');
	}
	else {
		$.messager.alert('提醒', '请选择一条数据进行操作！');
	}
}

/**
 * 数据编辑操作
 * 定义控制器及具体方法
 * @param str
 * @param action
 * @param method
 */
function doEditByController(str, action, method) {
	$("#ffxEdit" + str).form('submit', {
		url : baseurl + action + '/' + method,
		success : function(r_data) {
			var json = $.parseJSON(r_data);
			doJson(str, '编辑操作', json);
		}
	});
}

/**
 * List页面进行数据编辑
 * 定义控制器及具体方法
 * @param str
 * @param action
 * @param method
 */
function doEditOfListByController(str, action, method) {
	var row = $('#dg'+str).datagrid('getSelected');
	if(row) {
		$.messager.confirm('提醒', '你确定要编辑这条数据吗？', function(r) {
			if(r){
				 var map = new HashMap();
				 map.put("id", row.id);
				 ajaxPostData(str, baseurl + action + '/' + method, map);
			}
		});
	}
	else {
		$.messager.alert('提醒', '请选择一条数据进行编辑操作！');
	}
}

/**
 * 逻辑删除/作废一条数据
 * @param str
 * @param action
 */
function doVoidByController(str, action) {
	var row = $('#dg'+str).datagrid('getSelected');
	if(row) {
		$.messager.confirm('提醒', '你确定要作废这条数据吗？', function(r) {
			if(r){
				 var map = new HashMap();
				 map.put("id", row.id);
				 ajaxPostData(str, baseurl + action + '/ajaxVoid', map);
			}
		});
	}
	else {
		$.messager.alert('提醒', '请选择一条数据进行作废操作！');
	}
}

/**
 * 物理删除一条数据
 * @param str
 * @param action
 */
function doDeleteByController(str, action) {
	var row = $('#dg'+str).datagrid('getSelected');
	if(row) {
		$.messager.confirm('提醒', '你确定要删除这条数据吗？', function(r) {
			if(r){
				 var map = new HashMap();
				 map.put("id", row.id);
				 ajaxPostData(str, baseurl + action + '/ajaxDelete', map);
			}
		});
	}
	else {
		$.messager.alert('提醒', '请选择一条数据进行删除操作！');
	}
}

/**
 * ajax post 执行
 * @param str
 * @param url
 * @param map
 */
function ajaxPostData(str, url, map){
	var win = $.messager.progress({
		title : '请等待',
		msg : '数据正在处理中...'
	});
	var urldata = "";
	var keySet = map.keySet();
	for(var i in keySet){ 
		if(0 != i)
			urldata += "&";
		urldata += keySet[i] + "=" + map.get(keySet[i]);
	}
	$.ajax({
		async : true,                   /*同步通讯*/
		type : "POST",                  /*传输方式*/
		cache : false,                  /*下载最新数据（无缓存）*/
		url : url,                      /*请求地址*/
		data : urldata,
		success : function(obj) {
			$.messager.progress('close');
			var json = $.parseJSON(obj);
			doJson(str, '操作提示', json);
		}
	});
}

/**
 * 处理返回json信息
 * xucc
 * @param str
 * @param title
 * @param json
 */
function doJson(str, title, json) {
	if (json.status == '200') {
		$.messager.show({
			title : title,
			msg : json.msg
		});
		$('#dlgAdd'+str).dialog('close');
		$('#dlgEdit'+str).dialog('close');
		$('#dg'+str).datagrid('reload');
	}
	else if(json.status == '300') {
		$.messager.alert('警告', json.msg, 'warning');
	}
	else {
		
		$.messager.alert('错误', json.msg, 'error');
	}
}




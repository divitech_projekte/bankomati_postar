<?php
namespace Admin\Model;

/**
 * 终端信息
 * Admin\Model$SysClientModel
 * Copyright (c) 2016. 云绅（张家港）精密工业有限公司 版权所有
 * 修改人：jcccy
 * 修改时间：2016年3月7日 下午4:57:12
 * 修改内容：
 */
class SysClientModel extends BaseModel{
	// 定义自动验证   
	
	
	
	/**
	 * 数据修改
	 */
	public function verifyTo($data){
		if($this->create()){
			return $this->save($data);
		}else{
			return false;
		}
	
	}
}
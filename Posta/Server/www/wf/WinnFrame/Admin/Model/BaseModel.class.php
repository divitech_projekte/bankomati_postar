<?php
namespace Admin\Model;
use Common\Model;

/**
 * 后台模块模型基类
 * Admin\Model$BaseModel
 * Copyright (c) 2016. 云绅（张家港）精密工业有限公司 版权所有
 * 修改人：jcccy
 * 修改时间：2016-3-7 下午04:33:00
 * 修改内容：
 */
class BaseModel extends \Common\Model\MyModel{
	// 定义自动验证   
	protected $_validate    =   array(  
			
			
	);
	
	// 定义自动赋值
	protected $_auto    =   array(
			array('createtime','time',1,'function'),
			array('createuser','getUsername',1,'callback'),
			array('updatetime','time',2,'function'),
			array('updateuser','getUsername',2,'callback'),
	);
	
	/**
	 * 获得用户账户
	 */
	public function getUsername(){
		$logininfo = session('LOGININFO');
		return $logininfo['username'];
	}
	
	/**
	 * 数据添加
	 */
	public function addTo($data = NULL){
		if(NULL == $data && $this->create()){
			return $this->add();
		}else{
			return $this->add($data);
		}
	}
	
	/**
	 * 数据修改
	 */
	public function saveTo($data = NULL){
		if(NULL == $data && $this->create()){
			return $this->save();
		}else{
			return $this->save($data);
		}
	
	}
	
	/**
	 * 数据查询
	 * @param 查询条件 $where
	 * @param 页 $page
	 * @param 每页数量 $rows
	 */
	public function lists($where,$page,$rows){
	
	
	
		$data = array();
		$data['total']  = $this->where($where)->count();
		$data['rows'] =  $this->where($where)->order(array('createtime'=>'asc'))->page($page.','.$rows)->select();
	
		return $data;
	}
	
}
CREATE TABLE IF NOT EXISTS `wf_index`(
 `id`int(8)unsigned NOT NULL AUTO_INCREMENT,
 `data` varchar(255) NOT NULL,  
 PRIMARY KEY (`id`) 
 ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ;
 INSERT INTO `wf_index`(`id`,`data`) VALUES (1,'thinkphp'), (2,'php'), (3,'framework');
 
 CREATE TABLE `wf_form` (
  `id` smallint(4) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `content` varchar(255) NOT NULL,
  `create_time` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8



/* DROP DATABASE winnframe; */
CREATE DATABASE IF NOT EXISTS winnframe DEFAULT CHARACTER SET utf8;

/* 权限表 */
CREATE TABLE `wf_permission` (
   `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
   `title` VARCHAR(50) DEFAULT NULL COMMENT '权限简称',
   `content` VARCHAR(200) DEFAULT NULL COMMENT '备注',
   `action` VARCHAR(50) DEFAULT NULL COMMENT '权限',
   `count` INT(5) DEFAULT NULL COMMENT '热度',
   `isopen` TINYINT(1) DEFAULT 0 COMMENT '是否开放',
   `createtime` INT(11) UNSIGNED DEFAULT NULL COMMENT '创建时间',
   `createuid` INT(11) UNSIGNED DEFAULT NULL COMMENT '创建人',
   `updatetime` INT(11) UNSIGNED DEFAULT NULL COMMENT '修改时间',
   `updateuid` INT(11) UNSIGNED DEFAULT NULL COMMENT '修改人账户',
   `datastatus` INT(3) DEFAULT NULL COMMENT '数据状态',
   PRIMARY KEY (`id`),
   UNIQUE KEY `action` (`action`)
 ) ENGINE=MYISAM DEFAULT CHARSET=utf8;
 
 /* 账户表 */
 
 CREATE TABLE `wf_members` (
   `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
   `account` VARCHAR(50) DEFAULT NULL COMMENT '账户',
   `password` VARCHAR(200) DEFAULT NULL COMMENT '密码',
   `email` VARCHAR(200) DEFAULT NULL COMMENT '邮箱',
   `phone` VARCHAR(200) DEFAULT NULL COMMENT '手机',
   `status` INT(3) DEFAULT NULL COMMENT '账户状态',
   `title` VARCHAR(50) DEFAULT NULL COMMENT '标题',
   `content` VARCHAR(200) DEFAULT NULL COMMENT '备注',
   `createtime` INT(11) UNSIGNED DEFAULT NULL COMMENT '创建时间',
   `createuid` INT(11) UNSIGNED DEFAULT NULL COMMENT '创建人',
   `updatetime` INT(11) UNSIGNED DEFAULT NULL COMMENT '修改时间',
   `updateuid` INT(11) UNSIGNED DEFAULT NULL COMMENT '修改人账户',
   `datastatus` INT(3) DEFAULT NULL COMMENT '数据状态',
   PRIMARY KEY (`id`),
   UNIQUE KEY `account` (`account`),
   UNIQUE KEY `phone` (`phone`)
 ) ENGINE=MYISAM DEFAULT CHARSET=utf8;
 
 
  /* 角色表 */
 
 CREATE TABLE `wf_sys_role` (
   `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
   `title` VARCHAR(50) DEFAULT NULL COMMENT '角色名',
   `content` VARCHAR(200) DEFAULT NULL COMMENT '备注',
   `createtime` INT(11) UNSIGNED DEFAULT NULL COMMENT '创建时间',
   `createuid` INT(11) UNSIGNED DEFAULT NULL COMMENT '创建人',
   `updatetime` INT(11) UNSIGNED DEFAULT NULL COMMENT '修改时间',
   `updateuid` INT(11) UNSIGNED DEFAULT NULL COMMENT '修改人账户',
   `datastatus` INT(3) DEFAULT NULL COMMENT '数据状态',
   PRIMARY KEY (`id`),
   UNIQUE KEY `title` (`title`)
 ) ENGINE=MYISAM DEFAULT CHARSET=utf8;
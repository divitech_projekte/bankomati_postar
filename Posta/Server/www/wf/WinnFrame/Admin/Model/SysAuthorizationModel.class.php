<?php
namespace Admin\Model;

/**
 * 系统权限数据
 * Admin\Model$SysAuthorizationModel
 * Copyright (c) 2016. 云绅（张家港）精密工业有限公司 版权所有
 * 修改人：jcccy
 * 修改时间：2016年3月7日 下午4:56:57
 * 修改内容：
 */
class SysAuthorizationModel extends BaseModel{
	// 定义自动验证   
	
	/*
	 * 创建或更新权限
	 */
	public function record($action){
		$obj = $this->getByAction($action);
		
		if($obj){
			//如果action存在，则更新数据
			$obj['updatetime'] = time();
			$obj['updateuser'] = $this->getUsername();
			$obj['count'] = $obj['count']+1;
			$this->save($obj);
		}else{
			//如果不存在，这新增一条数据
			$this->action = $action;
			$this->createtime = time();
			$this->createuser = $this->getUsername();
			$this->count = 1;
			$authid = $this->add();
			$ra = D('SysRoleauthorise');
			$ra->addRelation(1,$authid);
		}
		///Admin/Index/getClientInfoPanelAccess
		if(!strpos($action, "getClientInfoPanel")){//如果操作该特征，则不执行日志记录
		//if("/Admin/Index/getClientInfoPanel" !== $action){
			$log = D('SysLog');
			$log->username = $this->getUsername();
			$log->time = time();
			$log->ip = $_SERVER["REMOTE_ADDR"];
			$log->module = "";
			$log->action = $action;
			$log->info = $obj?$obj['name']:"";
			$log->add();
		}
	}
	
	function isopen($action){
		$obj = $this->getByAction($action);
		////如果status为null,生产环境下默认应该为1，调试模式下默认应该为0
		//0：停用  表示权限全部开放，1：启用 表示权限被角色约束
		return (NULL==$obj['status'])?'1':$obj['status'];//<--
	}
	
	function judge($userid,$action){
		$sql = "SELECT * FROM  (SELECT `nn`.`action`,`nn`.`roleid` AS `nrole_id`,`nn`.`authid`,`mm`.`rolename`,`mm`.`roleid` AS `mrole_id`,`mm`.`userid` FROM (SELECT ( SELECT `b`.`action` FROM `wf_sys_authorization` AS `b` WHERE `b`.`id` = `a`.`menuid`) AS `action` ,`a`.`roleid`,`a`.`menuid` AS `authid` FROM `wf_sys_roleauthorise` AS `a`) AS `nn`INNER JOIN (SELECT (SELECT `name` FROM `wf_sys_role` WHERE `id` = `ab`.`roleid`) AS `rolename`,`ab`.`roleid`,`ab`.`userid` FROM `wf_sys_userrole` AS `ab`) AS `mm` ON `nn`.`roleid`=`mm`.`roleid`)AS `bb` WHERE `bb`.`userid`='$userid' AND `bb`.`action`='$action'";
		return $this->query($sql);
	}
	
	function ishasPerm($userid,$action){
		if('0' === $this->isopen($action) || 0!==count($this->judge($userid,$action))){//权限开放或者用户有该权限
			return true;
		}
		return false;
	}
	
	
}
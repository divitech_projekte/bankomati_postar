<?php
namespace Admin\Controller;

use Think\Model;

/**
 * 订单信息
 * Admin\Controller$VendingOrderController
 * Copyright (c) 2016. 云绅（张家港）精密工业有限公司 版权所有
 * 修改人：jcccy
 * 修改时间：2016年3月7日 下午4:53:28
 * 修改内容：
 */
class VendingOrderController extends BaseController {
	public $fields = 'status,orderno,storeid,clientid,tranno,type,telphone,price,actualprice,paytype,paymoney';
	
	public function _initialize(){
		parent::_initialize();
	}
	
	/**
	 * 加载界面
	 */
	public  function toList(){
		$this->views("list");
	}
	
	/**
	 * ajax加载分页数据
	 */
	public  function ajaxList(){
		

		$model = D('VendingOrder');
		$data = array();
		
		$datar = array();
		$datar['total']  = $model->where($data)->count();
		$datar['rows'] =  $model->alias('a')
		->field("`a`.`id`,`a`.`status`,`a`.`orderno`,`a`.`storeid`,`a`.`clientid`,
				`a`.`tranno`,`a`.`type`,`a`.`telphone`,`a`.`price`,`a`.`actualprice`,`a`.`paytype`,`a`.`paymoney`,
				`b`.`name` as `clientname`,`b`.`csn` as `clientcsn`,
				`c`.`name` as `storename`,`c`.`code` as `storecode`")
		->join('LEFT JOIN `wf_sys_client` AS `b` ON `b`.`id` =`a`.`clientid`')
		->join('LEFT JOIN `wf_vending_store` AS `c` ON `c`.`id` =`a`.`storeid`')
		->where($data)->order(array('`a`.`id`'=>'asc'))
		->page(I('get.page').','.I('get.rows'))->select();
		
		
		$this->ajaxReturn($datar);
		
		
		
		
		

	}
	
	/**
	 * ajax删除ID指定的数据
	 */
	public  function ajaxDel(){
		$model = D('VendingOrder');
		$id = I('post.id',0);
		$datab = array();
		$model->delete($id);
		if(true){
			$datab['msg'] = "ok";
		}else{
			$datab['msg'] = "no";
		}
		$this->ajaxReturn($datab);
	}
	
	/**
	 * ajax添加一条数据
	 */
	public  function ajaxAdd(){
		
		$datab = array();
		
	
		
		
		$model = D('VendingOrder');
		if( false !== $model->field($this->fields)->addTo()){
			$datab['msg'] = "ok";
		}else{
			$datab['msg'] = "no";
		}
		$this->ajaxReturn($datab);
	}
	
	/**
	 * ajax修改一条数据
	 */
	public  function ajaxEdit(){
			$datab = array();
		
			
		
			$model = D('VendingOrder');
			if( false !== $model->field('id,'.$this->fields)->saveTo()){
				$datab['msg'] = "ok";
			}else{
				$datab['msg'] = "no";
			}
			$this->ajaxReturn($datab);
	}
	
	
	/**
	 * ajax获取一条数据
	 */
	public  function ajaxGetDataById(){
		$datab = array();
		$model = D('VendingOrder');
		$id = I('get.id',0);
		$obj = $model->where('id='.$id)->field('id,'.$this->fields)->find();
	
		$this->ajaxReturn($obj);
	}
	
	
	/**
	 * ajax加载下拉数据
	 */
	public  function ajaxSelectList(){
	
		$model = D('VendingOrder');
		$data = array();
	
	
		$datar = $model->alias('a')
		
		->field("`a`.`id`,`a`.`orderno` as `text`,'' as `desc`")
		->join('LEFT JOIN `wf_sys_client` AS `b` ON `b`.`id` =`a`.`clientid`')
		->join('LEFT JOIN `wf_store` AS `c` ON `c`.`id` =`a`.`storeid`')
		->where($data)->order(array('`a`.`id`'=>'asc'))
		->select();
		$this->ajaxReturn($datar);
	
	}
	
	/**
	 * 牛奶机销售数据页面跳转
	 * milkOrderList
	 * @return return_type
	 * @throws
	 */
	public function milkOrderList()
	{
		$this->views('milkorderlist');
	}
	
	/**
	 * 加载牛奶机销售数据页面数据
	 * ajaxMilkOrderList
	 * @return return_type
	 * @throws
	 */
	public function ajaxMilkOrderList()
	{
		$clientid = I('get.clientid', '');
		
		$sql = "select
				 a.id
				,b.name as clientname
				,b.csn
				,a.orderno
				,a.paytype
				,c.value as paytypename
				,from_unixtime(a.createtime) as saletime
				,a.paymoney
				from wf_vending_order a, wf_sys_client b, (select itemname, value from wf_sys_item where code = 'pay_type') c
				where a.clientid = b.id
				and a.paytype = c.itemname
				and a.status = 0";
		
		if($clientid != '') {
			$sql = $sql . " and b.id = $clientid";
		}
		
		$sql = $sql . " order by a.createtime desc";
		
		$datar = array();
		$datar['total']  = $this->getPaginationDataSumCount($sql);
		$datar['rows'] = $this->getPaginationData($sql, I('get.page'), I('get.rows'));
		$this->ajaxReturn($datar);
	}
	
	/**
	 * 根据订单号码获取销售商品信息
	 * ajaxGetStorageDataById
	 * @return return_type
	 * @throws
	 */
	public function ajaxGetStorageDataById()
	{
		$orderid = I('get.orderid', '');
		
		if($orderid != '' && $orderid != null) {
			$sql = "select
					 a.id
					,c.name
					,c.code
					,c.price
					,b.no
					,from_unixtime(a.saletime) as saletime
					,from_unixtime(a.outtime) as outtime
					from wf_vending_storage a, wf_sys_box b, wf_vending_product c
					where a.boxid = b.id
					and a.productid = c.id
					and a.orderid = $orderid";
			$sql = $sql . " order by a.saletime, b.no";
			$model = new Model();
			$data = $model->query($sql);
			$this->ajaxReturn($data);
		}
		else {
			$this->ajaxReturn('');
		}
	}
}
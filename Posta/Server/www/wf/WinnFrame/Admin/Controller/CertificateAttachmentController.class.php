<?php
namespace Admin\Controller;

/**
 * 
 * Admin\Controller$CertificateAttachmentController
 * Copyright (c) 2016. 云绅（张家港）精密工业有限公司 版权所有
 * 修改人：jcccy
 * 修改时间：2016年4月15日 下午3:19:48
 * 修改内容：
 */
class CertificateAttachmentController extends BaseController {
	public $fields = 'title,certificateid,type';
	
	public function _initialize(){
		parent::_initialize();
	}

	/**
	 * 加载界面
	 */
	public  function toList(){
		$this->views("list");
	}
	
	/**
	 * ajax加载分页数据
	 */
	public  function ajaxList(){
		$model = D('CertificateAttachment');
		
		$data = array();
		
		$datar = array();
		$datar['total']  = $model->where($data)->count();
		$datar['rows'] =  $model->alias('a')
		->field("`a`.`id`,`a`.`title`,`a`.`certificateid`,`a`.`type`,`b`.`name`,`b`.`code`")
		->join('LEFT JOIN `wf_certificate_info` AS `b` ON `b`.`id` =`a`.`certificateid`')
		->where($data)->order(array('`a`.`id`'=>'asc'))
		->page(I('get.page').','.I('get.rows'))->select();
		$this->ajaxReturn($datar);
		
		
		
		

	}
	
	/**
	 * ajax删除ID指定的数据
	 */
	public  function ajaxDel(){
		$datab = array("msg"=>"no");
		
		$model = D('CertificateAttachment');
		$id = I('post.id',0);
		
		$obj = $model->getById(I('post.id',0));
		if($obj){
			//删除之前的图片
			unlink("./Public/".$obj['url']);
		}
		
		$model->delete($id);
		$datab['msg'] = "ok";
		$this->ajaxReturn($datab);
	}
	
	/**
	 * ajax添加一条数据
	 */
	public  function ajaxAdd(){
		$datab = array("msg"=>"no");
		$fields_tmp = "";
		$model = D('CertificateAttachment');
		$model->create();
		$info = fileOneUpload('file1');//上传图片，返回上传图片的信息
		if($info){//图片上传成功,则执行
			$fields_tmp = "url,";
			$model->url = $info['savepath'].$info['savename'];
		}
		if( false !== $model->field("id,createtime,createuser,".$fields_tmp.$this->fields)->add()){
			$datab['msg'] = "ok";
		}
		$this->ajaxReturn($datab);
	}
	
	/**
	 * ajax修改一条数据
	 */
	public  function ajaxEdit(){
			$datab = array("msg"=>"no");
			$model = D('CertificateAttachment');
			$obj = $model->getById(I('post.id',0));
			if($obj){
				$fields_tmp = "";
				$model->create();
				$info = fileOneUpload('file1');//上传图片，返回上传图片的信息
				if($info){//图片上传成功,则执行
					//删除之前的图片
					unlink("./Public/".$obj['url']);
					$fields_tmp = "url,";
					$model->url = $info['savepath'].$info['savename'];
				}
				
				if( false !== $model->field('id,updatetime,updateuser,'.$fields_tmp.$this->fields)->save()){
					$datab['msg'] = "ok";
				}
			}
			$this->ajaxReturn($datab);
	}
	
	
	/**
	 * ajax获取一条数据
	 */
	public  function ajaxGetDataById(){
		$datab = array();
		$model = D('CertificateAttachment');
		$id = I('get.id',0);
		$obj = $model->getById($id);//$model->where('id='.$id)->field('id,'.$this->fields)->find();
	
		$this->ajaxReturn($obj);
	}
}
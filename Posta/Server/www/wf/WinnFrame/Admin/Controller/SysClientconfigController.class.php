<?php
namespace Admin\Controller;

/**
 * 终端扩展配置信息
 * Admin\Controller$SysClientconfigController
 * Copyright (c) 2016. 云绅（张家港）精密工业有限公司 版权所有
 * 修改人：jcccy
 * 修改时间：2016年3月7日 下午4:53:09
 * 修改内容：
 */
class SysClientconfigController extends BaseController {
	public $fields = 'clientid,emergency,unit,freetime,interval,fee,isremotepower';
	
	
	
	public function _initialize(){
		parent::_initialize();
	}
	
	/**
	 * 加载界面
	 */
	public  function toList(){
		$this->views("list");
	}
	
	/**
	 * ajax加载分页数据
	 */
	public  function ajaxList(){
		
		$model = D('SysClientconfig');
		$data = array();
		$datar = array();
		$datar['total']  = $model->where($data)->count();
		$datar['rows'] =  $model->alias('a')
		->field("`a`.`id`,`a`.`emergency`,`a`.`clientid`,`a`.`isremotepower`,`a`.`unit`,`a`.`freetime`,`a`.`interval`,`a`.`fee`,`b`.`name` as `clientname`,`b`.`csn` as `clientcsn`")
		->join('LEFT JOIN `wf_sys_client` AS `b` ON `b`.`id` =`a`.`clientid`')
		->where($data)->order(array('`a`.`id`'=>'asc'))
		->page(I('get.page').','.I('get.rows'))->select();
		$this->ajaxReturn($datar);

	}
	
	/**
	 * ajax删除ID指定的数据
	 */
	public  function ajaxDel(){
		$model = D('SysClientconfig');
		$id = I('post.id',0);
		$datab = array();
		$model->delete($id);
		if(true){
			$datab['msg'] = "ok";
		}else{
			$datab['msg'] = "no";
		}
		$this->ajaxReturn($datab);
	}
	
	/**
	 * ajax添加一条数据
	 */
	public  function ajaxAdd(){
		
		$datab = array();
		
		
		
		$model = D('SysClientconfig');
		if( false !== $model->field($this->fields)->addTo()){
			$datab['msg'] = "ok";
		}else{
			$datab['msg'] = "no";
		}
		$this->ajaxReturn($datab);
	}
	
	/**
	 * ajax修改一条数据
	 */
	public  function ajaxEdit(){
		
			$model = D('SysClientconfig');
			if( false !== $model->field('id,'.$this->fields)->saveTo()){
				$datab['msg'] = "ok";
			}else{
				$datab['msg'] = "no";
			}
			$this->ajaxReturn($datab);
	}
	
	/**
	 * ajax获取一条数据
	 */
	public  function ajaxGetDataById(){
		$datab = array();
		$model = D('SysClientconfig');
		$id = I('get.id',0);
		$obj = $model->where('id='.$id)->field('id,'.$this->fields)->find();
	
		$this->ajaxReturn($obj);
	}
	
	/**
	 * 
	 * accessDoProfitConfig
	 * @return return_type
	 * @throws
	 */
	public function accessDoProfitConfig()
	{
		$model = D('SysClientconfig');
		$model->id = I('post.id', 0);
		$model->profit = I('post.profit', '');
		if($model->save() !== false) {
			//插入下发数据
			/*
			$model = D('SysClient');
			$dataClients = $model->where("status <> 0 and sysid='vending'")->select();
			foreach ($dataClients as $row) {
				$token = $row['token'];
				$otype = 407;
				$parameter = 2;
				$info = array(  'name' => I('post.name'),
								'code' => I('post.code'),
								'shelflife' => I('post.shelflife'),
								'price' => I('post.price'),
								'describe' => I('post.describe'));
				$jsoninfo = json_encode($info, JSON_UNESCAPED_UNICODE);
				sendCommand($token, $otype, $parameter, $jsoninfo);
			}
			*/
			$obj = $model->getById(I('post.id', 0));
			if($obj){
				$model = D('SysClient');
				$clientobj = $model->getById($obj['clientid']);
				$jsoninfo = json_encode(array('profit' => I('post.profit', '')),JSON_UNESCAPED_UNICODE);
				sendCommand($clientobj['token'],'411',2,$jsoninfo);//利润率信息下发
			}
			
			
			
			
			
			$this->jsonData['status'] = 200;
			$this->jsonData['msg'] = '利润率设置成功！';
		}
		else {
			$this->jsonData['status'] = 400;
			$this->jsonData['msg'] = '利润率设置失败！';
		}
		$this->ajaxReturn($this->jsonData);
	}
}
<?php
namespace Admin\Controller;


/**
 * 
 * Admin\Controller$LogisticsConfigController
 * Copyright (c) 2016. 云绅（张家港）精密工业有限公司 版权所有
 * 修改人：jcccy
 * 修改时间：2016年6月17日 上午11:02:53
 * 修改内容：
 */
class LogisticsConfigController extends SysConfigController {
	
	public function _initialize(){
		parent::_initialize();
	}
	
	/**
	 * 加载界面
	 */
	public  function logisticsConfigList(){
		$this->views("logisticsConfigList");
	}
	
	
	
	/**
	 * ajax加载分页数据
	 */
	public  function ajaxLogisticsConfigList(){
	
		$model = D('SysConfig');
		$data = array();
	
	
		if("" != I('get.key',"")){
			$data['`a`.`key`'] = array('like','%'.I('get.key').'%');
		}
	
		if("" != I('get.value',"")){
			$data['`a`.`value`'] = array('like','%'.I('get.value').'%');
		}
	
	
		$data['`a`.`sysid`'] = "logistics";
	
		$datar = array();
		$datar['total']  = $model->alias('a')->where($data)->count();
	
		$datar['rows'] =  $model->alias('a')
		->field("`a`.`id`,`a`.`key`,`a`.`sysid`,`a`.`status`,`a`.`value`,`a`.`remark`")
		->where($data)->order(array('`a`.`id`'=>'asc'))
		->page(I('get.page').','.I('get.rows'))->select();
	
		$this->ajaxReturn($datar);
	
	
	}
	
	/**
	 * ajax添加一条数据
	 */
	public  function ajaxAdd(){
		$datab = array("msg"=>"no");
	
		$model = D('SysConfig');
		$model->create();
	
		$model->status = "1";
		$model->sysid = "logistics";
	
		$ownerid = $model->field('id,status,sysid,key,value,remark')->add();
		if( false !== $ownerid){
			$datab['msg'] = "ok";
		}
			
		$this->ajaxReturn($datab);
	}
}
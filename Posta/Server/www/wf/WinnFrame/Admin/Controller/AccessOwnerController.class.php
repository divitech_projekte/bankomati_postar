<?php
namespace Admin\Controller;
use Think\Model;
/**
 * 
 * Admin\Controller$AccessOwnerController
 * Copyright (c) 2016. 云绅（张家港）精密工业有限公司 版权所有
 * 修改人：jcccy
 * 修改时间：2016年5月21日 上午11:34:40
 * 修改内容：
 */
class AccessOwnerController extends BaseController {
	
	/*
	 * 运营商控制器类架构函数
	 */
	public function _initialize()
	{
		// TODO
		parent::_initialize();
	}
	public function accessOwnerList(){
		$this->views("list");
	}
	
	public function toList(){
		$this->views("list");
	}
	
	/**
	 * ajax加载分页数据
	 */
	public  function ajaxList(){
		
		$model = D('AccessOwner');
		$data = array();
		
		if("" != I('get.name',"")){
			$data['name'] = array('like','%'.I('get.name').'%');
		}
		
		if("" != I('get.code',"")){
			$data['code'] = array('like','%'.I('get.code').'%');
		}
		
		
		$datar = array();
		$datar['total']  = $model->where($data)->count();
		
		$datar['rows'] =  $model->alias('a')
		->field("`a`.`id`,`a`.`name`,`a`.`code`,`a`.`short`,`a`.`address`,`a`.`linkman`,`a`.`phone`,`a`.`email`,`a`.`regdate`,`a`.`status`,`c`.`username`")
		->join('LEFT JOIN `wf_access_owneruser` AS `b` ON `b`.`ownerid` =`a`.`id`')
		->join('LEFT JOIN `wf_sys_user` AS `c` ON `c`.`id` =`b`.`userid`')
		->where($data)->order(array('`a`.`id`'=>'asc'))
		->page(I('get.page').','.I('get.rows'))->select();
		
		$this->ajaxReturn($datar);
	}
	
	
	/**
	 * ajax添加一条数据
	 */
	public  function ajaxAdd(){
		$datab = array("msg"=>"no");
	
		$model = D('AccessOwner');
		$model->create();
		
		$model->regdate = time();
		$model->status = "1";
	
		$ownerid = $model->field('id,createtime,createuser,status,name,code,short,address,linkman,phone,email,regdate,apikey,apipassword')->add();
		$datab['data'] = $model->getLastSql();
		if( false !== $ownerid){
			if("" !== I('post.username',"")){
				$model3 = D("SysUser");
				$model3->username = I('post.username');
				$model3->password =  md5(md5("000000").C('mark'));
				$model3->status = 1;
				$model3->createuser = getUserInfo();
				$model3->createtime = time();
				$model3->regdate = time();
				$model3->email = I("post.email");
				$model3->phone = I("post.phone");
				$userid = $model3->add();
				if( false !== $userid){
					
					$model5 = M("SysRole");
					$obj = $model5->getByKey("access_owner");//供应商
					$model4 = M("SysUserrole");
					$model4->userid = $userid;
					$model4->roleid = $obj['id'];
					$model4->add();
					
					$model2 = M("AccessOwneruser");
					$model2->ownerid = $ownerid;
					$model2->userid = $userid;
					$model2->add();
					$datab['msg'] = "ok";
				}
			}
			
		}
			
		$this->ajaxReturn($datab);
	}
	
	
	/**
	 * ajax获取一条数据
	 */
	public  function ajaxGetDataById(){
		$datab = array();
		$model = D('AccessOwner');
		$id = I('get.id',0);
		//$obj = $model->getById($id);
		$obj = $model->alias('a')
		->field("`a`.`id`,`a`.`name`,`a`.`code`,`a`.`short`,`a`.`address`,`a`.`linkman`,`a`.`phone`,`a`.`email`,`a`.`regdate`,`a`.`apikey`,`a`.`apipassword`,`a`.`status`,`c`.`username`")
		->join('LEFT JOIN `wf_access_owneruser` AS `b` ON `b`.`ownerid` =`a`.`id`')
		->join('LEFT JOIN `wf_sys_user` AS `c` ON `c`.`id` =`b`.`userid`')
		->where("`a`.`id`=$id")->find();
		$this->ajaxReturn($obj);
	}
	
	/**
	 * ajax删除ID指定的数据
	 */
	public  function ajaxDel(){
		$datab = array("msg"=>"no");
		$id = I('post.id',0);
		
		$model0 = M("SysClientconfig");
		$obj2 = $model0->getByOwner($id);
		
		if(!$obj2){
			$model = D("AccessOwner");
			$model->id = $id;
			$model->status = 0;
			if( false !== $model->field('id,status')->save()){
				$datab['msg'] = "ok";
			}
		}
		/*
		
		$id = I('post.id',0);
		
		$model0 = M("AccessOwneruser");
		$obj2 = $model0->getByOwnerid($id);
		$userid = $obj2['userid'];
		$model0->where("ownerid=$id")->delete();
		
		
		$model5 = M("SysRole");
		$obj = $model5->getByKey("access_owner");//供应商
		$roleid = $obj['id'];
		
		$model4 = M("SysUserrole");
		$model4->where("roleid=$roleid and userid=$userid")->delete();
		
		
		
		$model = D('AccessOwner');
		if( false !== $model->delete($id))
		{
			$datab['msg'] = "ok";
		}	
		*/
		$this->ajaxReturn($datab);
	}
	
	
	
	/**
	 * ajax修改一条数据
	 */
	public  function ajaxEdit(){
		$datab = array("msg"=>"no");
	
		$model = D('AccessOwner');
		$model->create();
		if( false !== $model->field('id,updatetime,updateuser,status,name,code,short,address,linkman,phone,email,regdate,apikey,apipassword')->save()){
			$ownerid = I("post.id");
			$userid = I("post.userid");
			/*
			$model2 = M("AccessOwneruser");
			$model2->where("ownerid=$ownerid")->delete();
			
			$model2->ownerid = $ownerid;
			$model2->userid = $userid;
			$model2->add();
			*/
			$datab['msg'] = "ok";
		}
		$this->ajaxReturn($datab);
	}
	
	
	/**
	 * ajax加载下拉数据
	 */
	public  function ajaxSelectList(){
		$model = new Model();// D('AccessOwner');
		$data = array();
		$data['`a`.`status`'] = 1;
		$datar = $model->table('wf_access_owner as `a`,wf_access_owneruser as `b`,wf_sys_user AS `c`')//->alias('a')
		->field("`a`.`id`,CONCAT(`a`.`name`,'/',`a`.`code`,'(',`c`.`username`,')') as `text`,'' as `desc`")
		->where("`a`.`id`=`b`.`ownerid` and `b`.`userid`=`c`.`id`")
		->where($data)->order(array('`a`.`id`'=>'asc'))
		->group('`a`.`id`')
		->select();
	
	
		$this->ajaxReturn($datar);
	
	}
	
	
	public function accessSupplierForOwnerList(){
		
	}
}
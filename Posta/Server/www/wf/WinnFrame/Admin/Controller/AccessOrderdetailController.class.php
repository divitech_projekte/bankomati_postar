<?php
namespace Admin\Controller;

/**
 * 订单详细信息
 * Admin\Controller$AccessOrderdetailController
 * Copyright (c) 2016. 云绅（张家港）精密工业有限公司 版权所有
 * 修改人：jcccy
 * 修改时间：2016年3月7日 下午4:53:39
 * 修改内容：
 */
class AccessOrderdetailController extends BaseController {
	public $fields = 'status,orderid,proname,procode,proprice,rate,applylength';
	
	public function _initialize(){
		parent::_initialize();
	}
	
	/**
	 * 加载界面
	 */
	public  function toList(){
		$this->views("list");
	}
	
	/**
	 * ajax加载分页数据
	 */
	public  function ajaxList(){
		
	
		
		
		
		
		$model = D('AccessOrderdetail');
		$data = array();
		$datar = array();
		$datar['total']  = $model->where($data)->count();
		$datar['rows'] =  $model->alias('a')
		->field("`a`.`id`,`a`.`status`,`a`.`orderid`,`a`.`proname`,`a`.`procode`,`a`.`proprice`,`a`.`rate`,`a`.`applylength`,
				`b`.`orderno` as `orderno`,
				`c`.`name` as `clientname`,`c`.`csn` as `clientcsn`,
				`d`.`name` as `storename`,`d`.`code` as `storecode`")
		->join('LEFT JOIN `wf_access_order` AS `b` ON `b`.`id` =`a`.`orderid`')
		->join('LEFT JOIN `wf_sys_client` AS `c` ON `c`.`id` =`b`.`clientid`')
		->join('LEFT JOIN `wf_access_store` AS `d` ON `d`.`id` =`b`.`storeid`')
		->where($data)->order(array('`a`.`id`'=>'asc'))
		->page(I('get.page').','.I('get.rows'))->select();
		$this->ajaxReturn($datar);
	}
	
	/**
	 * ajax删除ID指定的数据
	 */
	public  function ajaxDel(){
		$model = D('AccessOrderdetail');
		$id = I('post.id',0);
		$datab = array();
		$model->delete($id);
		if(true){
			$datab['msg'] = "ok";
		}else{
			$datab['msg'] = "no";
		}
		$this->ajaxReturn($datab);
	}
	
	/**
	 * ajax添加一条数据
	 */
	public  function ajaxAdd(){
		
		$datab = array();
		
	
		
		
		$model = D('AccessOrderdetail');
		if( false !== $model->field($this->fields)->addTo()){
			$datab['msg'] = "ok";
		}else{
			$datab['msg'] = "no";
		}
		$this->ajaxReturn($datab);
	}
	
	/**
	 * ajax修改一条数据
	 */
	public  function ajaxEdit(){
			$datab = array();
		
			
		
			$model = D('AccessOrderdetail');
			if( false !== $model->field('id,'.$this->fields)->saveTo()){
				$datab['msg'] = "ok";
			}else{
				$datab['msg'] = "no";
			}
			$this->ajaxReturn($datab);
	}
	
	
	/**
	 * ajax获取一条数据
	 */
	public  function ajaxGetDataById(){
		$datab = array();
		$model = D('AccessOrderdetail');
		$id = I('get.id',0);
		$obj = $model->where('id='.$id)->field('id,'.$this->fields)->find();
	
		$this->ajaxReturn($obj);
	}
}
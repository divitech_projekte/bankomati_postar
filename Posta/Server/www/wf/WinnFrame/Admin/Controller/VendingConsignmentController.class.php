<?php
namespace Admin\Controller;

/**
 * 上下货清单控制器
 * Admin\Controller$VendingConsignmentController
 * Copyright (c) 2016. 云绅（张家港）精密工业有限公司 版权所有
 * 创建人：徐辰辰
 * 创建时间：2016-3-21 上午08:10:39
 */
class VendingConsignmentController extends BaseController {
	
	/*
	 * 上下货清单控制器类架构函数
	 */
	public function _initialize()
	{
		// TODO
		parent::_initialize();
	}
	
	/**
	 * 加载上下货清单页面
	 * milkConsignmentList
	 * @return return_type
	 * @throws
	 */
	public function milkConsignmentList()
	{
		$this->views('milkconsignmentlist');
	}
	
	/**
	 * 加载上下货清单页面列表信息
	 * ajaxMilkConsignmentList
	 * @return return_type
	 * @throws
	 */
	public function ajaxMilkConsignmentList()
	{
		$listno = I('get.listno');
		$clientname = I('get.clientname');
		
		$sql = "select
				 a.listno as id
				,b.name
				,b.position
				,a.listno
				,from_unixtime(a.createtime) as createtime
				,a.status
				,c.value as statusvalue
				from (select distinct createtime, listno, status, clientid from wf_vending_consignment) a
						,wf_sys_client b
						,(select * from wf_sys_item where code = 'consignment_status') c
				where a.clientid = b.id
				and a.status = c.itemname";
		
		if($listno != "") {
			$sql = $sql . " and a.listno like '%$listno%'";
		}
		if($clientname != "") {
			$sql = $sql . " and b.name like '%$clientname%'";
		}
		
		$sql = $sql . " order by a.createtime desc";
		
		$datar = array();
		$datar['total']  = $this->getPaginationDataSumCount($sql);
		$datar['rows'] = $this->getPaginationData($sql, I('get.page'), I('get.rows'));
		$this->ajaxReturn($datar);
	}
	
	/**
	 * ajax逻辑删除指定数据
	 * ajaxVoid
	 * @return return_type
	 * @throws
	 */
	public function ajaxVoid()
	{
		$listno = I('post.id', '0');
		if($listno == '') {
			$listno = '0';
		}
		$model = D('VendingConsignment');
		$model->status = 0;
		$ret = $model->where("listno = $listno")->save();
		
		if($ret1 !== false) {
			$this->jsonData['status'] = 200;
			$this->jsonData['msg'] = '作废数据成功！';
		}
		else {
			$this->jsonData['status'] = 400;
			$this->jsonData['msg'] = '作废数据失败！';
		}
		$this->ajaxReturn($this->jsonData);
	}
	
	/**
	 * 显示修改页面数据
	 * ajaxGetDataById
	 * @return return_type
	 * @throws
	 */
	public function ajaxGetDataById()
	{
		$model = D('VendingConsignment');
		$id = I('get.id', 0);
		$obj = $model->where("id=$id")->find();
		$this->ajaxReturn($obj);
	}
	
	/**
	 * 执行修改保存操作
	 * ajaxMilkStorageEdit
	 * @return return_type
	 * @throws
	 */
	public function ajaxMilkStorageEdit()
	{
		$model = D('VendingConsignment');
		if($model->saveTo() !== false) {
			$this->jsonData['status'] = 200;
			$this->jsonData['msg'] = '编辑数据成功！';
		}
		else {
			$this->jsonData['status'] = 400;
			$this->jsonData['msg'] = '编辑数据失败！';
		}
		$this->ajaxReturn($this->jsonData);
	}
	
}
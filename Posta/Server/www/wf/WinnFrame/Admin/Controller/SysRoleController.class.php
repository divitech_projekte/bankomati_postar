<?php
namespace Admin\Controller;

/**
 * 角色信息
 * Admin\Controller$SysRoleController
 * Copyright (c) 2016. 云绅（张家港）精密工业有限公司 版权所有
 * 修改人：jcccy
 * 修改时间：2016年3月7日 下午4:54:58
 * 修改内容：
 */
class SysRoleController extends BaseController {
	public $fields = 'status,name';
	
	public function _initialize(){
		parent::_initialize();
	}

	/**
	 * 加载界面
	 */
	public  function toList(){
		$this->views("list");
	}
	
	/**
	 * ajax加载分页数据
	 */
	public  function ajaxList(){
		
		$model = D('SysRole');
	
		$data = array();
	
	
	
		$datar = array();
		$datar['total']  = $model->where($data)->count();
		$datar['rows'] =  $model->alias('a')
		->field("`a`.`id`,`a`.`status`,`a`.`name`")
		->where($data)->order(array('`a`.`id`'=>'asc'))->page(I('get.page').','.I('get.rows'))->select();
		
		$this->ajaxReturn($datar);
	}
	
	
	
	/**
	 * ajax加载分页数据
	 */
	public  function ajaxListByAuth(){
		/*
		 * sql查询判断语句
		 * SELECT CASE WHEN 1>2 THEN 'true' ELSE 'false' END AS `b`

			SELECT IF(1=1,"男","女") AS `a`
		 */
	
		$model = D('SysRole');
		$authid = I('get.authid','');
		$data = array();
		if("" != I('get.name',"")){
			$data['name'] = array('like','%'.I('get.name').'%');
		}
	
	
		$datar = array();
		$datar['total']  = $model->where($data)->count();
		if("" != $authid){
			$datar['rows'] =  $model->alias('a')
			//->field("`a`.`id`,`a`.`status`,`a`.`name`,IF ((select count(*) from wf_sysroleauthorise as b where b.menuid=".$authid." and b.roleid=a.id) = 0),'false','true') as  `checked`")
			->field("`a`.`id`,`a`.`status`,`a`.`name`,(select (SELECT IF (0=(select count(*) from wf_sys_roleauthorise as b where b.menuid=".$authid." and b.roleid=a.id),'','true') AS  `checked`)) AS  `checked`")
			//->field("`a`.`id`,`a`.`status`,`a`.`name`,cast((select count(*) from wf_sysroleauthorise as b where b.menuid=".$authid." and b.roleid=a.id) as char) as `num`")
			->where($data)->order(array('`a`.`id`'=>'asc'))
			->page(I('get.page').','.I('get.rows'))->select();
		}else{
			$datar['rows'] =  $model->alias('a')
			->field("`a`.`id`,`a`.`status`,`a`.`name`")
			->where($data)->order(array('`a`.`id`'=>'asc'))
			->page(I('get.page').','.I('get.rows'))->select();
		}
		$this->ajaxReturn($datar);
	}
	
	
	
	
	/**
	 * ajax加载分页数据
	 */
	public  function ajaxListByUser(){
		
		$model = D('SysRole');
		$userid = I('get.userid','');
		$data = array();
		if("" != I('get.name',"")){
			$data['name'] = array('like','%'.I('get.name').'%');
		}
	
	
		$datar = array();
		$datar['total']  = $model->where($data)->count();
		if("" != $userid){
			$datar['rows'] =  $model->alias('a')
			//->field("`a`.`id`,`a`.`status`,`a`.`name`,IF ((select count(*) from wf_sysroleauthorise as b where b.menuid=".$authid." and b.roleid=a.id) = 0),'false','true') as  `checked`")
			->field("`a`.`id`,`a`.`status`,`a`.`name`,(select (SELECT IF (0=(select count(*) from wf_sys_userrole as b where b.userid=".$userid." and b.roleid=a.id),'','true') AS  `checked`)) AS  `checked`")
			//->field("`a`.`id`,`a`.`status`,`a`.`name`,cast((select count(*) from wf_sysroleauthorise as b where b.menuid=".$authid." and b.roleid=a.id) as char) as `num`")
			->where($data)->order(array('`a`.`id`'=>'asc'))
			->page(I('get.page').','.I('get.rows'))->select();
		}else{
			$datar['rows'] =  $model->alias('a')
			->field("`a`.`id`,`a`.`status`,`a`.`name`")
			->where($data)->order(array('`a`.`id`'=>'asc'))
			->page(I('get.page').','.I('get.rows'))->select();
		}
		$this->ajaxReturn($datar);
	}
	
	
	/**
	 * ajax删除ID指定的数据
	 */
	public  function ajaxDel(){
		$model = D('SysRole');
		$id = I('post.id',0);
		$datab = array();
		$model->delete($id);
		if(true){
			$datab['msg'] = "ok";
		}else{
			$datab['msg'] = "no";
		}
		$this->ajaxReturn($datab);
	}
	
	/**
	 * ajax添加一条数据
	 */
	public  function ajaxAdd(){
		
		$model = D('SysRole');
		if( false !== $model->field($this->fields)->addTo()){
			$datab['msg'] = "ok";
		}else{
			$datab['msg'] = "no";
		}
		$this->ajaxReturn($datab);
	}
	
	/**
	 * ajax修改一条数据
	 */
	public  function ajaxEdit(){
			$datab = array();
		
		
			$model = D('SysRole');
			if( false !== $model->field('id,'.$this->fields)->saveTo()){
				$datab['msg'] = "ok";
			}else{
				$datab['msg'] = "no";
			}
			$this->ajaxReturn($datab);
	}
	
	/**
	 * ajax获取一条数据
	 */
	public  function ajaxGetDataById(){
		$datab = array();
		$model = D('SysRole');
		$id = I('get.id',0);
		$obj = $model->where('id='.$id)->field('id,'.$this->fields)->find();
	
		$this->ajaxReturn($obj);
	}
	/**
	 * 跳转到分配角色页面
	 */
	public function toAllotRoleList(){
		$this->views("allotRole");
	}
	
	
	
	/**
	 * ajax添加一条权限与角色关系数据
	 */
	public  function ajaxAddAuth(){
		$datab = array();
		$model = D('SysRoleauthorise');
		$data = array();
		
		$data['roleid'] = I("post.roleid",'');
		$data['menuid'] = I("post.authid",'');
		
		if( false !== $model->field("roleid,menuid")->addTo($data)){
			$datab['msg'] = "ok";
		}else{
			$datab['msg'] = "no";
		}
		$this->ajaxReturn($datab);
	}
	
	/**
	 * ajax删除一条权限与角色关系数据
	 */
	public  function ajaxDelAuth(){
		$datab = array();
		$model = D('SysRoleauthorise');
		
		if($model->where('roleid='.I("post.roleid",'0').' AND menuid='.I("post.authid",'0'))->delete()){
			$datab['msg'] = "ok";
		}else{
			$datab['msg'] = "no";
		}
		$this->ajaxReturn($datab);
	}
	
	
	
	
	
	
	/**
	 * ajax添加一条权限与角色关系数据
	 */
	public  function ajaxAddUser(){
		$datab = array();
		$model = D('SysUserrole');
		$data = array();
	
		$data['roleid'] = I("post.roleid",'');
		$data['userid'] = I("post.userid",'');
	
		if( false !== $model->field("roleid,userid")->addTo($data)){
			$datab['msg'] = "ok";
		}else{
			$datab['msg'] = "no";
		}
		$this->ajaxReturn($datab);
	}
	
	/**
	 * ajax删除一条权限与角色关系数据
	 */
	public  function ajaxDelUser(){
		$datab = array();
		$model = D('SysUserrole');
	
		if($model->where('roleid='.I("post.roleid",'0').' AND userid='.I("post.userid",'0'))->delete()){
			$datab['msg'] = "ok";
		}else{
			$datab['msg'] = "no";
		}
		$this->ajaxReturn($datab);
	}
}
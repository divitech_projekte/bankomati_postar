<?php
namespace Admin\Controller;

use Think\Model;

/**
 * 
 * Admin\Controller$LogisticsCardinfoController
 * Copyright (c) 2016. 云绅（张家港）精密工业有限公司 版权所有
 * 修改人：jcccy
 * 修改时间：2016年6月16日 上午8:30:13
 * 修改内容：
 */
class LogisticsCardinfoController extends BaseController {
	public $fields = 'status,cardno,othercardno,account,nickname,courierid,remark,money,warn,unit,type';
	
	public function _initialize(){
		parent::_initialize();
	}
	
	/**
	 * 加载界面
	 */
	public  function toList(){
		$this->views("list");
	}
	
	/**
	 * ajax加载分页数据
	 */
	public  function ajaxList(){
		$datar = array();

		$model = D('LogisticsCardinfo');
		$data = array();
		
		$companyids = getCompanyidsByUid(getUserId());
		$where = "1!=1";
		foreach($companyids as $vo){
			$where .= " or b.companyid=".$vo['companyid'];
		}
		
		
		if("" != I('get.couriername',"")){
			$data['`b`.`name`'] = array('like','%'.I('get.couriername').'%');//快递公司名称
		}
		if("" != I('get.cardno',"")){
			$data['`a`.`cardno`'] = array('like','%'.I('get.cardno').'%');
		}
		if("" != I('get.account',"")){
			$data['`a`.`account`'] = array('like','%'.I('get.account').'%');
		}
		
		
		
		
		$datar['total']  = $model->alias('a')
		->join('LEFT JOIN `wf_logistics_courier` AS `b` ON `b`.`id` =`a`.`courierid`')
			->where($data)->count();
		
		$datar['rows'] =  $model->alias('a')
		->field("`a`.`id`,`a`.`status`,`a`.`cardno`,`a`.`othercardno`,`a`.`account`,`a`.`type`,
				`a`.`nickname`,`a`.`courierid`,`b`.`name` as `couriername`,`a`.`remark`,`a`.`money`,`a`.`warn`,`a`.`unit`")
		->join('LEFT JOIN `wf_logistics_courier` AS `b` ON `b`.`id` =`a`.`courierid`')
		->where($where)
		->where($data)->order(array('`a`.`id`'=>'asc'))
		->page(I('get.page').','.I('get.rows'))->select();
		
		
		$this->ajaxReturn($datar);
		
		
		
		
		

	}
	
	/**
	 * ajax删除ID指定的数据
	 */
	public  function ajaxDel(){
		$datab = array("msg"=>"no");
		
		$model = D('LogisticsCardinfo');
		$id = I('post.id',0);
		
		if($model->delete($id)){
			$datab['msg'] = "ok";
		}
		$this->ajaxReturn($datab);
	}
	
	/**
	 * ajax添加一条数据
	 */
	public  function ajaxAdd(){
		$datab = array("msg"=>"no");
		$model = D('LogisticsCardinfo');
		$model->create();
		
		if("" != I("post.password","") && I("post.password") === I("post.passverify")){
			$model->password = md5(md5(I("post.password")).C('mark'));// 密码
			$this->fields .= ',password';
		}
		$model->status = 2;
		
		$cardid = $model->field('id,createtime,createuser,'.$this->fields)->add();
		if($cardid){
			
			
			$clientids = I("post.clientids");
			if($clientids){
				$model3 = D('LogisticsCardclient');
				$model3->where('cardid='.$cardid)->delete();
				foreach ($clientids as $val){
					if(0 != intval($val)){
						$model3->cardid = $cardid;
						$model3->clientid = $val;
						$model3->field('cardid,clientid')->add();
					}
				}
			}
			
			
			
			
			$datab['msg'] = "ok";
		}
		$this->ajaxReturn($datab);
	}
	
	/**
	 * ajax修改一条数据
	 */
	public  function ajaxEdit(){
			$datab = array("msg"=>"no");
			$model = D('LogisticsCardinfo');
			$cardid = I('post.id');
			$model->create();
			if("" != I("post.password","") && I("post.password") === I("post.passverify")){
				$model->password = md5(md5(I("post.password")).C('mark'));// 密码
				$this->fields .= ',password';
			}
			if($model->field('id,updatetime,updateuser,'.$this->fields)->save()){
				
				$clientids = I("post.clientids");
				if($clientids){
					$model3 = D('LogisticsCardclient');
					$model3->where('cardid='.$cardid)->delete();
					foreach ($clientids as $val){
						if(0 != intval($val)){
							$model3->cardid = $cardid;
							$model3->clientid = $val;
							$model3->field('cardid,clientid')->add();
						}
					}
				}
				
				
				$datab['msg'] = "ok";
			}
			$this->ajaxReturn($datab);
	}
	
	
	/**
	 * ajax获取一条数据
	 */
	public  function ajaxGetDataById(){
		$model = D('LogisticsCardinfo');
		$id = I('get.id',0);
		$obj = $model->alias('a')
				->field("`a`.`id`,`a`.`status`,`a`.`cardno`,`a`.`othercardno`,`a`.`account`,`a`.`nickname`,`a`.`courierid`,`a`.`remark`,`a`.`money`,`a`.`warn`,`a`.`unit`,`a`.`type`,IFNULL(GROUP_CONCAT(`c`.`clientid`),'') AS `clientids[]`,IFNULL(GROUP_CONCAT(`d`.`name`,'[',`d`.`csn`,']'),'') AS `clientname[]`")
				->join('LEFT JOIN `wf_logistics_cardclient` AS `c` ON `c`.`cardid` = `a`.`id`')
				->join('LEFT JOIN `wf_sys_client` AS `d` ON `d`.`id` = `c`.`clientid`')
				->where('`a`.`id`='.$id)->find();
		
		$this->ajaxReturn($obj);
	}
	
	
	/**
	 * ajax加载下拉数据
	 */
	public  function ajaxSelectList(){
		$model = D('LogisticsCardinfo');
		$data = array();
	
		$companyids = getCompanyidsByUid(getUserId());
		$where = "1!=1";
		foreach($companyids as $vo){
			$where .= " or b.companyid=".$vo['companyid'];
		}
		
		
		$datar = $model->alias('a')
		->field("`a`.`id`,CONCAT(`a`.`account`,'/',`a`.`cardno`,'[',`b`.`name`,']') as `text`,'' as `desc`")
		->join('LEFT JOIN `wf_logistics_courier` AS `b` ON `b`.`id` =`a`.`courierid`')
		->where($where)->where($data)->order(array('`a`.`id`'=>'asc'))
		->select();
		$this->ajaxReturn($datar);
	
	}
	
}
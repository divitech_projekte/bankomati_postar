<?php
namespace Admin\Controller;

/**
 * 存放店商产品信息
 * Admin\Controller$VendingProductController
 * Copyright (c) 2016. 云绅（张家港）精密工业有限公司 版权所有
 * 修改人：jcccy
 * 修改时间：2016年3月7日 下午4:53:45
 * 修改内容：
 */
use Think\Model;

class VendingProductController extends BaseController {
	
	public $fields = 'storeid, name, code, type, price, sort';
	
	public function _initialize(){
		parent::_initialize();
	}

	/**
	 * 加载界面
	 */
	public  function toList(){
		$this->views("list");
	}
	
	/**
	 * ajax加载分页数据
	 */
	public  function ajaxList(){
		
		
		
		$model = D('VendingProduct');
		
		$data = array();
		
		
		$datar['total']  = $model->where($data)->count();
		$datar['rows'] =  $model->alias('a')
		->field("`a`.`id`,`a`.`storeid`,`a`.`name`,`a`.`code`,`a`.`type`,`a`.`price`,`a`.`sort`,`d`.`name` as `storename`,`d`.`code` as `storecode`")
		->join('LEFT JOIN `wf_vending_store` AS `d` ON `d`.`id` =`a`.`storeid`')
		->where($data)->order(array('`a`.`id`'=>'asc'))
		->page(I('get.page').','.I('get.rows'))->select();
		$this->ajaxReturn($datar);
		

		
	}
	
	/**
	 * ajax添加一条数据
	 */
	public  function ajaxAdd(){
		
		$datab = array();
		
		
		$model = D('VendingProduct');
		if( false !== $model->field($this->fields)->addTo()){
			$datab['msg'] = "ok";
		}else{
			$datab['msg'] = "no";
		}
		$this->ajaxReturn($datab);
	}
	
	/**
	 * ajax修改一条数据
	 */
	public  function ajaxEdit(){
			$datab = array();
			
			
			$model = D('VendingProduct');
			if( false !== $model->field('id,'.$this->fields)->saveTo()){
				$datab['msg'] = "ok";
			}else{
				$datab['msg'] = "no";
			}
			$this->ajaxReturn($datab);
	}

	/**
	 * ajax删除ID指定的数据
	 */
	public  function ajaxDel(){
		$model = D('VendingProduct');
		$id = I('post.id',0);
		$datab = array();
		$model->delete($id);
		if(true){
			$datab['msg'] = "ok";
		}else{
			$datab['msg'] = "no";
		}
		$this->ajaxReturn($datab);
	}
	
	/*************************** 操作数据表公用方法 * 修改：徐辰辰 * 2016-03-19 * 适用于具体业务操作1.0版本 * 开始 ***************************/
	
	/**
	 * ajax物理删除指定数据
	 * ajaxDelete
	 * @return return_type
	 * @throws
	 */
	public function ajaxDelete()
	{
		$model = D('VendingProduct');
		$model->create();
		if($model->delete() !== false) {
			$this->jsonData['status'] = 200;
			$this->jsonData['msg'] = '删除数据成功！';
		}
		else {
			$this->jsonData['status'] = 400;
			$this->jsonData['msg'] = '删除数据失败！';
		}
		$this->ajaxReturn($this->jsonData);
	}
	
	/**
	 * ajax逻辑删除指定数据
	 * ajaxVoid
	 * @return return_type
	 * @throws
	 */
	public function ajaxVoid()
	{
		$proid = I('post.id', '0');
		
		$tempModel = new Model();
		$sql = "select * 
				from wf_sys_client a, wf_sys_box b, wf_sys_boxconfig c
				where a.id = b.clientid
				and b.id = c.boxid
				and a.status <> 0
				and c.productid = $proid";
		$tempData = $tempModel->query($sql);
		if(count($tempData) != 0) {
			$this->jsonData['status'] = 400;
			$this->jsonData['msg'] = '存在配置该商品的货道信息，请先修改货道配置！';
		}
		else {
			$model = D('VendingProduct');
			$model->create();
			$model->valid = 0;
			
			$id = $model->save();
	
			if($id !== false) {
				$data = $model->find(I('post.id'));
				
				//插入下发数据
				$model = D('SysClient');
				$dataClients = $model->where("status <> 0")->select();
				foreach ($dataClients as $row) {
					$token = $row['token'];
					$otype = 407;
					$parameter = 3;
					$info = array('code' => $data['code']);
					$jsoninfo = json_encode($info, JSON_UNESCAPED_UNICODE);
					sendCommand($token, $otype, $parameter, $jsoninfo);
				}
				
				$this->jsonData['status'] = 200;
				$this->jsonData['msg'] = '作废数据成功！';
			}
			else {
				$this->jsonData['status'] = 400;
				$this->jsonData['msg'] = '作废数据失败！';
			}
		}
		$this->ajaxReturn($this->jsonData);
	}
	
	/**
	 * ajax获取一条数据
	 * ajaxGetDataById
	 * @return return_type
	 * @throws
	 */
	public  function ajaxGetDataById()
	{
		$model = D('VendingProduct');
		$id = I('get.id', 0);
		$obj = $model->where("id=$id")->find();
		$this->ajaxReturn($obj);
	}
	
	/**
	 * ajax下拉数据
	 */
	public  function ajaxSelectList(){
	
		$model = D('VendingProduct');
		$data = array();
		$data['valid'] = 1;
		
		$datar = $model->alias('a')
		->field("`a`.`id`,CONCAT(`a`.`name`,`a`.`code`) as `text`,'' as `desc`")
		->where($data)->order(array('`a`.`id`'=>'asc'))
		->select();
		$this->ajaxReturn($datar);
	
	}
	
	/*************************** 操作数据表公用方法 * 修改：徐辰辰 * 2016-03-19 * 适用于具体业务操作1.0版本 * 开始 ***************************/
	
	/*************************** 自动牛奶售货机项目 **** 开始 ***************************/
	
	/**
	 * 加载牛奶信息页面
	 * milkProductList 
	 * @return return_type
	 * @throws
	 */
	public function milkProductList()
	{
		$this->views('milkProductList');
	}
	
	/**
	 * 加载牛奶信息页面列表信息
	 * ajaxMilkProductList
	 * @return return_type
	 * @throws
	 */
	public function ajaxMilkProductList()
	{
		$name = I('get.name', '');
		$code = I('get.code', '');
		
		$sql = "select
				 a.id
				,b.short
				,a.name
				,a.code
				,a.valid
				,(select value from wf_sys_item where code = 'product_valid' and itemname = a.valid) as validname
				,a.shelflife
				,a.type
				,(select value from wf_sys_item where code = 'product_type' and itemname = a.type) as typename
				,a.price
				from wf_vending_product a, wf_vending_store b
				where a.storeid = b.id";
		
		if($name != "") {
			$sql = $sql . " and a.name like '%$name%'";
		}
		if($code != "") {
			$sql = $sql . " and a.code like '%$code%'";
		}
		
		$sql = $sql . " order by a.valid desc, a.createtime desc";
		
		$datar = array();
		$datar['total']  = $this->getPaginationDataSumCount($sql);
		$datar['rows'] = $this->getPaginationData($sql, I('get.page'), I('get.rows'));
		$this->ajaxReturn($datar);
	}
	
	/**
	 * 执行添加牛奶信息数据操作
	 * ajaxMilkProductAdd
	 * @return return_type
	 * @throws
	 */
	public function ajaxMilkProductAdd()
	{
		$model = D('VendingProduct');
		
		if($model->addTo() !== false) {
			//插入下发数据
			$model = D('SysClient');
			$dataClients = $model->where("status <> 0 and sysid='vending'")->select();
			foreach ($dataClients as $row) {
				$token = $row['token'];
				$otype = 407;
				$parameter = 1;
				$info = array(  'name' => I('post.name'),
								'code' => I('post.code'),
								'shelflife' => I('post.shelflife'),
								'price' => I('post.price'),
								'describe' => I('post.describe'));
				$jsoninfo = json_encode($info, JSON_UNESCAPED_UNICODE);
				sendCommand($token, $otype, $parameter, $jsoninfo);
			}
			
			$this->jsonData['status'] = 200;
			$this->jsonData['msg'] = '添加数据成功！';
		}
		else {
			$this->jsonData['status'] = 400;
			$this->jsonData['msg'] = '添加数据失败！';
		}
		$this->ajaxReturn($this->jsonData);
	}
	
	/**
	 * 执行编辑牛奶信息数据操作
	 * ajaxMilkProductEdit
	 * @return return_type
	 * @throws
	 */
	public function ajaxMilkProductEdit()
	{
		$model = D('VendingProduct');
		if($model->saveTo() !== false) {
			//插入下发数据
			$model = D('SysClient');
			$dataClients = $model->where("status <> 0 and sysid='vending'")->select();
			foreach ($dataClients as $row) {
				$token = $row['token'];
				$otype = 407;
				$parameter = 2;
				$info = array(  'name' => I('post.name'),
								'code' => I('post.code'),
								'shelflife' => I('post.shelflife'),
								'price' => I('post.price'),
								'describe' => I('post.describe'));
				$jsoninfo = json_encode($info, JSON_UNESCAPED_UNICODE);
				sendCommand($token, $otype, $parameter, $jsoninfo);
			}
			
			$this->jsonData['status'] = 200;
			$this->jsonData['msg'] = '编辑数据成功！';
		}
		else {
			$this->jsonData['status'] = 400;
			$this->jsonData['msg'] = '编辑数据失败！';
		}
		$this->ajaxReturn($this->jsonData);
	}
	
	/*************************** 自动牛奶售货机项目 **** 结束 ***************************/
}
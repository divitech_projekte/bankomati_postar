<?php
namespace Admin\Controller;

use Think\Model;

/**
 * 
 * Admin\Controller$LogisticsOrderController
 * Copyright (c) 2016. 云绅（张家港）精密工业有限公司 版权所有
 * 修改人：jcccy
 * 修改时间：2016年6月16日 上午8:30:07
 * 修改内容：
 */
class LogisticsOrderController extends BaseController {
	public $fields = 'status,orderno,courierid,clientid,type,telphone,paystatus,paymoney,retention,price,orders,cnt';
	
	public function _initialize(){
		parent::_initialize();
	}
	
	/**
	 * 加载界面
	 */
	public  function toList(){
		$this->views("list");
	}
	
	
	
	/**
	 * 加载逾期订单
	 */
	public  function overdueList(){
		$this->views("overdueList");
	}
	
	/**
	 * ajax加载分页数据
	 */
	public  function ajaxList(){
		

		$model = D('LogisticsOrder');
		$data = array();
		if("" != I('get.orderno',"")){
			$data['`a`.`orderno`'] = array('like','%'.I('get.orderno').'%');
		}
		if("" != I('get.telphone',"")){
			$data['`a`.`telphone`'] = array('like','%'.I('get.telphone').'%');
		}
		
		if("" != I('get.orders',"")){
			$data['`a`.`orders`'] = array('like','%'.I('get.orders').'%');
		}
		
		
		$datar = array();
		$datar['total']  = $model->where($data)->count();
		$datar['rows'] =  $model->alias('a')
		->field("`a`.`id`,`a`.`orders`,`a`.`status`,`a`.`orderno`,`a`.`courierid`,`a`.`clientid`,
				`a`.`type`,`a`.`telphone`,`a`.`paystatus`,`a`.`paymoney`,`a`.`retention`,`a`.`price`")
		->join('LEFT JOIN `wf_sys_client` AS `b` ON `b`.`id` =`a`.`clientid`')
		->join('LEFT JOIN `wf_logistics_courier` AS `c` ON `c`.`id` =`a`.`courierid`')
		->where($data)->order(array('`a`.`id`'=>'asc'))
		->page(I('get.page').','.I('get.rows'))->select();
		
		
		$this->ajaxReturn($datar);
		
		
		
		
		

	}
	
	
	
	
	
	
	
	
	/**
	 * ajax删除ID指定的数据
	 */
	public  function ajaxDel(){
		$datab = array("msg"=>"no");
		$model = D('LogisticsOrder');
		$id = I('post.id',0);
		$model->delete($id);
		if(true){
			$datab['msg'] = "ok";
		}
		$this->ajaxReturn($datab);
	}
	
	/**
	 * ajax添加一条数据
	 */
	public  function ajaxAdd(){
		$datab = array("msg"=>"no");
		$model = D('LogisticsOrder');
		$model->create();
		if($model->field('id,createtime,createuser,'.$this->fields)->add()){
			$datab['msg'] = "ok";
		}
		$this->ajaxReturn($datab);
	}
	/**
	 * 终止订单
	 */
	public function doStop(){
		$datab = array("msg"=>"no");
		
		$id = I('post.id',0);
		$model = D('LogisticsOrder');
		$obj = $model->getById($id);
		if($obj){
			
			$status = $obj['status'];
			
			$model->id=$id;
			$model->status=3;//订单状态为异常结束
			if($model->field("id,status")->save()){
				
				if($status == '1') {
					$jsoninfo = array('orderno'=>$obj['orderno']);
					$token = $this->clientIdToToken($obj['clientid']);
					sendCommand ($token, '415', 1, json_encode($jsoninfo)); // 接受
				}
				$datab['msg'] = "ok";
			}
		}
		$this->ajaxReturn($datab);
	}
	
	/**
	 * ajax修改一条数据
	 */
	public  function ajaxEdit(){
			$datab = array("msg"=>"no");
			$model = D('LogisticsOrder');
			$model->create();
			if($model->field('id,updatetime,updateuser,'.$this->fields)->save()){
				$datab['msg'] = "ok";
			}
			$this->ajaxReturn($datab);
	}
	
	
	/**
	 * ajax获取一条数据
	 */
	public  function ajaxGetDataById(){
		$datab = array();
		$model = D('LogisticsOrder');
		$id = I('get.id',0);
		$obj = $model->where('id='.$id)->field('id,inputtime,pickuptime,'.$this->fields)->find();
		$this->ajaxReturn($obj);
	}
	
	
	/**
	 * ajax加载下拉数据
	 */
	public  function ajaxSelectList(){
		$model = D('LogisticsOrder');
		$data = array();
		$datar = $model->alias('a')
		->field("`a`.`id`,`a`.`orderno` as `text`,'' as `desc`")
		->join('LEFT JOIN `wf_sys_client` AS `b` ON `b`.`id` =`a`.`clientid`')
		->join('LEFT JOIN `wf_logistics_courier` AS `c` ON `c`.`id` =`a`.`courierid`')
		->where($data)->order(array('`a`.`id`'=>'asc'))
		->select();
		$this->ajaxReturn($datar);
	}
	
	
	/**
	 * 加载界面
	 */
	public  function toInfoList(){
		$this->views("infoList");
	}
	
	/**
	 * ajax加载分页数据
	 */
	public  function ajaxInfoList(){
	
	
		$model = D('LogisticsOrder');
		$data = array();
		//$data['`a`.`status`'] = array('NEQ',0);
		//$data['`a`.`status`'] = array('NEQ',3);
		if("" != I('get.orderno',"")){
			$data['`a`.`orderno`'] = array('like','%'.I('get.orderno').'%');
		}
		if("" != I('get.telphone',"")){
			$data['`a`.`telphone`'] = array('like','%'.I('get.telphone').'%');
		}
		
		if("" != I('get.clientid',"")){
			$data['`a`.`clientid`'] = I('get.clientid');
		}
		
		if("" != I('get.orders',"")){
			$data['`a`.`orders`'] = array('like','%'.I('get.orders').'%');
		}
		
		$companyids = getCompanyidsByUid(getUserId());
		$where = "(`a`.`status`!=0 and `a`.`status`!=3) and (1!=1";
		foreach($companyids as $vo){
			$where .= " or d.companyid=" . $vo['companyid'];
		}
		$where .= ")";
		
		$datar = array();
		$datar['total']  = $model->alias('a')
						->join('LEFT JOIN `wf_sys_client` AS `b` ON `b`.`id` =`a`.`clientid`')
						->join('LEFT JOIN `wf_logistics_courier` AS `c` ON `c`.`id` =`a`.`courierid`')
						->join('LEFT JOIN `wf_sys_companyclient` AS `d` ON `d`.`clientid` =`a`.`clientid`')
						->where($where)
						->where($data)->count();
		
		$datar['rows'] =  $model->alias('a')
		->field("`a`.`id`,`a`.`orders`,`a`.`status`,`a`.`orderno`,`a`.`courierid`,`a`.`clientid`,`a`.`password`,`a`.`inputtime`,`a`.`pickuptime`,
				`a`.`type`,`a`.`telphone`,`a`.`paystatus`,`a`.`paymoney`,`a`.`retention`,`a`.`price`,`b`.`csn` as `clientname`,`c`.`name` as `couriername`,`a`.`createtime`,`a`.`updatetime`")
					->join('LEFT JOIN `wf_sys_client` AS `b` ON `b`.`id` =`a`.`clientid`')
					->join('LEFT JOIN `wf_logistics_courier` AS `c` ON `c`.`id` =`a`.`courierid`')
					->join('LEFT JOIN `wf_sys_companyclient` AS `d` ON `d`.`clientid` =`a`.`clientid`')
					->where($where)
					->where($data)->order(array('`a`.`id`'=>'desc'))
					->page(I('get.page').','.I('get.rows'))->select();
	
					$datar['sqlxx'] = $model->getLastSql();
		$this->ajaxReturn($datar);
	}
	
	
	/**
	 * ajax加载分页数据
	 */
	public  function ajaxOverdueList(){
	
	
		$model = D('LogisticsOrder');
		$data = array();
		//$data['`a`.`status`'] = array('NEQ',0);
		//$data['`a`.`status`'] = array('NEQ',3);
		if("" != I('get.orderno',"")){
			$data['`a`.`orderno`'] = array('like','%'.I('get.orderno').'%');
		}
		if("" != I('get.telphone',"")){
			$data['`a`.`telphone`'] = array('like','%'.I('get.telphone').'%');
		}
	
		if("" != I('get.clientid',"")){
			$data['`a`.`clientid`'] = I('get.clientid');
		}
	
		if("" != I('get.orders',"")){
			$data['`a`.`orders`'] = array('like','%'.I('get.orders').'%');
		}
	
		
		
		
		
		
		$companyids = getCompanyidsByUid(getUserId());
		$where = "(1!=1";
		foreach($companyids as $vo){
			$where .= " or d.companyid=".$vo['companyid'];
		}
		$where .=") and `a`.`status`!=0 and `a`.`status`!=3 ";
	
/*
		$sysconfig = D ( 'SysConfig' ); // 系统配置表里提取boxtype值
		$objnorm = $sysconfig->where ( "`sysid`='logistics' and `status`=1 and `key`='arrears' and `companyid`=".intval(getCompanyidByUid(getUserId())) )->find ();
		if($objnorm){
			$overdueTime = $objnorm ['value'];//逾期时间
			$where .= "and `a`.`inputtime`<=".(time()-intval($overdueTime)*60);
		}else{//无逾期备注，则不现实逾期信息
			$where .= " and 1!=1";
		}*/
		$where .= "and `a`.`inputtime`<=(UNIX_TIMESTAMP(NOW())-IFNULL(`e`.`overtime`, 0)*60*60)";//小时
			$datar = array();
			$datar['total']  = $model->alias('a')
			->join('LEFT JOIN `wf_sys_client` AS `b` ON `b`.`id` =`a`.`clientid`')
			->join('LEFT JOIN `wf_sys_clientconfig` AS `e` ON `e`.`clientid` =`a`.`clientid`')
			->join('LEFT JOIN `wf_logistics_courier` AS `c` ON `c`.`id` =`a`.`courierid`')
			->join('LEFT JOIN `wf_sys_companyclient` AS `d` ON `d`.`clientid` =`a`.`clientid`')
			->where($where)
			->where($data)->count();
		
			$datar['rows'] =  $model->alias('a')
			->field("`a`.`id`,`a`.`orders`,`a`.`status`,`a`.`orderno`,`a`.`courierid`,`a`.`clientid`,`a`.`password`,`a`.`inputtime`,`a`.`pickuptime`,
					`a`.`type`,`a`.`telphone`,`a`.`paystatus`,`a`.`paymoney`,`a`.`retention`,`a`.`price`,`b`.`csn` as `clientname`,`c`.`name` as `couriername`,`a`.`createtime`,`a`.`updatetime`")
						->join('LEFT JOIN `wf_sys_client` AS `b` ON `b`.`id` =`a`.`clientid`')
						->join('LEFT JOIN `wf_sys_clientconfig` AS `e` ON `e`.`clientid` =`a`.`clientid`')
						->join('LEFT JOIN `wf_logistics_courier` AS `c` ON `c`.`id` =`a`.`courierid`')
						->join('LEFT JOIN `wf_sys_companyclient` AS `d` ON `d`.`clientid` =`a`.`clientid`')
						->where($where)
						->where($data)->order(array('`a`.`id`'=>'desc'))
						->page(I('get.page').','.I('get.rows'))->select();
			$datar['tmp'] = getCompanyidByUid(getUserId());
		
		$this->ajaxReturn($datar);
	}
	
	
	
	/**
	 * 加载界面
	 */
	public  function toHistoryList(){
		$this->views("historyList");
	}
	
	/**
	 * ajax加载分页数据
	 */
	public  function ajaxHistoryList(){
	
	
		$model = D('LogisticsOrder');
		$data = array();
		//$data['`a`.`status`'] = array('EQ',0);
		if("" != I('get.orderno',"")){
			$data['`a`.`orderno`'] = array('like','%'.I('get.orderno').'%');
		}
		if("" != I('get.telphone',"")){
			$data['`a`.`telphone`'] = array('like','%'.I('get.telphone').'%');
		}
		if("" != I('get.clientid',"")){
			$data['`a`.`clientid`'] = I('get.clientid');
		}
		
		if("" != I('get.orders',"")){
			$data['`a`.`orders`'] = array('like','%'.I('get.orders').'%');
		}
		
		$companyids = getCompanyidsByUid(getUserId());
		$where = "(`a`.`status`=0 or `a`.`status`=3)and (1!=1";
		foreach($companyids as $vo){
			$where .= " or d.companyid=".$vo['companyid'];
		}
		$where .= ")";
		$datar = array();
		$datar['total']  = $model->alias('a')
		->join('LEFT JOIN `wf_sys_client` AS `b` ON `b`.`id` =`a`.`clientid`')
		->join('LEFT JOIN `wf_logistics_courier` AS `c` ON `c`.`id` =`a`.`courierid`')
		->join('LEFT JOIN `wf_sys_companyclient` AS `d` ON `d`.`clientid` =`a`.`clientid`')
		->where($where)
		->where($data)->count();
	
		$datar['rows'] =  $model->alias('a')
		->field("`a`.`id`,`a`.`orders`,`a`.`status`,`a`.`orderno`,`a`.`courierid`,`a`.`clientid`,`a`.`password`,`a`.`inputtime`,`a`.`pickuptime`,
				`a`.`type`,`a`.`telphone`,`a`.`paystatus`,`a`.`paymoney`,`a`.`retention`,`a`.`price`,`a`.`cnt`,`b`.`csn` as `clientname`,`c`.`name` as `couriername`,`a`.`createtime`,`a`.`updatetime`")
					->join('LEFT JOIN `wf_sys_client` AS `b` ON `b`.`id` =`a`.`clientid`')
					->join('LEFT JOIN `wf_logistics_courier` AS `c` ON `c`.`id` =`a`.`courierid`')
					->join('LEFT JOIN `wf_sys_companyclient` AS `d` ON `d`.`clientid` =`a`.`clientid`')
					->where($where)
					->where($data)->order(array('`a`.`id`'=>'desc'))
					->page(I('get.page').','.I('get.rows'))->select();
	
					$this->ajaxReturn($datar);
	}
	
	/**
	 * 获取订单信息(首页最新订单)
	 * ajaxGetSixOrderList
	 * @return ajax
	 * @throws
	 */
	public function ajaxGetSixOrderList()
	{
	
		$model = D('LogisticsOrder');
		$data = array();
	
	
		$ownerid = $this->accessUsernameToRoleAndId(getUserInfo(), 'access_owner');
		if(0 < intval($ownerid)){//等于零表示有多个角色，但是不是admin。等于-1表示admin,-2(无角色)
			$data['`b`.`owner`'] = $ownerid;
		}
	
		$supplierid = $this->accessUsernameToRoleAndId(getUserInfo(), 'access_supplier');
		if(0 < intval($supplierid)){//等于零表示有多个角色，但是不是admin。等于-1表示admin,-2(无角色)
			$data['`b`.`supplier`'] =  $supplierid;
		}
	
		
		$companyids = getCompanyidsByUid(getUserId());
		$where = "1!=1";
		foreach($companyids as $vo){
			$where .= " or d.companyid=".$vo['companyid'];
		}
		$where .="";
		
	
		$datar = array();
		$datar['total']  = $model->alias('a')
		->join('LEFT JOIN `wf_sys_clientconfig` AS `b` ON `b`.`clientid` =`a`.`clientid`')
		->join('LEFT JOIN `wf_sys_client` AS `c` ON `c`.`id` =`a`.`clientid`')
		->join('LEFT JOIN `wf_sys_companyclient` AS `d` ON `d`.`clientid` =`a`.`clientid`')
		->where($where)->where($data)->count();
	
		$datar['rows'] =  $model->alias('a')
		->field("CONCAT(' ".L('order_num')."： ',`a`.`orderno`,'&nbsp;&nbsp;&nbsp;&nbsp;".L('terminal')."：',`c`.`name`,'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',from_unixtime(`a`.`createtime`, '%Y-%m-%d')) as `text`")
		->join('LEFT JOIN `wf_sys_clientconfig` AS `b` ON `b`.`clientid` =`a`.`clientid`')
		->join('LEFT JOIN `wf_sys_client` AS `c` ON `c`.`id` =`a`.`clientid`')
		->join('LEFT JOIN `wf_sys_companyclient` AS `d` ON `d`.`clientid` =`a`.`clientid`')
		->where($where)->where($data)->order(array('`a`.`id`'=>'desc'))
		->page(I('get.page').','.I('get.rows'))->select();
		//$datar['sql'] = $model->getLastSql();
		$this->ajaxReturn($datar);
	}
}
<?php
namespace Admin\Controller;

use Think\Model;
/**
 * 后台首页
 * Admin\Controller$IndexController
 * Copyright (c) 2016. 云绅（张家港）精密工业有限公司 版权所有
 * 修改人：jcccy
 * 修改时间：2016年3月7日 下午4:53:22
 * 修改内容：
 */
class IndexController extends BaseController {
	
	
	public function index(){    
		//判断用户是否有合法session
		if(session('?LOGININFO.userid')){
			
			
			//$supplierid = $this->accessUsernameToRoleAndId(getUserInfo(), 'access_supplier');//测试
			//$this->assign("supplierid",$supplierid);
			
			
			$this->index_views("index");
		}else{
			$this->redirect("Index/login");
		}
		
	  
	}
	
	public function formatterJs(){
		$this->display($this->theme."/Public/formatter");
	}
	
	public function formatterJsVending(){
		$this->display($this->theme."/Public/formatterVending");
	}
	
	public function formatterJsCertificate(){
		$this->display($this->theme."/Public/formatterCertificate");
	}
	
	public function formatterJsAccess(){
		$this->display($this->theme."/Public/formatterAccess");
	}
	
	public function formatterJsLogistics(){
		$this->display($this->theme."/Public/formatterLogistics");
	}
	
	public function formatterJsCharge(){
		$this->display($this->theme."/Public/formatterCharge");
	}
	
	
	public function formatterJsCurd(){
		$this->display($this->theme."/Public/curd");
	}
	
	public function login(){
		session(null);
		$this->index_views("login");
	}
	public function doLogin(){
		$datab = array("msg"=>"no");
		$model = D('SysUser');
		$username = I('post.username');
		$obj = $model->where("username='$username' and status=1")->find();//$model->getByUsername(I('post.username'));
		if(null != $obj && $obj['password'] === md5(md5(I('post.password')).C('mark'))){
			//成功登录创建session
			$arr = array();
			$arr['username'] = $obj['username'];
			$arr['userid'] = $obj['id'];
			$arr['auth'] = array();
			session('LOGININFO',$arr);
			
			$datab['msg'] = "ok";
		}
		
		$this->ajaxReturn($datab);
	}
	
	public function outLogin(){
		//退出销毁session
		session(null);
		$this->redirect("Index/login");
	}
  	
	/**
	 * 首页显示终端状态
	 * getClientInfoPanel
	 * @return return_type
	 * @throws
	 */
	public function getClientInfoPanel()
	{
		
		$mode = new Model();
		$time = time();
		$sql = "SELECT
				 a.clientid
				,c.name AS clientname
				,CASE WHEN SUM(CASE WHEN IFNULL(b.nearepcount, 0) > 0 THEN 1 ELSE 0 END) > 0 THEN 1 ELSE 0 END AS valid -- 1 存在近效期库存， 0 正常
				,CASE WHEN SUM(CASE WHEN IFNULL(b.cnt, 0) > 0 THEN 0 ELSE 1 END) > 0 THEN 1 ELSE 0 END AS oos -- 1 存在货道库存为0，0 正常
				,c.isonline
				,CASE WHEN c.status = 2 THEN 4 ELSE (CASE WHEN c.status = 3 THEN 6 ELSE 0 END) END AS status -- 2 停用返4  3 维护返6 1正常返0
				FROM
				(SELECT clientid, id FROM wf_sys_box WHERE STATUS = 1) a
				LEFT JOIN
				(SELECT boxid, COUNT(id) AS cnt, SUM(CASE WHEN UNIX_TIMESTAMP(NOW()) > duodate THEN 1 ELSE 0 END) AS nearepcount FROM wf_vending_storage WHERE STATUS = 1 GROUP BY boxid) b
				ON a.id = b.boxid
				INNER JOIN
				(SELECT id, NAME, STATUS, CASE WHEN $time > IFNULL(isonline, 0) + 30 THEN 0 ELSE 1 END AS isonline FROM wf_sys_client WHERE (STATUS = 1 OR STATUS = 2 OR STATUS = 3) and sysid = '" . C('SYSTEM_ID') . "') c
				ON a.clientid = c.id
				GROUP BY a.clientid
				";
		
		$data = $mode->query($sql);
		
		$this->assign("list",$data);
		$this->views("mainclientinfo");
	}
	
	/**
	 * 首页显示最新公告
	 * getNewsInfoPanel
	 * @return return_type
	 * @throws
	 */
	public function getNewsInfoPanel()
	{
		$this->views("mainnewsinfo");
	}
	
	/**
	 * 首页显示销售数据图表
	 * getSalesInfoPanel
	 * @return return_type
	 * @throws
	 */
	public function getSalesInfoPanel()
	{
		$this->views("mainsalesinfo");
	}
	
	
	
	
	/**
	 * 首页显示终端状态（洗衣柜专用）
	 * getClientInfoPanelAccess
	 * @return return_type
	 * @throws
	 */
	public function getClientInfoPanelAccess()
	{
	
		$mode = new Model();
		$time = time();
		$sysid = C('SYSTEM_ID');
		
		$where = "";
		$ownerid = $this->accessUsernameToRoleAndId(getUserInfo(), 'access_owner');//等于零表示有多个角色，但是不是admin。等于-1表示admin,-2(无对应的角色)
		$supplierid = $this->accessUsernameToRoleAndId(getUserInfo(), 'access_supplier');//等于零表示有多个角色，但是不是admin。等于-1表示admin,-2(无对应的角色)
		
		
		
		if("-1" !== $ownerid || "-1" !== $supplierid){
			$where01 = "OR 1!=1 ";
			
			$roles = getRoleKeysByUid(getUserId());
			if(in_array("assess_company", $roles)){
				$where01 = "OR 1=1 ";
			}
			
			
			
			$where .= "AND  ( `d`.`owner`=$ownerid OR `d`.`supplier`=$supplierid $where01)";
		}
		
		
		$sql = "SELECT
				a.clientid
				,c.name AS clientname
				,c.isonline
				,CASE WHEN c.status = 2 THEN 4 ELSE (CASE WHEN c.status = 3 THEN 6 ELSE 0 END) END AS STATUS -- 2 停用返4  3 维护返6 1正常返0
				FROM
				    (SELECT `x`.`id`, `x`.`name`, `x`.`status`,( CASE WHEN $time > IFNULL(`x`.isonline, 0) + 30 THEN 0 ELSE 1 END) AS isonline ,`d`.`owner`,`d`.`supplier`
     				FROM wf_sys_client `x`,wf_sys_clientconfig `d` WHERE  `x`.`id`=`d`.`clientid` $where   AND (`x`.`STATUS` = 1 OR `x`.`STATUS` = 2 OR `x`.`STATUS` = 3) AND `x`.`sysid` = '$sysid') c
				INNER JOIN
					(SELECT clientid, id FROM wf_sys_box WHERE STATUS = 1) a
				ON a.clientid = c.id
				GROUP BY a.clientid
						";
	
		$data = $mode->query($sql);
		
		
		$this->assign("list",$data);
		
		//$this->assign("sql",$mode->getLastSql());
		//$this->assign("roles",$roles);
		
		$this->views("mainclientinfo");
	}
}
<?php
namespace Admin\Controller;

/**
 * 
 * Admin\Controller$LogisticsSmsController
 * Copyright (c) 2016. 云绅（张家港）精密工业有限公司 版权所有
 * 修改人：jcccy
 * 修改时间：2016年6月16日 上午9:37:43
 * 修改内容：
 */
class LogisticsSmsController extends SysSmsController {
	
	/*
	 * 店铺管理控制器类架构函数
	 */
	public function _initialize()
	{
		// TODO
		parent::_initialize();
	}
	
	public function logisticsSmsList(){
		$this->views("logisticsSmsList");
	}
	
	
	
	

	/**
	 * ajax加载分页数据
	 */
	public  function ajaxSmsList(){
		$datar = array();
		$model = D('SysSms');
	
		$data = array();
	
		
	
		if(I("get.orderno")){
			$data['`a`.`orderno`'] = I("get.orderno");
		}
		if(I("get.sendee")){
			$data['`a`.`sendee`'] = I("get.sendee");
		}
	
		$companyids = getCompanyidsByUid(getUserId());
		$where = "1!=1";
		foreach($companyids as $vo){
			$where .= " or b.companyid=".$vo['companyid'];
		}
		
		$datar['total']  = $model->alias('a')
		->join('LEFT JOIN `wf_sys_smsstencil` AS `b` ON `b`.`id` =`a`.`stencilid`')
		->join('LEFT JOIN `wf_logistics_order` AS `c` ON `c`.`orderno` =`a`.`orderno`')
		->where($where)
		->where($data)->count();
		$datar['rows'] =  $model->alias('a')
				->field("`a`.`id`,`a`.`stencilid`,`a`.`orderno`,`a`.`sendee`,`a`.`content`,`a`.`sender`,`a`.`sendtime`,`a`.`resendtimes`,`a`.`status`,`a`.`tid`
				,`b`.`type`,`b`.`code`,`b`.`info`,`c`.`orders`")
					->join('LEFT JOIN `wf_sys_smsstencil` AS `b` ON `b`.`id` =`a`.`stencilid`')
					->join('LEFT JOIN `wf_logistics_order` AS `c` ON `c`.`orderno` =`a`.`orderno`')
					->where($where)
					->where($data)->order(array('`a`.`id`'=>'desc'))
					->page(I('get.page').','.I('get.rows'))->select();
					$datar['sql'] = $model->getLastSql();
				//	$datar['tttx'] = $companyids.":::".$where;
					$this->ajaxReturn($datar);
	}

}
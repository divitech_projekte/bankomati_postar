<?php
namespace Admin\Controller;

/**
 * 系统操作日志
 * Admin\Controller$SysLogController
 * Copyright (c) 2016. 云绅（张家港）精密工业有限公司 版权所有
 * 修改人：jcccy
 * 修改时间：2016年3月7日 下午4:54:53
 * 修改内容：
 */
class SysLogController extends BaseController {
	public $fields = 'username,time,ip,module,action,info';
	
	public function _initialize(){
		parent::_initialize();
	}

	/**
	 * 加载界面
	 */
	public  function toList(){
		$this->views("list");
	}
	
	/**
	 * ajax加载分页数据
	 */
	public  function ajaxList(){
		
		$model = D('SysLog');
		
		$data = array();
		
		if("" != I('get.username',"")){
			$data['username'] = array('like','%'.I('get.username').'%');
		}
		if("" != I('get.action',"")){
			$data['action'] = array('like','%'.I('get.action').'%');
		}
		
		
		
		
		
		
		
		
		$this->ajaxReturn($model->lists($data,I('get.page'),I('get.rows')));
		

	}
	
	/**
	 * ajax删除ID指定的数据
	 */
	public  function ajaxDel(){
		$model = D('SysLog');
		$id = I('post.id',0);
		$datab = array();
		$model->delete($id);
		if(true){
			$datab['msg'] = "ok";
		}else{
			$datab['msg'] = "no";
		}
		$this->ajaxReturn($datab);
	}
	
	/**
	 * ajax添加一条数据
	 */
	public  function ajaxAdd(){
		
		$datab = array();
		
	
		
		
		$model = D('SysLog');
		if( false !== $model->field($this->fields)->addTo()){
			$datab['msg'] = "ok";
		}else{
			$datab['msg'] = "no";
		}
		$this->ajaxReturn($datab);
	}
	
	/**
	 * ajax修改一条数据
	 */
	public  function ajaxEdit(){
			$datab = array();
		
			
		
			$model = D('SysLog');
			if( false !== $model->field('id,'.$this->fields)->saveTo()){
				$datab['msg'] = "ok";
			}else{
				$datab['msg'] = "no";
			}
			$this->ajaxReturn($datab);
	}
}
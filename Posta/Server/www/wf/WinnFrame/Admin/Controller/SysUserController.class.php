<?php
namespace Admin\Controller;

/**
 * 系统用户信息
 * Admin\Controller$SysUserController
 * Copyright (c) 2016. 云绅（张家港）精密工业有限公司 版权所有
 * 修改人：jcccy
 * 修改时间：2016年3月7日 下午4:55:17
 * 修改内容：
 */
class SysUserController extends BaseController {
	public $fields = 'status,username,cardno,display,type,phone,email,regdate';
	
	public function _initialize(){
		parent::_initialize();
	}

	/**
	 * 加载界面
	 */
	public  function toList(){
		$this->views("list");
	}
	
	/**
	 * ajax加载分页数据
	 */
	public  function ajaxList(){
	
		$model = D('SysUser');
		
		$data = array();
		
		
		if("" != I('get.username',"")){
			$data['username'] = array('like','%'.I('get.username').'%');
		}
		
		if("" != I('get.display',"")){
			$data['display'] = array('like','%'.I('get.display').'%');
		}
		
		
		
		$datar = array();
		$datar['total']  = $model->alias('a')->where($data)->count();
		$datar['rows']   = $model->alias('a')
		->field("`a`.`id`,`a`.`status`,`a`.`username`,`a`.`display`,`a`.`cardno`,`a`.`type`,`a`.`phone`,`a`.`email`,`a`.`regdate`")
		->where($data)->order(array('`a`.`type`'=>'desc'))
		->page(I('get.page').','.I('get.rows'))->select();
		
		
		$this->ajaxReturn($datar);
		
		

	}
	
	
	
	/**
	 * ajax加载分页数据
	 */
	public  function ajaxListByRole(){
	
		$model = D('SysUser');
		$roleid = I('get.roleid','');
		$data = array();
		if("" != I('get.username',"")){
			$data['`a`.`username`'] = array('like','%'.I('get.username').'%');
		}
		if("" != I('get.display',"")){
			$data['`a`.`display`'] = array('like','%'.I('get.display').'%');
		}
	
		$datar = array();
		$datar['total']  = $model->alias('a')->where($data)->count();
		if("" != $roleid){
			$datar['rows'] =  $model->alias('a')
			->field("group_concat('【',IFNULL(`c`.`name`,'无'),'】') as `companyname`,`a`.`id`,`a`.`status`,`a`.`username`,`a`.`display`,`a`.`type`,`a`.`phone`,`a`.`email`,`a`.`regdate`,(select (SELECT IF (0=(select count(*) from wf_sys_userrole as b where b.userid=a.id and b.roleid=".$roleid."),'','true') AS  `checked`)) AS  `checked`")
			->join("LEFT JOIN `wf_sys_companyuser` AS `b` ON `b`.`userid` =`a`.`id`")
			->join("LEFT JOIN `wf_sys_company` AS `c` ON `c`.`id` =`b`.`companyid`")
			->where($data)->order(array('`a`.`type`'=>'desc'))
			->page(I('get.page').','.I('get.rows'))->group('`a`.`id`')->select();
		}else{
			$datar['rows'] =  $model->alias('a')
			->field("group_concat('【',IFNULL(`c`.`name`,'无'),'】') as `companyname`,`a`.`id`,`a`.`status`,`a`.`username`,`a`.`display`,`a`.`type`,`a`.`phone`,`a`.`email`,`a`.`regdate`")
			->join("LEFT JOIN `wf_sys_companyuser` AS `b` ON `b`.`userid` =`a`.`id`")
			->join("LEFT JOIN `wf_sys_company` AS `c` ON `c`.`id` =`b`.`companyid`")
			->where($data)->order(array('`a`.`type`'=>'desc'))
			->page(I('get.page').','.I('get.rows'))->group('`a`.`id`')->select();
				
		}
		$this->ajaxReturn($datar);
	}
	
	
	/**
	 * ajax加载分页数据
	 */
	public  function ajaxListByCompany(){
	
		$model = D('SysUser');
		$companyid = I('get.companyid','');
		$data = array();
		if("" != I('get.username',"")){
			$data['`a`.`username`'] = array('like','%'.I('get.username').'%');
		}
		if("" != I('get.display',"")){
			$data['`a`.`display`'] = array('like','%'.I('get.display').'%');
		}
	
		$datar = array();
		$datar['total'] = $model->alias('a')
		->where($data)->count();//group_concat(IFNULL(`e`.`name`,''),IFNULL(`e`.`csn`,''))
		
		if("" != $companyid){
			$datar['rows'] =  $model->alias('a')
			->field("group_concat('【',IFNULL(`c`.`name`,'无'),'】') as `rolename`,`a`.`id`,`a`.`status`,`a`.`username`,`a`.`display`,`a`.`type`,`a`.`phone`,`a`.`email`,`a`.`regdate`,(select (SELECT IF (0=(select count(*) from wf_sys_companyuser as b where b.userid=a.id and b.companyid=".$companyid."),'','true') AS  `checked`)) AS  `checked`")
			->join("LEFT JOIN `wf_sys_userrole` AS `b` ON `b`.`userid` =`a`.`id`")
			->join("LEFT JOIN `wf_sys_role` AS `c` ON `c`.`id` =`b`.`roleid`")
			->where($data)->order(array('`a`.`type`'=>'desc'))
			->page(I('get.page').','.I('get.rows'))->group('`a`.`id`')->select();
			
			$datar['sql'] = $model->getLastSql();
		}else{
			$datar['rows'] =  $model->alias('a')
			->field("group_concat('【',IFNULL(`c`.`name`,'无'),'】') as `rolename`,`a`.`id`,`a`.`status`,`a`.`username`,`a`.`display`,`a`.`type`,`a`.`phone`,`a`.`email`,`a`.`regdate`")
			->join("LEFT JOIN `wf_sys_userrole` AS `b` ON `b`.`userid` =`a`.`id`")
			->join("LEFT JOIN `wf_sys_role` AS `c` ON `c`.`id` =`b`.`roleid`")
			->where($data)->order(array('`a`.`type`'=>'desc'))
			->page(I('get.page').','.I('get.rows'))->group('`a`.`id`')->select();
			$datar['sql2'] = $model->getLastSql();
	
		}
		$this->ajaxReturn($datar);
	}
	
	
	
	
	/**
	 * ajax删除ID指定的数据
	 */
	public  function ajaxDel(){
		$datab = array("msg"=>"no");
		$id = I('post.id',0);
		
		$model = D('SysUser');
		$model->delete($id);
		if(true){
			$datab['msg'] = "ok";
		}
		$this->ajaxReturn($datab);
	}
	
	/**
	 * ajax添加一条数据
	 */
	public  function ajaxAdd(){
		$datab = array("msg"=>"no");
		
		$model = D('SysUser');
		$model->create();
		if("" != I("post.password","") && I("post.password") === I("post.passverify")){
			$model->password = md5(md5(I("post.password")).C('mark'));// 密码
			$this->fields .= ',password';
		}
		$model->regdate = intval(strtotime(I("post.regdate")));//注册日期
			
		if( false !== $model->field('id,createtime,createuser,'.$this->fields)->add()){
			$datab['msg'] = "ok";
		}
			
		$this->ajaxReturn($datab);
	}
	
	/**
	 * ajax修改一条数据
	 */
	public  function ajaxEdit(){
			$datab['msg'] = "no";
		
			$model = D('SysUser');
			$model->create();
			if("" != I("post.password","") && I("post.password") === I("post.passverify")){
				$model->password = md5(md5(I("post.password")).C('mark'));// 密码
				$this->fields .= ',password';
			}
			$model->regdate = intval(strtotime(I("post.regdate")));//注册日期
			
			if( false !== $model->field('id,updatetime,updateuser,'.$this->fields)->save()){
				$datab['msg'] = "ok";
			}
			
			$this->ajaxReturn($datab);
	}
	
	
	/**
	 * ajax获取一条数据
	 */
	public  function ajaxGetDataById(){
		$datab = array();
		$model = D('SysUser');
		$id = I('get.id',0);
		$obj = $model->where('id='.$id)->field('id,status,username,display,cardno,type,phone,email,FROM_UNIXTIME(regdate,\'%Y-%m-%d\') as regdate')->find();//
	
		$this->ajaxReturn($obj);
	}
	
	
	/**
	 * ajax加载下拉数据
	 */
	public  function ajaxSelectList(){
		$model = D('SysUser');
		$data = array();
	
	
		$datar = $model->alias('a')
	
		->field("`a`.`id`,CONCAT(`a`.`username`,'/',`a`.`display`) as `text`,'' as `desc`")
		->where($data)->order(array('`a`.`id`'=>'asc'))
		->select();
		$this->ajaxReturn($datar);
	
	}
	
	
	
	
	
	/**
	 * 跳转到分配用户角色页面
	 */
	public function toAllotUserList(){
		$this->views("allotUser");
	}
	
	
	
	/**
	 * ajax修改密码
	 */
	public  function ajaxEditPassword(){
		$datab = array();
		$datab['msg'] = "no";
		
		$model = D('SysUser');
		
		$obj = $model->getById(I("post.id","0"));
		if( $obj && ($obj['password'] === md5(md5(I("post.oldpassword")).C('mark'))) ){
			$model->create();
			$model->password = md5(md5(I("post.password")).C('mark'));
			if( false !== $model->field('id,updatetime,updateuser,password')->save()){
				$datab['msg'] = "ok";
			}
		}
		
			
		
			
		$this->ajaxReturn($datab);
	}
	
	
	/**
	 * ajax加载下拉数据
	 */
	public  function ajaxSelectListByRole(){
		$model = D('SysUserrole');
		$data = array();
	
		if("" != I('get.rolekey',"")){
		
			$model2 = D('SysRole');
			$role = $model2->getByKey(I('get.rolekey'));
			$data['roleid'] = $role['id'];
			
			$datar = $model->alias('a')
			->field("`b`.`id`,CONCAT(`b`.`username`) as `text`,'' as `desc`")
			->join('LEFT JOIN `wf_sys_user` AS `b` ON `b`.`id` = `a`.`userid`')
			->where($data)->order(array('`b`.`id`'=>'asc'))
			->select();
		}
		
		
		
		
		$this->ajaxReturn($datar);
	
	}
	
	
	/**
	 * 判断账号是否存在
	 */
	public function ajaxJudgeUsernameIsExist(){
		$datab['msg'] = "no";
		$model = D('SysUser');
		
		if("" != I('get.username',"")){
			$obj = $model->getByUsername(I('get.username'));
			if($obj){
				$datab['msg'] = "ok";
			}
		}
		$this->ajaxReturn($datab);
	}
	
	
}
<?php
namespace Admin\Controller;

use Think\Model;
/**
 * 价目信息控制器
 * Admin\Controller$AccessProductController
 * Copyright (c) 2016. 云绅（张家港）精密工业有限公司 版权所有
 * 创建人：徐辰辰
 * 创建时间：2016-5-3 下午02:11:37
 */
class AccessProductController extends BaseController {
	
	/*
	 * 价目信息控制器类架构函数
	 */
	public function _initialize()
	{
		// TODO
		parent::_initialize();
	}
	
	
	
	public function toList(){
		$this->views("list");
	}
	
	
	
	
	
	
	
	
	/**
	 * ajax加载分页数据
	 */
	public  function ajaxList(){
	
		$model = D('AccessProduct');
		$data = array();
	
		if("" != I('get.name',"")){
			$data['name'] = array('like','%'.I('get.name').'%');
		}
	
		if("" != I('get.code',"")){
			$data['code'] = array('like','%'.I('get.code').'%');
		}
	
	
		$datar = array();
		$datar['total']  = $model->where($data)->count();
	
		$datar['rows'] =  $model->alias('a')
		->field("`a`.`id`,`a`.`name`,`a`.`code`,`a`.`storeid`,`a`.`valid`,`a`.`shelflife`,`a`.`type`,`a`.`price`,`a`.`describe`,`a`.`sort`,`b`.`name` as `storename`")
		->join('LEFT JOIN `wf_access_store` AS `b` ON `b`.`id` =`a`.`storeid`')
		->where($data)->order(array('`a`.`id`'=>'asc'))
		->page(I('get.page').','.I('get.rows'))->select();
	
		$this->ajaxReturn($datar);
	}
	
	
	
	/**
	 * ajax添加一条数据
	 */
	public  function ajaxAdd(){
		$datab = array("msg"=>"no");
	
		$model = D('AccessProduct');
		$model->create();
	
		$model->regdate = time();
		$model->status = "1";
	
		$ownerid = $model->field('id,createtime,createuser,name,code,storeid,valid,shelflife,type,price,describe,sort')->add();
		$datab['data'] = $model->getLastSql();
		if( false !== $ownerid){
			$datab['msg'] = "ok";
		}
			
		$this->ajaxReturn($datab);
	}
	
	
	
	
	/**
	 * ajax获取一条数据
	 */
	public  function ajaxGetDataById(){
		$datab = array();
		$model = D('AccessProduct');
		$id = I('get.id',0);
		$obj = $model->getById($id);
		
		$this->ajaxReturn($obj);
	}
	
	
	/**
	 * ajax删除ID指定的数据
	 */
	public  function ajaxDel(){
		$datab = array("msg"=>"no");
		/*
			$id = I('post.id',0);
			$model = D('SysConfig');
			if( false !== $model->delete($id))
			{
			$datab['msg'] = "ok";
			}
			*/
		$this->ajaxReturn($datab);
	}
	
	/**
	 * ajax修改一条数据
	 */
	public  function ajaxEdit(){
		$datab = array("msg"=>"no");
			
		$model = D('AccessProduct');
		$model->create();
		if( false !== $model->field('id,updatetime,updateuser,name,code,storeid,valid,shelflife,type,price,describe,sort')->save()){
			$datab['msg'] = "ok";
		}
		$this->ajaxReturn($datab);
	}
	
	
	//==========================================
	
	/**
	 * 跳转价目信息页面
	 * accessProductList
	 * @return return_type
	 * @throws
	 */
	public function accessProductList()
	{
		$this->views('accessProductList');
	}
	
	/**
	 * 分页加载价目信息列表数据
	 * ajaxAccessStoreList
	 * @return return_type
	 * @throws
	 */
	public function ajaxAccessProductList()
	{
		//查询条件
		$storeid = I('get.storeid', '');
		$name = I('get.name', '');
		
		//查询sql
		$sql = "select
				 b.id
				,a.name as storename
				,b.name
				,b.code
				,b.price
				,b.describe
				,a.status
				,b.valid
				from wf_access_store a, wf_access_product b, (select distinct supplier from wf_sys_clientconfig) c
				where a.id = b.storeid
				and a.id = c.supplier
				and a.status = 1
				and b.valid = 1";
		
		$storeId = $this->accessUsernameToRoleAndId(getUserInfo(), 'access_supplier');
		
		if($storeId != '-1') {
			$sql = $sql . " and c.supplier = $storeId";
		}
		else {
			//TODO
			$sql = "select
					 b.id
					,a.name as storename
					,b.name
					,b.code
					,b.price
					,b.describe
					,a.status
					,b.valid
					from wf_access_store a, wf_access_product b
					where a.id = b.storeid
					and a.status = 1
					and b.valid = 1";
		}
		
		if($storeid != '') {
			$sql = $sql . " and a.id = $storeid";
		}
		
		if($name != '') {
			$sql = $sql . " and b.name like '%$name%'";
		}
		
		$sql = $sql . " order by a.id, b.id desc";
		
		//分页绑定
		$datar = array();
		$datar['total']  = $this->getPaginationDataSumCount($sql);
		$datar['rows'] = $this->getPaginationData($sql, I('get.page'), I('get.rows'));
		$this->ajaxReturn($datar);
	}
	
	/**
	 * 执行添加价目信息操作
	 * ajaxAccessStoreAdd
	 * @return return_type
	 * @throws
	 */
	public function ajaxAccessProductAdd()
	{
		$model = D('AccessProduct');
		$model->create();
		$model->createuser = getUserInfo();
		$model->createtime = time();
		$model->valid = 1;
		
		$id = $model->add();
		
		if($id !== false) {
			//插入下发数据
			
			
			
			$storeid = I("post.storeid");
			$name = I("post.name");
			$code = I("post.code");
			$price = I("post.price");
			$describe = I("post.describe");
			
			$model = D('SysClient');
			$clientlist = $model->alias('a')
			->field('`a`.`token`')
			->join('LEFT JOIN `wf_sys_clientconfig` AS `b` ON `b`.`clientid` =`a`.`id`')
			->where("`b`.`supplier`=$storeid and `a`.`status`!=0  and `a`.`sysid` = 'access'")->select();
			
			$info = array(  'productid' => $id,
							'name' => $name,
							'code' => $code,
							'price' => $price,
							'storeid' => $storeid,
							'describe' => $describe);
			$jsoninfo = json_encode($info,JSON_UNESCAPED_UNICODE);
			foreach($clientlist as $val){
				sendCommand($val['token'],'410',1,$jsoninfo);//价目表信息下发接口
			}
			
			
			/*
			$model = D('SysClient');
			$dataClients = $model->where("status <> 0 and sysid = 'access'")->select();
			foreach ($dataClients as $row) {
				$token = $row['token'];
				$otype = 410;
				$parameter = 1;
				$info = array(  'productid' => $id,
								'name' => I('post.name'),
								'code' => I('post.code'),
								'price' => I('post.price'),
								'storeid' => I('post.storeid'),
								'describe' => I('post.describe'));
				$jsoninfo = json_encode($info, JSON_UNESCAPED_UNICODE);
				sendCommand($token, $otype, $parameter, $jsoninfo);
			}
			*/
			$this->jsonData['status'] = 200;
			$this->jsonData['msg'] = '添加数据成功！';
		}
		else {
			$this->jsonData['status'] = 400;
			$this->jsonData['msg'] = '添加数据失败！';
		}
		$this->ajaxReturn($this->jsonData);
	}
	
	/**
	 * 根据id获取数据
	 * ajaxGetDataById
	 * @return return_type
	 * @throws
	 */
	/*
	public function ajaxGetDataById()
	{
		$id = I('get.id', 0);
		
		$model = D('AccessProduct');
		$obj = $model->where("id=$id")->find();
		$this->ajaxReturn($obj);
	}
	*/
	/**
	 * 执行编辑价目信息操作
	 * ajaxAccessStoreEdit
	 * @return return_type
	 * @throws
	 */
	public function ajaxAccessProductEdit()
	{
		$model = D('AccessProduct');
		$model->create();
		$model->updateuser = getUserInfo();
		$model->updatetime = time();
		
		if($model->save() !== false) {
			//插入下发数据
			
			
			$storeid = I("post.storeid");
			$model = D('SysClient');
			$clientlist = $model->alias('a')
			->field('`a`.`token`')
			->join('LEFT JOIN `wf_sys_clientconfig` AS `b` ON `b`.`clientid` =`a`.`id`')
			->where("`b`.`supplier`=$storeid and `a`.`status`!=0  and `a`.`sysid` = 'access'")->select();
				
			$info = array(  'productid' => I("post.id"),
					'name' => I("post.name"),
					'code' => I("post.code"),
					'price' => I("post.price"),
					'storeid' => $storeid,
					'describe' => I("post.describe"));
			$jsoninfo = json_encode($info,JSON_UNESCAPED_UNICODE);
			foreach($clientlist as $val){
				sendCommand($val['token'],'410',2,$jsoninfo);//价目表信息下发接口
			}
				
			/*
			$model = D('SysClient');
			$dataClients = $model->where("status <> 0 and sysid = 'access'")->select();
			foreach ($dataClients as $row) {
				$token = $row['token'];
				$otype = 410;
				$parameter = 2;
				$info = array(  'productid' => I('post.id'),
								'name' => I('post.name'),
								'code' => I('post.code'),
								'price' => I('post.price'),
								'storeid' => I('post.storeid'),
								'describe' => I('post.describe'));
				$jsoninfo = json_encode($info, JSON_UNESCAPED_UNICODE);
				sendCommand($token, $otype, $parameter, $jsoninfo);
			}*/
			
			$this->jsonData['status'] = 200;
			$this->jsonData['msg'] = '编辑数据成功！';
		}
		else {
			$this->jsonData['status'] = 400;
			$this->jsonData['msg'] = '编辑数据失败！';
		}
		$this->ajaxReturn($this->jsonData);
	}
	
	/**
	 * 执行注销价目信息操作
	 * ajaxVoid
	 * @return return_type
	 * @throws
	 */
	public function ajaxVoid()
	{
		$productId = I('post.id', '0');
		
		//验证注销条件
		/*
		$tempModel = new Model();
		$sql = "select * 
				from wf_access_order a, wf_access_store b
				where a.storeid = b.id
				and a.status = 1
				and b.id = $productId";
		$tempData = $tempModel->query($sql);
		
		if(count($tempData) != 0) {
		*/
		if(false) {
			$this->jsonData['status'] = 400;
			$this->jsonData['msg'] = '该价目存在未完成的订单，无法注销！';
		}
		else {
			$model = D('AccessProduct');
			$model->create();
			$model->valid = 0;
			//返回执行条数
			$count = $model->save();
	
			if($count !== false) {
				//插入下发数据
				
				/*
				$model = D('SysClient');
				$dataClients = $model->where("status <> 0 and sysid = 'access'")->select();
				
				foreach ($dataClients as $row) {
					$token = $row['token'];
					$otype = 410;
					$parameter = 3;
					$info = array('productid' => $productId);
					$jsoninfo = json_encode($info, JSON_UNESCAPED_UNICODE);
					sendCommand($token, $otype, $parameter, $jsoninfo);
				}
				*/
				$proobj = $model->getById($productId);
				$storeid = $proobj['storeid'];
				$model = D('SysClient');
				$clientlist = $model->alias('a')
							->field('`a`.`token`')
							->join('LEFT JOIN `wf_sys_clientconfig` AS `b` ON `b`.`clientid` =`a`.`id`')
							->where("`b`.`supplier`=$storeid and `a`.`status`!=0  and `a`.`sysid` = 'access'")->select();
				
				$jsoninfo = json_encode(array('productid' => $productId), JSON_UNESCAPED_UNICODE);
				foreach($clientlist as $val){
					sendCommand($val['token'],'410',3,$jsoninfo);//价目表信息下发接口
				}
				
				
				
				
				$this->jsonData['status'] = 200;
				$this->jsonData['msg'] = '注销数据成功！';
			}
			else {
				$this->jsonData['status'] = 400;
				$this->jsonData['msg'] = '注销数据失败！';
			}
		}
		$this->ajaxReturn($this->jsonData);
	}
	
	/**
	 * 跳转价目管控页面
	 * accessProductForOwnerList
	 * @return return_type
	 * @throws
	 */
	public function accessProductForOwnerList()
	{
		$this->views('accessProductForOwnerList');
	}
	
	public function ajaxAccessProductForOwnerList()
	{
		//查询条件
		$storeid = I('get.storeid', '');
		$name = I('get.name', '');
		
		//查询sql
		$sql = "select
				 b.id
				,a.name as storename
				,b.name
				,b.code
				,b.price
				,b.describe
				,a.status
				,b.valid
				from wf_access_store a, wf_access_product b,(select distinct owner, supplier from wf_sys_clientconfig) c
				where a.id = b.storeid
				and a.id = c.supplier
				and a.status = 1
				and b.valid = 1";
		
		$ownerId = $this->accessUsernameToRoleAndId(getUserInfo(), 'access_owner');
	 	
	 	if($ownerId != '-1') {
			$sql = $sql . " and c.owner = $ownerId";
		}
		else {
			//TODO
			$sql = "select
					 b.id
					,a.name as storename
					,b.name
					,b.code
					,b.price
					,b.describe
					,a.status
					,b.valid
					from wf_access_store a, wf_access_product b
					where a.id = b.storeid
					and a.status = 1
					and b.valid = 1";
		}
		
		if($storeid != '') {
			$sql = $sql . " and a.id = $storeid";
		}
		
		if($name != '') {
			$sql = $sql . " and b.name like '%$name%'";
		}
		
		$sql = $sql . " order by a.id, b.id asc";
		
		//分页绑定
		$datar = array();
		$datar['total']  = $this->getPaginationDataSumCount($sql);
		$datar['rows'] = $this->getPaginationData($sql, I('get.page'), I('get.rows'));
		$this->ajaxReturn($datar);
	}
	
	function judgeCode(){
		$datab = array("msg"=>"no");
		$model = D('AccessProduct');
		$data = array();
		
		
		if("" != I('get.code',"")){
			$data['code'] =I('get.code');
		}
		$obj = $model->where($data)->find();
		if($obj){
			$datab['msg'] = "ok";//找到对象
		}
		$datab['sql'] = $model->getLastSql();
		$this->ajaxReturn($datab);
	}
}
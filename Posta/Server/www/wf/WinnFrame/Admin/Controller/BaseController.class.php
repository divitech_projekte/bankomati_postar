<?php
namespace Admin\Controller;

use Think\Model;
use Common\Controller;

/**
 * 后台模块控制器基类
 * Admin\Controller$BaseController
 * Copyright (c) 2016. 云绅（张家港）精密工业有限公司 版权所有
 * 修改人：jcccy
 * 修改时间：2016年3月7日 下午4:46:13
 * 修改内容：
 */
class BaseController extends \Common\Controller\MyController {
	protected $theme = "default";
	
	public $jsonData = array(
							'status' => null,           //200: 成功，300:警告； 400：错误 
							'msg' => null               //返回显示信息
							);
	
	/*
	 * 初始化后台模块控制器基类架构函数
	 */
	public function _initialize(){
		parent::_initialize();
		//根据配置读取主题
		$this->theme = C('theme');
		//判断action在本类对象中是否有对应方法
		//if(in_array(ACTION_NAME,get_class_methods($this))){
		$b= ACTION_NAME;
		if(1 == count(preg_grep("/^$b$/i", get_class_methods($this)))){
			$model = D('SysAuthorization');
			$action = '/'.MODULE_NAME.'/'.CONTROLLER_NAME.'/'. ACTION_NAME;
			//根据action判断数据是否存在，如果action存在，则更新数据，如果不存在，这新增一条数据
			$model->record($action);
			
			
			if(!isHasAuth($action)){//无权限
				echo "无权";
				$this->redirect("Index/login");
				die(0);//网页停止
			}
			
			
			
			
		}else{
			echo "Action Error<br />";
			die(0);//网页停止
		}
	}
	
	/**
	 * 
	 * @param 未找到的action名称 $actionName
	 */
	public function _empty($actionName) {
			echo "方法未定义！".'/'.MODULE_NAME.'/'.CONTROLLER_NAME.'/'.$actionName;
	}
	
	/**
	 * 
	 * @param 视图输出  $html
	 */
	public function views($html){
		$this->display($this->theme."/".CONTROLLER_NAME."/".$html);
	}
	
	/**
	 * 
	 * @param 视图模版输出 $html
	 */
	public function index_views($html){
		$this->display($this->theme."/Public/header");
		$this->display($this->theme."/".CONTROLLER_NAME."/".$html);
		$this->display($this->theme."/Public/footer");
	}
	
	/**
	 * 分页查询时总数据条数
	 * getPaginationDataSumCount 
	 * @param 纯sql查询语句 $sql
	 * @return 总条数 $count
	 * @throws
	 */
	public function getPaginationDataSumCount($sql)
	{
		$model = new Model();
		$datar = array();
		$datar = $model->query($sql);
		return count($datar);
	}
	
	/**
	 * 分页查询数据
	 * getPaginationData
	 * @param 纯sql查询语句 $sql
	 * @param 页数 $page
	 * @param 每页显示条数 $rows
	 * @return 查询数据
	 * @throws
	 */
	public function getPaginationData($sql, $page, $rows)
	{
		if($page != null && $rows != null) {
			$offset = ($page - 1) * $rows;
		}
		else {
			$offset = 0;
		}
		
		if($rows == null) {
			$rows = 10;
		}
		
		$model = new Model();
		$tempSql = $sql . " limit $offset, $rows";
		$data = $model->query($tempSql);
		return $data;
	}
	
	/**
     * Ajax方式返回数据到客户端, 重写
     * @access protected
     * @param mixed $data 要返回的数据
     * @param String $type AJAX返回数据格式
     * @param int $json_option 传递给json_encode的option参数
     * @return void
     */
    protected function ajaxReturn($data, $type='', $json_option=0) {
        if(empty($type)) $type  =   C('DEFAULT_AJAX_RETURN');
        switch (strtoupper($type)){
            case 'JSON' :
                // 返回JSON数据格式到客户端 包含状态信息
                header('Content-Type:application/json; charset=utf-8');
                exit(json_encode($data,$json_option));
          	case 'COMP' :
                // 返回JSON数据格式到客户端 包含状态信息 兼容IE浏览器
                header('Content-Type:text/html; charset=utf-8');
                exit(json_encode($data,$json_option));
            case 'XML'  :
                // 返回xml格式数据
                header('Content-Type:text/xml; charset=utf-8');
                exit(xml_encode($data));
            case 'JSONP':
                // 返回JSON数据格式到客户端 包含状态信息
                header('Content-Type:application/json; charset=utf-8');
                $handler  =   isset($_GET[C('VAR_JSONP_HANDLER')]) ? $_GET[C('VAR_JSONP_HANDLER')] : C('DEFAULT_JSONP_HANDLER');
                exit($handler.'('.json_encode($data,$json_option).');');  
            case 'EVAL' :
                // 返回可执行的js脚本
                header('Content-Type:text/html; charset=utf-8');
                exit($data);
            default     :
                // 用于扩展其他返回格式数据
                Hook::listen('ajax_return',$data);
        }
    }
    
    /**
     * 存取柜获取登陆用户是承包商还是供应商并获取对应id
     * accessUsernameToRoleAndId
     * @param unknown_type $username
     * @return id or 0(多角色) or -1(admin) or -2(无角色) or -3用户未绑定终端
     * @throws
     */
    public function accessUsernameToRoleAndId($username, $role)
    {
    	if($username == 'admin') {
    		return '-1';
    	}
    	else {
    		$model = new Model();
	    	$sql = "select a.id, c.key
					from wf_sys_user a, wf_sys_userrole b, wf_sys_role c
					where a.id = b.userid
					and c.id = b.roleid
					and a.username = '$username'
					and c.key = '$role'";
	    	$data = $model->query($sql);
	    	if(count($data) == 1) {
	    		$userId = $data[0]['id'];
		    	switch ($role) {
		    		case 'access_owner':
		    			$sql = "select a.id from wf_access_owner a, wf_access_owneruser b
								where a.id = b.ownerid
								and b.userid = $userId";
		    			$ret = $model->query($sql);
		    			if(count($ret) == 0) {
		    				$id = '-3';
		    			}
		    			else {
		    				$id = $ret[0]['id'];
		    			}		    			
		    			break;
		    		case 'access_supplier':
		    			$sql = "select a.id from wf_access_store a, wf_access_storeuser b
								where a.id = b.storeid
								and b.userid = $userId";
		    			$ret = $model->query($sql);
		    			if(count($ret) == 0) {
		    				$id = '-3';
		    			}
		    			else {
		    				$id = $ret[0]['id'];
		    			}		  
		    			break;
		    		default:
		    			$id = '0';
		    	}
		    	return $id;
	    	}
	    	else if(count($data) > 1) {
	    		//多个角色
	    		return '0';
	    	}
	    	else {
	    		//无角色信息
	    		return '-2';
	    	}
    	}
    }
    
    /**
     * 合计页面总条数
     * getFooterTotal
     * @param unknown_type $sql
     * @return return_type
     * @throws
     */
    public function getFooterTotal($sql)
    {
    	$model = new Model();
		$data = array();
		$data = $model->query($sql);
		return count($data);
    }
    
    /**
     * 合计页面数据
     * getFooterRows
     * @param unknown_type $sql
     * @return return_type
     * @throws
     */
    public function getFooterRows($sql)
    {
    	$model = new Model();
    	$data = $model->query($sql);
    	return $data;
    }
    
    /**
     * 合计数据获取
     * getFooterInfo
     * @param unknown_type $footerSql
     * @return return_type
     * @throws
     */
    public function getFooterInfo($footerSql)
    {
    	$model = new Model();
    	$data = $model->query($footerSql);
    	return $data;
    }
    
    /**
     * 终端id装token
     * clientIdToToken
     * @param unknown $clientId
     * @return \Think\mixed|string
     */
    public function clientIdToToken($clientId)
    {
    	$model = M("SysClient");
    	$data = $model->where("id = $clientId")->find();
    	if($data) {
    		return $data['token'];
    	}
    	else {
    		return '0';
    	}
    }
    

}
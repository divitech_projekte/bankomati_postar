<?php
namespace Admin\Controller;

use Think\Model;
/**
 * 
 * Admin\Controller$AccessStoreController
 * Copyright (c) 2016. 云绅（张家港）精密工业有限公司 版权所有
 * 修改人：jcccy
 * 修改时间：2016年6月2日 上午10:38:33
 * 修改内容：
 */
class AccessClientController extends SysClientController {
	
	/*
	 * 店铺管理控制器类架构函数
	 */
	public function _initialize()
	{
		// TODO
		parent::_initialize();
	}
	

	/**
	 * 保存客户端的配置信息
	 */
	public function ajaxSaveConfig(){
		$datab = array();
		$datab['msg'] = "no";
		$boo = false;
		$model = D('SysClient');
		$obj = $model->getById(I('post.id'));
		if($obj && ('1' === $obj['status'] || '2' === $obj['status'] || '3' === $obj['status'])){//状态只能是1：启用（可使用状态） 2：停用 3：维护 被配置
			$model2 = D('SysClientconfig');
			$obj2 = $model2->getByClientid(I('post.id'));
			if(null == $obj2){
				$model2->create();
				$model2->createuser = getUserInfo();
				$model2->createtime = time();
				$model2->clientid = I('post.id');
				$model2->field("createtime,createuser,clientid,emergency,owner,supplier")->add();
			}else{
				$model2->create();
				$model2->updateuser = getUserInfo();
				$model2->updatetime = time();
				$model2->id = $obj2['id'];
				$model2->field('id,updatetime,updateuser,emergency,owner,supplier')->save();
			}
			//sendCommand($obj['token'],'301',2,I('post.emergency'));//变更下发求助电话
			$ownerid = I('post.owner');
			$storeid = I('post.supplier');
			$emergency = I('post.emergency');
			
			$model3 = D('AccessStore');
			$store = $model3->getById($storeid);
			$phone = $store['phone'];
			sendCommand($obj['token'],'409',2,"{\"ownerid\":\"$ownerid\",\"storeid\":\"$storeid\",\"emergency\":\"$emergency\",\"phone\":\"$phone\"}");//变更下发求助电话
			
	
			$datab['msg'] = "ok";
		}
	
		$this->ajaxReturn($datab);
	}
}
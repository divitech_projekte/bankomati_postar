<?php
namespace Admin\Controller;

/**
 * 存放店商信息
 * Admin\Controller$VendingStoreController
 * Copyright (c) 2016. 云绅（张家港）精密工业有限公司 版权所有
 * 修改人：jcccy
 * 修改时间：2016年3月7日 下午4:53:56
 * 修改内容：
 */
class VendingStoreController extends BaseController {
	public $fields = 'status,name,code,short,address,linkman,phone,email,regdate';
	
	public function _initialize(){
		parent::_initialize();
	}
	
	/**
	 * 加载界面
	 */
	public  function toList(){
		$this->views("list");
	}
	
	/**
	 * ajax加载分页数据
	 */
	public  function ajaxList(){
		
		
		
		$model = D('VendingStore');
		$data = array();
		$datar = array();
		$datar['total']  = $model->where($data)->count();
		$datar['rows'] =  $model->alias('a')
		->field("`a`.`id`,`a`.`status`,`a`.`name`,`a`.`code`,`a`.`short`,`a`.`address`,`a`.`linkman`,`a`.`phone`,`a`.`email`,`a`.`regdate`")
		->where($data)->order(array('`a`.`id`'=>'asc'))
		->page(I('get.page').','.I('get.rows'))->select();
		$this->ajaxReturn($datar);
		
		

		

	}
	
	/**
	 * ajax删除ID指定的数据
	 */
	public  function ajaxDel(){
		$model = D('VendingStore');
		$id = I('post.id',0);
		$datab = array();
		$model->delete($id);
		if(true){
			$datab['msg'] = "ok";
		}else{
			$datab['msg'] = "no";
		}
		$this->ajaxReturn($datab);
	}
	
	/**
	 * ajax添加一条数据
	 */
	public  function ajaxAdd(){
		
		$datab = array();
		
	
		
		
		$model = D('VendingStore');
		
		
		$data = array();
		$data['status'] = I("post.status");
		$data['name'] = I("post.name");
		$data['code'] = I("post.code");
		$data['short'] = I("post.short");
		$data['address'] = I("post.address");
		$data['linkman'] = I("post.linkman");
		$data['phone'] = I("post.phone");
		$data['email'] = I("post.email");
		$data['regdate'] = intval(strtotime(I("post.regdate")));
		
		
		if( false !== $model->field($this->fields)->addTo($data)){
			$datab['msg'] = "ok";
		}else{
			$datab['msg'] = "no";
		}
		$this->ajaxReturn($datab);
	}
	
	/**
	 * ajax修改一条数据
	 */
	public  function ajaxEdit(){
			$datab = array();
			
			$data = array();
			
			$data['id'] = I("post.id");
			$data['status'] = I("post.status");
			$data['name'] = I("post.name");
			$data['code'] = I("post.code");
			$data['short'] = I("post.short");
			$data['address'] = I("post.address");
			$data['linkman'] = I("post.linkman");
			$data['phone'] = I("post.phone");
			$data['email'] = I("post.email");
			$data['regdate'] = intval(strtotime(I("post.regdate")));
			
		
			$model = D('VendingStore');
			if( false !== $model->field('id,'.$this->fields)->saveTo($data)){
				$datab['msg'] = "ok";
			}else{
				$datab['msg'] = "no";
			}
			$this->ajaxReturn($datab);
	}
	
	
	/**
	 * 加载店商下拉框选项
	 * ajaxSelectList
	 * @return return_type
	 * @throws
	 */
	public  function ajaxSelectList(){
	
		$model = D('VendingStore');
		$where = array();
		
		$datar = $model->alias('a')
		->field("`a`.`id`,`a`.`name` as `text`")
		->where($where)->order(array('`a`.`id`'=>'desc'))
		->select();
		$this->ajaxReturn($datar);
	}
	
	
	/**
	 * ajax获取一条数据
	 */
	public  function ajaxGetDataById(){
		$datab = array();
		$model = D('VendingStore');
		$id = I('get.id',0);
		//regdate
		
		$obj = $model->where('id='.$id)->field('id,status,name,code,short,address,linkman,phone,email,FROM_UNIXTIME(regdate,\'%Y-%m-%d\') as regdate')->find();
	
		$this->ajaxReturn($obj);
	}
}
<?php
namespace Admin\Controller;

/**
 * 信息模板
 * Admin\Controller$SysSmsstencilController
 * Copyright (c) 2016. 云绅（张家港）精密工业有限公司 版权所有
 * 修改人：jcccy
 * 修改时间：2016年3月7日 下午4:55:09
 * 修改内容：
 */
class SysSmsstencilController extends BaseController {
	public $fields = 'status,type,code,info';
	
	public function _initialize(){
		parent::_initialize();
	}

	/**
	 * 加载界面
	 */
	public  function toList(){
		$this->views("list");
	}
	
	/**
	 * ajax加载分页数据
	 */
	public  function ajaxList(){
		
		$model = D('SysSmsstencil');
		
		$data = array();
		
		
		
		
		
		
		$datar = array();
		$datar['total']  = $model->where($data)->count();
		$datar['rows'] =  $model->alias('a')
		->field("`a`.`id`,`a`.`status`,`a`.`type`,`a`.`code`,`a`.`info`")
		->where($data)->order(array('`a`.`id`'=>'asc'))
		->page(I('get.page').','.I('get.rows'))->select();
		
		
		$this->ajaxReturn($datar);
		
	}
	
	/**
	 * ajax删除ID指定的数据
	 */
	public  function ajaxDel(){
		$datab = array("msg"=>"no");
		$model = D('SysSmsstencil');
		$id = I('post.id',0);
		$model->delete($id);
		$datab['msg'] = "ok";
		$this->ajaxReturn($datab);
	}
	
	/**
	 * ajax注销ID指定的数据（status 一般0停用，1启用）
	 */
	public  function ajaxCancel(){
		$datab = array("msg"=>"no");
		
			
		
		$model = D('SysSmsstencil');
		$model->create();
		$model->status = 0;
		if( false !== $model->field('id,updatetime,updateuser,status')->save()){
			$datab['msg'] = "ok";
		}
		$this->ajaxReturn($datab);
		
	}
	
	
	
	/**
	 * ajax添加一条数据
	 */
	public  function ajaxAdd(){
		$datab = array("msg"=>"no");
		$model = D('SysSmsstencil');
		
		$type = I('post.type');
		$code = I('post.code');
		$status = I('post.status');
		$obj = $model->where("type='$type' and code='$code' and status='$status'")->count();
		if(0 == $obj  || '0' == I('post.status')){
			$model->create();
			$model->sysid =  C('SYSTEM_ID');
			if( false !== $model->field("id,createtime,createuser,sysid,".$this->fields)->add()){
				$datab['msg'] = "ok";
			}
		}else{
			$datab['data'] = "已经含有相同的模板类型和业务类型的模板被启用！";
		}
		
		$this->ajaxReturn($datab);
	}
	
	/**
	 * ajax修改一条数据
	 */
	public  function ajaxEdit(){
		$datab = array("msg"=>"no");
		$model = D('SysSmsstencil');
		
		$objx = $model->getById(I('post.id',0));
		$type = $objx['type'];
		$code = $objx['code'];
		$obj = $model->where("type='$type' and code='$code' and status='1'")->find();
		if(null === $obj || '0' == I('post.status') || $objx === $obj){
			$model->create();
			if( false !== $model->field('id,updatetime,updateuser,info,status')->save()){
				$datab['msg'] = "ok";
			}
		}else{
			$datab['data'] = "已经含有相同的模板类型和业务类型的模板被启用！";
		}
		$this->ajaxReturn($datab);
	}
	

	/**
	 * ajax获取一条数据
	 */
	public  function ajaxGetDataById(){
		$datab = array();
		$model = D('SysSmsstencil');
		$id = I('get.id',0);
		$obj = $model->where('id='.$id)->field('id,'.$this->fields)->find();
		$this->ajaxReturn($obj);
	}
	
	
	/**
	 * ajax加载分页数据
	 */
	public  function ajaxSelectList(){
	
		$model = D('SysSmsstencil');
		$data = array();
	
		$datar = $model->alias('a')
		->field("`a`.`id`,CONCAT(`a`.`type`,'/',`a`.`code`,'/',`a`.`info`) as `text`,'' as `desc`")
		->where($data)->order(array('`a`.`id`'=>'asc'))
		->select();
		$this->ajaxReturn($datar);
	
	}
	
	public function accessSmsstencilList(){
		$this->views("accessSmsstencilList");
	}
	
	
	/**
	 * ajax短信模板加载分页数据
	 */
	public  function ajaxSmsstencilList(){
		$model = D('SysSmsstencil');
	
		$data = array();
	
	
		if("" != I('get.info',"")){
			$data['info'] = array('like','%'.I('get.info').'%');
		}
		
		
		if("" != I('get.code',"")){
			$data['code'] = I('get.code');
		}
		
		if("" != I('get.type',"")){
			$data['type'] = I('get.type');
		}
		
		if("" != I('get.status',"")){
			$data['status'] = I('get.status');
		}
		
		
	
	
		$data['sysid'] = C('SYSTEM_ID');
	
	
		$datar = array();
		$datar['total']  = $model->where($data)->count();
		$datar['rows'] =  $model->alias('a')
		->field("`a`.`id`,`a`.`info`,`a`.`code`,`a`.`type`,`a`.`status`")
		->where($data)->order(array('`a`.`id`'=>'asc'))
		->page(I('get.page').','.I('get.rows'))->select();
	
	
		$this->ajaxReturn($datar);
	}
	
}
<?php
namespace Admin\Controller;

/**
 * 
 * Admin\Controller$LogisticsBoxController
 * Copyright (c) 2016. 云绅（张家港）精密工业有限公司 版权所有
 * 修改人：jcccy
 * 修改时间：2016年6月16日 上午9:37:43
 * 修改内容：
 */
class LogisticsBoxController extends SysBoxController {
	
	/*
	 * 店铺管理控制器类架构函数
	 */
	public function _initialize()
	{
		// TODO
		parent::_initialize();
	}
	
	/**
	 * 远程开门
	 */
	public function openBox(){
		$datab = array("msg"=>"no");
		
		$clientid = I("post.clientid");
		$id = I("post.id");
		if($clientid && $id){
			$model = D("SysClient");
			$obj = $model->getById($clientid);
			
			$model2 = D("SysBox");
			$obj2 = $model2->getById($id);
			if($obj && $obj2){
				sendCommand ( $obj ['token'], '201', $obj2['no'], "");
			}
			$datab['msg'] = "ok";
		}
		$this->ajaxReturn($datab);
	}
	/**
	 * 跳转到洗衣柜客户端货道运行状态界面
	 */
	public function logisticsBoxStatusList(){//milkBoxStatusList?clientid=4
		$this->views("logisticsBoxStatusList");
	}
	
	
	/**
	 * ajax List洗衣柜数据
	 */
	public  function ajaxLogisticsList(){
	
		$model = D('SysBox');
	
		$data = array();
	
	
		if(I('get.clientid')){
			$data['`a`.`clientid`'] = I('get.clientid');
		}
		if(I('get.productid')){
			$data['`c`.`productid`'] = I('get.productid');
		}
	
		$data['`b`.`status`'] = array('NEQ','0');
		$data['`b`.`sysid`'] =  'logistics';
	
		$datar = array();
	
		$datar['total']  = $model->alias('a')
		->join('LEFT JOIN `wf_sys_client` AS `b` ON `b`.`id` =`a`.`clientid`')
		->join('LEFT JOIN `wf_sys_boxconfig` AS `c` ON `c`.`boxid` = `a`.`id`')->where($data)->count();
	
	//norm
		$datar['rows'] =  $model->alias('a')
		->field("`a`.`id`,`a`.`status`,`a`.`norm`,`a`.`clientid`,`a`.`no`,`a`.`type`,`a`.`storagestatus`,`a`.`isopen`,
				`b`.`name` as `clientname`,`b`.`csn` as `clientcsn`
				") 
				/*
				 ,
				`c`.`maxcapacity`,CONCAT(`d`.`name`,'（',`d`.`code`,'）') as `productname`
				  ,
				 (select count(*) from `wf_vending_storage` as `f` where `f`.`boxid`=`a`.`id` and `f`.`status` = 1 and `f`.`duodate` < unix_timestamp(now())) as `duonum`,
				 (select count(*) from `wf_vending_storage` as `e` where `e`.`boxid`=`a`.`id` and `e`.`status` = 1) as `num`*/
					->join('LEFT JOIN `wf_sys_client` AS `b` ON `b`.`id` =`a`.`clientid`')
					->join('LEFT JOIN `wf_sys_boxconfig` AS `c` ON `c`.`boxid` = `a`.`id`')
					/* ->join('LEFT JOIN `wf_vending_product` AS `d` ON `d`.`id` = `c`.`productid`') */
					->where($data)->order(array('`a`.`clientid`'=>'asc','`a`.`no`'=>'asc'))
					->page(I('get.page').','.I('get.rows'))->select();
	
	
					$this->ajaxReturn($datar);
	
	
	}

}
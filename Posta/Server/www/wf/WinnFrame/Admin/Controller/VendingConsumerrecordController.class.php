<?php
namespace Admin\Controller;

/**
 * 用户卡使用记录控制器
 * Admin\Controller$VendingConsumerrecordController
 * Copyright (c) 2016. 云绅（张家港）精密工业有限公司 版权所有
 * 创建人：徐辰辰
 * 创建时间：2016-3-30 下午02:50:10
 */
class VendingConsumerrecordController extends BaseController {
	
	/*
	 * 上下货清单控制器类架构函数
	 */
	public function _initialize()
	{
		// TODO
		parent::_initialize();
	}
	
	/**
	 * 用户卡充值记录页面跳转
	 * milkRechargeRecord
	 * @return return_type
	 * @throws
	 */
	public function milkRechargeRecord()
	{
		$this->views('milkrechargelist');
	}
	
	/**
	 * 加载用户卡充值记录页面数据
	 * ajaxMilkRechargeList
	 * @return return_type
	 * @throws
	 */
	public function ajaxMilkRechargeRecordList()
	{
		$cardno = I('get.cardno', '');
		$username = I('get.username', '');
		
		$sql = "select
				 a.id
				,b.cardno
				,b.username
				,from_unixtime(a.createtime) as rechargetime
				,a.amountchange
				,a.createuser
				,a.remark
				from wf_vending_consumerrecord a, wf_vending_consumer b
				where a.cardno = b.cardno
				and a.type = 1";
		
		if($cardno != "") {
			$sql = $sql . " and a.cardno = '$name'";
		}
		if($username != "") {
			$sql = $sql . " and b.username like '%$username%'";
		}
		
		$sql = $sql . " order by a.createtime desc";
		
		$datar = array();
		$datar['total']  = $this->getPaginationDataSumCount($sql);
		$datar['rows'] = $this->getPaginationData($sql, I('get.page'), I('get.rows'));
		$this->ajaxReturn($datar);
	}
	
	/**
	 * 用户卡消费记录页面跳转
	 * milkConsumingRecord
	 * @return return_type
	 * @throws
	 */
	public function milkConsumingRecord()
	{
		$this->views('milkconsuminglist');
	}
	
	/**
	 * 加载用户卡消费记录页面数据
	 * ajaxMilkConsumingList
	 * @return return_type
	 * @throws
	 */
	public function ajaxMilkConsumingList()
	{
		$cardno = I('get.cardno', '');
		$username = I('get.username', '');
		
		$sql = "select
				 a.id
				,b.cardno
				,b.username
				,c.name as clientname
				,from_unixtime(a.createtime) as saletime
				,a.orderno
				,a.amountchange
				from wf_vending_consumerrecord a, wf_vending_consumer b, wf_sys_client c
				where a.cardno = b.cardno
				and a.remark = c.token
				and a.type = 4";
		
		if($cardno != "") {
			$sql = $sql . " and a.cardno = '$name'";
		}
		if($username != "") {
			$sql = $sql . " and b.username like '%$username%'";
		}
		
		$sql = $sql . " order by a.createtime desc";
		
		$datar = array();
		$datar['total']  = $this->getPaginationDataSumCount($sql);
		$datar['rows'] = $this->getPaginationData($sql, I('get.page'), I('get.rows'));
		$this->ajaxReturn($datar);
	}
}
<?php
namespace Admin\Controller;

/**
 * 发送信息数据
 * Admin\Controller$SysSmsController
 * Copyright (c) 2016. 云绅（张家港）精密工业有限公司 版权所有
 * 修改人：jcccy
 * 修改时间：2016年3月7日 下午4:55:03
 * 修改内容：
 */
class SysSmsController extends BaseController {
	public $fields = 'stencilid,orderno,sendee,content,sender,sendtime,resendtimes,status,tid';
	
	public function _initialize(){
		parent::_initialize();
	}

	/**
	 * 加载界面
	 */
	public  function toList(){
		$this->views("list");
	}
	
	/**
	 * ajax加载分页数据
	 */
	public  function ajaxList(){
		
		$model = D('SysSms');
		
		$data = array();
		
		
		$datar = array();
		$datar['total']  = $model->where($data)->count();
		$datar['rows'] =  $model->alias('a')
		->field("`a`.`id`,`a`.`stencilid`,`a`.`orderno`,`a`.`sendee`,`a`.`content`,`a`.`sender`,`a`.`sendtime`,`a`.`resendtimes`,`a`.`status`,`a`.`tid`
				,CONCAT(`b`.`type`,'/',`b`.`code`,'/',`b`.`info`) as `stencil`")//,`c`.`orderno`
		->join('LEFT JOIN `wf_sys_smsstencil` AS `b` ON `b`.`id` =`a`.`stencilid`')
		//->join('LEFT JOIN `wf_order` AS `c` ON `c`.`id` =`a`.`orderno`')
		->where($data)->order(array('`a`.`id`'=>'asc'))
		->page(I('get.page').','.I('get.rows'))->select();
		
		
		$this->ajaxReturn($datar);
	}
	
	/**
	 * ajax删除ID指定的数据
	 */
	public  function ajaxDel(){
		$datab = array("msg"=>"no");
		$model = D('SysSms');
		$id = I('post.id',0);
		$model->delete($id);
		$datab['msg'] = "ok";
		$this->ajaxReturn($datab);
	}
	
	/**
	 * ajax添加一条数据
	 */
	public  function ajaxAdd(){
		$datab = array("msg"=>"no");
		$model = D('SysSms');
		$model->create();
		$model->sysid =  C('SYSTEM_ID');
		if( false !== $model->field("id,createtime,createuser,sysid,".$this->fields)->add()){
			$datab['msg'] = "ok";
		}
		$this->ajaxReturn($datab);
	}
	
	/**
	 * ajax修改一条数据
	 */
	public  function ajaxEdit(){
			$datab = array("msg"=>"no");
			$model = D('SysSms');
			$model->create();
			if( false !== $model->field('id,updatetime,updateuser,'.$this->fields)->save()){
				$datab['msg'] = "ok";
			}
			$this->ajaxReturn($datab);
	}
	
	/**
	 * ajax获取一条数据
	 */
	public  function ajaxGetDataById(){
		$datab = array();
		$model = D('SysSms');
		$id = I('get.id',0);
		$obj = $model->where('id='.$id)->field('id,'.$this->fields)->find();
	
		$this->ajaxReturn($obj);
	}
	
	public function accessSmsList(){
		$this->views("accessSmsList");
	}
	
	
	
	/**
	 * ajax加载分页数据
	 */
	public  function ajaxSmsList(){
		$datar = array();
		$model = D('SysSms');
	
		$data = array();
		
		$ownerid = $this->accessUsernameToRoleAndId(getUserInfo(), 'access_owner');
		if(0 < intval($ownerid)){//等于零表示有多个角色，但是不是admin。等于-1表示admin,-2(无角色)
			$data['`a`.`sender`'] = getUserInfo();
		}elseif(0 === intval($ownerid)){
			$data['`a`.`sender`'] = "0";
		}
		
		if(I("get.orderno")){
			$data['`a`.`orderno`'] = I("get.orderno");
		}
		if(I("get.sendee")){
			$data['`a`.`sendee`'] = I("get.sendee");
		}
		
		
		
		$datar['total']  = $model->alias('a')
				->join('LEFT JOIN `wf_sys_smsstencil` AS `b` ON `b`.`id` =`a`.`stencilid`')
				->join('LEFT JOIN `wf_logistics_order` AS `c` ON `c`.`orderno` =`a`.`orderno`')
				->where($data)->count();
		$datar['rows'] =  $model->alias('a')
		->field("`a`.`id`,`a`.`stencilid`,`a`.`orderno`,`a`.`sendee`,`a`.`content`,`a`.`sender`,`a`.`sendtime`,`a`.`resendtimes`,`a`.`status`,`a`.`tid`
				,`b`.`type`,`b`.`code`,`b`.`info`,`c`.`orders`")
					->join('LEFT JOIN `wf_sys_smsstencil` AS `b` ON `b`.`id` =`a`.`stencilid`')
					->join('LEFT JOIN `wf_logistics_order` AS `c` ON `c`.`orderno` =`a`.`orderno`')
		->where($data)->order(array('`a`.`id`'=>'desc'))
		->page(I('get.page').','.I('get.rows'))->select();
		$datar['sql'] = $model->getLastSql();
	
		$this->ajaxReturn($datar);
	}
	
	
	/**
	 * ajax加载分页数据
	 */
	public  function ajaxSmsPromotionList(){
		$datar = array();
		$model = D('SysSms');
	
		$data = array();
	
		$ownerid = $this->accessUsernameToRoleAndId(getUserInfo(), 'access_owner');
		if(0 < intval($ownerid)){//等于零表示有多个角色，但是不是admin。等于-1表示admin,-2(无角色)
			$data['`a`.`sender`'] = getUserInfo();
		}elseif(0 === intval($ownerid)){
			$data['`a`.`sender`'] = "0";
		}
	
		if(I("get.sendee")){
			$data['`a`.`sendee`'] = I("get.sendee");
		}
		
		$data['`b`.`code`'] = 18;//短信推广
	
	
	
	
		$datar['total']  = $model->alias('a')
		->join('LEFT JOIN `wf_sys_smsstencil` AS `b` ON `b`.`id` =`a`.`stencilid`')
		->join('LEFT JOIN `wf_access_order` AS `c` ON `c`.`id` =`a`.`orderno`')
		->where($data)->count();
		$datar['rows'] =  $model->alias('a')
		->field("`a`.`id`,`a`.`stencilid`,`a`.`orderno`,`a`.`sendee`,`a`.`content`,`a`.`sender`,`a`.`sendtime`,`a`.`resendtimes`,`a`.`status`,`a`.`tid`
				,`b`.`type`,`b`.`code`,`b`.`info`")
					->join('LEFT JOIN `wf_sys_smsstencil` AS `b` ON `b`.`id` =`a`.`stencilid`')
					->join('LEFT JOIN `wf_access_order` AS `c` ON `c`.`id` =`a`.`orderno`')
					->where($data)->order(array('`a`.`id`'=>'asc'))
					->page(I('get.page').','.I('get.rows'))->select();
					$datar['sql'] = $model->getLastSql();
	
					$this->ajaxReturn($datar);
	}
	
	
	/**
	 * ajax获取一条查看的数据
	 */
	public  function ajaxGetShowById(){
		$datab = array();
		$model = D('SysSms');
		$id = I('get.id',0);
		$obj = $model->alias('a')
		->field("`a`.`id`,`a`.`stencilid`,`a`.`orderno`,`a`.`sendee`,`a`.`content`,`a`.`sender`,FROM_UNIXTIME(`a`.`sendtime`) as sendtime,`a`.`resendtimes`,`a`.`status`,`a`.`tid`
				,`b`.`type`,`b`.`code`,`b`.`info`")
		->join('LEFT JOIN `wf_sys_smsstencil` AS `b` ON `b`.`id` =`a`.`stencilid`')
			->where('`a`.`id`='.$id)->find();
	
		$this->ajaxReturn($obj);
	}
	
	
	public function accessSmsPromotionList(){
		$this->views("accessSmsPromotionList");
	}
	
	public function ajaxSendSMS(){
		$datab = array("msg"=>"no");
		
		
		$ownerid = $this->accessUsernameToRoleAndId(getUserInfo(), 'access_owner');
		$where = "";
		if(0 <= intval($ownerid)){//等于零表示有多个角色，但是不是admin。等于-1表示admin,-2(无角色)
			$where ="`b`.`owner`=$ownerid";
		}
		
		$order_model = D("AccessOrder");
		$list = $order_model->alias('a')
		->field("`a`.`telphone`,`b`.`owner`")
		->join('LEFT JOIN `wf_sys_clientconfig` AS `b` ON `b`.`clientid` =`a`.`clientid`')
		->where($where)->group('`a`.`telphone`')->select();
		$da = "";
		foreach($list as $value){
			if(10==strlen($value['telphone'])){
				$value['telphone'] = substr($value['telphone'],-9);
			}
			$da .= sendSMS($value['telphone'],
					'1',//短息
					'18',//信息推广
					array('msg'=>I("post.content")),//推广信息
					getSerNo(),//流水号
					0,//无订单
					getUserInfo()
					).",";
		}
		$datab['data'] = $da;
		$datab['ownerid'] = $ownerid;
		$datab['msg'] = "ok";
		$this->ajaxReturn($datab);
	}
	
}
<?php
function getTest($uid){
	$aa = L('lan_define');
	return 'abcabcabc'.$uid.$aa;
}
/**
 * 获取用户登录信息
 */
function getUserInfo(){
	$logininfo = session('LOGININFO');
	return $logininfo['username'];
}
/**
 * 获取用户登录ID
 */
function getUserId(){
	$logininfo = session('LOGININFO');
	return $logininfo['userid'];
}
/**
 * 获取LOGININFO信息
 */
function getLoginInfo(){
	$logininfo = session('LOGININFO');
	var_dump($logininfo);
}
/**
 * 根据action判断当前用户是否有权限
 * @param unknown $action
 */
function isHasAuth($action){
	$model = D('SysAuthorization');
	$logininfo = session('LOGININFO');
	$boo = null;
	if(null == $logininfo)
		$logininfo = array();
	
	if(null == $logininfo['auth'])
		$logininfo['auth'] = array();
	
	if(null == $logininfo['auth'][$action]){
		$boo = $model->ishasPerm($logininfo['userid'],$action);
		$logininfo['auth'][$action] = $boo;
		session('LOGININFO',$logininfo);
	}else{
		$boo = $logininfo['auth'][$action];
	}
	return $boo;
}

/**
 * 与Common\Controller$MyController下的getSerialNo功能相同
 * function.php无法调用到getSerialNo，重新写个
 * 
 * 获取流水号
 * 返回 1-9999
 */
function getSerNo()
{
	$sysconfig = M("SysConfig");
		
		$condition = array();
		$condition['status'] = 1;
		$condition['key'] = 'serialno';
        $data = $sysconfig->where($condition)->field('value')->find();
        $serialNo = (int)$data['value'];
        
       	//更新流水号
       	$newSerialNo = 1;
        if($serialNo > 0 && $serialNo < 9999) {
        	$newSerialNo = $serialNo + 1;
        }
        $sysconfig->value = $newSerialNo;
      	$sysconfig->where($condition)->save();
        
        return $serialNo;
}
	
/**
 * 
 * @param unknown $token
 * @param unknown $otype
 * @param unknown $parameter
 * @param unknown $jsoninfo
 * @return boolean 
 */
function sendCommand($token,$otype,$parameter = '',$jsoninfo = ''){
	$model2 = M('SysCommand');
	$model2->createuser = getUserInfo();
	$model2->createtime = time();
	$model2->status = 0;//待执行
	$model2->serialno = getSerNo();//获取流水号
	$model2->token = $token;
	$model2->otype = $otype;
	$model2->parameter = $parameter;
	$model2->jsoninfo = $jsoninfo;
	return $model2->field('status,serialno,token,otype,parameter,jsoninfo,createuser,createtime')->add();
}

/**
 * 
 * 文件上传
 
 * @param 上传时附件NAME $fileName
 * @param 上传保存类型，FILE保存到文件夹，DB保存到数据库 $target
 * @param 设置附件上传类型 $exts
 * @return boolean
 */
function fileOneUpload($fileName,$target = 'FILE',$exts = array('jpg', 'gif', 'png', 'jpeg')){
	if('FILE' === $target){
		$upload = new \Think\Upload();// 实例化上传类
		$upload->maxSize   =     3145728 ;// 设置附件上传大小(3MB)
		$upload->exts      =     $exts;//array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
		$upload->rootPath  =     './Public/'; // 设置附件上传根目录
		$upload->savePath  =     'Uploads/'; // 设置附件上传（子）目录
		//$upload->saveName =  time().'_'.rand(11111,99999);//array('date','Y-m-d_'.rand(11111,99999));
		$upload->saveName = time().'_'.mt_rand();
		// 上传文件
		$info   =   $upload->uploadOne($_FILES[$fileName]);
		if(!$info) {// 上传错误提示错误信息
			//	$this->error($upload->getError());
			return false;
		}
		return $info;
	}else{//保留
		return false;
	}
}

/**
 * 通过用户ID获取角色数组
 * @param unknown $uid
 */
function getRoleKeysByUid($uid){
	$model = M("SysUserrole");
	$list = $model->alias('a')->field('`b`.`key`')->join('LEFT JOIN `wf_sys_role` AS `b` ON `b`.`id` =`a`.`roleid`')->where("`a`.`userid`=$uid")->select();
	return $list[0];
}


/**
 * 通过用户ID获取公司ID数组
 * @param unknown $uid
 */
function getCompanyidsByUid($uid){
	$model = M("SysCompanyuser");//`a`.`companyid`
	$list = $model->alias('a')->field('`a`.`companyid`')->where("`a`.`userid`=$uid")->order(array('`a`.`companyid`'=>'asc'))->select();
	return $list;
}


/**
 * 通过用户ID获取单个公司ID
 * @param unknown $uid
 */
function getCompanyidByUid($uid){
	$model = M("SysCompanyuser");//`a`.`companyid`
	$obj = $model->where("`userid`=$uid")->order(array('`companyid`'=>'asc'))->find();
	return $obj['companyid'];
}


/**
 * 通过ClientID获取公司ID数组
 * @param unknown $uid
 */
function getCompanyidsByClientid($clientid){
	$model = M("SysCompanyclient");
	$list = $model->alias('a')->field('`a`.`companyid`')->where("`a`.`clientid`=$clientid")->order(array('`a`.`companyid`'=>'asc'))->select();
	return $list;
}




/**
 * 写入日志
 * @param unknown $username
 * @param unknown $module
 * @param unknown $action
 * @param unknown $info
 * @return \Think\mixed|boolean
 */
function setSysLog($username, $module, $action, $info)
{
	$model = M("SysLog");
	$model->username = $username;
	$model->time = time();
	$model->module = $module;
	$model->action = $action;
	$model->info = $info;
	$id = $model->add();
	if($id) {
		return $id;
	}
	else {
		return false;
	}
}




/**
 * 模拟post进行url请求
 * 
 * @param string $url        	
 * @param array $post_data        	
 *
 */
function request_post($url = '', $post_data = array()) {
	if (empty ( $url ) || empty ( $post_data )) {
		return false;
	}
	
	$o = "";
	foreach ( $post_data as $k => $v ) {
		$o .= "$k=" . urlencode ( $v ) . "&";
	}
	$post_data = substr ( $o, 0, - 1 );
	
	$postUrl = $url;
	$curlPost = $post_data;
	$ch = curl_init (); // 初始化curl
	curl_setopt ( $ch, CURLOPT_URL, $postUrl ); // 抓取指定网页
	curl_setopt ( $ch, CURLOPT_HEADER, 0 ); // 设置header
	curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 ); // 要求结果为字符串且输出到屏幕上
	curl_setopt ( $ch, CURLOPT_POST, 1 ); // post提交方式
	curl_setopt ( $ch, CURLOPT_POSTFIELDS, $curlPost );
	$data = curl_exec ( $ch ); // 运行curl
	curl_close ( $ch );
	setSysLog($url."?".$curlPost, 'API', "request_post", $data);
	return $data;
}

function getTimestampByString($timestring){
	if (preg_match ("/^[12]\d\d\d-[0-1]?[1-9]-[0]?\d{1,2}$/i", $timestring)) {//2016-09-01 
		return strtotime($timestring);
	}else if (preg_match ("/^[12]\d\d\d-[0-1]?[1-9]-[0]?\d{1,2} \d{2}:\d{2}:\d{2}$/i", $timestring)) {//2017-12-31 10:24:15
		return strtotime($timestring);
	}else if(preg_match ("/^[0-1]?[1-9]\/\d{1,2}\/[12]\d\d\d$/i", $timestring)){ //09/01/2016
		$year =  substr($timestring, strrpos($timestring,'/')+1);
		$tmp = substr($timestring, strpos($timestring,'/')+1);
		$day = substr($tmp,0, strpos($tmp,'/'));
		$month = substr($timestring,0, strpos($timestring,'/'));
		//echo "asdfa";
		return strtotime($year.'-'.$month.'-'.$day);
	}else if(preg_match ("/^[0-1]?[1-9]\/\d{1,2}\/[12]\d{3} \d{2}:\d{2}:\d{2}$/i", $timestring)){ //09/01/2016 10:24:15 
		
		$year =  substr($timestring, strrpos($timestring,'/')+1,4);
		$tmp = substr($timestring, strpos($timestring,'/')+1);
		$day = substr($tmp,0, strpos($tmp,'/'));
		$month = substr($timestring,0, strpos($timestring,'/'));
		
		$time = substr($timestring, strrpos($timestring,' ')+1);
		return strtotime($year."-".$month.'-'.$day.' '.$time);
	}else{
		return 0;
	}
}

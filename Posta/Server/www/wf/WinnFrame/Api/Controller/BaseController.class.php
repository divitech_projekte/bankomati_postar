<?php
namespace Api\Controller;
use Common\Controller\MyController;

/**
 * 通讯模块控制器基类
 * Api\Controller$BaseController
 * Copyright (c) 2016. 云绅（张家港）精密工业有限公司 版权所有
 * 修改人：xucc
 * 修改时间：2016-7-14 下午04:08:04
 * 修改内容：
 */
class BaseController extends MyController
{
	/*
	 * 初始化通讯模块控制器基类架构函数
	 */
	public function _initialize()
	{
		// TODO
		parent::_initialize();
	}

	/**
	 * 接口模块
	 * 返回通讯状态信息
	 * @param 状态 $status 1 成功(无数据) 0 失败  2成功（有数据）
	 * @param 信息代码 $msgCode
	 */
	public function retMsg($status, $info)
	{
		$data = array('st'=>$status, 'info'=>$info);
		$this->ajaxReturn($data);
	}
	
	/**
	 * 接口模块
	 * 验证接口标示是否有效
	 * @param 接口标示
	 * @return true or false
	 */
	public function validateApikey($apikey)
	{
		$model = M('SysCompany');
		$data = $model->getByApikey($apikey);
		if($data) {
			return true;
		}
		else {
			return false;
		}
	}
	
	/**
	 * 接口模块
	 * 根据apikey获取companyid
	 * @param 接口标示
	 * @return companyid
	 */
	public function getCompanyidByApikey($apikey)
	{
		$model = M('SysCompany');
		$data = $model->where("apikey = '$apikey'")->find();
		if($data) {
			return $data['id'];
		}
		else {
			return '0';
		}
	}
	
	/**
	 * 接口模块
	 * 验证接口标示是否有效
	 * @param 接口标示
	 * @return true or false
	 */
	public function send_post($url, $post_data)
	{
	  	$postdata = http_build_query($post_data);
	  	$options = array(
	  		'http' => array(
	  			'method' => 'POST',//or GET
	  			'header' => 'Content-type:application/x-www-form-urlencoded',
	  			'content' => $postdata,
	  			'timeout' => 2 // 超时时间*2（单位:s）
	  			)
	  		);
	  	$context = stream_context_create($options);
		$doCnt = 0;
		$result = null;
		while ($result == null && $doCnt < 3) {
			$doCnt++;
			$result = file_get_contents($url, false, $context);
		}
		return $result;
  	}
	
  	/**
  	 * 接口模块
  	 * 写入日志
  	 * @param unknown $username
  	 * @param unknown $module
  	 * @param unknown $action
  	 * @param unknown $info
  	 * @return \Think\mixed|boolean
  	 */
  	public function setSysLog($username, $module, $action, $info)
  	{
  		$model = M("SysLog");
  		$model->username = $username;
  		$model->time = time();
  		$model->module = $module;
  		$model->action = $action;
  		$model->info = $info;
  		$id = $model->add();
  		if($id) {
  			return $id;
  		}
  		else {
  			return false;
  		}
  	}
  	
  	/**
	 * 接口模块
	 * 根据csn获取clientid
	 * @param 终端csn
	 * @return clientid
	 */
  	public function csnToClienId($csn)
  	{
  		$model = M("SysClient");
  		$data = $model->where("csn = '$csn'")->find();
  		if($data) {
  			return $data['id'];
  		}
  		else {
  			return '0';
  		}
  	}
  	
  	/**
  	 * 根据柜子号码获取柜子id
  	 * boxnoToId
  	 * @param unknown_type $clientid
  	 * @param unknown_type $boxno
  	 * @return return_type
  	 * @throws
  	 */
  	public function boxnoToId($clientid, $boxno)
  	{
  		$model = M("SysBox");
  		$data = $model->where("clientid = $clientid and no = '$boxno'")->find();
  		if($data) {
  			return $data['id'];
  		}
  		else {
  			return false;
  		}
  	}
  	
  	/**
  	 * 根据柜门id获取no
  	 * @param unknown $boxid
  	 * @return \Think\mixed|boolean
  	 */
  	public function boxidToNo($boxid)
  	{
  		$model = M("SysBox");
  		$data = $model->where("id = '$boxid'")->find();
  		if($data) {
  			return $data['no'];
  		}
  		else {
  			return false;
  		}
  	}
  	
  	/**
  	 * 根据clientid获取token信息
  	 * @param unknown $clientid
  	 * @return \Think\mixed|string
  	 */
  	public function clientidToToken($clientid)
  	{
  		$model = M("SysClient");
  		$data = $model->where("id = '$clientid'")->find();
  		if($data) {
  			return $data['token'];
  		}
  		else {
  			return '0';
  		}
  	}
  	
  	/**
  	 * 下发指令
  	 * @param unknown $token
  	 * @param unknown $otype
  	 * @param string $parameter
  	 * @param string $jsoninfo
  	 * @return \Think\mixed
  	 */
  	function sendCommand($token, $otype, $parameter = '', $jsoninfo = '')
  	{
  		$model = M('SysCommand');
  		$model->createuser = 'sendCommand';
  		$model->createtime = time();
  		$model->status = 0;//待执行
  		$model->serialno = $this->getSerialNo();//获取流水号
  		$model->token = $token;
  		$model->otype = $otype;
  		$model->parameter = $parameter;
  		$model->jsoninfo = $jsoninfo;
  		return $model->field('status,serialno,token,otype,parameter,jsoninfo,createuser,createtime')->add();
  	}
}
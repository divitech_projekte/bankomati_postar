<?php
namespace Api\Controller;

use Think\Model;

/**
 * 物流柜第三方调用接口控制器类
 * Api\Controller$LogisticsController
 * Copyright (c) 2016. 云绅（张家港）精密工业有限公司 版权所有
 * 创建人：徐辰辰
 * 创建时间：2016-7-13 上午10:27:22
 */
class LogisticsController extends BaseController
{
	
	/*
	 * 物流柜控制器类架构函数
	 */
	public function _initialize()
	{
		// TODO
		parent::_initialize();
	}
	
	/*
	 * 默认方法
	 */
	public function index()
	{
		// TODO
		$this->show("Welcome To LogisticsController.<br />");
	}
	
	/**
	 * 第三方接口统一入口方法
	 * setApiInfo
	 * @return result json
	 * @throws
	 */
	public function setApiInfo()
	{
		$pRequest = urldecode(htmlspecialchars_decode(I('pRequest')));
		$this->setSysLog('PostInSuccess', 'API', 'setApiInfo', $pRequest);
		$jsonRequest = json_decode($pRequest);
		
		//操作类型代码
		$otype = $jsonRequest->otype;
		//接入标示
		$apikey = $jsonRequest->apikey;
		//传入参数
		$input = $jsonRequest->input;
		
		if($otype == null || $otype == '' || $apikey == null || $apikey == '' || $input == null || $input == '') {
			$this->retMsg(0, 301);
		}
		else if($this->validateApikey($apikey) == false) {
			$this->retMsg(0, 302);
		}
		else {
			$companyid = $this->getCompanyidByApikey($apikey);
			
			switch ($otype)
			{
				//订单信息接收操作
				case '2001':
					$ret = $this->setWebOrderInfo($input);
					if($ret) {
						$info = array();
						$this->retMsg(1, $info);
					}
					else {
						$info = array(
								'msg' => '新增订单信息失败',
						);
						$this->retMsg(0, $info);
					}
					break;
				//手动获取密码操作
				case '2002':
					$ret = $this->getOrderPassword($input);
					if($ret) {
						$this->retMsg(1, $ret);
					}
					else {
						$info = array(
								'msg' => '获取密码信息失败',
						);
						$this->retMsg(0, $info);
					}
					break;
				//用户验证反馈操作
				case '2003':
					$ret = $this->setValidInfo($input);
					if($ret) {
						$info = array();
						$this->retMsg(1, $info);
					}
					else {
						$info = array(
								'msg' => '新增用户验证信息失败',
						);
						$this->retMsg(0, $info);
					}
					break;
				//获取终端信息
				case '2004':
					$ret = $this->getClientInfo($companyid);
					if($ret) {
						$info = array();
						$this->retMsg(1, $info);
					}
					else {
						$info = array(
								'msg' => '获取终端信息失败',
						);
						$this->retMsg(0, $info);
					}
					break;
				default:
					$this->retMsg(0, 303);
			}
		}
	}
	
	/**
	 * 第三方订单信息添加
	 * setOrderInfo
	 * @return return_type
	 * @throws
	 */
	public function setWebOrderInfo($input)
	{
		$orders = '';
		if($input->barcode != null) {
			$orders = $input->barcode;
		}
		$clientid = $this->csnToClienId($input->terminalSN);
		
		$model = M('LogisticsOrder');
		$data = $model->where("orders = '$orders' and clientid = '$clientid'")->find();
		if($data) {
			if($data[status] == '2') {
				$model->id = $data['id'];
				$model->updateuser = 'setWebOrderInfo';
				$model->updatetime = time();
				$model->status = 2;
				$model->clientid = $clientid;
				$model->customname = $input->customName;
				$model->telphone = $input->mobile;
				$model->membernumber = $input->memberNumber;
				$model->citizennumber = $input->citizenNumber;
				$model->orders = $orders;
				$retInfo = $model->save();
			}
			else {
				$retInfo = false;
			}
		}
		else {
			$model->createuser = 'setWebOrderInfo';
			$model->createtime = time();
			$model->status = 2;
			$model->clientid = $clientid;
			$model->customname = $input->customName;
			$model->telphone = $input->mobile;
			$model->membernumber = $input->memberNumber;
			$model->citizennumber = $input->citizenNumber;
			$model->orders = $orders;
			$retInfo = $model->add();
		}
		
		if($retInfo) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public function getOrderPassword($input)
	{
		$orders = '';
		if($input->barcode != null) {
			$orders = $input->barcode;
		}
		$clientid = $this->csnToClienId($input->terminalSN);
		
		$model = M('LogisticsOrder');
		$data = $model->where("clientid = '$clientid' and orders = '$orders' and type = 2")->find();
		if($data) {
			$retData = array(
					'password' => $data['password'],
					'created' => $data['inputtime'],
			);
			return $retData;
		}
		else {
			return false;
		}
	}
	
	/**
	 * 获取反馈的会员验证信息
	 * setValidInfo
	 * @param unknown $input_info
	 * @return boolean
	 */
	public function setValidInfo($input)
	{
		$model = M('LogisticsVipvalid');
		$model->createuser = 'setValidInfo';
		$model->createtime = time();
		$model->status = 0;
		$model->valid = $input->Valid;
		$model->token = $input->Token;
		$model->vipid = $input->VipID;
		$model->customerid = $input->CustomerID;
		$id = $model->add();
		if($id) {
			return true;
		}
		else {
			return false;
		}
	}
	
	/**
	 * 获取物流公司信息
	 * getCourierInfo
	 * @return return_type
	 * @throws
	 */
	public function getCourierInfo($companyid)
	{
		$model = M('LogisticsCourier');
		$data = $model->field('id, name, short, code')->where("companyid = '$companyid' and status = 1")->select();
		if($data) {
			return $data;
		}
		else {
			return false;
		}
	}
	
	/**
	 * 获取柜子终端信息
	 * getClientInfo
	 * @return return_type
	 * @throws
	 */
	public function getClientInfo($companyid)
	{
		$model = new Model();
		$sql = "select
				 a.csn
				,a.name
				,a.position
				,a.longitude
				,a.latitude
				from wf_sys_client a, wf_sys_companyclient b
				where a.id = b.clientid
				and a.status = 1
				and b.companyid = '$companyid'";
		$data = $model->query($sql);
		if($data) {
			return $data;
		}
		else {
			return false;
		}
	}
}
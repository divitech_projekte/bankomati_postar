<?php
namespace Api\Controller;

use Think\Model;

/**
 * 充电柜第三方调用接口控制器类
 * Api\Controller$ChargeController
 * Copyright (c) 2016. 云绅（张家港）精密工业有限公司 版权所有
 * 创建人：徐辰辰
 * 创建时间：2016-9-24 上午10:27:22
 */
class ChargeController extends BaseController
{
	
	/*
	 * 充电柜控制器类架构函数
	 */
	public function _initialize()
	{
		// TODO
		parent::_initialize();
	}
	
	/*
	 * 默认方法
	 */
	public function index()
	{
		// TODO
		$this->show("Welcome To ChargeController.<br />");
	}
	
	/**
	 * 第三方接口统一入口方法
	 * setApiInfo
	 * @return result json
	 * @throws
	 */
	public function setApiInfo()
	{
		$pRequest = urldecode(htmlspecialchars_decode(I('pRequest')));
		$this->setSysLog('ChargeController', 'API', 'setApiInfo', $pRequest);
		$jsonRequest = json_decode($pRequest);
		
		//操作类型代码
		$otype = $jsonRequest->otype;
		//接入标示
		$apikey = $jsonRequest->apikey;
		//传入参数
		$input = $jsonRequest->input;
		
		if($otype == null || $otype == '' || $apikey == null || $apikey == '' || $input == null || $input == '') {
			$this->retMsg(0, 301);
		}
		else if($this->validateApikey($apikey) == false) {
			$this->retMsg(0, 302);
		}
		else {
			$companyid = $this->getCompanyidByApikey($apikey);
			
			switch ($otype)
			{
				//生成充电订单信息
				case '1001':
					$ret = $this->setChargeOrderInfo($input);
					if($ret) {
						$info = array(
								'msg' => 'Charging order created successfully.',
						);
						$this->retMsg(1, $info);
					}
					else {
						$info = array(
								'msg' => 'Charging order created unsuccessfully.',
						);
						$this->retMsg(0, $info);
					}
					break;
				//任务完成反馈是否允许充电
				case '1002':
					$ret = $this->allowCharging($input);
					if($ret) {
						$info = array(
								'msg' => 'Charging Password created successfully.',
						);
						$this->retMsg(1, $info);
					}
					else {
						$info = array(
								'msg' => 'Charging Password created unsuccessfully.',
						);
						$this->retMsg(0, $info);
					}
					break;
				default:
					$this->retMsg(0, 303);
			}
		}
	}
	
	/**
	 * 生成充电订单信息
	 * setChargeOrderInfo
	 * @return return_type
	 * @throws
	 */
	public function setChargeOrderInfo($input)
	{
		$mobile = $input->mobile;
		$content = $input->content;
		$sendtime = $input->sendtime;
		
		$this->setSysLog('ChargeController', 'API', 'setChargeOrderInfo', $mobile . '|' . $content . '|' . $sendtime);
		if($mobile != null && $content != null && $sendtime != null) {
			$retMsg = true;
			
			$clientNo = substr($content, 1, 3);
			$boxNo = substr($content, 5, 2);
			
			$clientInfo = $this->clientNoToClientInfoForGoCharge($clientNo);
			if($clientInfo == false) {
				/*美国GoMo短信平台
				 $retObj = getSession();
				 $retObjSMS = sendSMS_GoMo($retObj['sessionid'], $mobile, $info);
				 */
				/*信海云短信平台*/
				$retObjSMS = sendSMS_SynHey($mobile, 'the station number: ' . $clientNo . ' is error, please try it again.');
				$retMsg = false;
			}
			else {
				$intBoxNo = (int)$boxNo;
				$boxId = $this->boxnoToId($clientInfo['id'], $intBoxNo);
				if($boxId == false) {
					/*美国GoMo短信平台
					 $retObj = getSession();
					 $retObjSMS = sendSMS_GoMo($retObj['sessionid'], $mobile, $info);
					 */
					/*信海云短信平台*/
					$retObjSMS = sendSMS_SynHey($mobile, 'the locker number: ' . $boxNo . ' is error, please try it again.');
					$retMsg = false;
				}
				else {
					$orderno = $clientInfo['token'] . time();
					
					$model = M("ChargeOrder");
					$model->createuser = 'setChargeOrderInfo';
					$model->createtime = time();
					$model->orderno = $orderno;
					$model->status = 1;
					$model->smscontent = $content;
					$model->smstime = $sendtime;
					$model->telphone = $mobile;
					$model->clientid = $clientInfo['id'];
					$model->boxid = $boxId;
					$model->add();
					
					/*美国GoMo短信平台
					 $retObj = getSession();
					 $retObjSMS = sendSMS_GoMo($retObj['sessionid'], $mobile, $info);
					 */
					/*信海云短信平台*/
					$retObjSMS = sendSMS_SynHey($mobile, 'Please click the following link to answer a brief survey to obtain free mobile charging.' . $clientInfo['beforecharge'] . '?orderno=' . $orderno);
				}
			}
			return $retMsg;
		}
		else {
			return false;
		}
	}
	
	/**
	 * 任务完成反馈是否允许充电
	 * @param unknown $input
	 * @return boolean
	 */
	public function allowCharging($input)
	{
		$orderNo = $input->orderno;
		$feedbackstatus = $input->status;
		$feedbacktime = $input->time;
		$feedbackmsg = $input->msg;
		
		$this->setSysLog('ChargeController', 'API', 'allowCharging', $orderNo . '|' . $feedbackstatus . '|' . $feedbacktime . '|' . $feedbackmsg);
		switch ($feedbackstatus)
		{
			case '1':
				//允许充电
				$model = M("ChargeOrder");
				$data = $model->where("orderno = '$orderNo'")->find();
				if($data) {
					$token = $this->clientidToToken($data['clientid']);
					$boxno = $this->boxidToNo($data['boxid']);
					$pwd = rand(100000,999999);
					$param = array(
							'tel' => $data['telphone'],
							'pwd' => $pwd . '',
							'orderno' => $data['orderno'],
					);
					$info = json_encode($param);
					//下发充电终端
					$commandid = $this->sendCommand($token, '416', $boxno, $info);
					
					//发送充电密码
					/*美国GoMo短信平台
					 $retObj = getSession();
					 $retObjSMS = sendSMS_GoMo($retObj['sessionid'], $mobile, $info);
					 */
					/*信海云短信平台*/
					$retObjSMS = sendSMS_SynHey($data['telphone'], 'Thanks for completing survey, your charging password is ' . $pwd);
					
					//更新订单信息
					$model->updateuser = 'allowCharging';
					$model->updatetime = time();
					$model->feedbackstatus = $feedbackstatus;
					$model->feedbacktime = $feedbacktime;
					$model->feedbackmsg = $feedbackmsg;
					$model->commandid = $commandid;
					$model->password = $pwd;
					$model->where("orderno = '$orderNo'")->save();
					
					return true;
				}
				else {
					return false;
				}
				break;
			case '0':
				//不允许充电
				return false;
				break;
			default:
				return false;
		}
	}
	
	/**
	 * GoCharge终端号转clientid
	 * @param 终端号码 $clientNo
	 * @return number
	 */
	public function clientNoToClientInfoForGoCharge($clientNo)
	{
		$model = new Model();
		$sql = "select c.id, c.csn, c.token, substr(c.csn, locate('-', c.csn) + 1) as clientno, d.beforecharge, d.aftercharge
				from wf_sys_company a, wf_sys_companyclient b, wf_sys_client c, wf_sys_clientconfig d
				where a.id = b.companyid
				and b.clientid = c.id
				and c.id = d.clientid
				and a.code = 'GoCharge'
				and substr(c.csn, locate('-', c.csn) + 1) = $clientNo";
		$data = $model->query($sql);
		if($data) {
			return $data[0];
		}
		else {
			return false;
		}
	}
}
<?php
namespace Api\Controller;

/**
 * 第三方调用接口模块默认访问控制器类
 * Api\Controller$IndexController
 * Copyright (c) 2016. 云绅（张家港）精密工业有限公司 版权所有
 * 创建人：徐辰辰
 * 创建时间：2016-7-13 下午02:10:50
 */
class IndexController extends BaseController
{
    
	/*
	 * 控制器类架构函数
	 */
	public function _initialize()
	{
		// TODO
		parent::_initialize();
	}
	
	/*
	 * 默认方法
	 */
	public function index()
	{
		// TODO
		$this->show("welcome to use winnsen api module.<br />");
		$this->show("<br />错误代码：<br />");
		$this->show("301: 接口参数错误.<br />");
		$this->show("302: 接口标示不存在.<br />");
		$this->show("303: 操作类型不存在.<br />");
		$this->show("<br />操作类型：<br />");
		$this->show("1001: 生成充电订单信息.<br />");
		$this->show("1002: 充电是否允许接口.<br />");
		$this->show("2001: 订单信息接收操作.<br />");
		$this->show("2002: 手动获取密码操作.<br />");
		$this->show("2003: 用户验证反馈操作.<br />");
		$this->show("2004: 获取柜子终端信息.<br />");
	}
	
	/**
	 * 网购模式物流柜测试
	 */
	public function ttxTest()
	{
		$barcode = I('get.barcode');
		$terminalSN = I('get.terminalSN');
		$mobile = I('get.mobile');
		$memberNumber = I('get.memberNumber');
		$citizenNumber = I('get.citizenNumber');
		$customName = I('get.customName');
		/*
		$barcode = '20161012008';
		$terminalSN = 'YS001-01';
		$mobile = '13706228602';
		$memberNumber = '123456';
		$citizenNumber = '123;456;789';
		$customName = '王磊';
		*/
		if($barcode != null && $terminalSN != null && $mobile != null && $memberNumber != null 
				&& $citizenNumber != null && $customName != null) {
			$url= "http://52.78.161.139/wf/LogisticsApi";
		
			$input = array(
					'barcode' => $barcode,
					'terminalSN' => $terminalSN,
					'mobile' => $mobile,
					'memberNumber' => $memberNumber,
					'citizenNumber' => $citizenNumber,
					'customName' => $customName,
			);
		
			$pRequest = array(
					'otype' => '2001',
					'apikey' => 'f5e4a89a318b4ca88c60336f0e67e0dc',
					'input' => $input,
			);
		
			$pRequest = json_encode($pRequest);
			$param = array(
					'pRequest' => urlencode($pRequest)
			);
			$json = $this->send_post($url, $param);
			dump($json);
		}
		else {
			dump('error.');
		}
	
		
		
		/*
		$url= "http://52.78.161.139/wf/LogisticsApi";
		
		$input = array(
				'barcode' => '20161009004',
				'terminalSN' => 'YS20160914001-01',
		);
		
		$pRequest = array(
				'otype' => '2002',
				'apikey' => 'f5e4a89a318b4ca88c60336f0e67e0dc',
				'input' => $input,
		);
		
		$pRequest = json_encode($pRequest);
		$param = array(
				'pRequest' => urlencode($pRequest)
		);
		$json = $this->send_post($url, $param);
		
		dump($json);
		*/
	}
	
	/**
	 * 洗衣柜测试
	 */
	public function niceTest()
	{
		//上门取件订单反馈接口
		
		$url= "http://52.78.161.139/wf/LogisticsApi";
		
		$input = array(
				'barcode' => '20161009004',
				'terminalSN' => 'YS20160914001-01',
				'mobile' => '13706228705',
				'memberNumber' => '123456',
				'citizenNumber' => '111111;222222',
				'customName' => 'John Liu',
		);
		
		$pRequest = array(
				'otype' => '2001',
				'apikey' => 'f5e4a89a318b4ca88c60336f0e67e0dc',
				'input' => $input,
		);
		
		$pRequest = json_encode($pRequest);
		$param = array(
				'pRequest' => urlencode($pRequest)
		);
		$json = $this->send_post($url, $param);
		
		dump($json);
		
		//系统体外验证信息反馈
		/*
		$url= "http://52.78.161.139/wf/LogisticsApi";
		
		$input = array(
				'Valid' => '1',
				'Token' => 'YS20160914001-01',
				'VipID' => '12322121',
		);
		
		$pRequest = array(
				'otype' => '2003',
				'apikey' => 'f5e4a89a318b4ca88c60336f0e67e0dc',
				'input' => $input,
		);
		
		$pRequest = json_encode($pRequest);
		$param = array(
				'pRequest' => urlencode($pRequest)
		);
		$json = $this->send_post($url, $param);
		
		dump($json);
		*/
		
		$url= "http://52.78.161.139/wf/LogisticsApi";
		
		$input = array(
				
		);
		
		$pRequest = array(
				'otype' => '2004',
				'apikey' => 'f5e4a89a318b4ca88c60336f0e67e0dc',
				'input' => $input,
		);
		
		$pRequest = json_encode($pRequest);
		$param = array(
				'pRequest' => urlencode($pRequest)
		);
		$json = $this->send_post($url, $param);
		
		dump($json);
	}
	
	/**
	 * 美国问卷调查充电柜测试
	 */
	public function chargeTest()
	{
		/*
		$url= "http://52.78.161.139/wf/ChargingApi";
		
		$input = array(
				'mobile' => '13706228602',
				'content' => '#001#04*',
				'sendtime' => time(),
		);
		
		$pRequest = array(
				'otype' => '1001',
				'apikey' => 'a05c930364526c86f9ae23aa193fb598',
				'input' => $input,
		);
		
		$pRequest = json_encode($pRequest);
		$param = array(
				'pRequest' => urlencode($pRequest)
		);
		$json = $this->send_post($url, $param);
		
		dump($json);
		*/
		$session = $this->getSession();
		echo $session;
		if($session != '') {
			$mobile = '6463355307';
			$message = 'winnsen sms test. please check http://www.winnsen.com';
			$id = $this->sendSMS($session, $mobile, $message);
			echo $session;
			if($id != '') {
				echo 'sendSMS is success.';
			}
			else {
				echo 'sendSMS is error.';
			}
		}
		else {
			echo 'getSession is error.';
		}
	}
	
	public function getSession()
	{
		$url = "http://goldgroup.gomostratus.com/campaign/3.4/api";
		$param = array(
				'action' => 'getSession',
				'username' => 'goCharge',
				'password' => 'W0rldT3xt!',
		);
		$json = $this->send_post($url, $param);
		$obj = json_decode($json);
		if($obj != null && $obj->status == 'OK') {
			return $obj->sessionid;
		}
		else {
			return '';
		}
	}
	
	public function sendSMS($session, $mobile, $message)
	{
		$url = "http://goldgroup.gomostratus.com/campaign/3.4/api";
		$param = array(
				'action' => 'sendSMS',
				'sessionid' => $session,
				'mobile' => $mobile,
				'message' => $message,
				'cid' => '613',
		);
		$json = $this->send_post($url, $param);
		$obj = json_decode($json);
		if($obj != null && $obj->status == 'OK') {
			return $obj->id;
		}
		else {
			return '';
		}
	}
	
	/**
	 * 订单状态变更
	 */
	public function setOrderState()
	{
		$barcode = I('post.barcode', '');
		$status = I('post.status', '');
		$lockerID = I('post.lockerID', '');
		$terminalSN = I('post.terminalSN', '');
		$password = I('post.password', '');
		$created = I('post.created', '');
		$method = I('post.method', '');
		$cardNumber = I('post.cardNumber', '');
		
		$info = $barcode . '|' . $status . '|' . $lockerID . '|' . $terminalSN . '|' . $password . '|' . $created
		. '|' . $method . '|' . $cardNumber;
		
		$this->setSysLog('IndexTest', 'API', 'setOrderStatus', $info);
		
		$retInfo = array(
				'st' => '1',
				'info' => ''
		);
		$this->ajaxReturn($retInfo);
	}
	
	/**
	 * 断线预警
	 */
	public function offlineWarning()
	{
		$terminalSN = I('post.terminalSN', '');
		$onlinetime = I('post.onlinetime', '');
		
		$info = $terminalSN . '|' . $onlinetime;
		
		$this->setSysLog('IndexTest', 'API', 'offlineWarning', $info);
		
		$retInfo = array(
				'st' => '1',
				'info' => ''
		);
		$this->ajaxReturn($retInfo);
	}
	
	/**
	 * 开发测试类方法
	 */
	public function test()
	{
		//$date = substr(date("Ymd ", time()), 0, 8);
		$date = '20161009';
		$maxOrders = $this->getMaxFieldByStr('LogisticsOrder', 'orders', $date, 1);
		if($maxOrders) {
			echo $maxOrders + 1;
		}
		else {
			echo $date . '001';
		}
	}
	
	public function getMaxFieldByStr($table, $field, $value, $start)
	{
		$len = strlen($value);
		$model = M($table);
		$maxData = $model->where("substr($field, $start, $len) = '$value'")->max($field);
		
		if($maxData) {
			return $maxData;
		}
		else {
			return false;
		}
	}
	
}
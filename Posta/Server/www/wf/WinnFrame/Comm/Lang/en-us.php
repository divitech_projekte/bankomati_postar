<?php
return array(
		
		'the_user_into_the_item'=>'Klienti vendosi paketen ne sirtarin ',
		'the_item_after_info'=>'.',
		'courier_romove_out_before'=>'Postieri terhoqi paketen nga sirtari ',
		'courier_romove_out_after'=>'.',
		'courier_put_into_before'=>'Postieri vendosi paketen ne sirtarin ',
		'courier_put_into_after'=>'.',
		'user_romove_out_before'=>'Klienti terhoqi paketen nga sirtari ',
		'user_romove_out_after'=>'.',
		'courier_return_step'=>' (Kthim ne Magazine).',
		
		'user_put_into_before'=>'Klienti vendosi paketen ne sirtarin ',
		'user_put_into_after'=>'.',
		'supplier_romove_out_before'=>'Furnitori terhoqi paketen nga sirtari ',
		'supplier_romove_out_after'=>'.',
		'supplier_put_into_before'=>'Furnitori vendosi paketen ne sirtarin ',
		'supplier_put_into_after'=>'.',
		'user_romove_out_before'=>'Klienti terhoqi paketen nga sirtari ',
		'user_romove_out_after'=>'.',
);
<?php
namespace Comm\Controller;

use Think\Model;

/**
 * 充电柜控制器类
 * Comm\Controller$ChargeController
 * Copyright (c) 2016. 云绅（张家港）精密工业有限公司 版权所有
 * 创建人：徐辰辰
 * 创建时间：2016-9-27 上午10:27:22
 */
class ChargeController extends BaseController
{
	/*
	 * 物流柜控制器类架构函数
	 */
	public function _initialize()
	{
		// TODO
		parent::_initialize();
	}
	
	
	public function setChargeStatus()
	{
		$token = I('get.token', '');
		if($token == '') {
			$token = '0';
		}
		
		$orderno = I('get.orderno', '');
		if($orderno == '') {
			$orderno = '0';
		}
		
		$tid = I('get.tid', '');
		if($tid == '') {
			$tid = '0';
		}
		
		$chargestate = I('get.chargestate', '');
		
		$time = I('get.time', '');
		
		if($token == '0' || $orderno == '0' || $tid == '0' || $chargestate == '' || $time == '') {
			//参数信息错误
			$this->retMsg(0, 431);
		}
		else if($this->validateToken($token)) {
			//客户端异常
			$this->retMsg(0, 425);
		}
		else if($this->validateTidForChargeOrder($tid)) {
			//操作记录流水号存在
			$this->retMsg(1, 202);
		}
		else {
			$company = $this->tokenToCompanyInfo($token);
			$clientConfig = $this->tokenToClientConfig($token);
			
			$model = M("ChargeOrder");
			$data = $model->where("status = 1 and orderno = '$orderno'")->find();
			if($data) {
				$orderId = $data['id'];
				switch ($chargestate)
				{
					case '1':
						//开始充电
						$model->id = $orderId;
						$model->updateuser = 'setChargeStatus';
						$model->updatetime = time();
						$model->tid = $tid;
						$model->starttime = $time;
						$model->save();
						
						$this->retMsg(1, 200);
						break;
					case '2':
						//充电完成
						$model->id = $orderId;
						$model->updateuser = 'setChargeStatus';
						$model->updatetime = time();
						$model->status = '0';
						$model->tid = $tid;
						$model->endtime = $time;
						$model->save();
						
						//发送第二份问卷
						/*美国GoMo短信平台
						 $retObj = getSession();
						 $retObjSMS = sendSMS_GoMo($retObj['sessionid'], $mobile, $info);
						 */
						/*信海云短信平台*/
						$retObjSMS = sendSMS_SynHey($data['telphone'], 'Thank you for using goCharge! To receive more great offers, please click the following link for a brief survey.' . $clientConfig['aftercharge']);
						
						$this->retMsg(1, 200);
						break;
					default:
						$this->setSysLog('commSystem', 'ERROR', 'setChargeStatus', 'Chargestate Is Error.');
						$this->retMsg(0, 400);
				}
				
			}
			else {
				//进行中的订单信息不存在
				$this->retMsg(0, 432);
			}
		}
	}
	
	/**
	 * 发送短信
	 * @param 模板编号 $code
	 * @param 公司id $companyId
	 * @param 条件参数 $param
	 * @param 手机号码 $mobile
	 * @return void|boolean
	 */
	public function sendByStencil($code, $companyId, $param, $mobile)
	{
		$this->setSysLog('commSystem', 'INFO', 'sendByStencil', $code . '|' . $companyId);
		
		$model = M('SysSmsstencil');
		$data = $model->where("code = '$code' and status = 1 and companyid = '$companyId'")->find();
		if($data) {
			$info = $data['info'];
			
			$ispost = true;
			$num = preg_match_all('/{%[^%]+%}/', $info, $match);
			foreach ($match[0] as $value)
			{
				$val = substr($value, 2, strlen($value)-4);
				if(isset($param[$val])){
					$info = str_replace($value, $param[$val], $info);
				}
				else{
					$ispost = false;
					return;
				}
			}
			$this->setSysLog('commSystem', 'INFO', 'sendByStencil', $info);
			
			if($ispost) {
				/*美国GoMo短信平台
				$retObj = getSession();
				$retObjSMS = sendSMS_GoMo($retObj['sessionid'], $mobile, $info);
				*/
				/*信海云短信平台*/
				$retObjSMS = sendSMS_SynHey($mobile, $info);
				
				$this->setSysLog('commSystem', 'INFO', 'sendByStencil', $retObjSMS);
				return true;
			}
			else {
				return false;
			}
		}
		else {
			return false;
		}
	}
	
	/**
	 * 验证操作记录流水号
	 * @param 流水号 $tid
	 * @return boolean
	 */
	public function validateTidForChargeOrder($tid)
	{
		$model = M("ChargeOrder");
		$data = $model->where("tid='$tid'")->find();
		if($data) {
			return true;
		}
		else {
			return false;
		}
	}
}
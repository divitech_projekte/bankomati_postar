<?php
namespace Comm\Controller;

use Think\Model;

/**
 * 牛奶机控制器类
 * Comm\Controller$VendingController
 * Copyright (c) 2016. 云绅（张家港）精密工业有限公司 版权所有
 * 修改人：xucc
 * 修改时间：2016-3-16 上午14:11:45
 * 修改内容：
 */
class VendingController extends BaseController
{
	/*
	 * 牛奶机控制器类架构函数
	 */
	public function _initialize()
	{
		// TODO
		parent::_initialize();
	}
	
	/**
	 * 近效期数据获取
	 * Enter description here ...
	 */
	public function getEffectPeriod()
	{
		$token = I('get.token', '');
		if($token == '') {
			$token = '0';
		}
		
		$model = new Model();
		$sql = "select c.id, ifnull(d.nearepcount, 0) nearepcount
				from
				(select b.id
				 from wf_sys_client a, wf_sys_box b
				 where a.id = b.clientid
				 and a.token = $token) c
				left join
				(select boxid, sum(case when unix_timestamp(now()) > duodate then 1 else 0 end) as nearepcount
				 from wf_vending_storage
				 where status = 1
				 group by boxid) d
				on c.id = d.boxid";
		$data = $model->query($sql);
		if($data) {
			$ret = array();
			$i = 0;
			foreach($data as $row) {
				$ret[$i] = (int)$row['nearepcount'];
				$i++;
			}
			$this->retMsg(1, json_encode($ret));
		}
		else {
	 		$this->retMsg(0, 421);
		}
	}
	
	/**
	 * 上下货完成上报
	 * Enter description here ...
	 */
	public function putEndOperation()
	{
		$token = I('get.token', '');
		if($token == '') {
			$token = '0';
		}
		$cardno = I('get.cardno', '');
		
		$clientid = $this->tokenToId($token);
		
		$tempModel = new Model();
		$sql = "select a.boxid, d.no, b.productid, c.shelflife, a.id as listid, a.takedown, a.amount
				from wf_vending_consignment a, wf_sys_boxconfig b, wf_vending_product c, wf_sys_box d
				where a.boxid = b.boxid
				and b.productid = c.id
				and b.boxid = d.id
				and a.status = 1
				and a.clientid = $clientid
				order by d.no";
		
		$data = $tempModel->query($sql);
		if($data) {
			
			$ontime = time();
			
			$takedown = array();
			$amount = array();
			$i = 0;
			
			foreach($data as $row) {
				$boxid = $row['boxid'];
				$listid = $row['listid'];
				$takedownCount = (int)$row['takedown'];              //下架数量
				$amountCount = (int)$row['amount'];                  //上货数量
				$diff = (int)$row['shelflife'] - 1;
				
				$takedown[$i] = $takedownCount;
				$amount[$i] = $amountCount;
				
				$model = M('VendingStorage');
				$stockData = $model->where("status = 1 and boxid = $boxid")->order('duodate')->select();
				$stockCount = count($stockData);                     //原库存数量
				
				//更新库存数据取出
				for($j = 0; $j < $takedownCount; $j++) {
					//防止数组溢出
					if($j < $stockCount) {
						$id = $stockData[$j]['id'];
						$model->updateuser = $cardno;
						$model->updatetime = time();
						$model->status = 4;
						$model->where("id=$id")->save();
					}
				}
				//更新库存数据放入
				for($j = 0; $j < $amountCount; $j++) {
					$model->createuser = $cardno;
					$model->createtime = time();
					$model->boxid = $boxid;
					$model->productid = $row['productid'];
					$model->status = 1;
					$model->duodate = strtotime(date("Y-m-d", strtotime($diff . " day")));
					$model->listid = $listid;
					$model->ontime = $ontime;
					$model->add();
				}
				//更新上下货清单状态
				$model = M('VendingConsignment');
				$model->updateuser = $cardno;
				$model->updatetime = time();
				$model->status = 2;
				$model->where("id=$listid")->save();
				//添加上下货清单记录
				$model = M('SysBoxrecord');
				$model->createuser = $cardno;
				$model->createtime = time();
				$model->boxid = $boxid;
				$model->operateuser = $cardno;
				$model->operatetype = 29;
				$model->operatetime = $ontime;
				$model->remark = '原库存：' . $stockCount . '件，本次下货：' . $takedownCount . '件，本次上货：' . $amountCount . '件';
				$model->add();
				
				$i++;
			}
			$data = array('st'=>1, 'takedown'=>$takedown, 'amount'=>$amount);
			$this->ajaxReturn($data);
		}
		else {
			$this->retMsg(0, 422);
		}
	}
	
	/**
	 * 刷卡验证及消费接口
	 * Enter description here ...
	 */
	public function setMilkCard()
	{
		$token = I('get.token', '');
		if($token == '') {
			$token = '0';
		}
		
		$cardno = I('get.cardno', '');
		if($cardno == '') {
			$cardno = '0';
		}
		
		$wo = I('get.wo', '');
		if($wo == '') {
			$wo = '0';
		}
		
		$orderno = I('get.orderno', '');
		if($orderno == '') {
			$orderno = '0';
		}
		
		$money = I('get.money', '');
		if($money == '') {
			$money = '0';
		}
		
		if($token == '0' || $cardno == '0') {
			//参数信息错误
			$this->retMsg(0, 423);
		}
		else if($this->validateToken($token)) {
			//客户端异常
			$this->retMsg(0, 425);
		}
		else {
			$model = M('VendingConsumer');
			$data = $model->where("cardno='$cardno' and status=1")->find();
			if($data) {
				$type = $data['type'];
				
				if($type == 1) {
					//验证为会员
					if($orderno == '0' || $wo == '0' || $money == '0') {
						//参数信息错误
						$this->retMsg(0, 423);
					}
					else if($this->validateField('VendingConsumerrecord', 'tid', $wo)) {
						//流水号存在
						$this->retMsg(1, 202);
					}
					else {
						//流水号不存在
						$balance = $data['balance'] - $money;
						if($balance < 0) {
							//余额不足
							$this->retMsg(0, 427);
						}
						else {
							//更新卡片余额
							$model = M('VendingConsumer');
							$model->updateuser = $cardno;
							$model->updatetime = time();
							$model->balance = $balance;
							$model->save();
							
							//插入刷卡记录
							$model = M('VendingConsumerrecord');
							$model->createuser = $cardno;
							$model->createtime = time();
							$model->cardno = $cardno;
							$model->orderno = $orderno;
							$model->type = 4;
							$model->tid = $wo;
							$model->amountchange = $money;
							$model->remark = $token;
							$model->add();
							
							$jsonVal = array('st'=>1, 'info'=>$balance);
							$this->ajaxReturn($jsonVal);
						}
					}
				}
				else {
					//验证为送货员
					
					//插入刷卡记录
					$model = M('VendingConsumerrecord');
					$model->createuser = $cardno;
					$model->createtime = time();
					$model->cardno = $cardno;
					$model->type = 5;
					$model->remark = '终端（' . $token . '）送货刷卡';
					$model->add();
					
					$this->retMsg(1, 201);
				}
			}
			else {
				//数据不存在
				$this->retMsg(0, 424);
			}
		}
	}
	
	/**
	 * 销售数据上报
	 * Enter description here ...
	 */
	public function putSalesData()
	{
		$token = I('get.token', '');
		if($token == '') {
			$token = '0';
		}
		
		$wo = I('get.wo', '');
		if($wo == '') {
			$wo = '0';
		}
		
		$orderno = I('get.orderno', '');
		if($orderno == '') {
			$orderno = '0';
		}
		
		$type = I('get.type', '');
		if($type == '') {
			$type = '0';
		}
		
		$money = I('get.money', '');
		if($money == '') {
			$money = '0';
		}
		
		//双引号转译
		$jsonstring = str_replace('&quot;', '"', I('get.data', ''));
		$jsonstring = str_replace('\\', '', $jsonstring);
		$dataJson = json_decode($jsonstring, true);
		
		if($token == '0' || $wo == '0' || $orderno == '0' || $type == '0' || $money == '0' || $jsonstring == '') {
			//参数信息错误
			$this->retMsg(0, 423);
		}
		else if($this->validateToken($token)) {
			//客户端异常
			$this->retMsg(0, 425);
		}
		else {
			if($this->validateField('VendingOrder', 'tid', $wo)) {
				//流水号存在
				$this->retMsg(1, 202);
			}
			else {
				//流水号不存在
				
				$dataBn = $dataJson['bn'];         //货道号码
				$dataSn = $dataJson['sn'];         //每个货道的销售数量
				
				$clientid = $this->tokenToId($token);
				
				//新增订单信息
				$model = M('VendingOrder');
				$model->createuser = $token;
				$model->createtime = time();
				$model->status = 1;
				$model->orderno = $orderno;
				$model->storeid = 1;
				$model->clientid = $clientid;
				$model->tid = $wo;
				$model->type = 1;
				$model->paytype = $type;
				$model->paymoney = $money;
				$orderid = $model->add();
				
				//是否操作成功
				$isvalid = true;
				
				//更新库存数据
				for($i = 0; $i < count($dataBn); $i++) {
					$boxno = $dataBn[$i];
					$modelBox = M('SysBox');
					$dataBox = $modelBox->where("clientid=$clientid and no=$boxno and status=1")->find();
					if($dataBox) {
						$boxid = $dataBox['id'];
						$modelStorage = M('VendingStorage');
						for($j = 0; $j < (int)$dataSn[$i]; $j++) {
							$data = $modelStorage->where("boxid=$boxid and status=1")->order("duodate")->find();
							if($data) {
								//执行更新
								$modelStorage->updateuser = $token;
								$modelStorage->updatetime = time();
								$modelStorage->id = $data['id'];
								$modelStorage->status = 2;
								$modelStorage->orderid = $orderid;
								$modelStorage->saletime = time();
								$modelStorage->save();
							}
							else {
								$isvalid = false;
								break;
							}
						}
						
					}
					else {
						$isvalid = false;
						break;
					}
				}
				
				if($isvalid) {
					//数据上报成功
					$this->retMsg(1, 200);
				}
				else {
					//更新库存信息时报错
					$model = M('VendingStorage');
					$model->status = 1;
					$model->orderid = null;
					$model->saletime = null;
					$model->where("orderid=$orderid")->save();
					
					$model = M('VendingOrder');
					$model->where("id=$orderid")->delete();
					
					$this->retMsg(0, 426);
				}
			}
		}
	}
	
	/**
	 * 出库数据上报
	 * Enter description here ...
	 */
	public function putOutStorage()
	{
		$token = I('get.token', '');
		if($token == '') {
			$token = '0';
		}
		
		$orderno = I('get.orderno', '');
		if($orderno == '') {
			$orderno = '0';
		}
		
		//双引号转译
		$jsonstring = str_replace('&quot;', '"', I('get.data', ''));
		$jsonstring = str_replace('\\', '', $jsonstring);
		$data = json_decode($jsonstring, true);
		
		if($token == '0' || $orderno == '0' || $jsonstring == '') {
			//参数信息错误
			$this->retMsg(0, 423);
		}
		else if($this->validateToken($token)) {
			//客户端异常
			$this->retMsg(0, 425);
		}
		else {
			$dataBn = $data['bn'];         //货道号码
			$dataSn = $data['sn'];         //每个货道的销售数量
				
			$clientid = $this->tokenToId($token);
			$orderid = $this->ordernoToId($orderno);

			//是否操作成功
			$isvalid = true;
				
			//更新库存数据
			for($i = 0; $i < count($dataBn); $i++) {
				$boxno = $dataBn[$i];
				$modelBox = M('SysBox');
				$dataBox = $modelBox->where("clientid=$clientid and no=$boxno and status=1")->find();
				if($dataBox) {
					$boxid = $dataBox['id'];
					$modelStorage = M('VendingStorage');
					for($j = 0; $j < (int)$dataSn[$i]; $j++) {
						$data = $modelStorage->where("boxid=$boxid and status=2")->order("duodate")->find();
						if($data) {
							//执行更新
							$modelStorage->updateuser = $token;
							$modelStorage->updatetime = time();
							$modelStorage->id = $data['id'];
							$modelStorage->status = 3;
							$modelStorage->outtime = time();
							$modelStorage->save();
						}
						else {
							$isvalid = false;
							break;
						}
					}
				}
				else {
					$isvalid = false;
					break;
				}
			}
			if($isvalid) {
				//更新订单信息
				$model = M('VendingOrder');
				$model->id = $orderid;
				$model->updateuser = $token;
				$model->updatetime = time();
				$model->status = 0;
				$model->save();
				//数据上报成功
				$this->retMsg(1, 200);
			}
			else {
				//更新库存信息时报错
				$model = M('VendingStorage');
				$model->status = 2;
				$model->outtime = null;
				$model->where("orderid=$orderid")->save();
					
				$this->retMsg(0, 426);
			}
		}
	}
	
	/**
	 * 终端异常上报
	 * Enter description here ...
	 */
	public function putError()
	{
		$this->show('putError');
	}
	
	
	/**
	 * orderno转orderid
	 * ordernoToId
	 * @param orderno $orderno
	 * @return orderid or 0
	 * @throws
	 */
	public function ordernoToId($orderno)
	{
		if($orderno == '') {
			$orderno = '0';
		}
		$model = M('VendingOrder');
		$data = $model->where("orderno=$orderno and status = 1")->find();
		if($data) {
			return $data['id'];
		}
		else {
			return '0';
		}
	}
}
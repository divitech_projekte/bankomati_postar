<?php
namespace Comm\Controller;

/**
 * 客户端注册控制器类
 * Comm\Controller$RegisterController
 * Copyright (c) 2016. 云绅（张家港）精密工业有限公司 版权所有
 * 修改人：xucc
 * 修改时间：2016-3-8 下午02:27:08
 * 修改内容：
 */
class RegisterController extends BaseController
{
	/*
	 * 客户端注册控制器类架构函数
	 */
	public function _initialize()
	{
		// TODO
		parent::_initialize();
	}
    
    /**
     * 客户端注册控制器
     * 客户端注册
     */
    public function clientRegister()
    {
    	//获取get信息
	 	$csn = I("get.csn", "");
	 	$boxNum = I("get.boxnum", "");
	 	
	 	if($csn <> "" && $boxNum <> "") {
	 		$sysClient = M("SysClient");
	 		
	 		//判断序列号是否存在
	 		$condition['csn'] = $csn;
	 		$dataClient = $sysClient->where($condition)->find();
	 		if($dataClient) {
	 			$this->retMsg(0, 403);
	 		}
	 		else {
	 			//不存在新增注册信息
	 			$token = time();
	 			
	 			$data['createuser'] = $token;
	 			$data['createtime'] = time();
	 			$data['sysid'] = C('SYSTEM_ID');
	 			$data['status'] = 4;
	 			$data['csn'] = $csn;
	 			$data['token'] = $token;
	 			$data['boxnum'] = $boxNum;
	 			$res = $sysClient->add($data);
	 			
	 			if($res) {
	 				$this->retMsg(1, $token);
	 			}
	 			else {
	 				$this->retMsg(0, 501);
	 			}
	 		}
	 	}
	 	else {
	 		$this->retMsg(0, 402);
	 	}
    }
    
    /**
     * 客户端注册控制器
     * 客户端注销
     */
    public function clientCancel()
    {
    	//获取get信息
    	$token = I("get.token", "");
    	
    	//根据token判断客户端是否存在
    	$client = $this->havingClient($token);
    	if($client) {
    		//判断是否满足注销条件
    		if($this->cancelCondition($client['id'])) {
    			$sysClient = M("SysClient");
    			$condition['token'] = $token;
    			$sysClient->updateuser = $token;
    			$sysClient->updatetime = time();
    			$sysClient->status = 0;
    			$res = $sysClient->where($condition)->save();
    			
    			if($res) {
    				$this->retMsg(1, 200);
    			}
    			else {
    				$this->retMsg(0, 502);
    			}
    		}
    		else {
    			$this->retMsg(0, 405);
    		}
    	}
    	else {
    		$this->retMsg(0, 404);
    	}
    }
    
    
    
}
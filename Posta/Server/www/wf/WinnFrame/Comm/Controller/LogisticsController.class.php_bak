<?php
namespace Comm\Controller;

use Think\Model;

/**
 * 物流柜控制器类
 * Comm\Controller$LogisticsController
 * Copyright (c) 2016. 云绅（张家港）精密工业有限公司 版权所有
 * 创建人：徐辰辰
 * 创建时间：2016-6-23 上午10:27:22
 */
class LogisticsController extends BaseController
{
	/*
	 * 物流柜控制器类架构函数
	 */
	public function _initialize()
	{
		// TODO
		parent::_initialize();
	}
	
	/**
	 * 短信通用接口
	 * setSmsInfo
	 * @return return_type
	 * @throws
	 */
	public function setSmsInfo()
	{
		$token = I('get.token', '');
		if($token == '') {
			$token = '0';
		}
		
		$tel = I('get.tel', '');
		if($tel == '') {
			$tel = '0';
		}
		
		$mould = I('get.mould', '');
		if($mould == '') {
			$mould = '0';
		}
		
		$tid = I('get.tid', '');
		if($tid == '') {
			$tid = '0';
		}
		
		$orderno = I('get.orderno', '');
		if($orderno == '') {
			$orderno = '0';
		}
		
		$info = I('get.info', ''); //password,boxnum,
		
		//获取公司信息
		$company = $this->tokenToCompanyInfo($token);
		$issms = $company['issms'];
		//判断是否需要发送短信
		if($issms == '1') {
		
			if($token == '0' || $tel == '0' || $mould == '0' || $tid == '0') {
				//参数信息错误
				$this->retMsg(0, 431);
			}
			else if($this->validateToken($token)) {
				//客户端异常
				$this->retMsg(0, 425);
			}
			else if($this->validateTidForSms($tid)) {
				//短信已发送
				$this->retMsg(1, 202);
			}
			else {
				$cc = $company['cc'];
				$companyId = $company['id'];
				//获取模板
				$model = M("SysSmsstencil");
				$data = $model->where("code = '$mould' and status = 1 and type = 1 and sysid = 'logistics' and companyid = '$companyId'")->find();
				if($data) {
					//解析参数
					$param = array();
					$jsonstring = str_replace('&quot;', '"', $info);
					$jsonstring = str_replace('\\', '', $jsonstring);
					$param = json_decode($jsonstring, true);
				
					//token转终端名称及地址
					$modelClient = M('SysClient');
					$dataClient = $modelClient->where("token=$token and status = 1")->find();
					$param['clientname'] = $dataClient['name'];
					$param['clientaddress'] = $dataClient['position'];
					
					$modelOrder = M('LogisticsOrder');
					$dataOrder = $modelOrder->where("orderno = '$orderno'")->find();
					$param['orders'] = $dataOrder['orders'];
					
					$param['orderno'] = $orderno;
					
					//拼接接受短信号码
					$tel = $cc . $tel;
				
					$ret = sendSMS_New($tel, $param, $tid, $orderno, $token, $data['id']);
				
					if($ret == 1) {
						$this->retMsg(1, 203);
					}
					else {
						$this->retMsg(0, '436' . $ret);
					}
				}
				else {
					$this->retMsg(0, 435);
				}
			}
		}
		else {
			$this->retMsg(1, 205);
		}
	}
	
	/**
	 * 订单信息获取接口
	 * getOrderInfo
	 * @return return_type
	 * @throws
	 */
	public function getOrderInfo()
	{
		$token = I('get.token', '');
		if($token == '') {
			$token = '0';
		}
		
		$orders = I('get.orders', '');
		if($orders == '') {
			$orders = '0';
		}
		
		if($token == '0' || $orderno == '0') {
			//参数信息错误
			$this->retMsg(0, 431);
		}
		else if($this->validateToken($token)) {
			//客户端异常
			$this->retMsg(0, 425);
		}
		else {
			$clientId = $this->tokenToId($token);
			$model = M("LogisticsOrder");
			$data = $model->where("orders = '$orders' and clientid = '$clientId'")->find();
			if($data) {
				$info = array(	'id'=>$data['id'],
								'status'=>$data['status'],
								'orderno'=>$data['orderno'],
								'courierid'=>$data['courierid'],
								'type'=>$data['type'],
								'customname'=>$data['customname'],
								'telphone'=>$data['telphone'],
								'membernumber'=>$data['membernumber'],
								'citizennumber'=>$data['citizennumber'],
								'password'=>$data['password'],
								'orders'=>$data['orders'],
				);
				
				$ret = array('st'=>1, 'info'=>json_encode($info));
				$this->ajaxReturn($ret);
			}
			else {
				//进行中的订单信息不存在
				$this->retMsg(0, 432);
			}
		}
	}
	
	/**
	 * 用于网购模式的订单数据上报
	 * getOrderInfo
	 * @return return_type
	 * @throws
	 */
	public function updateWebOrder()
	{
		$token = I('get.token', '');
		if($token == '') {
			$token = '0';
		}
		
		$tid = I('get.tid', '');
		if($tid == '') {
			$tid = '0';
		}
		
		$orders = I('get.orders', '');
		if($orders == '') {
			$orders = '0';
		}
		
		$json_info = I('get.info', '');
		
		if($token == '0' || $tid == '0' || $orders == '0') {
			//参数信息错误
			$this->retMsg(0, 431);
		}
		else if($this->validateToken($token)) {
			//客户端异常
			$this->retMsg(0, 425);
		}
		else if($this->validateTidForOrder($tid)) {
			//订单流水号存在
			$this->retMsg(1, 202);
		}
		else {
			//解析订单信息
			$jsonstring = str_replace('&quot;', '"', $json_info);
			$jsonstring = str_replace('\\', '', $jsonstring);
			$info = json_decode($jsonstring, true);
				
			//快递公司标识转换
			if($info['firmid'] == '') {
				$courierId = $this->courierGuidToId($info['customerid']);
			}
			else {
				$courierId = $info['firmid'];
			}
				
			$clientId = $this->tokenToId($token);
			
			$boxno = $info['boxno'];
			$pwd = $info['pwd'];
			
			$model = M("LogisticsOrder");
			$data = $model->where("orders='$orders' and clientid = '$clientId'")->find();
			$jsonPwd = $data['password'];
			if($jsonPwd == null) {
				if($pwd != null && $pwd != '') {
					$arrayPwd = array(
							$boxno => $pwd,
					);
					$strPwd = json_encode($arrayPwd);
				}
				else {
					$strPwd = null;
				}
			}
			else {
				$arrayPwd = json_decode($jsonPwd);
				$arrayPwd->$boxno = $pwd;
				$strPwd = json_encode($arrayPwd);
			}
			
			$this->setSysLog('LogisticsController', 'INFO', 'updateWebOrder', $boxno . '|' . $pwd . '|' . $strPwd);
			
			$model->updateuser = 'updateWebOrder';
			$model->updatetime = time();
			$model->status = 1;
			$model->orderno = $info['orderno'];
			$model->courierid = $courierId;
			$model->tid = $tid;
			$model->type = $info['type'];
			$model->password = $strPwd;
			$model->paystatus = '0';
			if($model->where("orders='$orders' and clientid = '$clientId'")->save() !== false) {
				$this->retMsg(1, 200);
			}
			else {
				$this->retMsg(0, 502);
			}
		}
	}
	
	/**
	 * 订单信息上报接口
	 * setOrderInfo
	 * @return return_type
	 * @throws
	 */
	public function setOrderInfo()
	{
		$token = I('get.token', '');
		if($token == '') {
			$token = '0';
		}
		
		$tid = I('get.tid', '');
		if($tid == '') {
			$tid = '0';
		}
		
		$orderno = I('get.orderno', '');
		if($orderno == '') {
			$orderno = '0';
		}
		
		$json_info = I('get.info', '');
		
		if($token == '0' || $tid == '0' || $orderno == '0' || $json_info == '') {
			//参数信息错误
			$this->retMsg(0, 431);
		}
		else if($this->validateToken($token)) {
			//客户端异常
			$this->retMsg(0, 425);
		}
		else if($this->validateTidForOrder($tid)) {
			//订单流水号存在
			$this->retMsg(1, 202);
		}
		else {
			//解析订单信息
			$jsonstring = str_replace('&quot;', '"', $json_info);
			$jsonstring = str_replace('\\', '', $jsonstring);
			$info = json_decode($jsonstring, true);
			
			//快递公司标识转换
			if($info['firmid'] == '') {
				$courierId = $this->courierGuidToId($info['customerid']);
			}
			else {
				$courierId = $info['firmid'];
			}
			
			$boxno = $info['boxno'];
			$pwd = $info['pwd'];
			
			$clientId = $this->tokenToId($token);
			//dump($info);
			if($this->ordernoToId($orderno)) {
				//已存在订单，更新订单信息
				$model = M("LogisticsOrder");
				$data = $model->where("orderno='$orderno'")->find();
				$jsonPwd = $data['password'];
				if($jsonPwd == null) {
					if($pwd != null && $pwd != '') {
						$arrayPwd = array(
								$boxno => $pwd,
						);
						$strPwd = json_encode($arrayPwd);
					}
					else {
						$strPwd = null;
					}
				}
				else {
					$arrayPwd = json_decode($jsonPwd);
					$arrayPwd->$boxno = $pwd;
					$strPwd = json_encode($arrayPwd);
				}
				
				$this->setSysLog('LogisticsController', 'INFO', 'setOrderInfo', 'Update：' . $boxno . '|' . $pwd . '|' . $strPwd);
				
				$model->updateuser = $token;
				$model->updatetime = time();
				$model->status = 1;
				$model->courierid = $courierId;
				$model->clientid = $clientId;
				$model->tid = $tid;
				$model->type = $info['type'];
				$model->telphone = $info['tel'];
				$model->membernumber = $info['membernumber'];
				$model->password = $strPwd;
				$model->paystatus = '0';
				$model->orders = $info['orders'];
				$model->cnt = $info['cnt'];
				if($model->where("orderno='$orderno'")->save() !== false) {
					$this->retMsg(1, 200);
				}
				else {
					$this->retMsg(0, 502);
				}
			}
			else {
				//本地订单，新增订单信息
				
				if($pwd != null && $pwd != '') {
					$arrayPwd = array(
							$boxno => $pwd,
					);
					$strPwd = json_encode($arrayPwd);
				}
				else {
					$strPwd = null;
				}
				
				$this->setSysLog('LogisticsController', 'INFO', 'setOrderInfo', 'Insert：' . $boxno . '|' . $pwd . '|' . $strPwd);
				
				$model = M("LogisticsOrder");
				$model->createuser = $token;
				$model->createtime = time();
				$model->status = 1;
				$model->orderno = $orderno;
				$model->courierid = $courierId;
				$model->clientid = $clientId;
				$model->tid = $tid;
				$model->type = $info['type'];
				$model->telphone = $info['tel'];
				$model->membernumber = $info['membernumber'];
				$model->password = $strPwd;
				$model->paystatus = '0';
				$model->orders = $info['orders'];
				$model->cnt = $info['cnt'];
				if($model->add() !== false) {
					$this->retMsg(1, 200);
				}
				else {
					$this->retMsg(0, 431);
				}
			}
		}
	}
	
	/**
	 * 订单支付接口
	 * payInfo
	 * @return return_type
	 * @throws
	 */
	public function payInfo()
	{
		$token = I('get.token', '');
		if($token == '') {
			$token = '0';
		}
		
		$tid = I('get.tid', '');
		if($tid == '') {
			$tid = '0';
		}
		
		$orderno = I('get.orderno', '');
		if($orderno == '') {
			$orderno = '0';
		}
		
		$type = I('get.type', '');
		if($type == '') {
			$type = '0';
		}
		
		$retention = I('get.retention', '');
		if($retention == '') {
			$retention = '0';
		}
		
		$money = I('get.money', '');
		if($money == '') {
			$money = '0';
		}
		
		if($token == '0' || $tid == '0' || $orderno == '0' || $type == '0') {
			//参数信息错误
			$this->retMsg(0, 431);
		}
		else if($this->validateToken($token)) {
			//客户端异常
			$this->retMsg(0, 425);
		}
		else if($this->validateTidForOrder($tid)) {
			//支付流水号存在
			$this->retMsg(1, 202);
		}
		else {
			$model = M("LogisticsOrder");
			$model = M("AccessOrder");
			$model->updateuser = $token;
			$model->updatetime = time();
			$model->tid = $tid;
			$model->retention = $retention;
			$model->paystatus = 1;
			$model->paymoney = $money;
			$count = $model->where("orderno = '$orderno'")->save();
			if($count) {
				$this->retMsg(1, 200);
			}
			else {
				$this->retMsg(0, 502);
			}
		}
	}
	
	/**
	 * 柜子使用信息上报接口
	 * setBoxRecord
	 * @return return_type
	 * @throws
	 */
	public function setBoxRecord()
	{
		$token = I('get.token', '');
		if($token == '') {
			$token = '0';
		}
	
		$tid = I('get.tid', '');
		if($tid == '') {
			$tid = '0';
		}
	
		$orderno = I('get.orderno', '');
		if($orderno == '') {
			$orderno = '0';
		}
	
		$type = I('get.type', '');
		if($type == '') {
			$type = '0';
		}
	
		$json_boxno = I('get.bn', '');
		
		$json_info = I('get.info', '');
	
		if($token == '0' || $tid == '0' || $orderno == '0' || $type == '0' || $json_boxno == '') {
			//参数信息错误
			$this->retMsg(0, 431);
		}
		else if($this->validateToken($token)) {
			//客户端异常
			$this->retMsg(0, 425);
		}
		else if($this->validateTidForBoxrecord($tid)) {
			//操作记录流水号存在
			$this->retMsg(1, 202);
		}
		else {
			//解析订单信息
			$jsonstring = str_replace('&quot;', '"', $json_info);
			$jsonstring = str_replace('\\', '', $jsonstring);
			$info = json_decode($jsonstring, true);
			
			$orderId = $this->ordernoToId($orderno);
			$clientId = $this->tokenToId($token);
			
			$company = $this->tokenToCompanyInfo($token);
			$companyCode = $company['code'];
			$isapi = $company['isapi'];
			$apiurl = $company['apiurl'];
			
			if($orderId) {
				//解析柜子信息
				$jsonstring = str_replace('&quot;', '"', $json_boxno);
				$jsonstring = str_replace('\\', '', $jsonstring);
				$boxno = json_decode($jsonstring, true);
				
				$retStatus = true;
				
				$orders = '';
				
				foreach ($boxno as $row) {
					$boxId = $this->boxnoToId($clientId, $row);
					
					//数据初始化
					$modelCabinet = M("SysClient");
					$dataCabinet = $modelCabinet->where("id = $clientId")->find();
					$modelDoor = M("SysBox");
					$dataDoor = $modelDoor->where("id = $boxId")->find();
					$modelOrder = M("LogisticsOrder");
					$dataOrder = $modelOrder->where("id = $orderId")->find();
					$courierId = $dataOrder['courierid'];
					$modelCourier = M("LogisticsCourier");
					$dataCourier = $modelCourier->where("id = $courierId")->find();
					
					$orders = $dataOrder['orders'];
					
					//获取对应柜门的密码
					$doorNo = $dataDoor['no'];
					$jsonPwd = $dataOrder['password'];
					$objPwd = json_decode($jsonPwd);
					
					/* 解析操作过程 */
					$modelBox = M("SysBox");
					$modelBox->id = $boxId;
					
					$model = M("SysBoxrecord");
					$model->createuser = $token;
					$model->createtime = time();
					$model->boxid = $boxId;
					$model->orderid = $orderId;
					$model->tid = $tid;
					$model->operatetype = $type;
					$model->operatetime = time();
					
					switch ($type) {
						case '21':
							//更新柜子状态
							$modelBox->storagestatus = '1';
							$modelBox->save();
							
							//记录操作日志
							$model->operateuser = $dataOrder['telphone'];
							$model->remark = L('the_user_into_the_item') . $row . L('the_item_after_info');
							$model->add();
							
							if($isapi == '1') {
								switch ($companyCode) {
									case 'NDC':
										$url = $apiurl . 'VipDdistributionCabinet';
										$paramInfo = array(
												'Token' => $token,
												'CustomerID' => $dataCourier['guid'],
												'VipID' => $dataOrder['membernumber'],
												'CabinetNo' => $dataCabinet['csn'],
												'CabinetState' => $dataCabinet['status'],
												'CabinetType' => '21',
												'DoorNorm' => $dataDoor['norm'],
												'DoorNo' => $dataDoor['no'],
												'DoorState' => '3',
												'Clothes' => $dataOrder['cnt'],
												'Tid' => $tid,
										);
										$parameters = array(
												'Parameters' => $paramInfo
										);
										$pRequest = json_encode($parameters);
										$param = array(
												'pRequest' => urlencode($pRequest)
												//'pRequest' => $pRequest
										);
										$json = $this->send_post($url, $param);
										if($json) {
											$objjson = json_decode($json);
											$this->setSysLog('VipDdistributionCabinet', 'API', 'setBoxRecord', $pRequest . '|' . $json);
											if($objjson->DataCode == '1') {
												$orders = $objjson->OrderNo;
											}
											else {
												$retStatus = false;
												$this->setSysLog('VipDdistributionCabinet', 'DEBUG', 'setBoxRecord', $url . '?' . 'pRequest=' . urlencode($pRequest));
											}
										}
										else {
											$retStatus = false;
											$this->setSysLog('VipDdistributionCabinet', 'ERROR', 'setBoxRecord', $url . '?' . 'pRequest=' . urlencode($pRequest));
										}
										break;
									default:
										$this->setSysLog('21', 'ERROR', 'setBoxRecord', 'CompanyCode Is Error.');
								}
							}
							else {
								//存件时如果不存在第三方接口，本地生成订单号码
								$date = substr(date("Ymd ", time()), 0, 8);
								$maxOrders = $this->getMaxFieldByStr('LogisticsOrder', 'orders', $date, 1);
								if($maxOrders) {
									$orders = $maxOrders + 1;
								}
								else {
									$orders = $date . '0001';
								}
							}
							//更新订单状态
							$modelOrder->updateuser = $token;
							$modelOrder->updatetime = time();
							$modelOrder->id = $orderId;
							$modelOrder->inputtime = time();
							$modelOrder->orders = $orders;
							$modelOrder->save();
							break;
						case '22':
							//更新柜子状态
							$modelBox->storagestatus = '0';
							$modelBox->save();
							
							//记录操作日志
							$cardid = $info['cardid'];
							$modelCardInfo = M('LogisticsCardinfo');
							$cardInfo = $modelCardInfo->where("id = '$cardid'")->find();
							if($cardInfo) {
								$operateuser = $cardInfo['nickname'];
							}
							else {
								$operateuser = $cardid;
							}
							$model->operateuser = $operateuser;
							$model->remark = L('courier_romove_out_before') . $row . L('courier_romove_out_after');
							$model->add();
							
							if($isapi == '1') {
								switch ($companyCode) {
									case 'NDC':
										$url = $apiurl . 'NiceTakeClothes';
										
										$paramInfo = array(
												'Token' => $token,
												'CustomerID' => $dataCourier['guid'],
												'UserID' => $info['cardid'],
												'CabinetNo' => $dataCabinet['csn'],
												'CabinetState' => $dataCabinet['status'],
												'CabinetType' => '22',
												'DoorNo' => $dataDoor['no'],
												'DoorState' => '0',
												'OrderNo' => $orders,
												'Tid' => $tid,
										);
										$parameters = array(
												'Parameters' => $paramInfo
										);
										$pRequest = json_encode($parameters);
										$param = array(
												'pRequest' => urlencode($pRequest)
												//'pRequest' => $pRequest
										);
										$json = $this->send_post($url, $param);
										if($json) {
											$objjson = json_decode($json);
											$this->setSysLog('NiceTakeClothes', 'API', 'setBoxRecord', $pRequest . '|' . $json);
											if($objjson->DataCode == '1') {
												//TODO
												//$orders = $dataOrder['orders'];
											}
											else {
												$retStatus = false;
												$this->setSysLog('NiceTakeClothes', 'DEBUG', 'setBoxRecord', $url . '?' . 'pRequest=' . urlencode($pRequest));
											}
										}
										else {
											$retStatus = false;
											$this->setSysLog('NiceTakeClothes', 'ERROR', 'setBoxRecord', $url . '?' . 'pRequest=' . urlencode($pRequest));
										}
										break;
									default:
										$this->setSysLog('22', 'ERROR', 'setBoxRecord', 'CompanyCode Is Error.');
								}
							}
							//更新订单状态
							$modelOrder->updateuser = $token;
							$modelOrder->updatetime = time();
							$modelOrder->id = $orderId;
							$modelOrder->pickuptime = time();
							$modelOrder->status = 0;
							$modelOrder->save();
							break;
						case '23':
							//更新柜子状态
							$modelBox->storagestatus = '1';
							$modelBox->save();
							
							//记录操作日志
							$cardid = $info['cardid'];
							$modelCardInfo = M('LogisticsCardinfo');
							$cardInfo = $modelCardInfo->where("id = '$cardid'")->find();
							if($cardInfo) {
								$operateuser = $cardInfo['nickname'];
							}
							else {
								$operateuser = $cardid;
							}
							$model->operateuser = $operateuser;
							$model->remark = L('courier_put_into_before') . $row . L('courier_put_into_after');
							$model->add();
							
							$inputtime = time();
							
							if($isapi == '1') {
								switch ($companyCode) {
									case 'NDC':
										$url = $apiurl . 'NiceTakeGiveValet';
										
										$paramInfo = array(
												'Token' => $token,
												'CustomerID' => $dataCourier['guid'],
												'UserID' => $info['cardid'],
												'CabinetNo' => $dataCabinet['csn'],
												'CabinetState' => $dataCabinet['status'],
												'CabinetType' => '23',
												'DoorNorm' => $dataDoor['norm'],
												'DoorNo' => $dataDoor['no'],
												'DoorState' => '3',
												'OrderNo' => $orders,
												'DeliveryTime' => $dataOrder['createtime'],
												'PickupCode' => $objPwd->$doorNo,
												'Tid' => $tid,
										);
										$parameters = array(
												'Parameters' => $paramInfo
										);
										$pRequest = json_encode($parameters);
										$param = array(
												'pRequest' => urlencode($pRequest)
												//'pRequest' => $pRequest
										);
										
										$json = $this->send_post($url, $param);
										if($json) {
											$objjson = json_decode($json);
											$this->setSysLog('NiceTakeGiveValet', 'API', 'setBoxRecord', $pRequest . '|' . $json);
											if($objjson->DataCode == '1') {
												//TODO
												//$orders = $dataOrder['orders'];
											}
											else {
												$retStatus = false;
												$this->setSysLog('NiceTakeGiveValet', 'DEBUG', 'setBoxRecord', $url . '?' . 'pRequest=' . urlencode($pRequest));
											}
										}
										else {
											$retStatus = false;
											$this->setSysLog('NiceTakeGiveValet', 'ERROR', 'setBoxRecord', $url . '?' . 'pRequest=' . urlencode($pRequest));
										}
										break;
									case 'TTX':
										$url = $apiurl . 'setOrderState';
										$param = array(
												'barcode' => $orders,
												'status' => '1',
												'lockerID' => $dataDoor['no'],
												'terminalSN' => $dataCabinet['csn'],
												'password' => $objPwd->$doorNo,
												'created' => $inputtime,
												'method' => $info['method'],
												'cardNumber' => $info['cardnumber'],
										);
										$json = $this->send_post($url, $param);
										if($json) {
											$objjson = json_decode($json);
											$this->setSysLog('setOrderState', 'API', 'setBoxRecord', json_encode($param) . '|' . $json);
											if($objjson->st == '1') {
												//TODO
												//$orders = $dataOrder['orders'];
											}
											else {
												$retStatus = false;
												$this->setSysLog('setOrderState', 'DEBUG', 'setBoxRecord', json_encode($param) . '|' . $json);
											}
										}
										else {
											$retStatus = false;
											$this->setSysLog('setOrderState', 'ERROR', 'setBoxRecord', json_encode($param));
										}
										break;
									default:
										$this->setSysLog('23', 'ERROR', 'setBoxRecord', 'CompanyCode Is Error.');
								}
							}
							//更新订单状态
							$modelOrder->updateuser = $token;
							$modelOrder->updatetime = time();
							$modelOrder->id = $orderId;
							$modelOrder->inputtime = $inputtime;
							$modelOrder->orders = $orders;
							$modelOrder->save();
							break;
						case '24':
							//更新柜子状态
							$modelBox->storagestatus = '0';
							$modelBox->save();
							
							//记录操作日志
							$model->operateuser = $dataOrder['telphone'];
							$model->remark = L('user_romove_out_before') . $row . L('user_romove_out_after');
							$model->add();
							
							if($isapi == '1') {
								switch ($companyCode) {
									case 'NDC':
										$url = $apiurl . 'VipTakeClothesCarryOut';
										$paramInfo = array(
												'Token' => $token,
												'CustomerID' => $dataCourier['guid'],
												'OrderNo' => $orders,
												'CabinetNo' => $dataCabinet['csn'],
												'CabinetState' => $dataCabinet['status'],
												'CabinetType' => '24',
												'DoorNo' => $dataDoor['no'],
												'DoorState' => '0',
												'Tid' => $tid,
										);
										$parameters = array(
												'Parameters' => $paramInfo
										);
										$pRequest = json_encode($parameters);
										$param = array(
												'pRequest' => urlencode($pRequest)
												//'pRequest' => $pRequest
										);
										$json = $this->send_post($url, $param);
										if($json) {
											$objjson = json_decode($json);
											$this->setSysLog('VipTakeClothesCarryOut', 'API', 'setBoxRecord', $pRequest . '|' . $json);
											if($objjson->DataCode == '1') {
												//TODO
												//$orders = $dataOrder['orders'];
											}
											else {
												$retStatus = false;
												$this->setSysLog('VipTakeClothesCarryOut', 'DEBUG', 'setBoxRecord', $url . '?' . 'pRequest=' . urlencode($pRequest));
											}
										}
										else {
											$retStatus = false;
											$this->setSysLog('VipTakeClothesCarryOut', 'ERROR', 'setBoxRecord', $url . '?' . 'pRequest=' . urlencode($pRequest));
										}
										break;
									case 'TTX':
										$url = $apiurl . 'setOrderState';
										$param = array(
												'barcode' => $orders,
												'status' => '2',
												'lockerID' => $dataDoor['no'],
												'terminalSN' => $dataCabinet['csn'],
												'password' => $objPwd->$doorNo,
												'created' => time(),
												'method' => $info['method'],
												'cardNumber' => $info['cardnumber'],
										);
										$json = $this->send_post($url, $param);
										if($json) {
											$objjson = json_decode($json);
											$this->setSysLog('setOrderStatus', 'API', 'setBoxRecord', json_encode($param) . '|' . $json);
											if($objjson->st == '1') {
												//TODO
												//$orders = $dataOrder['orders'];
											}
											else {
												$retStatus = false;
												$this->setSysLog('setOrderStatus', 'DEBUG', 'setBoxRecord', json_encode($param) . '|' . $json);
											}
										}
										else {
											$retStatus = false;
											$this->setSysLog('setOrderStatus', 'ERROR', 'setBoxRecord', json_encode($param));
										}
										break;
									default:
										$this->setSysLog('24', 'ERROR', 'setBoxRecord', 'CompanyCode Is Error.');
								}
							}
							//更新订单状态
							$modelOrder->updateuser = $token;
							$modelOrder->updatetime = time();
							$modelOrder->id = $orderId;
							$modelOrder->pickuptime = time();
							$modelOrder->status = 0;
							$modelOrder->save();
							break;
						case '25':
							//更新柜子状态
							$modelBox->storagestatus = '0';
							$modelBox->save();
							
							//记录操作日志
							$cardid = $info['cardid'];
							$modelCardInfo = M('LogisticsCardinfo');
							$cardInfo = $modelCardInfo->where("id = '$cardid'")->find();
							$model->operateuser = $cardInfo['nickname'];
							$model->remark = L('courier_romove_out_before') . $row . L('courier_return_step');
							$model->add();
							
							if($isapi == '1') {
								switch ($companyCode) {
									case 'NDC':
										$url = $apiurl . 'NiceChargebackClothes';
										$paramInfo = array(
												'Token' => $token,
												'CustomerID' => $dataCourier['guid'],
												'UserID' => $info['cardid'],
												'CabinetNo' => $dataCabinet['csn'],
												'CabinetState' => $dataCabinet['status'],
												'CabinetType' => '25',
												'DoorNo' => $dataDoor['no'],
												'DoorState' => '0',
												'OrderNo' => $orders,
												'Tid' => $tid,
										);
										$parameters = array(
												'Parameters' => $paramInfo
										);
										$pRequest = json_encode($parameters);
										$param = array(
												'pRequest' => urlencode($pRequest)
												//'pRequest' => $pRequest
										);
										$json = $this->send_post($url, $param);
										if($json) {
											$objjson = json_decode($json);
											$this->setSysLog('NiceChargebackClothes', 'API', 'setBoxRecord', $pRequest . '|' . $json);
											if($objjson->DataCode == '1') {
												//TODO
												//$orders = $dataOrder['orders'];
											}
											else {
												$retStatus = false;
												$this->setSysLog('NiceChargebackClothes', 'DEBUG', 'setBoxRecord', $url . '?' . 'pRequest=' . urlencode($pRequest));
											}
										}
										else {
											$retStatus = false;
											$this->setSysLog('NiceChargebackClothes', 'ERROR', 'setBoxRecord', $url . '?' . 'pRequest=' . urlencode($pRequest));
										}
										break;
									case 'TTX':
										$url = $apiurl . 'setOrderState';
										$param = array(
												'barcode' => $orders,
												'status' => '3',
												'lockerID' => $dataDoor['no'],
												'terminalSN' => $dataCabinet['csn'],
												'password' => $objPwd->$doorNo,
												'created' => time(),
												'method' => $info['method'],
												'cardNumber' => $info['cardnumber'],
										);
										$json = $this->send_post($url, $param);
										if($json) {
											$objjson = json_decode($json);
											$this->setSysLog('setOrderStatus', 'API', 'setBoxRecord', json_encode($param) . '|' . $json);
											if($objjson->st == '1') {
												//TODO
												//$orders = $dataOrder['orders'];
											}
											else {
												$retStatus = false;
												$this->setSysLog('setOrderStatus', 'DEBUG', 'setBoxRecord', json_encode($param) . '|' . $json);
											}
										}
										else {
											$retStatus = false;
											$this->setSysLog('setOrderStatus', 'ERROR', 'setBoxRecord', json_encode($param));
										}
										break;
									default:
										$this->setSysLog('25', 'ERROR', 'setBoxRecord', 'CompanyCode Is Error.');
								}
							}
							//更新订单状态
							$modelOrder->updateuser = $token;
							$modelOrder->updatetime = time();
							$modelOrder->id = $orderId;
							$modelOrder->pickuptime = time();
							$modelOrder->status = 3;
							$modelOrder->save();
							break;
						default :
							$this->setSysLog('commSystem', 'ERROR', 'setBoxRecord', 'OperateType Is Error.');
					}
				}
				if($retStatus) {
					$retInfo = array('orders' => $orders);
					$this->retMsg(1, json_encode($retInfo));
				}
				else {
					$this->retMsg(0, 440);
				}
			}
			else {
				$this->retMsg(0, 432);
			}
		}
	}
	
	/**
	 * 雇员身份信息验证
	 * getCardInfo
	 * @return return_type
	 * @throws
	 */
	public function getCardInfo()
	{
		$token = I('get.token', '');
		if($token == '') {
			$token = '0';
		}
		
		//rfid卡
		$cardno = I('get.cardno', '');
		if($cardno == '') {
			$cardno = '0';
		}
		//二维码
		$qrcode = I('get.qrcode', '');
		if($qrcode == '') {
			$qrcode = '0';
		}
		//手机号密码
		$phone = I('get.phone', '');
		if($phone == '') {
			$phone = '0';
		}
		$password = I('get.password', '');
	
		if($token == '0' || ($cardno == '0' && $phone == '0' && $qrcode == '0')) {
			//参数信息错误
			$this->retMsg(0, 431);
		}
		else if($this->validateToken($token)) {
			//客户端异常
			$this->retMsg(0, 425);
		}
		else {
			$clientId = $this->tokenToId($token);
			$this->setSysLog('LogisticsController', 'INFO', 'getCardInfo', $token . '|' . $cardno . '|' . $phone . '|' . $password . '|' . $qrcode);
			$model = new Model();
			$sql = "select
					 a.account
					,a.password
					,a.cardno
					,a.othercardno
					,a.type
					,a.id
					,a.courierid
					from wf_logistics_cardinfo a, wf_logistics_cardclient b, wf_sys_client c
					where a.id = b.cardid
					and b.clientid = c.id
					and c.id = $clientId";
			if($cardno != '0') {
				//用户卡验证
				$sql = $sql . " and a.cardno = '$cardno'";
				$data = $model->query($sql);
				if($data) {
					$info = array('id'=>$data[0]['id'], 'type'=>$data[0]['type'], 'firmid'=>$data[0]['courierid']);
					$ret = array('st'=>1, 'info'=>json_encode($info));
					$this->ajaxReturn($ret);
				}
				else {
					$this->retMsg(0, 434);
				}
			}
			else if($qrcode != '0') {
				//二维码验证
				$sql = $sql . " and a.othercardno = '$qrcode'";
				$data = $model->query($sql);
				if($data) {
					$info = array('id'=>$data[0]['id'], 'type'=>$data[0]['type'], 'firmid'=>$data[0]['courierid']);
					$ret = array('st'=>1, 'info'=>json_encode($info));
					$this->ajaxReturn($ret);
				}
				else {
					$this->retMsg(0, 434);
				}
			}
			else {
				//账号验证
				$sql = $sql . " and a.account = '$phone'";
				$data = $model->query($sql);
				if($data) {
					$pw = $data[0]['password'];
					$pw1 = md5(md5($password).C('mark'));
					if($pw1 == $pw) {
						$info = array('id'=>$data[0]['id'], 'type'=>$data[0]['type'], 'firmid'=>$data[0]['courierid']);
						$ret = array('st'=>1, 'info'=>json_encode($info));
						$this->ajaxReturn($ret);
					}
					else {
						$this->retMsg(0, 434);
					}
				}
				else {
					$this->retMsg(0, 434);
				}
			}
		}
	}
	
	/**
	 * 终端初始化接口
	 * getInitialize
	 * @return return_type
	 * @throws
	 */
	public function getInitialize()
	{
		$token = I('get.token', '');
		if($token == '') {
			$token = '0';
		}
	
		if($this->validateToken($token)) {
			//客户端异常
			$this->retMsg(0, 425);
		}
		else {
			$clientId = $this->tokenToId($token);
			$model = M("SysCompanyclient");
			$data = $model->where("clientid = $clientId")->find();
			$companyId = $data['companyid'];
			if($companyId == '') {
				//客户端配置错误
				$this->retMsg(0, 437);
			}
			else {
				//系统参数信息
				$modelConfig = M("SysConfig");
				$dataConfig = $modelConfig->where("sysid = 'logistics' and status = 1")->select();
				$jsonConfig = array();
				//$i = 0;
				foreach ($dataConfig as $row) {
					$jsonConfig[$row['key']] = $row['value'];
					//$i++;
				}
				//快递公司信息
				$modelCourier = M("LogisticsCourier");
				$dataCourier = $modelCourier->where("status = 1 and companyid = $companyId")->select();
				$jsonCourier = array();
				$i = 0;
				foreach ($dataCourier as $row) {
					$jsonInfo = array(  'id'=>$row['id'],
										'name'=>$row['name'],
										'code'=>$row['code'],
										'short'=>$row['short'],
										'phone'=>$row['phone']);
					$jsonCourier[$i] = $jsonInfo;
					$i++;
				}
				$ret_json = array('config'=>$jsonConfig, 'courier'=>$jsonCourier);
				$this->ajaxReturn($ret_json);
			}
		}
	}
	
	/**
	 * 获取会员验证信息
	 * getVipValid
	 * @return return_type
	 * @throws
	 */
	public function getVipValid()
	{
		$token = I('get.token', '');
		$model = M("LogisticsVipvalid");
		$data = $model->where("token='$token' and status = 0 and valid = 1")->order(array('id'=>'desc'))->limit(1)->find();
		if($data) {
			$id = $data['id'];
			$info = array(
					'vipid'=>$data['vipid'],
					'customerid'=>$data['customerid']);
			//获取的本条数据
			$model->updateuser = 'getVipValid';
			$model->updatetime = time();
			$model->status = 1;
			$model->where("id = $id")->save();
			//其他未执行数据
			$model->updateuser = 'getVipValid';
			$model->updatetime = time();
			$model->status = 2;
			$model->where("token='$token' and status = 0")->save();
			
			$ret = array('st'=>2, 'info'=>json_encode($info));
			$this->ajaxReturn($ret);
		}
		else {
			$this->retMsg(1, '');
		}
	}
	
	/**
	 * 获取快递员验证信息
	 * getUserValid
	 * @return return_type
	 * @throws
	 */
	public function getUserValid()
	{
		$token = I('get.token', '');
		$customerID = I('get.customerid', '');
		$userID = I('get.userid', '');
		
		$retStatus = true;
		
		$company = $this->tokenToCompanyInfo($token);
		$companyCode = $company['code'];
		$isapi = $company['isapi'];
		$apiurl = $company['apiurl'];
		
		if($isapi == '1') {
			switch ($companyCode) {
				case 'NDC':
					$url = $apiurl . 'NiceTakeAuthentication';
					$paramInfo = array(
							'Token' => $token,
							'CustomerID' => $customerID,
							'UserID' => $userID
					);
					$parameters = array(
							'Parameters' => $paramInfo
					);
					$pRequest = json_encode($parameters);
					$param = array(
							'pRequest' => urlencode($pRequest)
							//'pRequest' => $pRequest
					);
					$json = $this->send_post($url, $param);
					if($json) {
						$objjson = json_decode($json);
						$this->setSysLog('NiceTakeAuthentication', 'API', 'setBoxRecord', $pRequest . '|' . $json);
						if($objjson->DataCode != '1') {
							$retStatus = false;
							$this->setSysLog('NiceTakeAuthentication', 'DEBUG', 'setBoxRecord', $url . '?' . 'pRequest=' . urlencode($pRequest));
						}
					}
					else {
						$retStatus = false;
						$this->setSysLog('NiceTakeAuthentication', 'ERROR', 'setBoxRecord', $url . '?' . 'pRequest=' . urlencode($pRequest));
					}
					break;
				default:
					$this->setSysLog('25', 'ERROR', 'setBoxRecord', 'CompanyCode Is Error.');
			}
		}
		if($retStatus) {
			$this->retMsg(1, 200);
		}
		else {
			$this->retMsg(0, 434);
		}
	}
	
	/**
	 * 订单号码转Id
	 * ordernoToId
	 * @param unknown_type $orderno
	 * @return return_type
	 * @throws
	 */
	public function ordernoToId($orderno)
	{
		$model = M("LogisticsOrder");
		$data = $model->where("orderno='$orderno'")->find();
		if($data) {
			return $data['id'];
		}
		else {
			return false;
		}
	}
	
	/**
	 * GUID转Id
	 * courierGuidToId
	 * @param unknown $guid
	 * @return \Think\mixed|boolean
	 */
	public function courierGuidToId($guid)
	{
		$model = M("LogisticsCourier");
		$data = $model->where("guid='$guid'")->find();
		if($data) {
			return $data['id'];
		}
		else {
			return false;
		}
	}
	
	/**
	 * 验证订单流水号
	 * validateTidForOrder
	 * @param unknown_type $tid
	 * @return return_type
	 * @throws
	 */
	public function validateTidForOrder($tid)
	{
		$model = M("LogisticsOrder");
		$data = $model->where("tid='$tid'")->find();
		if($data) {
			return true;
		}
		else {
			return false;
		}
	}
	
	/**
	 * 验证短信流水号
	 * validateTidForSms
	 * @param unknown_type $tid
	 * @return return_type
	 * @throws
	 */
	public function validateTidForSms($tid)
	{
		$model = M("SysSms");
		$data = $model->where("tid='$tid' and status = 1")->find();
		if($data) {
			return true;
		}
		else {
			return false;
		}
	}
	
	/**
	 * 验证操作记录流水号
	 * validateTidForBoxrecord
	 * @param unknown_type $tid
	 * @return return_type
	 * @throws
	 */
	public function validateTidForBoxrecord($tid)
	{
		$model = M("SysBoxrecord");
		$data = $model->where("tid='$tid'")->find();
		if($data) {
			return true;
		}
		else {
			return false;
		}
	}
}
<?php
namespace Comm\Controller;

use Think\Model;
use Think\Think;

/**
 * 投取柜控制器类
 * Comm\Controller$AccessController
 * Copyright (c) 2016. 云绅（张家港）精密工业有限公司 版权所有
 * 创建人：徐辰辰
 * 创建时间：2016-4-23 上午10:27:22
 */
class AccessController extends BaseController
{
	/*
	 * 投取柜控制器类架构函数
	 */
	public function _initialize()
	{
		// TODO
		parent::_initialize();
	}
	
	/**
	 * 短信通用接口
	 * setSmsInfo
	 * @return return_type
	 * @throws
	 */
	public function setSmsInfo()
	{
		$token = I('get.token', '');
		if($token == '') {
			$token = '0';
		}
		
		$tel = I('get.tel', '');
		if($tel == '') {
			$tel = '0';
		}
		
		$mould = I('get.mould', '');
		if($mould == '') {
			$mould = '0';
		}
		
		$tid = I('get.tid', '');
		if($tid == '') {
			$tid = '0';
		}
		
		$orderno = I('get.orderno', '');
		if($orderno == '') {
			$orderno = '0';
		}
		
		$info = I('get.info', '');
		
		if($token == '0' || $tel == '0' || $mould == '0' || $tid == '0') {
			//参数信息错误
			$this->retMsg(0, 431);
		}
		else if($this->validateToken($token)) {
			//客户端异常
			$this->retMsg(0, 425);
		}
		else if($this->validateTidForSms($tid)) {
			//短信已发送
			$this->retMsg(1, 202);
		}
		else {
			$model = M("SysSmsstencil");
			$data = $model->where("code = '$mould' and status = 1 and type = 1 and sysid = 'access'")->find();
			if($data) {
				//解析参数
				$param = array();
				$jsonstring = str_replace('&quot;', '"', $info);
				$jsonstring = str_replace('\\', '', $jsonstring);
				$param = json_decode($jsonstring, true);
				
				//token转终端名称及地址
				$modelParam = new Model();
				$sql = "select
						 a.name as clientname
						,a.position as clientaddress
						,c.phone as supplierphone
						,d.orderno
						,d.price
						,d.telphone
						,b.profit
						,e.code as abn
						,d.paymoney
						,from_unixtime(d.updatetime) as date
						from wf_sys_client a, wf_sys_clientconfig b, wf_access_store c, wf_access_order d, wf_access_owner e
						where a.id = b.clientid
						and b.supplier = c.id
						and a.id = d.clientid
						and b.owner = e.id
						and a.status = 1
						and a.token = '$token'
						and d.orderno = '$orderno'";
				$dataParam = $modelParam->query($sql);
				if($dataParam) {
					$param['clientname'] = $dataParam[0]['clientname'];
					$param['clientaddress'] = $dataParam[0]['clientaddress'];
					$param['supplierphone'] = $dataParam[0]['supplierphone'];
					$param['orderno'] = $dataParam[0]['orderno'];
					$param['telphone'] = $dataParam[0]['telphone'];
					$param['date'] = $dataParam[0]['date'];
					$param['paymoney'] = $dataParam[0]['paymoney'];
					$param['abn'] = $dataParam[0]['abn'];
					
					$price = $dataParam[0]['price'];
					$profit = $dataParam[0]['profit'];
					
					$modelConfig = M("SysConfig");
					$dataConfig = $modelConfig->where("sysid = 'access' and status = 1")->select();
					foreach ($dataConfig as $row) {
						$param[$row['key']] = $row['value'];
					}
					
					$tax = $param['tax'];
					
					$param['usermoney'] = round(($price * (0.01 * $profit + 1)) * (1 + 0.01 * $tax), 2);
					$param['suppliermoney'] = $price;
					
					$orderId = $this->ordernoToId($orderno);
					$orderDetail = '';
					$modelDetail = M("AccessOrderdetail");
					$dataDetail = $modelDetail->field("count(proname) as cnt,proname")->where("orderid = '$orderId'")->group("proname")->select();
					foreach ($dataDetail as $row) {
						$orderDetail = $orderDetail . ' - ' . $row['proname'] . '(' . $row['cnt'] . ')';
					}
					$param['orderdetail'] = $orderDetail;
				}
				
				if(10==strlen($tel)){
					$tel = substr($tel,-9);
				}
				$ret = sendSMS($tel, '1', $mould, $param, $tid, $orderno, $token);
				//$ret = 1;
				if($ret == 1) {
					$this->retMsg(1, 203);
				}
				else {
					$this->retMsg(0, 436);
				}
			}
			else {
				$this->retMsg(0, 435);
			}
		}
	}
	
	/**
	 * 订单信息获取接口
	 * getOrderInfo
	 * @return return_type
	 * @throws
	 */
	public function getOrderInfo()
	{
		$token = I('get.token', '');
		if($token == '') {
			$token = '0';
		}
		
		$orderno = I('get.orderno', '');
		if($orderno == '') {
			$orderno = '0';
		}
		
		if($token == '0' || $orderno == '0') {
			//参数信息错误
			$this->retMsg(0, 431);
		}
		else if($this->validateToken($token)) {
			//客户端异常
			$this->retMsg(0, 425);
		}
		else {
			$model = M("AccessOrder");
			$data = $model->where("orderno = '$orderno'")->find();
			if($data) {
				$info = array(	'storeid'=>$data['storeid'],
								'type'=>$data['type'],
								'tel'=>$data['telphone'],
								'price'=>$data['price']);
				//获取订单明细
				$orderId = $data['id'];
				$orderDetail = array();
				$i = 0;
				
				$modelDetail = new Model();
				$sql = "select procode, count(procode) as num
						from wf_access_orderdetail
						where orderid = $orderId
						and status = 1
						group by procode";
				$dataDetail = $modelDetail->query($sql);
				if($dataDetail) {
					foreach($dataDetail as $row) {
						$orderDetail[$i] = array('procode'=>$row['procode'], 'num'=>$row['num']);
						$i++;
					}
					$ret = array('st'=>1, 'info'=>json_encode($info), 'orderdetail'=>json_encode($dataDetail));
					
					$this->ajaxReturn($ret);
				}
				else {
					//进行中的订单明细不存在
					$this->retMsg(0, 433);
				}
			}
			else {
				//进行中的订单信息不存在
				$this->retMsg(0, 432);
			}
		}
	}
	
	/**
	 * 订单信息上报接口
	 * setOrderInfo
	 * @return return_type
	 * @throws
	 */
	public function setOrderInfo()
	{
		$token = I('get.token', '');
		if($token == '') {
			$token = '0';
		}
		
		$tid = I('get.tid', '');
		if($tid == '') {
			$tid = '0';
		}
		
		$orderno = I('get.orderno', '');
		if($orderno == '') {
			$orderno = '0';
		}
		
		$json_info = I('get.info', '');
		$json_orderDetail = I('get.orderdetail', '');
		
		if($token == '0' || $tid == '0' || $orderno == '0' || $json_info == '' || $json_orderDetail == '') {
			//参数信息错误
			$this->retMsg(0, 431);
		}
		else if($this->validateToken($token)) {
			//客户端异常
			$this->retMsg(0, 425);
		}
		else if($this->validateTidForOrder($tid)) {
			//订单流水号存在
			$this->retMsg(1, 202);
		}
		else {
			//解析订单信息
			$jsonstring = str_replace('&quot;', '"', $json_info);
			$jsonstring = str_replace('\\', '', $jsonstring);
			$info = json_decode($jsonstring, true);
			//dump($info);
			if($info['type'] == '3') {
				//空中订单，更新订单信息
				$model = M("AccessOrder");
				$model->updateuser = $token;
				$model->updatetime = time();
				$model->status = 1;
				$model->clientid = $this->tokenToId($token);
				$model->tid = $tid;
				if($model->where("orderno='$orderno'")->save() !== false) {
					$this->retMsg(1, 200);
				}
				else {
					$this->retMsg(0, 502);
				}
			}
			else {
				//本地订单，新增订单信息
				$jsonstring = str_replace('&quot;', '"', $json_orderDetail);
				$jsonstring = str_replace('\\', '', $jsonstring);
				$orderDetail = json_decode($jsonstring, true);
				
				$clientId = $this->tokenToId($token);
				
				$modelConfig = M("SysConfig");
				$dataConfig = $modelConfig->where("sysid = 'access' and `key` = 'tax'")->find();
				$tax = $dataConfig['value'];
				
				$modelClient = M("SysClientconfig");
				$dataClient = $modelClient->where("clientid = $clientId")->find();
				$profit = $dataClient['profit'];
				
				if(count($orderDetail) > 0) {
					//新增订单信息
					$model = M("AccessOrder");
					$model->createuser = $token;
					$model->createtime = time();
					$model->status = 1;
					$model->orderno = $orderno;
					$model->storeid = $info['storeid'];
					$model->clientid = $clientId;
					$model->tid = $tid;
					$model->type = $info['type'];
					$model->telphone = $info['tel'];
					$model->price = $info['price'];
					$model->profit = $profit;
					$model->tax = $tax;
					$model->paystatus = 0;
					$model->balance = 0;
					$orderid = $model->add();
					
					foreach ($orderDetail as $row) {
						//新增订单明细
						$proId = $row['proid'];
						$modeltemp = M("AccessProduct");
						$datatemp = $modeltemp->where("id = $proId")->find();
						for($i = 0; $i < $row['num']; $i++) {
							$modelDetail = M("AccessOrderdetail");
							$modelDetail->createuser = $token;
							$modelDetail->createtime = time();
							$modelDetail->status = 1;
							$modelDetail->orderid = $orderid;
							$modelDetail->proname = $datatemp['name'];
							$modelDetail->procode = $datatemp['code'];
							$modelDetail->proprice = $datatemp['price'];
							$modelDetail->add();
						}
					}
					//执行成功
					$this->retMsg(1, 200);
				}
				else {
					$this->retMsg(0, 431);
				}
			}
		}
	}
	
	/**
	 * 获取实际应付金额
	 * getActualPrice
	 * @return return_type
	 * @throws
	 */
	public function getActualPrice()
	{
		$token = I('get.token', '');
		if($token == '') {
			$token = '0';
		}
		
		$orderno = I('get.orderno', '');
		if($orderno == '') {
			$orderno = '0';
		}
		
		if($token == '0' || $orderno == '0') {
			//参数信息错误
			$this->retMsg(0, 431);
		}
		else if($this->validateToken($token)) {
			//客户端异常
			$this->retMsg(0, 425);
		}
		else {
			$model = M("AccessOrder");
			$data = $model->where("orderno = '$orderno' and status = 1")->find();
			if($data) {
				$this->retMsg(1, $data['actualprice']);
			}
			else {
				$this->retMsg(0, 432);
			}
		}
	}
	
	/**
	 * 上报实际应付金额
	 * setActualPrice
	 * @return return_type
	 * @throws
	 */
	public function setActualPrice()
	{
		$token = I('get.token', '');
		if($token == '') {
			$token = '0';
		}
		
		$orderno = I('get.orderno', '');
		if($orderno == '') {
			$orderno = '0';
		}
		$jsonprice = I('get.price', '');
		
		if($token == '0' || $orderno == '0' || $jsonprice == '') {
			//参数信息错误
			$this->retMsg(0, 431);
		}
		else if($this->validateToken($token)) {
			//客户端异常
			$this->retMsg(0, 425);
		}
		else {
			$model = M("AccessOrder");
			$data = $model->where("orderno = '$orderno' and status = 1")->find();
			$price = $data['price'];
			$profit = $data['profit'] * 0.01;
			$tax = $data['tax'] * 0.01;
			
			$jsonstring = str_replace('&quot;', '"', $jsonprice);
			$jsonstring = str_replace('\\', '', $jsonstring);
			$temp = json_decode($jsonstring, true);
			
			$extra = $temp['extra'];
			$retention = $temp['retention'];
			$actualprice = (($price + $extra) * (1 + $profit) + $retention) * (1 + $tax);
			
			$model->updateuser = $token;
			$model->updatetime = time();
			$model->extra = $extra;
			$model->retention = $retention;
			$model->actualprice = $actualprice;
			
			$ret = $model->where("orderno = '$orderno' and status = 1")->save();
			if($ret == false) {
				$this->retMsg(0, 502);
			}
			else {
				$this->retMsg(1, 200);
			}
		}
	}
	
	/**
	 * 订单支付接口
	 * payInfo
	 * @return return_type
	 * @throws
	 */
	public function payInfo()
	{
		$token = I('get.token', '');
		if($token == '') {
			$token = '0';
		}
		
		$tid = I('get.tid', '');
		if($tid == '') {
			$tid = '0';
		}
		
		$orderno = I('get.orderno', '');
		if($orderno == '') {
			$orderno = '0';
		}
		
		$type = I('get.type', '');
		if($type == '') {
			$type = '0';
		}
	
		$retention = I('get.retention', '');
		if($retention == '') {
			$retention = '0';
		}
		
		$money = I('get.money', '');
		if($money == '') {
			$money = '0';
		}
		
		if($token == '0' || $tid == '0' || $orderno == '0' || $type == '0') {
			//参数信息错误
			$this->retMsg(0, 431);
		}
		else if($this->validateToken($token)) {
			//客户端异常
			$this->retMsg(0, 425);
		}
		else if($this->validateTidForOrder($tid)) {
			//支付流水号存在
			$this->retMsg(1, 202);
		}
		else {
			$model = M("AccessOrder");
			$data = $model->where("orderno = '$orderno' and status = 1")->find();
			$price = $data['price'];
			$extra = $data['extra'];
			$profit = $data['profit'] * 0.01;
			$tax = $data['tax'] * 0.01;
			
			$actualprice = round((($price + $extra) * (1 + $profit) + $retention) * (1 + $tax), 2);
			
			$model = M("AccessOrder");
			$model->updateuser = $token;
			$model->updatetime = time();
			$model->tid = $tid;
			$model->retention = $retention;
			$model->actualprice = $actualprice;
			$model->paystatus = 1;
			$model->paytype = $type;
			$model->paymoney = $money;
			$count = $model->where("orderno = '$orderno'")->save();
			if($count) {
				$this->retMsg(1, 200);
			}
			else {
				$this->retMsg(0, 502);
			}
		}
	}
	
	/**
	 * 支付操作
	 * paymentOperation
	 * @return return_type
	 * @throws
	 */
	public function paymentOperation()
	{
		$token = I('get.token', '');
		$orderno = I('get.orderno', '');
		$userName = I('get.name', '');
		$userCardNumber = I('get.cardnumber', '');
		$userCardExpiryMonth = I('get.cardexpirymonth', '');
		$userCardExpiryYear = I('get.cardexpiryyear', '');
		$money = I('get.money', '');
		
		if($token == '' || $orderno == '' || $userName == '' || $userCardNumber == '' || $userCardExpiryMonth == '' || $userCardExpiryYear == '' || $money == '') {
			//参数信息错误
			$this->retMsg(0, 431);
		}
		else if($this->validateToken($token)) {
			//客户端异常
			$this->retMsg(0, 425);
		}
		else if($this->ordernoToId($orderno) == false) {
			//订单不存在
			$this->retMsg(0, 432);
		}
		else if($this->validateTxn($orderno)) {
			//已支付
			$this->retMsg(1, 204);
		}
		else {
			$clientId = $this->tokenToId($token);
			
			$model = new Model();
			$sql = "select b.apikey, b.apipassword
					from wf_sys_clientconfig a, wf_access_owner b
					where a.owner = b.id
					and a.clientid = $clientId";
			$data = $model->query($sql);
			if($data[0]['apikey'] == null || $data[0]['apipassword'] == null) {
				//未配置eway信息
				$this->retMsg(0, 438);
			}
			else {
				$ownApiKey = $data[0]['apikey'];
				$ownApiPassword = $data[0]['apipassword'];
				$obj = payByEway($ownApiKey, $ownApiPassword, $userName, $userCardNumber, $userCardExpiryMonth, $userCardExpiryYear, $money);
				if($obj) {
					$model = M("AccessOrder");
					$model->txn = $obj;
					$count = $model->where("orderno = '$orderno'")->save();
					if($count) {
						//返回银行反馈代号
						$this->retMsg(1, $obj);
					}
					else {
						$this->retMsg(0, 502);
					}
				}
				else {
					$this->retMsg(0, 439);
				}
			}
		}
	}
	
	/**
	 * 柜子使用信息上报接口
	 * setBoxRecord
	 * @return return_type
	 * @throws
	 */
	public function setBoxRecord()
	{
		$token = I('get.token', '');
		if($token == '') {
			$token = '0';
		}
		
		$tid = I('get.tid', '');
		if($tid == '') {
			$tid = '0';
		}
		
		$orderno = I('get.orderno', '');
		if($orderno == '') {
			$orderno = '0';
		}
		
		$type = I('get.type', '');
		if($type == '') {
			$type = '0';
		}
		
		$json_boxno = I('get.bn', '');
		
		if($token == '0' || $tid == '0' || $orderno == '0' || $type == '0' || $json_boxno == '') {
			//参数信息错误
			$this->retMsg(0, 431);
		}
		else if($this->validateToken($token)) {
			//客户端异常
			$this->retMsg(0, 425);
		}
		else if($this->validateTidForBoxrecord($tid)) {
			//操作记录流水号存在
			$this->retMsg(1, 202);
		}
		else {
			$orderId = $this->ordernoToId($orderno);
			$clientId = $this->tokenToId($token);
			if($orderId) {
				//解析柜子信息
				$jsonstring = str_replace('&quot;', '"', $json_boxno);
				$jsonstring = str_replace('\\', '', $jsonstring);
				$boxno = json_decode($jsonstring, true);
				foreach ($boxno as $row) {
					$boxId = $this->boxnoToId($clientId, $row);
					
					/* 解析操作过程 */
					$modelBox = M("SysBox");
					$modelBox->id = $boxId;
					
					$model = M("SysBoxrecord");
					$model->createuser = $token;
					$model->createtime = time();
					$model->boxid = $boxId;
					$model->orderid = $orderId;
					$model->tid = $tid;
					$model->operateuser = $token;
					$model->operatetype = $type;
					$model->operatetime = time();
					
					switch ($type) {
						case '21':
							//更新柜子状态
							$modelBox->storagestatus = '1';
							$modelBox->save();
							//记录操作日志
							$model->remark = L('user_put_into_before') . $row . L('user_put_into_after');
							$model->add();
							break;
						case '22':
							//更新柜子状态
							$modelBox->storagestatus = '0';
							$modelBox->save();
							//记录操作日志
							$model->remark = L('supplier_romove_out_before') . $row . L('supplier_romove_out_after');
							$model->add();
							break;
						case '23':
							//更新柜子状态
							$modelBox->storagestatus = '1';
							$modelBox->save();
							//记录操作日志
							$model->remark = L('supplier_put_into_before') . $row . L('supplier_put_into_after');
							$model->add();
							break;
						case '24':
							//更新柜子状态
							$modelBox->storagestatus = '0';
							$modelBox->save();
							//记录操作日志
							$model->remark = L('user_romove_out_before') . $row . L('user_romove_out_after');
							$model->add();
							//更新订单状态
							$modelOrder = M("AccessOrder");
							$modelOrder->updateuser = $token;
							$modelOrder->updatetime = time();
							$modelOrder->id = $orderId;
							$modelOrder->status = 0;
							$modelOrder->save();
							break;
						default :
							break;
					}
					
					
					
				}
				$this->retMsg(1, 200);
			}
			else {
				$this->retMsg(0, 432);
			}
		}
	}
	
	/**
	 * 雇员身份信息验证
	 * getCardInfo
	 * @return return_type
	 * @throws
	 */
	public function getCardInfo()
	{
		$token = I('get.token', '');
		if($token == '') {
			$token = '0';
		}
		
		$cardno = I('get.cardno', '');
		if($cardno == '') {
			$cardno = '0';
		}
		
		$phone = I('get.phone', '');
		if($phone == '') {
			$phone = '0';
		}
		
		$password = I('get.password', '');
		
		if($token == '0' || ($cardno == '0' && $phone == '0')) {
			//参数信息错误
			$this->retMsg(0, 431);
		}
		else if($this->validateToken($token)) {
			//客户端异常
			$this->retMsg(0, 425);
		}
		else {
			$clientId = $this->tokenToId($token);
			
			$model = new Model();
			$sql = "select
					 c.username
					,c.password
					,c.cardno
					,c.type
					,c.id
					from wf_sys_clientconfig a, wf_access_storeuser b, wf_sys_user c
					where a.supplier = b.storeid
					and b.userid = c.id
					and c.status = 1
					and a.clientid = $clientId";
			if($cardno != '0') {
				//用户卡验证
				$sql = $sql . " and c.cardno = '$cardno'";
				$data = $model->query($sql);
				if($data) {
					$info = array('id'=>$data[0]['id'], 'type'=>$data[0]['type']);
					$ret = array('st'=>1, 'info'=>json_encode($info));
					$this->ajaxReturn($ret);
				}
				else {
					$this->retMsg(0, 434);
				}
			}
			else {
				//手机号码验证
				$sql = $sql . " and c.phone = '$phone'";
				$data = $model->query($sql);
				if($data) {
					$pw = $data[0]['password'];
					$pw1 = md5(md5($password).C('mark'));
					if($pw1 == $pw) {
						$info = array('id'=>$data[0]['id'], 'type'=>$data[0]['type']);
						$ret = array('st'=>1, 'info'=>json_encode($info));
						$this->ajaxReturn($ret);
					}
					else {
						$this->retMsg(0, 434);
					}
				}
				else {
					$this->retMsg(0, 434);
				}
			}
		}
	}
	
	/**
	 * 终端初始化接口
	 * getInitialize
	 * @return return_type
	 * @throws
	 */
	public function getInitialize()
	{
		$token = I('get.token', '');
		if($token == '') {
			$token = '0';
		}
		
		if($this->validateToken($token)) {
			//客户端异常
			$this->retMsg(0, 425);
		}
		else {
			$clientId = $this->tokenToId($token);
			$model = M("SysClientconfig");
			$data = $model->where("clientid = $clientId")->find();
			$storeId = $data['supplier'];
			if($storeId == '') {
				//客户端配置错误
				$this->retMsg(0, 437);
			}
			else {
				//系统参数信息
				$modelConfig = M("SysConfig");
				$dataConfig = $modelConfig->where("sysid = 'access' and status = 1")->select();
				$jsonConfig = array();
				//$i = 0;
				foreach ($dataConfig as $row) {
					$jsonConfig[$row['key']] = $row['value'];
					//$i++;
				}
				//价目信息
				$modelProduct = M("AccessProduct");
				$dataProduct = $modelProduct->where("valid = 1 and storeid = $storeId")->select();
				$jsonProduct = array();
				$i = 0;
				foreach ($dataProduct as $row) {
					$jsonInfo = array(  'productid'=>$row['id'],
										'name'=>$row['name'],
										'code'=>$row['code'],
										'price'=>$row['price'],
										'storeid'=>$row['storeid'],
										'describe'=>$row['describe']);
					$jsonProduct[$i] = $jsonInfo;
					$i++;
				}
				$ret_json = array('config'=>$jsonConfig, 'product'=>$jsonProduct);
				$this->ajaxReturn($ret_json);
			}
		}
	}
	
	/**
	 * 订单号码转id
	 * ordernoToId
	 * @param unknown_type $orderno
	 * @return return_type
	 * @throws
	 */
	public function ordernoToId($orderno)
	{
		$model = M("AccessOrder");
		$data = $model->where("orderno='$orderno'")->find();
		if($data) {
			return $data['id'];
		}
		else {
			return false;
		}
	}
	
	/**
	 * 验证订单流水号
	 * validateTidForOrder
	 * @param unknown_type $tid
	 * @return return_type
	 * @throws
	 */
	public function validateTidForOrder($tid)
	{
		$model = M("AccessOrder");
		$data = $model->where("tid='$tid'")->find();
		if($data) {
			return true;
		}
		else {
			return false;
		}
	}
	
	/**
	 * 验证短信流水号
	 * validateTidForSms
	 * @param unknown_type $tid
	 * @return return_type
	 * @throws
	 */
	public function validateTidForSms($tid)
	{
		$model = M("SysSms");
		$data = $model->where("tid='$tid'")->find();
		if($data) {
			return true;
		}
		else {
			return false;
		}
	}
	
	/**
	 * 验证操作记录流水号
	 * validateTidForBoxrecord
	 * @param unknown_type $tid
	 * @return return_type
	 * @throws
	 */
	public function validateTidForBoxrecord($tid)
	{
		$model = M("SysBoxrecord");
		$data = $model->where("tid='$tid'")->find();
		if($data) {
			return true;
		}
		else {
			return false;
		}
	}
	
	/**
	 * 验证订单是否已支付成功
	 * validateTxn
	 * @param unknown_type $orderno
	 * @return return_type
	 * @throws
	 */
	public function validateTxn($orderno)
	{
		$model = M("AccessOrder");
		$data = $model->where("orderno='$orderno'")->find();
		$txn = $data['txn'];
		if($txn == null) {
			return false;
		}
		else {
			return true;
		}
	}
	
	/**
	 * 测试
	 * test
	 * @return return_type
	 * @throws
	 */
	public function test()
	{
		$nowDate = date('Ymd',time());
		$money = round(19.29375, 2);
		echo $money;
	}
}
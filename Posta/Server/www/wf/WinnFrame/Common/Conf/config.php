<?php
return array(
	//'配置项'=>'配置值'
	'DB_TYPE'=>'mysql',                   // 数据库类型
	'DB_USER'=>'root',                    // 用户名
	'DB_HOST'=>'localhost',         // 服务器地址
	//'DB_HOST'=>'115.29.243.107',         // 服务器地址
	'DB_PWD'=>'winnsen@123',              // 密码 
	//'DB_PWD'=>'winnsen@123',              // 密码
	'DB_PORT'=>3306,                      // 端口 
	'DB_NAME'=>'wfdb',                    // 数据库名 
	'DB_PREFIX'=>'wf_',                   // 数据库表前缀 
	'DB_CHARSET'=>'utf8',                 // 数据库字符集
	
	'mark'=>'b59c67b',//生成密码用的混淆标示
		
	// 显示页面Trace信息
	//'SHOW_PAGE_TRACE'=>true,
	// 默认错误跳转对应的模板文件
	'TMPL_ACTION_ERROR'=>'./dispatch_jump',
	// 默认成功跳转对应的模板文件
	'TMPL_ACTION_SUCCESS'=>'./dispatch_jump',

	'URL_MODEL'=>'2',
	
	'URL_ROUTER_ON'=>true,
	
	'URL_MAP_RULES'=>array(
			'dgd'=>'Comm/Devolve/getDevolve',                  	//心跳包访问
			'drd'=>'Comm/Devolve/retDevolve',                  	//心跳包验证操作
			'rcr'=>'Comm/Register/clientRegister',             	//终端注册
			'rcc'=>'Comm/Register/clientCancel',               	//终端注销
			'ccd'=>'Comm/ClientBase/clientDisable',            	//终端停用
			'cce'=>'Comm/ClientBase/clientEnable',             	//终端启用
			'crc'=>'Comm/ClientBase/remoteControl',            	//终端上报是否接受远程操作
			'cbe'=>'Comm/ClientBase/boxOpenOrClose',            //柜门的开关状态检测
			'cbe'=>'Comm/ClientBase/boxError',             		//柜门异常数据上报
			'cgo'=>'Comm/ClientBase/getOfflineClient',        	//终端掉线数据采集
	
			'vep'=>'Comm/Vending/getEffectPeriod',         		//近效期数据获取
			'veo'=>'Comm/Vending/putEndOperation',         		//上下货完成上报
			'vmc'=>'Comm/Vending/setMilkCard',             		//订奶卡消费接口
			'vsd'=>'Comm/Vending/putSalesData',             	//销售数据上报
			'vos'=>'Comm/Vending/putOutStorage',          		//出库数据上报
			'vpe'=>'Comm/Vending/putError',             		//终端异常上报
	
			'cci'=>'Comm/Certificate/certificateInfo',         	//身份验证并获取证件信息
	
			'ass'=>'Comm/Access/setSmsInfo',					//短信通用接口
			'ago'=>'Comm/Access/getOrderInfo',					//订单信息获取接口
			'aso'=>'Comm/Access/setOrderInfo',					//订单信息上报接口
			'agp'=>'Comm/Access/getActualPrice',				//获取实际应付金额
			'asp'=>'Comm/Access/setActualPrice',				//上报实际应付金额
			'api'=>'Comm/Access/payInfo',						//订单支付接口
			'apo'=>'Comm/Access/paymentOperation',				//支付操作
			'asr'=>'Comm/Access/setBoxRecord',					//柜子使用信息上报接口
			'agc'=>'Comm/Access/getCardInfo',					//雇员身份信息验证
			'agi'=>'Comm/Access/getInitialize',					//终端初始化接口
			
			'lss'=>'Comm/Logistics/setSmsInfo',					//短信通用接口
			'lgo'=>'Comm/Logistics/getOrderInfo',				//订单信息获取接口
			'luo'=>'Comm/Logistics/updateWebOrder',				//用于网购模式的订单数据上报
			'lso'=>'Comm/Logistics/setOrderInfo',				//订单信息上报接口
			'lpi'=>'Comm/Logistics/payInfo',					//订单支付接口
			'lsr'=>'Comm/Logistics/setBoxRecord',				//柜子使用信息上报接口
			'lgc'=>'Comm/Logistics/getCardInfo',				//用户卡身份验证接口
			'lgi'=>'Comm/Logistics/getInitialize',				//终端初始化接口
			'lgv'=>'Comm/Logistics/getVipValid',				//获取会员验证信息
			'lgu'=>'Comm/Logistics/getUserValid',				//获取快递员验证信息
			
			'css'=>'Comm/Charge/setChargeStatus',				//充电状态上报接口
			
			'LogisticsApi'=>'Api/Logistics/setApiInfo',			//物流柜第三方默认接口
			'ChargingApi'=>'Api/Charge/setApiInfo',				//充电柜第三方默认接口
	),
	
	'PROJECT'=>'wf',
		
	//项目
	//'SYSTEM_ID'=>'vending',						//自动售货柜
	//'SYSTEM_ID'=>'certificate',					//证件打印柜
	//'SYSTEM_ID'=>'access',						//自助存取柜
	'SYSTEM_ID'=>'logistics',						//智能物流柜
	//'SYSTEM_ID'=>'charge',						//手机充电柜
	
	//短信配置
	'SMS'=>array(
			'Secret'=>'0stLliRh',
			'Key'=>'xvlvuo',
			'CC'=>'86',								//从数据库获取
			'Sign'=>''
	),
	'GoMo'=>array(
			'User'=>'goCharge',
			'Password'=>'W0rldT3xt!',
			'CID'=>'613',
			'Shortcode'=>'52046',
			'Keyword'=>'ChgNow'
	),
			
);
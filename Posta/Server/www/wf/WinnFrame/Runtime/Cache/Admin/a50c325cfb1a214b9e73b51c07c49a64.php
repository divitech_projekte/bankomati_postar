<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>终端销售情况</title>
</head>
<body>
  	<div style="padding:10px;height:96%;overflow:hidden;">
    	<div id="loading" class="panel-body"></div>
    	<table border="0px" width="100%" height="100%">
    		<tr>
    			<td height="50px" valign="top">
    				<div class="easyui-panel" style="padding:5px;">
    					<form action="" id="tongji3Storage">
    						<table border="0" width="100%">
					    		<tr>
					    			<td class="search-table-td"><?php echo (L("terminal_serial_number")); ?>：</td>
		    						<td class="search-table-td">
										<select class="easyui-combobox" id="logisticsStatistics3client"  editable="false" name="clientid" 
											data-options="
												valueField:'id',
												url:'/wf/Admin/LogisticsStatistics/../LogisticsClient/ajaxSelectList?sysid=logistics',
												width:150">
						    			</select>
									</td>
									
									
									<td class="search-table-td"><?php echo (L("starting_time")); ?>：</td>
									<td class="search-table-td">
										<input class="easyui-datebox" id="logisticsStatistics3_saletime_start" />
									</td>
									<td class="search-table-td"><?php echo (L("end_time")); ?>：</td>
									<td class="search-table-td">
										<input class="easyui-datebox" id="logisticsStatistics3_saletime_end" />
									</td>
									<td>&nbsp;</td>
					    			<td width="60px">
					    				<a href="javascript:void(0)" class="easyui-linkbutton" style="width: 60px"
					    					onclick="searchTongji3StorageForm()"><?php echo (L("query")); ?></a>
					    			</td>
					    			<td width="60px">
					    				<a href="javascript:void(0)" class="easyui-linkbutton" style="width: 60px"
					    					onclick="clearTongji3StorageForm()"><?php echo (L("clear")); ?></a>
					    			</td>
					    		</tr>
					    	</table>
					    	<script type="text/javascript">
					    	function searchTongji3StorageForm(){
					    		
					    		$('#dgLogisticsStatistics3').datagrid({
					                queryParams: {
					                	clientid: $("#logisticsStatistics3client").combobox("getValue"),
					                	saletime_start: $("#logisticsStatistics3_saletime_start").datebox('getValue'),
					                	saletime_end: $("#logisticsStatistics3_saletime_end").datebox('getValue')
					                	
					                }
					        	});
					    	}
							function onLoadEasyUI(){
					    		var tt = getLastMonthFirst().Format("MM/dd/yyyy");
					    		$("#logisticsStatistics3_saletime_start").datebox('setValue',tt);
					    	}
					    	function clearTongji3StorageForm(){
					    		$('#tongji3Storage').form('clear');
					    	}
					    	</script>
    					</form>
    				</div>
    			</td>
    		</tr>
    		
   			<tr>
				<td valign="top">
					<table id="dgLogisticsStatistics3" class="easyui-datagrid" title="<?php echo (L("s0037")); ?>" style="padding-top:20px;"
							data-options="
								rownumbers: true,
								fit: true,
								singleSelect: true,
								showFooter: true,
								url: '/wf/Admin/LogisticsStatistics/../LogisticsStatistics/ajaxStorage3?saletime_start='+getLastMonthFirst().Format('MM/dd/yyyy'),
								method: 'get'">
						<thead>
							<tr>
								<th data-options="field:'id', hidden:true">id</th>
								<th data-options="field:'clientname', width:180"><?php echo (L("terminal_serial_number")); ?></th>
								<th data-options="field:'updatetime', width:200,formatter:formatDateBoxFull,align:'right'"><?php echo (L("s0035")); ?></th>
								<th data-options="field:'paymoney', width:75,align:'right'"><?php echo (L("s0036")); ?></th>
							</tr>
						</thead>
					</table>
				</td>
   			</tr>
    	</table>
	</div>
</body>
</html>
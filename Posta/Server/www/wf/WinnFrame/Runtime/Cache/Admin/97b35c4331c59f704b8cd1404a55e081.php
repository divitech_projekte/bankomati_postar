<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>快递员卡信息管理</title>
</head>
<body>
	<div style="padding:10px;height:96%;overflow:hidden;">
		<div id="loading" class="panel-body"></div>
		<table border="0px" width="100%" height="100%">
			<tr>
				<td height="50px" valign="top">
					<div class="easyui-panel" style="padding:5px;">
						<form id="ffLogisticsCardinfo">
							<table  border="0" width="100%">
								<tr>
									<td class="search-table-td"><?php echo (L("contractor_name")); ?>:</td>
					    			<td class="search-table-td"><input class="easyui-textbox" type="text" id="form_couriername" name="couriername" ></input></td>
					    			<td class="search-table-td"><?php echo (L("account_card_number")); ?>:</td>
					    			<td class="search-table-td"><input class="easyui-textbox" type="text" id="form_cardno" name="cardno" ></input></td>
					    			<td class="search-table-td"><?php echo (L("phone_number")); ?>:</td>
					    			<td class="search-table-td"><input class="easyui-textbox" type="text" id="form_cardno" name="account" ></input></td>
									
									<td>&nbsp;</td>
					    			<td width="60px">
					    				<a href="javascript:void(0)" class="easyui-linkbutton" style="width: 60px"
					    					onclick="searchLogisticsCardinfoForm()"><?php echo (L("query")); ?></a>
					    			</td>
					    			<td width="60px">
					    				<a href="javascript:void(0)" class="easyui-linkbutton" style="width: 60px"
					    					onclick="clearLogisticsCardinfoForm()"><?php echo (L("clear")); ?></a>
					    			</td>
								</tr>
							</table>
							<script type="text/javascript">
							function searchLogisticsCardinfoForm(){
								 $('#dgLogisticsCardinfo').datagrid({
						                queryParams: {
						                	couriername: $("#form_couriername").val(),
						                	cardno: $("#form_cardno").val()
						                }
						        });
							}
							function clearLogisticsCardinfoForm(){
								$('#ffLogisticsCardinfo').form('clear');
							}
							</script>
						</form>
					</div>
				</td>
			</tr>
			<tr>
				<td valign="top">
					<table id="dgLogisticsCardinfo"  class="easyui-datagrid" title="" style="padding-top:20px"
						data-options="
						rownumbers:true,
						fit: true,
						toolbar: '#tbLogisticsCardinfo',
						singleSelect:true,
						pagination:true,
						url:'/wf/Admin/LogisticsCardinfo/../LogisticsCardinfo/ajaxList',
						method:'get'">
						<thead>
							<tr>
								<th data-options="field:'id',width:80,hidden:true">ID</th>
								<th data-options="field:'account',width:100"><?php echo (L("phone_number")); ?></th>
								<th data-options="field:'couriername',width:150"><?php echo (L("contractor_name")); ?></th>
								<th data-options="field:'courierid',width:80,hidden:true">courierid</th>
								<th data-options="field:'nickname',width:100"><?php echo (L("nickname")); ?></th>
								<th data-options="field:'cardno',width:150"><?php echo (L("account_card_number")); ?></th>
								<th data-options="field:'othercardno',width:100"><?php echo (L("s0007")); ?></th>
								<!-- <th data-options="field:'money',width:100"><?php echo (L("residual_amount")); ?></th>
								 
								<th data-options="field:'warn',width:100"><?php echo (L("remind_the_amount")); ?></th>
								<th data-options="field:'unit',width:100"><?php echo (L("toll_rate")); ?></th>-->
								<th data-options="field:'type',width:100,formatter:formatLogisticsCardinfoType"><?php echo (L("role")); ?></th>
								
								<!-- 
								
								<th data-options="field:'email',width:100">邮箱</th>
								<th data-options="field:'regdate',width:100,formatter:formatDateBox">注册日期</th> -->
								<th data-options="field:'status',width:100,formatter:formatLogisticsCardinfoStatus"><?php echo (L("state")); ?></th>
							</tr>
						</thead>
					 </table>
					 <div id="tbLogisticsCardinfo" style="height:auto">
							<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add-item', plain:true"
								onclick="LogisticsCardinfo_List_ToAdd()">&nbsp;<?php echo (L("add")); ?></a>
							<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-edit', plain:true"
								onclick="LogisticsCardinfo_List_ToEdit()">&nbsp;<?php echo (L("edit")); ?></a>
								<!-- 
							<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-delete-item', plain:true"
								onclick="doDel('LogisticsCardinfo','LogisticsCardinfo')">&nbsp;<?php echo (L("del")); ?></a>
								 -->
							 <script type="text/javascript">
							 function LogisticsCardinfo_List_ToEdit(){
								 
								 toEdit('LogisticsCardinfo','LogisticsCardinfo');
								 
								 var row = $('#dgLogisticsCardinfo').datagrid('getSelected');
								 var url = '/wf/Admin/LogisticsCardinfo/../LogisticsCourier/ajaxSelectClientListByCourierid?courierid=' + row.courierid;
							  	$('#LogisticsCardinfo_list_clientids_edit').combobox('reload', url);
							  	
							  	
								 
								//问题没做好编辑界面的下拉联动
							 }
							 
							 function LogisticsCardinfo_List_ToAdd(){
								 var url = '/wf/Admin/LogisticsCardinfo/../LogisticsCourier/ajaxSelectClientListByCourierid?courierid=';
						  		$('#LogisticsCardinfo_list_clientids_add').combobox('reload', url);
								 
								var str = "LogisticsCardinfo";
								 
								 $("#ffxAdd"+str).form('disableValidation');
								 $("#ffxAdd"+str).form('clear');
								 


								 var timestamp=new Date().getTime()+"";
								
								 var hash = MD5(timestamp);
								 
								 $("#LogisticsCardinfo_list_add_othercardno").textbox({'value':hash});
								 $('#dlgAdd'+str).dialog('open');
							 }
							 
							 </script>
					</div>
				</td>
			</tr>
		</table>
		<!-- add_start -->
		<div id="dlgAddLogisticsCardinfo" class="easyui-dialog" title="&nbsp;<?php echo (L("add")); ?>"
			style="width:800px; height:375px; padding:10px;"
			data-options="
				iconCls: 'icon-push-pin',
				toolbar: '#dlgAdd-tbLogisticsCardinfo',
				modal: false,
				closed: true,
				inline: true">
				<div id="dlgAdd-tbLogisticsCardinfo" style="height: auto">
					<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-save', plain:true"
						onclick="doAdd('LogisticsCardinfo','LogisticsCardinfo')">&nbsp;<?php echo (L("save")); ?></a>
					<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-reload', plain:true"
						onclick="clearAddLogisticsCardinfo()">&nbsp;<?php echo (L("clear")); ?></a>
					<script type="text/javascript">
						function clearAddLogisticsCardinfo(){
							$('#ffxAddLogisticsCardinfo').form('clear');
							var url = '/wf/Admin/LogisticsCardinfo/../LogisticsCourier/ajaxSelectClientListByCourierid?courierid=';
				            $('#LogisticsCardinfo_list_clientids_add').combobox('reload', url);
						}
					</script>
				</div>
				<div class="easyui-panel" style="padding: 20px 5px 20px 5px;">
					<form id="ffxAddLogisticsCardinfo" method="post">
						<table border="0" width="100%">
						
						<tr>
								<td class="double-from-lable"><?php echo (L("phone_number")); ?>：</td>
								<td class="double-from-text" >
									<input class="easyui-textbox" type="text" name="account" data-options="required:true" ></input>
								</td>
								<td class="double-from-lable"><?php echo (L("nickname")); ?>：</td>
								<td class="double-from-text" >
									<input class="easyui-textbox" type="text" name="nickname" data-options="" ></input>
								</td>
								
							</tr>
							
							<tr>
								<td class="double-from-lable"><?php echo (L("contractor_name")); ?>：</td>
								<td class="double-from-text">
									<select class="easyui-combobox"  editable="false" name="courierid" style="width: 120px"
										data-options="
													required:true,
													valueField:'id',
													textField:'text',
													url:'/wf/Admin/LogisticsCardinfo/../LogisticsCourier/ajaxSelectList',
													queryParams: {},
													onSelect: function(rec){
											            var url = '/wf/Admin/LogisticsCardinfo/../LogisticsCourier/ajaxSelectClientListByCourierid?courierid='+rec.id;
											            $('#LogisticsCardinfo_list_clientids_add').combobox('reload', url);
											        }">
				    				</select>
								</td>
							</tr>
							<tr>
								
								<td class="double-from-lable"><?php echo (L("account_card_number")); ?>：</td>
								<td class="double-from-text">
									<input class="easyui-textbox" type="text" name="cardno" data-options="required:true"></input>
								</td>
								<td class="double-from-lable"><?php echo (L("s0007")); ?>：</td>
								<td class="double-from-text" >
									<input class="easyui-textbox" type="text" name="othercardno" id="LogisticsCardinfo_list_add_othercardno" data-options="" ></input>
								</td>
							</tr>
							
							<tr>
								<td class="double-from-lable"><?php echo (L("password")); ?>：</td>
								<td class="double-from-text" >
									<input validType="length[4,32]" class="easyui-validatebox easyui-textbox" type="password" id="addLogisticsCardinfoPassword" name="password" data-options=""></input>
									<span style="color:red;"><?php echo (L("six_digits")); ?></span>
								</td>
								<td class="double-from-lable"><?php echo (L("confirm_password")); ?>：</td>
								<td class="double-from-text">
									<input  type="password"  class="easyui-validatebox easyui-textbox" name="passverify" validType="equalTo['#addLogisticsCardinfoPassword']" invalidMessage="两次输入密码不匹配" data-options=""></input>
								</td>
							</tr>
							
							<tr>
								<td class="double-from-lable"><?php echo (L("role")); ?>：</td>
								<td class="double-from-text" >
									<select class="easyui-combobox" name="type" >
										<option value="0"><?php echo (L("courier")); ?></option>
										<option value="1"><?php echo (L("the_administrator")); ?></option>
									</select>
								</td>
								
								<td class="double-from-lable"><?php echo (L("memo")); ?>：</td>
								<td class="double-from-text" >
									<input class="easyui-textbox" type="text" name="remark" data-options="" ></input>
								</td>
							</tr>
							<!-- 
							<tr>
							
								<td class="double-from-lable"><?php echo (L("residual_amount")); ?>：</td>
								<td class="double-from-text">
									<input class="easyui-textbox" type="text" name="money" data-options="required:true"></input>
								</td> 
								<td class="double-from-lable"><?php echo (L("remind_the_amount")); ?>：</td>
								<td class="double-from-text">
									<input class="easyui-textbox" type="text" name="warn" data-options="required:true"></input>
								</td>
							</tr>
							
							<tr>
								<td class="double-from-lable"><?php echo (L("toll_rate")); ?>：</td>
								<td class="double-from-text" colspan="3">
									<input class="easyui-textbox" type="text" name="unit" data-options="" ></input>
								</td>
							</tr> -->
							
							
							<!-- 
							<tr>
								<td class="double-from-lable"><?php echo (L("state")); ?>：</td>
								<td class="double-from-text" colspan="3">
									<select class="easyui-combobox" editable="false" name="status">
				    					<option value="0"><?php echo (L("stop")); ?></option>
				    					<option value="1"><?php echo (L("register_to_be_audited")); ?></option>
				    					<option value="2"><?php echo (L("approved")); ?></option>
				    					<option value="3">审核退回</option>
				    				</select>
								</td>
								
							</tr>
							
			    		 -->
			    			<tr>
				    			<td class="double-from-lable"><?php echo (L("binding_terminal")); ?>：</td>
				    			<td class="double-from-text" colspan="3">
				    				<select class="easyui-combobox" id="LogisticsCardinfo_list_clientids_add"  name="clientids[]" data-options="
				    				multiple:true,
				    				multiline:true,
				    				editable:false,
				    				url:'/wf/Admin/LogisticsCardinfo/../LogisticsCourier/ajaxSelectClientListByCourierid?courierid=0',
									method:'get',
									valueField:'id',
									textField:'text'," 
									style="height: 60px; width: 500px">
				    				
				    				</select>
				    			</td>
				    		</tr>
						</table>
					</form>
				</div>
		</div>
		<!-- add_end -->
		
		
		<!-- edit_start -->
		<div id="dlgEditLogisticsCardinfo" class="easyui-dialog" title="&nbsp;<?php echo (L("edit")); ?>"
			style="width:800px; height:450px; padding:10px;"
			data-options="
				iconCls: 'icon-push-pin',
				toolbar: '#dlgEdit-tbLogisticsCardinfo',
				modal: false,
				closed: true,
				inline: true">
			<div id="dlgEdit-tbLogisticsCardinfo" style="height: auto">
			<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-save', plain:true"
				onclick="doEdit('LogisticsCardinfo', 'LogisticsCardinfo')">&nbsp;<?php echo (L("save")); ?></a>
			</div>
			<div class="easyui-panel" style="padding: 20px 5px 20px 5px;">
				<form id="ffxEditLogisticsCardinfo" method="post">
					<input type="hidden" name="id" />
					<table border="0" width="100%">
							<tr>
								<td class="double-from-lable"><?php echo (L("phone_number")); ?>：</td>
								<td class="double-from-text" >
									<input class="easyui-textbox" type="text" name="account" data-options="required:true" ></input>
								</td>
								<td class="double-from-lable"><?php echo (L("nickname")); ?>：</td>
								<td class="double-from-text" >
									<input class="easyui-textbox" type="text" name="nickname" data-options="" ></input>
								</td>
								
							</tr>
							<tr>
								<td class="double-from-lable"><?php echo (L("contractor_name")); ?>：</td>
								<td class="double-from-text">
									<select class="easyui-combobox"  editable="false" name="courierid" style="width: 120px"
										data-options="
													required:true,
													valueField:'id',
													textField:'text',
													url:'/wf/Admin/LogisticsCardinfo/../LogisticsCourier/ajaxSelectList',
													queryParams: {},
													onSelect: function(rec){
											            var url = '/wf/Admin/LogisticsCardinfo/../LogisticsCourier/ajaxSelectClientListByCourierid?courierid='+rec.id;
											            $('#LogisticsCardinfo_list_clientids_edit').combobox('reload', url);
											        }">
				    				</select>
								</td>
							</tr>
							<tr>
								
								<td class="double-from-lable"><?php echo (L("account_card_number")); ?>：</td>
								<td class="double-from-text">
									<input class="easyui-textbox" type="text" name="cardno" data-options="required:true"></input>
								</td>
								<td class="double-from-lable"><?php echo (L("s0007")); ?>：</td>
								<td class="double-from-text" >
									<input class="easyui-textbox" type="text" name="othercardno" data-options="" ></input>
								</td>
							</tr>
							
							
							<tr>
								<td class="double-from-lable"><?php echo (L("password")); ?>：</td>
								<td class="double-from-text" >
									<input validType="length[4,32]" class="easyui-validatebox easyui-textbox" type="password" id="editLogisticsCardinfoPassword" name="password" data-options=""></input>
									<span style="color:red;"><?php echo (L("six_digits")); ?></span>
								</td>
								<td class="double-from-lable"><?php echo (L("confirm_password")); ?>：</td>
								<td class="double-from-text">
									<input  type="password"  class="easyui-validatebox easyui-textbox" name="passverify" validType="equalTo['#editLogisticsCardinfoPassword']" invalidMessage="两次输入密码不匹配" data-options=""></input>
								</td>
							</tr>
							<tr>
								<td class="double-from-lable"><?php echo (L("role")); ?>：</td>
								<td class="double-from-text" >
									<select class="easyui-combobox" name="type" >
										<option value="0"><?php echo (L("courier")); ?></option>
										<option value="1"><?php echo (L("the_administrator")); ?></option>
									</select>
								</td>
								
								<td class="double-from-lable"><?php echo (L("memo")); ?>：</td>
								<td class="double-from-text" >
									<input class="easyui-textbox" type="text" name="remark" data-options="" ></input>
								</td>
							</tr>
							<!--
							<tr> 
								<td class="double-from-lable"><?php echo (L("residual_amount")); ?>：</td>
								<td class="double-from-text">
									<input class="easyui-textbox" type="text" name="money" data-options="required:true"></input>
								</td> 
								<td class="double-from-lable"><?php echo (L("remind_the_amount")); ?>：</td>
								<td class="double-from-text">
									<input class="easyui-textbox" type="text" name="warn" data-options="required:true"></input>
								</td>
							</tr>
							
							<tr>
								<td class="double-from-lable"><?php echo (L("toll_rate")); ?>：</td>
								<td class="double-from-text" colspan="3">
									<input class="easyui-textbox" type="text" name="unit" data-options="" ></input>
								</td>
							</tr>
							-->
							
							
							<tr>
								<td class="double-from-lable"><?php echo (L("state")); ?>：</td>
								<td class="double-from-text" colspan="3">
									<select class="easyui-combobox" editable="false" name="status" >
				    					<option value="0"><?php echo (L("stop")); ?></option>
				    					<option value="1"><?php echo (L("register_to_be_audited")); ?></option>
				    					<option value="2"><?php echo (L("approved")); ?></option>
				    					<option value="3"><?php echo (L("reject")); ?></option>
				    				</select>
								</td>
								
							</tr>
							<tr>
				    			<td class="double-from-lable"><?php echo (L("binding_terminal")); ?>：</td>
				    			<td class="double-from-text" colspan="3">
				    				<select class="easyui-combobox"  name="clientids[]" id="LogisticsCardinfo_list_clientids_edit" data-options="
					    				multiple:true,
					    				multiline:true,
					    				editable:false,
										method:'get',
										url:'',
										valueField:'id',
										textField:'text',
									" 
									style="height: 60px; width: 500px">
				    				
				    				</select>
				    			</td>
				    		</tr>
			    		
			    		

					</table>
				</form>
			</div>
		</div>
		<script type="text/javascript">
		function onLoadEasyUI(){
			
			
			
		}
		</script>
		<!-- edit_end -->
	</div>
	
</body>
</html>
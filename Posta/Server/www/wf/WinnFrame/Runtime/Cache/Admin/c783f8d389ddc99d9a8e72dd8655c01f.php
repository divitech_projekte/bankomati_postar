<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>短信模板</title>
</head>
<body>
	<div  style="padding: 10px; height: 96%; overflow: hidden;">
		<div id="loading" class="panel-body"></div>
		<table  border="0px" width="100%" height="100%">
			<tr>
				<td height="50px" valign="top">
					<div class="easyui-panel" style="padding:5px;">
	    				<form id="sbLogisticsSmsstencil">
		    				<table border="0" width="100%">
					    		<tr>
					    			<td class="search-table-td"><?php echo (L("template_content")); ?>：</td>
					    			<td class="search-table-td">
					    				<input class="easyui-textbox" type="text" id="sbLogisticsSmsstencilInfo" name="info"></input>
					    			</td>
					    			<td class="search-table-td"><?php echo (L("template_type")); ?>：</td>
					    			<td class="search-table-td">
					    				<select class="easyui-combobox"   name="type" id="sbLogisticsSmsstencilType" style="width: 60px" data-options="">
					    					<option value=""><?php echo (L("all")); ?></option>
											<option value="1"><?php echo (L("sms")); ?></option>
					    					<option value="2"><?php echo (L("email")); ?></option>
					    					<option value="3"><?php echo (L("wechat")); ?></option>		
					    				</select>
					    			</td>
					    			<td class="search-table-td"><?php echo (L("template_application")); ?>：</td>
					    			<td class="search-table-td">
					    				<select class="easyui-combobox"  name="code"  id="sbLogisticsSmsstencilCode" style="width: 180px" data-options="">
					    					<option value=""><?php echo (L("all")); ?></option>
											<option value="10"><?php echo (L("the_customer_login_authentication")); ?></option>
					    					<option value="11"><?php echo (L("deposited_in_the_goods_successfully")); ?></option>
					    					<option value="12"><?php echo (L("items_stored")); ?></option>
					    					<option value="13"><?php echo (L("courier_items_are_removed")); ?></option>
					    					<option value="14"><?php echo (L("items_are_stored_courier")); ?></option>
					    					<option value="15"><?php echo (L("items_taken_out_customers")); ?></option>
					    					<option value="16"><?php echo (L("late_remind")); ?></option>
					    					<option value="17"><?php echo (L("goods_return")); ?></option>
					    					<option value="18"><?php echo (L("sms_to_promote")); ?></option>
					    					<option value="19"><?php echo (L("expiration_reminder")); ?></option>
					    					<option value="20"><?php echo (L("two_hours_expiration_reminder")); ?></option>
					    					<option value="21"><?php echo (L("consumer_credentials")); ?></option>
					    				</select>
					    			</td>
					    			<td class="search-table-td"><?php echo (L("state")); ?>：</td>
					    			<td class="search-table-td">
					    				<select class="easyui-combobox"   name="status" id="sbLogisticsSmsstencilStatus" style="width: 60px" data-options="">
											<option value=""><?php echo (L("all")); ?></option>
											<option value="0"><?php echo (L("stop")); ?></option>
					    					<option value="1"><?php echo (L("start")); ?></option>
					    				</select>
					    			</td>
					    			<td>&nbsp;<span style="color:red;"><?php echo (L("do_not_operate")); ?></span></td>
					    			<td width="60px">
					    				<a href="javascript:void(0)" class="easyui-linkbutton" style="width: 60px"
					    					onclick="searchLogisticsSmsstencil()"><?php echo (L("query")); ?></a>
					    			</td>
					    			<td width="60px">
					    				<a href="javascript:void(0)" class="easyui-linkbutton" style="width: 60px"
					    					onclick="clearLogisticsSmsstencil()"><?php echo (L("clear")); ?></a>
					    			</td>
					    		</tr>
					    	</table>
					    </form>
					</div>
				</td>
			</tr>
			<tr>
				<td valign="top">
					<table id="dgLogisticsSmsstencil" class="easyui-datagrid" title="" style="padding-top:20px"
							data-options="
								rownumbers: true,
								fit: true,
								toolbar: '#tbLogisticsSmsstencil',
								singleSelect: true,
								pagination: true,
								url: '/wf/Admin/LogisticsSmsstencil/../LogisticsSmsstencil/ajaxSmsstencilList',
								method: 'get'">
						<thead>
							<tr>
								<th data-options="field:'id', hidden:true">id</th>
								<th data-options="field:'info', width:600"><?php echo (L("template_content")); ?></th>
								<th data-options="field:'code', width:200,formatter:formatCode"><?php echo (L("template_application")); ?></th>
								<th data-options="field:'type', width:75,formatter:formatType6"><?php echo (L("template_type")); ?></th>
								<th data-options="field:'status', width:75,formatter:formatStatus7"><?php echo (L("state")); ?></th>
							</tr>
						</thead>
					</table>
					<div id="tbLogisticsSmsstencil" style="height:auto">
						<?php if(isHasAuth('/Admin/LogisticsSmsstencil/ajaxAdd')): endif; ?>
							<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add-item', plain:true"
								onclick="toAdd('LogisticsSmsstencil')">&nbsp;<?php echo (L("add")); ?></a>
						
						<?php if(isHasAuth('/Admin/LogisticsSmsstencil/ajaxGetDataById')): endif; ?>
							<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-edit', plain:true"
								onclick="toEdit('LogisticsSmsstencil','LogisticsSmsstencil')">&nbsp;<?php echo (L("edit")); ?></a>
						
						<?php if(isHasAuth('/Admin/LogisticsSmsstencil/ajaxDel')): ?><a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-delete-item', plain:true"
								onclick="doDel('LogisticsSmsstencil','LogisticsSmsstencil')">&nbsp;<?php echo (L("del")); ?></a><?php endif; ?>
						<?php if(isHasAuth('/Admin/LogisticsSmsstencil/ajaxCancel')): endif; ?>
							<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-delete-item', plain:true"
								onclick="doCancelM()">&nbsp;<?php echo (L("stop")); ?></a>
							
					</div>
				</td>
			</tr>
		</table>
		
		
		<div id="dlgAddLogisticsSmsstencil" class="easyui-dialog" title="<?php echo (L("add")); ?>"
			style="width:800px; height:375px; padding:10px;"
			data-options="
				iconCls: 'icon-push-pin',
				toolbar: '#dlgAdd-tbLogisticsSmsstencil',
				modal: false,
				closed: true,
				inline: true">
			<div class="easyui-panel" style="padding: 20px 5px 20px 5px;">
				<form id="ffxAddLogisticsSmsstencil" method="post">
					<input type="hidden" id="milkproductvalid" name="valid"></input>
					<table border="0" width="100%">
						
						
						<tr>
							<td class="double-from-lable"><?php echo (L("template_application")); ?>：</td>
							<td class="double-from-text">
								<select class="easyui-combobox"  name="code" style="width: 180px" data-options="required:true">
									<option value=""><?php echo (L("all")); ?></option>
									<option value="10"><?php echo (L("the_customer_login_authentication")); ?></option>
			    					<option value="11"><?php echo (L("deposited_in_the_goods_successfully")); ?></option>
			    					<option value="12"><?php echo (L("items_stored")); ?></option>
			    					<option value="13"><?php echo (L("courier_items_are_removed")); ?></option>
			    					<option value="14"><?php echo (L("items_are_stored_courier")); ?></option>
			    					<option value="15"><?php echo (L("items_taken_out_customers")); ?></option>
			    					<option value="16"><?php echo (L("late_remind")); ?></option>
			    					<option value="17"><?php echo (L("goods_return")); ?></option>
			    					<option value="18"><?php echo (L("sms_to_promote")); ?></option>
			    					<option value="19"><?php echo (L("expiration_reminder")); ?></option>
			    					<option value="20"><?php echo (L("two_hours_expiration_reminder")); ?></option>
			    					<option value="21"><?php echo (L("consumer_credentials")); ?></option>
			    				</select>
							</td>
							<td class="double-from-lable"><?php echo (L("template_type")); ?>：</td>
							<td class="double-from-text">
								<select class="easyui-combobox"   name="type" style="width: 120px" data-options="required:true">
									<option value=""><?php echo (L("all")); ?></option>
									<option value="1"><?php echo (L("sms")); ?></option>
			    					<option value="2"><?php echo (L("email")); ?></option>
			    					<option value="3"><?php echo (L("wechat")); ?></option>	
			    				</select>
			    				
							</td>
						</tr>
						<tr>
							<td class="double-from-lable"><?php echo (L("state")); ?>：</td>
							<td class="double-from-text">
								<select class="easyui-combobox"   name="status" style="width: 180px" data-options="required:true">
									<option value="0"><?php echo (L("stop")); ?></option>
			    					<option value="1"><?php echo (L("start")); ?></option>
			    				</select>
							</td>
							<td class="double-from-lable"></td>
							<td class="double-from-text">
							</td>
						</tr>
						<tr>
							<td class="double-from-lable"><?php echo (L("template_content")); ?>：</td>
							<td class="double-from-text" colspan="3" data-options="required:true">
								<input class="easyui-textbox" name="info" data-options="multiline:true" style="height: 150px; width: 500px"></input>
							</td>
						</tr>
					</table>
				</form>
			</div>
			<div>
				<table>
				
				</table>
			</div>
		</div>
		<div id="dlgAdd-tbLogisticsSmsstencil" style="height: auto">
			<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-save', plain:true"
				onclick="doAdd('LogisticsSmsstencil','LogisticsSmsstencil')">&nbsp;<?php echo (L("save")); ?></a>
			<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-reload', plain:true"
				onclick="clearAddLogisticsSmsstencil()">&nbsp;<?php echo (L("clear")); ?></a>
		</div>
		
		
		
		
		<div id="dlgEditLogisticsSmsstencil" class="easyui-dialog" title="&nbsp;<?php echo (L("edit")); ?>"
			style="width:800px; height:375px; padding:10px;"
			data-options="
				iconCls: 'icon-push-pin',
				toolbar: '#dlgEdit-tbLogisticsSmsstencil',
				modal: false,
				closed: true,
				inline: true">
			<div class="easyui-panel" style="padding: 20px 5px 20px 5px;">
				<form id="ffxEditLogisticsSmsstencil" method="post">
					<input type="hidden" name="id" />
					<table border="0" width="100%">
						<tr>
							<td class="double-from-lable"><?php echo (L("template_application")); ?>：</td>
							<td class="double-from-text">
								<select class="easyui-combobox"  name="code" style="width: 180px" data-options="disabled:true">
									<option value=""><?php echo (L("all")); ?></option>
									<option value="10"><?php echo (L("the_customer_login_authentication")); ?></option>
			    					<option value="11"><?php echo (L("deposited_in_the_goods_successfully")); ?></option>
			    					<option value="12"><?php echo (L("items_stored")); ?></option>
			    					<option value="13"><?php echo (L("courier_items_are_removed")); ?></option>
			    					<option value="14"><?php echo (L("items_are_stored_courier")); ?></option>
			    					<option value="15"><?php echo (L("items_taken_out_customers")); ?></option>
			    					<option value="16"><?php echo (L("late_remind")); ?></option>
			    					<option value="17"><?php echo (L("goods_return")); ?></option>
			    					<option value="18"><?php echo (L("sms_to_promote")); ?></option>
			    					<option value="19"><?php echo (L("expiration_reminder")); ?></option>
			    					<option value="20"><?php echo (L("two_hours_expiration_reminder")); ?></option>
			    					<option value="21"><?php echo (L("consumer_credentials")); ?></option>
			    				</select>
							</td>
							<td class="double-from-lable"><?php echo (L("template_type")); ?>：</td>
							<td class="double-from-text">
								<select class="easyui-combobox"   name="type" style="width: 120px" data-options="disabled:true">
									<option value=""><?php echo (L("all")); ?></option>
									<option value="1"><?php echo (L("sms")); ?></option>
			    					<option value="2"><?php echo (L("email")); ?></option>
			    					<option value="3"><?php echo (L("wechat")); ?></option>	
			    				</select>
							</td>
						</tr>
						<tr>
							<td class="double-from-lable"><?php echo (L("state")); ?>：</td>
							<td class="double-from-text">
								<select class="easyui-combobox"   name="status" style="width: 180px" >
									<option value="0"><?php echo (L("stop")); ?></option>
			    					<option value="1"><?php echo (L("start")); ?></option>
			    				</select>
							</td>
							<td class="double-from-lable"></td>
							<td class="double-from-text">
							</td>
						</tr>
						<tr>
							<td class="double-from-lable"><?php echo (L("template_content")); ?>：</td>
							<td class="double-from-text" colspan="3">
								<input class="easyui-textbox" name="info" data-options="multiline:true" style="height: 150px; width: 500px"></input>
							</td>
						</tr>
					</table>
				</form>
			</div>
			<div>
				<table>
					
				</table>
			</div>
		</div>
		<div id="dlgEdit-tbLogisticsSmsstencil" style="height: auto">
			<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-save', plain:true"
				onclick="doEdit('LogisticsSmsstencil', 'LogisticsSmsstencil')">&nbsp;<?php echo (L("save")); ?></a>
		</div>
		
		<script type="text/javascript">
			
			//条件查询
			function searchLogisticsSmsstencil()
			{
				$('#dgLogisticsSmsstencil').datagrid({
	                queryParams: {
	                	info: $("#sbLogisticsSmsstencilInfo").val(),
	                	type: $("#sbLogisticsSmsstencilType").combobox("getValue"),
	                	code: $("#sbLogisticsSmsstencilCode").combobox("getValue"),
	                	status: $("#sbLogisticsSmsstencilStatus").combobox("getValue")
	                	
	                }
	        	});
			}
			
			//清空查询条件
			function clearLogisticsSmsstencil()
			{
				$('#sbLogisticsSmsstencil').form('reset');
				
				/* $("#sbLogisticsSmsstencilInfo").val("");
            	 $("#sbLogisticsSmsstencilType").val("");
            	 $("#sbLogisticsSmsstencilCode").val("");
            	 $("#sbLogisticsSmsstencilStatus").val("");*/
			}
			
			//清空添加表单
			function clearAddLogisticsSmsstencil()
			{
				$('#ffxAddLogisticsSmsstencil').form('clear');
			}
			
			//注销
			function doCancelM(){
				var row = $('#dgLogisticsSmsstencil').datagrid('getSelected');
				if (row){
					if('0' != row.status ){
						doCancel('LogisticsSmsstencil','LogisticsSmsstencil');
					}else{
						$.messager.alert('<?php echo (L("warning")); ?>','<?php echo (L("the_state_can_t_operate")); ?>!');
					}
				}else{
					$.messager.alert('<?php echo (L("warning")); ?>','<?php echo (L("please_select_a_data")); ?>!');
				}
				
			}
		
		</script>
	</div>
</body>
</html>
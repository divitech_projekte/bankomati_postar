<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<div  style="padding:20px;">
		<div id="loading" class="panel-body"></div>
		<table>
			<tr>
				<td>
				
				
				
				
		<div>
			<table id="dgAuth" class="easyui-datagrid" title="权限" style="width:550px;height:600px"
					data-options="rownumbers:true,
					singleSelect:true,
					checkOnSelect:false,
					selectOnCheck:false,
					pagination:true,
					url:'/wf/admin/sys_authorization/../SysAuthorization/ajaxListByRole',
					method:'get',
					onSelect:dgAuthOnSelect,
					onCheck:dgAuthOnCheck,
					onUncheck:dgAuthOnUncheck,
					onCheckAll:dgAuthOnCheckAll,
					onUncheckAll:dgAuthOnUncheckAll,
					toolbar:'#tbAuth'">
				<thead>
					<tr>
						<th data-options="field:'ck',checkbox:true,hidden:true"></th>
						<th data-options="field:'id',width:80,hidden:true">ID</th>
						<th data-options="field:'name',width:100">显示名称</th>
						<th data-options="field:'action',width:300">操作</th>
						<th data-options="field:'status',width:50,formatter:formatSysautStatus"><?php echo (L("state")); ?></th>
					</tr>
				</thead>
			</table>
			<div id="tbAuth" style="padding:2px 5px;">
				<form id="ffAuth">
					名称: <input class="easyui-textbox" id="form_one_name" style="width:110px">
					操作: <input class="easyui-textbox" id="form_one_action"  style="width:110px">
					<a href="#" class="easyui-linkbutton" iconCls="icon-search" onclick = "searchAuthForm()">查找</a>
					<a href="javascript:void(0)" class="easyui-linkbutton" onclick="clearAuthForm()">清除</a>
				</form>
			</div>
			<script type="text/javascript">
			
			function searchAuthForm(){
				 $('#dgAuth').datagrid({
		                queryParams: {
		                	name: $("#form_one_name").val(),
		                	action: $("#form_one_action").val(),
		                	roleid:function(){
		                			var row = $('#dgRole').datagrid("getSelected");
		                			return (null==row?"":row.id);
		                		}
		                }
		        });
			}
			function clearAuthForm(){
				$('#ffAuth').form('clear');
			}
			</script>
		</div>
		
	</td>
	<td>
				
				
				
		<div style="margin-left:20px;">
			<table id="dgRole" class="easyui-datagrid" title="角色" style="width:460px;height:600px"
					data-options="rownumbers:true,
					singleSelect:true,
					checkOnSelect:false,
					selectOnCheck:false,
					pagination:true,
					url:'/wf/admin/sys_authorization/../SysRole/ajaxListByAuth',
					method:'get',
					onSelect:dgRoleOnSelect,
					onCheck:dgRoleOnCheck,
					onUncheck:dgRoleOnUncheck,
					onCheckAll:dgRoleOnCheckAll,
					onUncheckAll:dgRoleOnUncheckAll,
					toolbar:'#tbRole'">
				<thead>
					<tr>
						<th data-options="field:'ck',checkbox:true,hidden:true"></th>
						<th data-options="field:'id',width:80,hidden:true">ID</th>
						<th data-options="field:'name',width:100">角色名称</th>
					</tr>
				</thead>
			</table>
			<div id="tbRole" style="padding:2px 5px;">
				<form id="ffRole">
					名称: <input class="easyui-textbox" id="form_two_name" style="width:110px">
					<a href="#" class="easyui-linkbutton" iconCls="icon-search" onclick = "searchRoleForm()">查找</a>
					<a href="javascript:void(0)" class="easyui-linkbutton" onclick="clearRoleForm()">清除</a>
				</form>
			</div>
			<script type="text/javascript">
			function searchRoleForm(){
				 $('#dgRole').datagrid({
		                queryParams: {
		                	name: $("#form_two_name").val(),
		                	authid:function(){
		                			var row = $('#dgAuth').datagrid("getSelected");
		                			return (null==row?"":row.id);
		                		}
		                }
		        });
			}
			function clearRoleForm(){
				$('#ffRole').form('clear');
			}
			</script>
		</div>
		
		</td>
	</tr>
</table>
		<script type="text/javascript">
		function dgAuthOnCheck(index,row){/* 获取角色被选中的ID */ 
			var row1 = $('#dgRole').datagrid("getSelected");
			
			var map = new HashMap(); 
			map.put("authid",row.id); 
			map.put("roleid",row1.id); 
			ajaxPost("/wf/admin/sys_authorization/../SysRole/ajaxAddAuth",map); 
			
			
			
		}
		function dgAuthOnUncheck(index,row){
			var row1 = $('#dgRole').datagrid("getSelected");
			
			var map = new HashMap(); 
			map.put("authid",row.id); 
			map.put("roleid",row1.id); 
			ajaxPost("/wf/admin/sys_authorization/../SysRole/ajaxDelAuth",map); 
		}
		function dgAuthOnCheckAll(rows){
			for(var i=0;i<rows.length;i++) {
				dgAuthOnCheck(i,rows[i]);
			}
		}
		function dgAuthOnUncheckAll(rows){
			for(var i=0;i<rows.length;i++) {
				dgAuthOnUncheck(i,rows[i]);
			}
		}
		
		
		function dgRoleOnCheck(index,row){
			var row1 = $('#dgAuth').datagrid("getSelected");/* 获取权限被选中的ID */
			
			var map = new HashMap(); 
			map.put("authid",row1.id);/* 权限ID */
			map.put("roleid",row.id);/* 角色ID */
			ajaxPost("/wf/admin/sys_authorization/../SysRole/ajaxAddAuth",map);/* 增加关系 */
			
			
		}
		
		function dgRoleOnUncheck(index,row){
			var row1 = $('#dgAuth').datagrid("getSelected");/* 获取权限被选中的ID */
			
			var map = new HashMap(); 
			map.put("authid",row1.id);/* 权限ID */
			map.put("roleid",row.id);/* 角色ID */
			ajaxPost("/wf/admin/sys_authorization/../SysRole/ajaxDelAuth",map);/* 删除关系 */
			
			
		}
		
		function dgRoleOnCheckAll(rows){
			for(var i=0;i<rows.length;i++) {
				dgRoleOnCheck(i,rows[i]);
			}
		}
		
		
		function dgRoleOnUncheckAll(rows){
			for(var i=0;i<rows.length;i++) {
				dgRoleOnUncheck(i,rows[i]);
			}
		}
		function dgAuthOnSelect(index,row){
			
			$('#dgAuth').datagrid('hideColumn','ck');
			$('#dgRole').datagrid('showColumn','ck');
			
			
			$('#dgRole').datagrid({url:'/wf/admin/sys_authorization/../SysRole/ajaxListByAuth',
				queryParams:{
					authid:row.id,
					name: $("#form_two_name").val()
				}
			});
			
			
			return true;
		}
		function dgRoleOnSelect(index,row){
			
			$('#dgRole').datagrid('hideColumn','ck');
			$('#dgAuth').datagrid('showColumn','ck');
			
			$('#dgAuth').datagrid({url:'/wf/admin/sys_authorization/../SysAuthorization/ajaxListByRole',
				queryParams:{
					roleid:row.id,
					name: $("#form_one_name").val(),
                	action: $("#form_one_action").val()
				}
			});
			
			return true;
		}
		
		</script>
		<div style="clear:both;"></div>
	</div>
</body>
</html>
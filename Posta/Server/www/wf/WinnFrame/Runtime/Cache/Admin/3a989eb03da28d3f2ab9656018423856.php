<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>历史短信</title>
</head>
<body>
	<div  style="padding: 10px; height: 96%; overflow: hidden;">
		<div id="loading" class="panel-body"></div>
		<table  border="0px" width="100%" height="100%">
			<tr>
				<td height="50px" valign="top">
					<div class="easyui-panel" style="padding:5px;">
	    				<form id="sbLogisticsSms">
		    				<table border="0" width="100%">
					    		<tr>
					    			<td class="search-table-td"><?php echo (L("order_num")); ?>：</td>
					    			<td class="search-table-td">
					    				<input class="easyui-textbox" type="text" id="sbLogisticsSmsorderno" name="orderno"></input>
					    			</td>
					    			<td class="search-table-td"><?php echo (L("receiver")); ?>：</td>
					    			<td class="search-table-td">
					    				<input class="easyui-textbox" type="text" id="sbLogisticsSmssendee" name="sendee"></input>
					    			</td>
					    			<td>&nbsp;</td>
					    			<td width="60px">
					    				<a href="javascript:void(0)" class="easyui-linkbutton" style="width: 60px"
					    					onclick="searchLogisticsSms()"><?php echo (L("query")); ?></a>
					    			</td>
					    			<td width="60px">
					    				<a href="javascript:void(0)" class="easyui-linkbutton" style="width: 60px"
					    					onclick="clearLogisticsSms()"><?php echo (L("clear")); ?></a>
					    			</td>
					    		</tr>
					    	</table>
					    </form>
					</div>
				</td>
			</tr>
			<tr>
				<td valign="top">
					<table id="dgLogisticsSms" class="easyui-datagrid" title="" style="padding-top:20px"
							data-options="
								rownumbers: true,
								fit: true,
								toolbar: '#tbLogisticsSms',
								singleSelect: true,
								pagination: true,
								url: '/wf/Admin/LogisticsSms/../LogisticsSms/ajaxSmsList',
								method: 'get'">
						<thead>
							<tr>
								<th data-options="field:'id', hidden:true">id</th>
								
							<!--	<th data-options="field:'orderno', width:150"><?php echo (L("related_order")); ?></th> -->
								<th data-options="field:'orders',width:160"><?php echo (L("order_num")); ?></th>
								<th data-options="field:'sendee', width:150"><?php echo (L("receiver")); ?></th>
								
								<th data-options="field:'content', width:150"><?php echo (L("content")); ?></th>
								<!-- <th data-options="field:'sender', width:150"><?php echo (L("sender")); ?></th>  -->
								<th data-options="field:'sendtime', width:150,formatter:formatDateBoxFull"><?php echo (L("send_time")); ?></th>
								
								<!-- <th data-options="field:'tid', width:150"><?php echo (L("transaction_number")); ?></th>  -->
								
								
								
								<!-- <th data-options="field:'info', width:150"><?php echo (L("template_content")); ?></th>  -->
								<th data-options="field:'code', width:200,formatter:formatCode"><?php echo (L("template_application")); ?></th>
								<th data-options="field:'type', width:200,formatter:formatType6"><?php echo (L("template_type")); ?></th>
								
							<!-- 	<th data-options="field:'status', width:250,formatter:formatStatus7"><?php echo (L("state")); ?></th>  
								<th data-options="field:'resendtimes', width:150"><?php echo (L("send_times")); ?></th> -->
							</tr>
						</thead>
					</table>
					<div id="tbLogisticsSms" style="height:auto">
						<!-- 
						<?php if(isHasAuth('/Admin/LogisticsSms/ajaxAdd')): endif; ?>
							<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add-item', plain:true"
								onclick="toAdd('LogisticsSms')">&nbsp;<?php echo (L("add")); ?></a>
						
						<?php if(isHasAuth('/Admin/LogisticsSms/ajaxGetDataById')): endif; ?>
							<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-edit', plain:true"
								onclick="toEdit('LogisticsSms','LogisticsSms')">&nbsp;<?php echo (L("edit")); ?></a>
						
						<?php if(isHasAuth('/Admin/LogisticsSms/ajaxDel')): endif; ?>
							<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-delete-item', plain:true"
								onclick="doDel('LogisticsSms','LogisticsSms')">&nbsp;<?php echo (L("del")); ?></a>
						
						<?php if(isHasAuth('/Admin/LogisticsSms/ajaxCancel')): endif; ?>
							<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-delete-item', plain:true"
								onclick="doCancelM()">&nbsp;<?php echo (L("stop")); ?></a>
						 -->
						 <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-lookup', plain:true"
								onclick="toShow('LogisticsSms','LogisticsSms')">&nbsp;<?php echo (L("view")); ?></a>	
					</div>
				</td>
			</tr>
		</table>
		
		
		
		
		
		
		<div id="dlgShowLogisticsSms" class="easyui-dialog" title="&nbsp;<?php echo (L("to_view_information")); ?>"
			style="width:800px; height:300px; padding:10px;"
			data-options="
				iconCls: 'icon-push-pin',
				toolbar: '#dlgShow-tbLogisticsSms',
				modal: false,
				closed: true,
				inline: true">
			<div class="easyui-panel" style="padding: 20px 5px 20px 5px;">
				<form id="ffxShowLogisticsSms" method="post">
					<input type="hidden" name="id" />
					<table border="0" width="100%">
						<tr>
							<!-- <td class="double-from-lable"><?php echo (L("sender")); ?>：</td>
							<td class="double-from-text">
								<input class="easyui-textbox"  name="sender" data-options="readonly:true" />
							</td> -->
							<td class="double-from-lable"><?php echo (L("send_time")); ?>：</td>
							<td class="double-from-text">
								<input class="easyui-datetimebox"  name="sendtime" data-options="readonly:true" />
							</td>
						</tr>
						<tr>
							<td class="double-from-lable"><?php echo (L("receiver")); ?>：</td>
							<td class="double-from-text">
								<input class="easyui-textbox"  name="sendee" data-options="readonly:true" />
							</td>
							<td class="double-from-lable"><?php echo (L("transaction_number")); ?>：</td>
							<td class="double-from-text">
								<input class="easyui-textbox"  name="tid" data-options="readonly:true" />
							</td>
						</tr>
						
						<tr>
							<!-- <td class="double-from-lable"><?php echo (L("send_times")); ?>：</td>
							<td class="double-from-text">
								<input class="easyui-textbox"  name="resendtimes" data-options="readonly:true" />
							</td> -->
							<td class="double-from-lable"></td>
							<td class="double-from-text">
							</td>
						</tr>
						
						<tr>
							<td class="double-from-lable"><?php echo (L("associated_with_the_order")); ?>：</td>
							<td class="double-from-text">
								<input class="easyui-textbox"  name="orderno" data-options="readonly:true" />
							</td>
							<td class="double-from-lable"><?php echo (L("state")); ?>：</td>
							<td class="double-from-text">
								<select class="easyui-combobox"   name="status" style="width: 180px">
									<option value="0"><?php echo (L("stop")); ?></option>
			    					<option value="1"><?php echo (L("start")); ?></option>
			    				</select>
							</td>
						</tr>
						
						
						
						
						<tr>
							<td class="double-from-lable"><?php echo (L("template_application")); ?>：</td>
							<td class="double-from-text">
								<select class="easyui-combobox"  name="code" style="width: 180px" data-options="readonly:true">
									<option value=""><?php echo (L("all")); ?></option>
									<option value="10"><?php echo (L("the_customer_login_authentication")); ?></option>
			    					<option value="11"><?php echo (L("deposited_in_the_goods_successfully")); ?></option>
			    					<option value="12"><?php echo (L("items_stored")); ?></option>
			    					<option value="13"><?php echo (L("courier_items_are_removed")); ?></option>
			    					<option value="14"><?php echo (L("items_are_stored_courier")); ?></option>
			    					<option value="15"><?php echo (L("items_taken_out_customers")); ?></option>
			    					<option value="16"><?php echo (L("late_remind")); ?></option>
			    					<option value="17"><?php echo (L("goods_return")); ?></option>
			    					<option value="18"><?php echo (L("sms_to_promote")); ?></option>
			    					<option value="19"><?php echo (L("expiration_reminder")); ?></option>
			    					<option value="20"><?php echo (L("two_hours_expiration_reminder")); ?></option>
			    					<option value="21"><?php echo (L("consumer_credentials")); ?></option>
			    				</select>
							</td>
							<td class="double-from-lable"><?php echo (L("template_type")); ?>：</td>
							<td class="double-from-text">
								<select class="easyui-combobox"   name="type" style="width: 120px" data-options="readonly:true">
									<option value=""><?php echo (L("all")); ?></option>
									<option value="1"><?php echo (L("sms")); ?></option>
			    					<option value="2"><?php echo (L("email")); ?></option>
			    					<option value="3"><?php echo (L("wechat")); ?></option>			
			    				</select>
							</td>
						</tr>
						
						<tr>
							<td class="double-from-lable"><?php echo (L("information_content")); ?>：</td>
							<td class="double-from-text" colspan="3">
								<input class="easyui-textbox" name="content" data-options="multiline:true,readonly:true" style="height: 60px; width: 500px"></input>
							</td>
						</tr>
					</table>
				</form>
			</div>
		</div>
		<div id="dlgShow-tbLogisticsSms" style="height: auto">
			<!-- <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-save', plain:true"
				onclick="doEdit('LogisticsSms', 'LogisticsSms')">&nbsp;保存</a>
			 -->
		</div>
		
		<script type="text/javascript">
			
			//条件查询
			function searchLogisticsSms()
			{
				$('#dgLogisticsSms').datagrid({
	                queryParams: {
	                	orderno: $("#sbLogisticsSmsorderno").val(),
	                	sendee: $("#sbLogisticsSmssendee").val(),
	                }
	        	});
			}
			
			//清空查询条件
			function clearLogisticsSms()
			{
				$('#sbLogisticsSms').form('clear');
			}
			//查看详情
			function toShow(str,controller){
				
				
				$("#ffxShow"+str).form('disableValidation');
				var row = $('#dg'+str).datagrid('getSelected');
				if (row){
					
					$("#ffxShow"+str).form('clear');
					
					$('#ffxShow'+str).form('load', '/wf/Admin/LogisticsSms/../'+controller+'/ajaxGetShowById?id='+row.id);
					
					$('#dlgShow'+str).dialog('open');
				}else{
					$.messager.alert('提醒','<?php echo (L("please_select_a_data")); ?>');
				}
			}
		
		</script>
	</div>
</body>
</html>
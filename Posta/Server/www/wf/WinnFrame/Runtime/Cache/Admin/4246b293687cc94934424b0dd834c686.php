<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>销售统计</title>
</head>
<body>
	<div style="margin: 0px; padding: 0px">
		
			<div id="main5" style="height:250px;width:100%;"></div>
			  <script type="text/javascript">
			  var myChart5 = echarts.init(document.getElementById('main5'));
	
			  $(document).ready(function(){
			  	
			  	var myDate=new Date()
			  	
			  	$.ajax({
			  		async:true,/* 同步通讯*/
			  		type:"POST",/*传输方式*/
			  		cache:false,/*下载最新数据（无缓存）*/
			  		url:"/wf/Admin/Logistics/../LogisticsStatistics/ajaxStorage2",/*请求地址*/
			  		data:"month="+(myDate.getMonth()+1)+"&year="+myDate.getFullYear(),
			  		success:function(obj){
			  			//alert(obj);
			  			var json = $.parseJSON(obj);
			  			myChart5.setOption(json);
			  			
			  			
			  			
			  		}
			  	});
			  });
			  </script>
		  
	</div>
</body>
</html>
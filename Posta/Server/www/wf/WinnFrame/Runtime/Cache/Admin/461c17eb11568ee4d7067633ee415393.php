<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<link rel="stylesheet" type="text/css" href="/<?php echo (C("PROJECT")); ?>/Public/jquery-easyui-1.4.4/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="/<?php echo (C("PROJECT")); ?>/Public/jquery-easyui-1.4.4/themes/icon.css">
<link rel="stylesheet" type="text/css" href="/<?php echo (C("PROJECT")); ?>/Public/jquery-easyui-1.4.4/demo/demo.css">

<link rel="stylesheet" type="text/css" href="/<?php echo (C("PROJECT")); ?>/Public/winnsen/css/icon.css">
<link rel="stylesheet" type="text/css" href="/<?php echo (C("PROJECT")); ?>/Public/css/style.css">

<script type="text/javascript" src="/<?php echo (C("PROJECT")); ?>/Public/jquery-easyui-1.4.4/jquery.min.js"></script>
<script type="text/javascript" src="/<?php echo (C("PROJECT")); ?>/Public/jquery-easyui-1.4.4/jquery.easyui.min.js"></script>

<?php if(LANG_SET == 'en-us'): ?><script type="text/javascript" src="/<?php echo (C("PROJECT")); ?>/Public/jquery-easyui-1.4.4/locale/easyui-lang-en.js"></script>
<?php elseif(LANG_SET == 'zh-cn'): ?>
<script type="text/javascript" src="/<?php echo (C("PROJECT")); ?>/Public/jquery-easyui-1.4.4/locale/easyui-lang-zh_CN.js"></script>
<?php else: ?>
<script type="text/javascript" src="/<?php echo (C("PROJECT")); ?>/Public/jquery-easyui-1.4.4/locale/easyui-lang-en.js"></script><?php endif; ?>


<script type="text/javascript" src="/<?php echo (C("PROJECT")); ?>/Public/js/util_hashmap.js"></script>
<script type="text/javascript" src="/<?php echo (C("PROJECT")); ?>/Public/js/util_formatDate.js"></script>
<script type="text/javascript" src="/<?php echo (C("PROJECT")); ?>/Public/js/webtoolkit.md5.js"></script>


<!-- script type="text/javascript" src="/<?php echo (C("PROJECT")); ?>/Public/winnsen/js/curd.js"></script>  -->
<script type="text/javascript" src="/<?php echo (C("PROJECT")); ?>/Admin/Index/formatterJsCurd"></script>


<script type="text/javascript" src="/<?php echo (C("PROJECT")); ?>/Admin/Index/formatterJs"></script>



<?php if((C('SYSTEM_ID') == 'vending')): ?><script type="text/javascript" src="/<?php echo (C("PROJECT")); ?>/Admin/Index/formatterJsVending"></script>
<?php elseif((C('SYSTEM_ID') == 'certificate')): ?>
	<script type="text/javascript" src="/<?php echo (C("PROJECT")); ?>/Admin/Index/formatterJsCertificate"></script>
<?php elseif((C('SYSTEM_ID') == 'access')): ?>
	<script type="text/javascript" src="/<?php echo (C("PROJECT")); ?>/Admin/Index/formatterJsAccess"></script>
<?php elseif((C('SYSTEM_ID') == 'logistics')): ?>
	<script type="text/javascript" src="/<?php echo (C("PROJECT")); ?>/Admin/Index/formatterJsLogistics"></script>
<?php elseif((C('SYSTEM_ID') == 'charge')): ?>
	<script type="text/javascript" src="/<?php echo (C("PROJECT")); ?>/Admin/Index/formatterJsCharge"></script>
<?php else: endif; ?>



<script type="text/javascript" src="/<?php echo (C("PROJECT")); ?>/Public/echarts/js/echarts.min.js"></script>

<style type="text/css">


.tree-node-selected {
    background: #E0ECFF none repeat scroll 0 0;
    color: #000000;
}
.accordion .accordion-header-selected {
    background: #E0ECFF none repeat scroll 0 0;
}
.datagrid-row-selected {
	background: #E0ECFF;
  	color: #000000;
}
.combobox-item-selected {
	background-color: #E0ECFF;
    color: #000000;
}

</style>

<title><?php echo (L("winnsen_network_manager")); ?></title>
</head>
<body class="easyui-layout" >
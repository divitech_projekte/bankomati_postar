<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>使用历史</title>
</head>
<body>
	<div style="padding: 10px; height: 96%; overflow: hidden;">
		<div id="loading" class="panel-body"></div>
		<table border="0px" width="100%" height="100%">
			<tr>
				<td  height="50px" valign="top">
					<div class="easyui-panel" style="padding:5px;">
	    				<form id="sbLogisticsPrint">
		    				<table border="0" width="100%">
					    		<tr>
					    			<!-- 
					    			<td class="search-table-td"><?php echo (L("terminal_name")); ?>：</td>
					    			<td class="search-table-td">
					    				<input class="easyui-textbox" type="text" id="sbLogisticsPrintClientname" name="clientname"></input>
					    			</td>
					    			 -->
					    			
					    			<td class="search-table-td"><?php echo (L("terminal_serial_number")); ?>：</td>
		    						<td class="search-table-td">
										<select class="easyui-combobox" id="sbLogisticsPrintClientname"  editable="false" name="clientid" 
											data-options="
												valueField:'id',
												url:'/wf/Admin/LogisticsBoxrecord/../LogisticsClient/ajaxSelectList?sysid=logistics',
												width:150">
						    			</select>
									</td>
									
									
									
					    			<td class="search-table-td"><?php echo (L("order_num")); ?>：</td>
					    			<td class="search-table-td">
					    				<input class="easyui-textbox" type="text" id="sbLogisticsPrintcardid" name="orderno" ></input>
					    			</td>
					    			<td>&nbsp;</td>
					    			<td width="60px">
					    				<a href="javascript:void(0)" class="easyui-linkbutton" style="width: 60px"
					    					onclick="searchLogisticsPrint()"><?php echo (L("query")); ?></a>
					    			</td>
					    			<td width="60px">
					    				<a href="javascript:void(0)" class="easyui-linkbutton" style="width: 60px"
					    					onclick="clearLogisticsPrint()"><?php echo (L("clear")); ?></a>
					    			</td>
					    		</tr>
					    	</table>
					    </form>
					</div>
				</td>
			</tr>
			<tr>
				<td valign="top">
					<table id="dgLogisticsPrint" class="easyui-datagrid" title="" style="padding-top:20px"
							data-options="
								rownumbers: true,
								fit: true,
								toolbar: '#tbLogisticsPrint',
								singleSelect: true,
								pagination: true,
								url: '/wf/Admin/LogisticsBoxrecord/../LogisticsBoxrecord/ajaxLogisticsPrintList',
								method: 'get'">
						<thead>
							<tr>
								<th data-options="field:'id', hidden:true">id</th>
								<th data-options="field:'clientname', width:150"><?php echo (L("terminal_name")); ?></th>
								<th data-options="field:'position', width:200"><?php echo (L("terminal_position")); ?></th>
								<th data-options="field:'orderno', width:200"><?php echo (L("order_num")); ?></th>
								<th data-options="field:'operatetime', width:200"><?php echo (L("operation_time")); ?></th>
								<th data-options="field:'remark', width:250"><?php echo (L("operation_detail")); ?></th>
							</tr>
						</thead>
					</table>
					<div id="tbLogisticsPrint" style="height:auto">
						<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-print', plain:true"
							onclick="printLogisticsList()">&nbsp;<?php echo (L("print")); ?></a>
					</div>
				</td>
			</tr>
		</table>
		<script type="text/javascript">
			
			//条件查询
			function searchLogisticsPrint()
			{
				$('#dgLogisticsPrint').datagrid({
	                queryParams: {
	                	clientid: $("#sbLogisticsPrintClientname").combobox("getValue"),
	                	orderno: $("#sbLogisticsPrintcardid").val()
	                }
	        	});
			}
			
			//清空查询条件
			function clearLogisticsPrint()
			{
				$('#sbLogisticsPrint').form('clear');
			}
			
			//打印打印清单
			function printLogisticsList()
			{
				$.messager.alert('<?php echo (L("warning")); ?>', '<?php echo (L("function_in_the_development")); ?>…');
			}
		
		</script>
		
	</div>
</body>
</html>
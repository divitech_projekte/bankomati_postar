<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>快递公司信息管理</title>
</head>
<body>
	<div style="padding:10px;height:96%;overflow:hidden;">
		<div id="loading" class="panel-body"></div>
		<table border="0px" width="100%" height="100%">
			<tr>
				<td height="50px" valign="top">
					<div class="easyui-panel" style="padding:5px;">
						<form id="ffLogisticsCourier">
							<table  border="0" width="100%">
								<tr>
									<td class="search-table-td"><?php echo (L("contractor_name")); ?>:</td>
					    			<td class="search-table-td"><input class="easyui-textbox" type="text" id="form_name" name="name" ></input></td>
					    			<td class="search-table-td"><?php echo (L("contractor_code")); ?>:</td>
					    			<td class="search-table-td"><input class="easyui-textbox" type="text" id="form_code" name="code" ></input></td>
									<td>&nbsp;</td>
					    			<td width="60px">
					    				<a href="javascript:void(0)" class="easyui-linkbutton" style="width: 60px"
					    					onclick="searchLogisticsCourierForm()"><?php echo (L("query")); ?></a>
					    			</td>
					    			<td width="60px">
					    				<a href="javascript:void(0)" class="easyui-linkbutton" style="width: 60px"
					    					onclick="clearLogisticsCourierForm()"><?php echo (L("clear")); ?></a>
					    			</td>
								</tr>
							</table>
							<script type="text/javascript">
							function searchLogisticsCourierForm(){
								 $('#dgLogisticsCourier').datagrid({
						                queryParams: {
						                	name: $("#form_name").val(),
						                	code: $("#form_code").val()
						                }
						        });
							}
							function clearLogisticsCourierForm(){
								$('#ffLogisticsCourier').form('clear');
							}
							</script>
						</form>
					</div>
				</td>
			</tr>
			<tr>
				<td valign="top">
					<table id="dgLogisticsCourier"  class="easyui-datagrid" title="" style="padding-top:20px"
						data-options="
						rownumbers:true,
						fit: true,
						toolbar: '#tbLogisticsCourier',
						singleSelect:true,
						pagination:true,
						url:'/wf/Admin/LogisticsCourier/../LogisticsCourier/ajaxList',
						method:'get'">
						<thead>
							<tr>
								<th data-options="field:'id',width:80,hidden:true">ID</th>
								<th data-options="field:'name',width:100,editor:'textbox'"><?php echo (L("contractor_name")); ?></th>
								<th data-options="field:'code',width:100,editor:'textbox'"><?php echo (L("contractor_code")); ?></th>
								<th data-options="field:'short',width:100,editor:'textbox'"><?php echo (L("name_abbr")); ?></th>
								<th data-options="field:'linkman',width:100,editor:'textbox'"><?php echo (L("contact_person")); ?></th>
								<th data-options="field:'phone',width:100,editor:'textbox'"><?php echo (L("phone_number")); ?></th>
								<th data-options="field:'email',width:100,editor:'textbox'"><?php echo (L("email")); ?></th>
								<th data-options="field:'regdate',width:100,editor:'textbox',formatter:formatDateBox"><?php echo (L("register_date")); ?></th>
								<th data-options="field:'status',width:100,editor:'textbox',formatter:formatLogisticsCourierStatus"><?php echo (L("state")); ?></th>
							</tr>
						</thead>
					 </table>
					 <div id="tbLogisticsCourier" style="height:auto">
							<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add-item', plain:true"
								onclick="toAdd('LogisticsCourier')">&nbsp;<?php echo (L("add")); ?></a>
							<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-edit', plain:true"
								onclick="toEdit('LogisticsCourier','LogisticsCourier')">&nbsp;<?php echo (L("edit")); ?></a>
							<!-- <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-delete-item', plain:true"
								onclick="doDel('LogisticsCourier','LogisticsCourier')">&nbsp;<?php echo (L("del")); ?></a>  -->
					</div>
				</td>
			</tr>
		</table>
		<!-- add_start -->
		<div id="dlgAddLogisticsCourier" class="easyui-dialog" title="&nbsp;<?php echo (L("add")); ?>"
			style="width:800px; height:375px; padding:10px;"
			data-options="
				iconCls: 'icon-push-pin',
				toolbar: '#dlgAdd-tbLogisticsCourier',
				modal: false,
				closed: true,
				inline: true">
				<div id="dlgAdd-tbLogisticsCourier" style="height: auto">
					<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-save', plain:true"
						onclick="doAdd('LogisticsCourier','LogisticsCourier')">&nbsp;<?php echo (L("save")); ?></a>
					<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-reload', plain:true"
						onclick="clearAddLogisticsCourier()">&nbsp;<?php echo (L("clear")); ?></a>
					<script type="text/javascript">
						function clearAddLogisticsCourier(){
							$('#ffxAddLogisticsCourier').form('clear');
						}
					</script>
				</div>
				<div class="easyui-panel" style="padding: 20px 5px 20px 5px;">
					<form id="ffxAddLogisticsCourier" method="post">
						<table border="0" width="100%">
							<tr>
								<td class="double-from-lable"><?php echo (L("contractor_name")); ?>：</td>
								<td class="double-from-text" >
									<input class="easyui-textbox" type="text" name="name" data-options="required:true" ></input>
								</td>
								<td class="double-from-lable"><?php echo (L("contractor_code")); ?>：</td>
								<td class="double-from-text">
									<input class="easyui-textbox" type="text" name="code" data-options="required:true"></input>
								</td>
							</tr>
							
							<tr>
								<td class="double-from-lable"><?php echo (L("name_abbr")); ?>：</td>
								<td class="double-from-text" >
									<input class="easyui-textbox" type="text" name="short" data-options="required:true" ></input>
								</td>
								<td class="double-from-lable"><?php echo (L("contact_person")); ?>：</td>
								<td class="double-from-text" >
									<input class="easyui-textbox" type="text" name="linkman" data-options="required:true" ></input>
								</td>
								
							</tr>
							
							<tr>
								<td class="double-from-lable"><?php echo (L("address")); ?>：</td>
								<td class="double-from-text" colspan="3">
									<input class="easyui-textbox" type="text" name="address" data-options="required:true" ></input>
								</td>
							</tr>
							
							<tr>
								<td class="double-from-lable"><?php echo (L("phone_number")); ?>：</td>
								<td class="double-from-text">
									<input class="easyui-textbox" type="text" name="phone" data-options="required:true"></input>
								</td>
							</tr>
							
							<tr>
								<td class="double-from-lable"><?php echo (L("email")); ?>：</td>
								<td class="double-from-text" >
									<input class="easyui-textbox" data-options="validType:'email'" name="email" ></input>
								</td>
								<td class="double-from-lable"><?php echo (L("state")); ?>：</td>
								<td class="double-from-text" >
									<select class="easyui-combobox" editable="false" name="status">
				    					<option value="0"><?php echo (L("stoping")); ?></option>
				    					<option value="1"><?php echo (L("starting")); ?></option>
				    				</select>
								</td>
							</tr>
							
							<!-- logistics_courierclient -->
							
							<tr>
				    			<td class="double-from-lable"><?php echo (L("binding_terminal")); ?>：</td>
				    			<td class="double-from-text" colspan="3">
				    				<select class="easyui-combobox"  name="clientids[]" data-options="
					    				multiple:true,
					    				multiline:true,
					    				editable:false,
					    				url:'/wf/Admin/LogisticsCourier/../LogisticsClient/ajaxSelectList',
										method:'get',
										valueField:'id',
										textField:'text'," 
										style="height: 60px; width: 500px">
				    				
				    				</select>
				    			</td>
				    		</tr>
							
			    		
			    		
						</table>
					</form>
				</div>
		</div>
		<!-- add_end -->
		
		
		<!-- edit_start -->
		<div id="dlgEditLogisticsCourier" class="easyui-dialog" title="&nbsp;<?php echo (L("edit")); ?>"
			style="width:800px; height:375px; padding:10px;"
			data-options="
				iconCls: 'icon-push-pin',
				toolbar: '#dlgEdit-tbLogisticsCourier',
				modal: false,
				closed: true,
				inline: true">
			<div id="dlgEdit-tbLogisticsCourier" style="height: auto">
			<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-save', plain:true"
				onclick="doEdit('LogisticsCourier', 'LogisticsCourier')">&nbsp;<?php echo (L("save")); ?></a>
			</div>
			<div class="easyui-panel" style="padding: 20px 5px 20px 5px;">
				<form id="ffxEditLogisticsCourier" method="post">
					<input type="hidden" name="id" />
					<table border="0" width="100%">
							<tr>
								<td class="double-from-lable"><?php echo (L("contractor_name")); ?>：</td>
								<td class="double-from-text" >
									<input class="easyui-textbox" type="text" name="name" data-options="required:true" ></input>
								</td>
								<td class="double-from-lable"><?php echo (L("contractor_code")); ?>：</td>
								<td class="double-from-text">
									<input class="easyui-textbox" type="text" name="code" data-options="required:true"></input>
								</td>
							</tr>
							
							<tr>
								<td class="double-from-lable"><?php echo (L("name_abbr")); ?>：</td>
								<td class="double-from-text" >
									<input class="easyui-textbox" type="text" name="short" data-options="required:true" ></input>
								</td>
								<td class="double-from-lable"><?php echo (L("contact_person")); ?>：</td>
								<td class="double-from-text" >
									<input class="easyui-textbox" type="text" name="linkman" data-options="required:true" ></input>
								</td>
								
							</tr>
							
							<tr>
								<td class="double-from-lable"><?php echo (L("address")); ?>：</td>
								<td class="double-from-text" colspan="3">
									<input class="easyui-textbox" type="text" name="address" data-options="required:true" ></input>
								</td>
							</tr>
							
							<tr>
								<td class="double-from-lable"><?php echo (L("phone_number")); ?>：</td>
								<td class="double-from-text">
									<input class="easyui-textbox" type="text" name="phone" data-options="required:true"></input>
								</td>
							</tr>
							
							<tr>
								<td class="double-from-lable"><?php echo (L("email")); ?>：</td>
								<td class="double-from-text" >
									<input class="easyui-textbox" data-options="validType:'email'" name="email" ></input>
								</td>
								<td class="double-from-lable"><?php echo (L("state")); ?>：</td>
								<td class="double-from-text" >
									<select class="easyui-combobox" editable="false" name="status">
				    					<option value="0"><?php echo (L("stoping")); ?></option>
				    					<option value="1"><?php echo (L("starting")); ?></option>
				    				</select>
								</td>
							</tr>
							
							
							<tr>
								
								<td class="double-from-lable"><?php echo (L("register_date")); ?>：</td>
								<td class="double-from-text" colspan="3">
									<input class="easyui-datetimebox"  name="regdate" data-options="formatter:formatDateBoxFullByOption,parser:timeStamp,disabled:true"></input>
								</td>
							</tr>
							<!-- logistics_courierclient -->
							
							<tr>
				    			<td class="double-from-lable"><?php echo (L("binding_terminal")); ?>：</td>
				    			<td class="double-from-text" colspan="3">
				    				<select class="easyui-combobox"  name="clientids[]" data-options="
				    				multiple:true,
				    				multiline:true,
				    				editable:false,
				    				url:'/wf/Admin/LogisticsCourier/../LogisticsClient/ajaxSelectList',
									method:'get',
									valueField:'id',
									textField:'text'," 
									style="height: 60px; width: 500px">
				    				
				    				</select>
				    			</td>
				    		</tr>
			    		
			    		

					</table>
				</form>
			</div>
		</div>
		<!-- edit_end -->
	</div>
	
</body>
</html>
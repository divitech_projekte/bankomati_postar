<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>运单信息</title>
</head>
<body>
	<div style="padding:10px;height:96%;overflow:hidden;">
		<div id="loading" class="panel-body"></div>
		<table border="0px" width="100%" height="100%">
			<tr>
				<td height="50px" valign="top">
					<div class="easyui-panel" style="padding:5px;">
						<form id="ffLogisticsOrderOverdueList">
							<table  border="0" width="100%">
								<tr>
									<td class="search-table-td"><?php echo (L("terminal_serial_number")); ?>：</td>
		    						<td class="search-table-td">
										<select class="easyui-combobox" id="LogisticsOrderOverdueListClientid"  editable="false" name="clientid" 
											data-options="
												valueField:'id',
												url:'/wf/Admin/LogisticsOrder/../LogisticsClient/ajaxSelectList?sysid=logistics',
												width:150">
						    			</select>
									</td>
									<td class="search-table-td"><?php echo (L("order_num")); ?>:</td>
					    			<td class="search-table-td"><input class="easyui-textbox" type="text" id="form_LogisticsOrderInfo_orderno" name="orderno" ></input></td>
					    			<!-- <td class="search-table-td"><?php echo (L("operator")); ?>:</td>
					    			<td class="search-table-td"><input class="easyui-textbox" type="text" id="form_LogisticsOrderInfo_telphone" name="telphone" ></input></td> -->
									<td class="search-table-td"><?php echo (L("operator")); ?>:</td>
					    			<td class="search-table-td"><input class="easyui-textbox" type="text" id="form_LogisticsOrderInfo_orders" name="orders" ></input></td>
									
									
									<td>&nbsp;</td>
					    			<td width="60px">
					    				<a href="javascript:void(0)" class="easyui-linkbutton" style="width: 60px"
					    					onclick="searchLogisticsOrderOverdueListForm()"><?php echo (L("query")); ?></a>
					    			</td>
					    			<td width="60px">
					    				<a href="javascript:void(0)" class="easyui-linkbutton" style="width: 60px"
					    					onclick="clearLogisticsOrderOverdueListForm()"><?php echo (L("clear")); ?></a>
					    			</td>
								</tr>
							</table>
							<script type="text/javascript">
							function searchLogisticsOrderOverdueListForm(){
								 $('#dgLogisticsOrderOverdueList').datagrid({
						                queryParams: {
						                	clientid: $("#LogisticsOrderOverdueListClientid").combobox('getValue'),
						                	orderno: $("#form_LogisticsOrderInfo_orderno").val(),
						                	telphone: $("#form_LogisticsOrderInfo_telphone").val(),
						                	orders:$("#form_LogisticsOrderInfo_orders").val(),
						                }
						        });
							}
							function clearLogisticsOrderOverdueListForm(){
								$('#ffLogisticsOrderOverdueList').form('clear');
							}
							</script>
						</form>
					</div>
				</td>
			</tr>
			<tr>
				<td valign="top">
					<table id="dgLogisticsOrderOverdueList"  class="easyui-datagrid" title="" style="padding-top:20px"
						data-options="
						rownumbers:true,
						fit: true,
						toolbar: '#tbLogisticsOrderOverdueList',
						singleSelect:true,
						pagination:true,
						url:'/wf/Admin/LogisticsOrder/../LogisticsOrder/ajaxOverdueList',
						method:'get'">
						<thead>
							<tr>
								<th data-options="field:'id',width:80,hidden:true">ID</th>
								<th data-options="field:'orders',width:200"><?php echo (L("order_num")); ?></th>
								<th data-options="field:'createtime',width:146,formatter:formatDateBoxFull"><?php echo (L("waybill_generation_time")); ?></th>
								
								
								<th data-options="field:'telphone',width:100"><?php echo (L("operator")); ?></th>
								
								<th data-options="field:'password',width:100"><?php echo (L("extract_password")); ?></th>
								
								<th data-options="field:'clientname',width:100"><?php echo (L("terminal_serial_number")); ?></th>
								<th data-options="field:'couriername',width:100"><?php echo (L("express_company")); ?></th>
								
								 <th data-options="field:'paymoney',width:100"><?php echo (L("amount_paid")); ?></th>
								
								<th data-options="field:'status',width:100,formatter:formatLogisticsOrderStatus"><?php echo (L("state")); ?></th>
								
								
								
							</tr>
						</thead>
					 </table>
					 <div id="tbLogisticsOrderOverdueList" style="height:auto">
					
							<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-lookup', plain:true"
								onclick="toLogisticsOrderView('LogisticsOrderOverdueList','LogisticsOrder')">&nbsp;<?php echo (L("view")); ?></a>
								
							
								
					</div>
					<script type="text/javascript">
					
					
					function toLogisticsOrderView(str,controller){
						$("#ffxView"+str).form('disableValidation');
						var row = $('#dg'+str).datagrid('getSelected');
						if (row){
							$("#ffxView"+str).form('clear');
							
							$('#ffxView'+str).form('load', '/wf/Admin/LogisticsOrder/../'+controller+'/ajaxGetDataById?id='+row.id);
							
							$('#dlgView'+str).dialog('open');
							
							
							
							
							$('#LogisticsOrderOverdueList').datalist({
							    url: '/wf/Admin/LogisticsOrder/../LogisticsBoxrecord/ajaxLogisticsBoxrecordList?clientid='+row.clientid+"&orderid="+row.id
							});
							
						}else{
							$.messager.alert("<?php echo (L("warning")); ?>","<?php echo (L("please_select_a_data")); ?>");
						}
					}
					</script>
				</td>
			</tr>
		</table>
		
		
		<!-- view_start -->
		<div id="dlgViewLogisticsOrderOverdueList" class="easyui-dialog" title="&nbsp;<?php echo (L("view")); ?>"
			style="width:900px; height:400px; padding:10px;"
			data-options="
				iconCls: 'icon-push-pin',
				modal: false,
				closed: true,
				inline: true">
			
			<div class="easyui-panel" style="padding: 20px 5px 20px 5px;">
				<form id="ffxViewLogisticsOrderOverdueList" method="post">
					<input type="hidden" name="id" />
					<table border="0" width="100%">
							<tr>
								<td class="double-from-lable"><?php echo (L("order_num")); ?>：</td>
								<td class="double-from-text" >
									<input class="easyui-textbox" type="text" name="orders" data-options="required:true,readonly:true" ></input>
								</td>
								<td class="double-from-lable"><?php echo (L("type")); ?>：</td>
								<td class="double-from-text" >
									<select class="easyui-combobox"  name="type" style="width: 180px" data-options="required:true,readonly:true">
										<option value="1"><?php echo (L("delivery_list")); ?></option>
				    					<option value="2"><?php echo (L("delivery_note")); ?></option>
				    				</select>
								</td>
								
							</tr>
							
							<tr>
								<td class="double-from-lable"><?php echo (L("express_company")); ?>：</td>
								<td class="double-from-text">
									<select class="easyui-combobox"  editable="false" name="courierid" style="width: 120px"
										data-options="
													required:true,
													valueField:'id',
													textField:'text',
													readonly:true,
													url:'/wf/Admin/LogisticsOrder/../LogisticsCourier/ajaxSelectList',
													queryParams: {}">
				    				</select>
								</td>
								<td class="double-from-lable"><?php echo (L("terminal_serial_number")); ?>：</td>
								<td class="double-from-text">
									<select class="easyui-combobox"  editable="false" name="clientid" style="width: 120px"
										data-options="
													required:true,
													valueField:'id',
													textField:'text',
													readonly:true,
													url:'/wf/Admin/LogisticsOrder/../LogisticsClient/ajaxSelectList?sysid=logistics',
													">
				    				</select>
								</td>
							</tr>
							<tr>
								
								<td class="double-from-lable"><?php echo (L("operator")); ?>：</td>
								<td class="double-from-text">
									<input class="easyui-textbox" type="text" name="telphone" data-options="required:true,readonly:true"></input>
								</td>
								<!-- 
								<td class="double-from-lable"><?php echo (L("order_amount")); ?>：</td>
								<td class="double-from-text" >
									<input class="easyui-textbox" type="text" name="price" data-options="readonly:true" ></input>
								</td>  -->
							<!-- </tr>
							
							
							<tr>
								<td class="double-from-lable"><?php echo (L("retention_money")); ?>：</td>
								<td class="double-from-text" >
									<input class="easyui-textbox" type="text" name="retention" data-options="readonly:true" ></input>
								</td> 
								<td class="double-from-lable"><?php echo (L("payment_status")); ?>：</td>
								<td class="double-from-text" >
									<select class="easyui-combobox"  name="paystatus" style="width: 180px" data-options="required:true,readonly:true">
										<option value="0"><?php echo (L("unpaid")); ?></option>
				    					<option value="1"><?php echo (L("paid")); ?></option>
				    				</select>
								</td>
							</tr>
							
							<tr>-->
								<!-- <td class="double-from-lable"><?php echo (L("amount_paid")); ?>：</td>
								<td class="double-from-text">
									<input class="easyui-textbox" type="text" name="paymoney" data-options="required:true,readonly:true"></input>
								</td> -->
								<td class="double-from-lable"><?php echo (L("state")); ?>：</td>
								<td class="double-from-text" >
									<select class="easyui-combobox" editable="false" name="status" data-options="readonly:true">
				    					<option value="0"><?php echo (L("complete_the_waybill")); ?></option>
				    					<option value="1"><?php echo (L("in_the_air_waybill")); ?></option>
				    					<option value="2"><?php echo (L("receiving_waybill")); ?></option>
				    					<option value="3"><?php echo (L("abnormal_end")); ?></option>
				    				</select>
								</td>
							</tr>
							
							
			    			<tr>
								<td class="double-from-lable"><?php echo (L("waybill_generation_time")); ?>：</td>
								<td class="double-from-text">
									<input class="easyui-datetimebox" type="text" name="createtime" data-options="formatter:formatDateBoxFullByOption,parser:timeStamp,required:true,readonly:true"></input>
								</td>
								<td class="double-from-lable"><?php echo (L("the_last_update_time")); ?>：</td>
								<td class="double-from-text">
									<input class="easyui-datetimebox" type="text" name="updatetime" data-options="formatter:formatDateBoxFullByOption,parser:timeStamp,required:true,readonly:true"></input>
								</td>
							</tr>
			    			<tr>
								<td class="double-from-lable"><?php echo (L("amount_paid")); ?>：</td>
								<td class="double-from-text" colspan="3">
									<input class="easyui-textbox" type="text" name="paymoney" data-options="required:true,readonly:true"></input>
								</td>
							</tr>

					</table>
				</form>
			</div>
			<div class="easyui-panel"  style="padding: 20px 5px 20px 5px;margin-top:10px;width:100%;height:135px;">
						<div id="LogisticsOrderOverdueList" class="easyui-datalist">
						
						</div>
			</div>
		</div>
		<!-- view_end -->
	</div>
	
</body>
</html>
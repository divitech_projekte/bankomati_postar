<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<div  style="padding:20px;">
		<div id="loading" class="panel-body"></div>
		<table>
			<tr>
				<td>
				
				
		<div>
			<table id="dgUser2" class="easyui-datagrid" title="账户" style="width:550px;height:600px"
					data-options="rownumbers:true,
					singleSelect:true,
					checkOnSelect:false,
					selectOnCheck:false,
					pagination:true,
					url:'/wf/admin/sys_company/../SysUser/ajaxListByCompany',
					method:'get',
					onSelect:dgUser2OnSelect,
					onCheck:dgUser2OnCheck,
					onUncheck:dgUser2OnUncheck,
					onCheckAll:dgUser2OnCheckAll,
					onUncheckAll:dgUser2OnUncheckAll,
					toolbar:'#tbUser2'">
				<thead>
					<tr>
						<th data-options="field:'ck',checkbox:true,hidden:true"></th>
						<th data-options="field:'id',width:80,hidden:true">ID</th>
						<th data-options="field:'username',width:100">账户</th>
						<th data-options="field:'display',width:100">昵称</th>
						<th data-options="field:'rolename',width:300">拥有的角色</th>
					</tr>
				</thead>
			</table>
			<div id="tbUser2" style="padding:2px 5px;">
				<form id="ffUser2">
					账户: <input class="easyui-textbox" id="form_one_username" style="width:110px">
					昵称: <input class="easyui-textbox" id="form_one_display"  style="width:110px">
					<a href="#" class="easyui-linkbutton" iconCls="icon-search" onclick = "searchUser2Form()">查找</a>
					<a href="javascript:void(0)" class="easyui-linkbutton" onclick="clearUser2Form()">清除</a>
				</form>
			</div>
			<script type="text/javascript">
			function searchUser2Form(){
				 $('#dgUser2').datagrid({
		                queryParams: {
		                	username: $("#form_one_username").val(),
		                	display: $("#form_one_display").val(),
		                	companyid:function(){
		                			var row = $('#dgCompany').datagrid("getSelected");
		                			return (null==row?"":row.id);
		                		}
		                }
		        });
			}
			function clearUser2Form(){
				$('#ffUser2').form('clear');
			}
			</script>
		</div>
		</td>
				<td>
				
				
		<div style="margin-left:20px;">
			<table id="dgCompany" class="easyui-datagrid" title="公司" style="width:460px;height:600px"
					data-options="rownumbers:true,
					singleSelect:true,
					checkOnSelect:false,
					selectOnCheck:false,
					pagination:true,
					url:'/wf/admin/sys_company/../SysCompany/ajaxListByUser',
					method:'get',
					onSelect:dgCompanyOnSelect,
					onCheck:dgCompanyOnCheck,
					onUncheck:dgCompanyOnUncheck,
					onCheckAll:dgCompanyOnCheckAll,
					onUncheckAll:dgCompanyOnUncheckAll,
					toolbar:'#tbCompany'">
				<thead>
					<tr>
						<th data-options="field:'ck',checkbox:true,hidden:true"></th>
						<th data-options="field:'id',width:80,hidden:true">ID</th>
						<th data-options="field:'name',width:100">公司名称</th>
					</tr>
				</thead>
			</table>
			<div id="tbCompany" style="padding:2px 5px;">
				<form id="ffCompany">
					名称: <input class="easyui-textbox" id="form_two_name" style="width:110px">
					<a href="#" class="easyui-linkbutton" iconCls="icon-search" onclick = "searchRoleForm()">查找</a>
					<a href="javascript:void(0)" class="easyui-linkbutton" onclick="clearRoleForm()">清除</a>
				</form>
			</div>
			<script type="text/javascript">
			function searchRoleForm(){
				 $('#dgCompany').datagrid({
		                queryParams: {
		                	name: $("#form_two_name").val(),
		                	userid:function(){
		                			var row = $('#dgUser2').datagrid("getSelected");
		                			return (null==row?"":row.id);
		                		}
		                }
		        });
			}
			function clearRoleForm(){
				$('#ffCompany').form('clear');
			}
			</script>
		</div>
		</td>
			</tr>
		</table>
		<script type="text/javascript">
		function dgUser2OnCheck(index,row){/* 获取角色被选中的ID */ 
			var row1 = $('#dgCompany').datagrid("getSelected");
			
			var map = new HashMap(); 
			map.put("userid",row.id); 
			map.put("companyid",row1.id); 
			ajaxPost("/wf/admin/sys_company/../SysCompany/ajaxAddUser",map); 
			
			
		}
		function dgUser2OnUncheck(index,row){
			var row1 = $('#dgCompany').datagrid("getSelected");
			
			var map = new HashMap(); 
			map.put("userid",row.id); 
			map.put("companyid",row1.id); 
			ajaxPost("/wf/admin/sys_company/../SysCompany/ajaxDelUser",map); 
		}
		function dgUser2OnCheckAll(rows){
			for(var i=0;i<rows.length;i++) {
				dgUser2OnCheck(i,rows[i]);
			}
		}
		function dgUser2OnUncheckAll(rows){
			for(var i=0;i<rows.length;i++) {
				dgUser2OnUncheck(i,rows[i]);
			}
		}
		
		
		function dgCompanyOnCheck(index,row){
			var row1 = $('#dgUser2').datagrid("getSelected");/* 获取权限被选中的ID */
			
			var map = new HashMap(); 
			map.put("userid",row1.id);/* 权限ID */
			map.put("companyid",row.id);/* 角色ID */
			ajaxPost("/wf/admin/sys_company/../SysCompany/ajaxAddUser",map);/* 增加关系 */
			
			
		}
		
		function dgCompanyOnUncheck(index,row){
			var row1 = $('#dgUser2').datagrid("getSelected");/* 获取权限被选中的ID */
			
			var map = new HashMap(); 
			map.put("userid",row1.id);/* 权限ID */
			map.put("companyid",row.id);/* 角色ID */
			ajaxPost("/wf/admin/sys_company/../SysCompany/ajaxDelUser",map);/* 删除关系 */
			
			
		}
		
		function dgCompanyOnCheckAll(rows){
			for(var i=0;i<rows.length;i++) {
				dgCompanyOnCheck(i,rows[i]);
			}
		}
		
		
		function dgCompanyOnUncheckAll(rows){
			for(var i=0;i<rows.length;i++) {
				dgCompanyOnUncheck(i,rows[i]);
			}
		}
		function dgUser2OnSelect(index,row){
			
			$('#dgUser2').datagrid('hideColumn','ck');
			$('#dgCompany').datagrid('showColumn','ck');
			
			
			$('#dgCompany').datagrid({url:'/wf/admin/sys_company/../SysCompany/ajaxListByUser',
				queryParams:{
					userid:row.id,
					name: $("#form_two_name").val()
				}
			});
			
			
			return true;
		}
		function dgCompanyOnSelect(index,row){
			
			$('#dgCompany').datagrid('hideColumn','ck');
			$('#dgUser2').datagrid('showColumn','ck');
			
			$('#dgUser2').datagrid({url:'/wf/admin/sys_company/../SysUser/ajaxListByCompany',
				queryParams:{
					companyid:row.id,
					username: $("#form_one_username").val(),
                	display: $("#form_one_display").val()
				}
			});
			
			return true;
		}
		
		</script>
		<div style="clear:both;"></div>
	</div>
</body>
</html>
<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>终端运行状态</title>
</head>
<body>
	<div style="padding: 10px; height: 96%; overflow: hidden;">
		<div id="loading" class="panel-body"></div>
		
		<table border="0px" width="100%" height="100%">
			<tr>
				<td height="50px" valign="top">
					<div class="easyui-panel" style="padding:5px;">
			 			<form id="ffLogisticsClient2">
			    			<table border="0" width="100%">
			    				<tr>
			    					<td class="search-table-td"><?php echo (L("terminal_name")); ?>：</td>
		    						<td class="search-table-td">
		    							<input class="easyui-textbox" type="text" id="form_accessClientList2_name" name="name" ></input>
		    						</td>
		    						<td class="search-table-td"><?php echo (L("terminal_serial_number")); ?>：</td>
		    						<td class="search-table-td">
		    							<input class="easyui-textbox" type="text" id="form_accessClientList2_csn" name="csn" ></input>
		    						</td>
		    						<td>&nbsp;</td>
			    					<td width="60px">
		    							<a href="javascript:void(0)" class="easyui-linkbutton" style="width: 60px"
		    								onclick="searchLogisticsClientList2Form()"><?php echo (L("query")); ?></a>
		    						</td>
		    						<td width="60px">
					    				<a href="javascript:void(0)" class="easyui-linkbutton" style="width: 60px"
					    					onclick="clearLogisticsClientList2Form()"><?php echo (L("clear")); ?></a>
					    			</td>
			    				</tr>
			    			</table>
			    		</form>
			    	</div>
			    	<script type="text/javascript">
					function searchLogisticsClientList2Form(){
						 $('#dgLogisticsClient2').datagrid({
				                queryParams: {
				                	name: $("#form_accessClientList2_name").val(),
				                	csn: $("#form_accessClientList2_csn").val()
				                }
				        });
					}
					function clearLogisticsClientList2Form(){
						$('#ffLogisticsClient2').form('clear');
					}
			</script>
				</td>
			</tr>
			
		 	<tr>
				<td valign="top">
					<table id="dgLogisticsClient2" class="easyui-datagrid" title=""
							data-options="rownumbers:true,
							fit : true,
							toolbar: '#tbSysclientLogisticsStatusList',
							singleSelect:true,
							pagination:true,
							url:'/wf/Admin/LogisticsClient/../LogisticsClient/ajaxLogisticsClientStatusList',
							method:'get',
							onDblClickRow : function(index,row){
								detailAccessBox();
							}">
						<thead>
							<tr>
								<th data-options="field:'id', hidden:true">id</th>
								<th data-options="field:'name', width:150"><?php echo (L("terminal_name")); ?></th>
								<th data-options="field:'csn', width:150"><?php echo (L("terminal_serial_number")); ?></th>
								<th data-options="field:'position', width:200"><?php echo (L("terminal_position")); ?></th>
								<th data-options="field:'type', width:100, formatter:formatSysClientListType"><?php echo (L("terminal_type")); ?></th>
								<th data-options="field:'status',width:100,formatter:formatSysClientListStatus"><?php echo (L("state")); ?></th>
								<!-- <th data-options="field:'isremote',width:100,formatter:formatSysClientListIsOpen"><?php echo (L("remote_control")); ?></th>  -->
								<th data-options="field:'isonline',width:100,formatter:formatSysClientListIsOnLine"><?php echo (L("isonline")); ?></th>
								<!-- <th data-options="field:'idx',width:400,formatter:formatSysClientOperate"><?php echo (L("operation")); ?></th>  -->
							</tr>
						</thead>
					</table>
					<div id="tbSysclientLogisticsStatusList" style="height:auto">
						<div id="milkboxlisttitle" style="display: none;" title="<?php echo (L("box_state")); ?>"><?php echo (L("box_state")); ?></div>
						<a href="javascript:void(0)" 
							class="easyui-linkbutton" data-options="iconCls:'icon-reload',plain:true" onclick="toRebootSysclient()">&nbsp;<?php echo (L("reboot")); ?></a>
						<a href="javascript:void(0)" 
							class="easyui-linkbutton" data-options="iconCls:'icon-network-pc',plain:true" onclick="doStartSysclient()">&nbsp;<?php echo (L("start")); ?></a>
						<a href="javascript:void(0)" 
							class="easyui-linkbutton" data-options="iconCls:'icon-shut-down',plain:true" onclick="doStopSysclient()">&nbsp;<?php echo (L("stop")); ?></a>
						<!-- <a href="javascript:void(0)" 
							class="easyui-linkbutton" data-options="iconCls:'icon-lookup',plain:true" onclick="detailAccessBox()">&nbsp;<?php echo (L("view")); ?></a> -->
						
					</div>
				</td>
			</tr>
		</table>
		
		
		
		
		
		<div id="dlgSysclientReboot" class="easyui-dialog" title="<?php echo (L("reboot")); ?>" style="width:350px;height:150px;"
			data-options="
				iconCls: 'icon-reload',
				modal:false,
				closed:true,
				inline:true
			">
		
			<div style="padding:10px 60px 20px 35px">
			    <form id="ffxSysclientstatus" method="post" >
			    	<input type="hidden" name="id" />
			    	<table cellpadding="5">
			    		
			    		
			    		
			    		<tr>
			    			<td><?php echo (L("ready_to_restart")); ?>:</td>
			    			<td>
			    				<select class="easyui-combobox" editable="false" name="mode" data-options="width:100">
			    					<option value="1"><?php echo (L("terminal")); ?></option>
			    					<option value="2"><?php echo (L("software")); ?></option>
			    				</select>
			    			</td>
			    		</tr>
			    		
			    	</table>
			    </form>
			    <div style="text-align:center;padding:5px">
			    	<a href="javascript:void(0)" class="easyui-linkbutton" onclick="doRebootSysclient()"><?php echo (L("confirm")); ?></a>
			    </div>
		    </div>
		
	</div>
		<script type="text/javascript">
		function detailAccessBox()
		{
			var row = $('#dgLogisticsClient2').datagrid('getSelected');
			if(row) {
				$('#milkboxlisttitle').get(0).title = '<?php echo (L("box_state")); ?>' + "-" + row.name;//+"("+row.csn+")";
				addTab($('#milkboxlisttitle').get(0), '/wf/Admin/LogisticsClient/../LogisticsBox/logisticsBoxStatusList?clientid=' + row.id);
			}
			else {
				$.messager.alert('<?php echo (L("warning")); ?>', '<?php echo (L("please_select_a_data")); ?>');
			}
		}
		
		function toRebootSysclient(){
			var row = $('#dgLogisticsClient2').datagrid('getSelected');
			if (row){
				if('1' == row.status ){
					$('#ffxSysclientstatus').form('load', '/wf/Admin/LogisticsClient/../LogisticsClient/ajaxGetDataById?id='+row.id);
					$('#dlgSysclientReboot').dialog('open');
				}else{
					$.messager.alert('<?php echo (L("warning")); ?>','<?php echo (L("the_state_can_t_operate")); ?>');
				}
			}else{
				$.messager.alert('<?php echo (L("warning")); ?>','<?php echo (L("please_select_a_data")); ?>');
			}
		}
		
		function doRebootSysclient(){
			$("#ffxSysclientstatus").form('submit', {
				url : '/wf/Admin/LogisticsClient/../LogisticsClient/doClientReboot',
				success : function(d) {
					var json = $.parseJSON(d);
					if (json.msg == "ok") {
					    	$.messager.show({
					       title:'<?php echo (L("successful")); ?>',
					       msg:json.msg
					     });
					    	$('#dlgSysclientReboot').dialog('close');
					     $('#dgLogisticsClient2').datagrid('reload');    
					 }else{
					     $.messager.alert('<?php echo (L("warning")); ?>','<?php echo (L("operation_is_not_successful")); ?>','error');
					 }
				}
			});
		}
		
		function doStopSysclient(){
			var row = $('#dgLogisticsClient2').datagrid('getSelected');
			if (row){
				if('1' == row.status ){
					$.messager.confirm('<?php echo (L("warning")); ?>', '<?php echo (L("are_you_sure_the_stop")); ?>？', function(r){
						if (r){
						 var map = new HashMap(); 
						 map.put("id",row.id); 
						 ajaxPost("/wf/Admin/LogisticsClient/../LogisticsClient/doClientStop",map);
						 $('#dgLogisticsClient2').datagrid('reload');
						}
					});
				}else{
					$.messager.alert('<?php echo (L("warning")); ?>','<?php echo (L("the_state_can_t_operate")); ?>');
				}
			}else{
				$.messager.alert('<?php echo (L("warning")); ?>','<?php echo (L("please_select_a_data")); ?>');
			}
		}
		
		
		function doStartSysclient(){
			var row = $('#dgLogisticsClient2').datagrid('getSelected');
			if (row){
				if('2' == row.status ){
					$.messager.confirm('<?php echo (L("warning")); ?>', '<?php echo (L("are_you_sure_the_start")); ?>？', function(r){
						if (r){
							 var map = new HashMap(); 
							 map.put("id",row.id); 
							 
							 ajaxPost("/wf/Admin/LogisticsClient/../LogisticsClient/doClientStart",map);
							 $('#dgLogisticsClient2').datagrid('reload');
						}
					});
				}else{
					$.messager.alert('<?php echo (L("warning")); ?>','<?php echo (L("the_state_can_t_operate")); ?>');
				}
			}else{
				$.messager.alert('<?php echo (L("warning")); ?>','<?php echo (L("please_select_a_data")); ?>');
			}
		}
		
		/* doLogisticsClientStart */
		/* doLogisticsClientStop */
		</script>
	</div>
</body>
</html>
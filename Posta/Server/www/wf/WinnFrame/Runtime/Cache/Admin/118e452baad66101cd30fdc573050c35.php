<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>运行状态</title>
</head>
<body>
	<div style="padding: 10px; height: 96%; overflow: hidden;">
		<div id="loading" class="panel-body"></div>
		
		<table border="0px" width="100%" height="100%">
			<!-- <tr>
				<td height="50px" valign="top">
					<div class="easyui-panel" style="padding:5px;">
						<form id="ffLogisticsBox3_<?php echo ($_GET['clientid']); ?>">
						
					    	<table border="0" width="100%">
					    		<tr>
					    			<td class="search-table-td">选择商品：</td>
						    		<td class="search-table-td">
						    			<select class="easyui-combobox" id="form_accessBoxList3_<?php echo ($_GET['clientid']); ?>_product" editable="false" name="productid" style="width: 150px" 
						    				data-options="
						    					valueField:'id',
						    					url:'/wf/Admin/LogisticsBox/../VendingProduct/ajaxSelectList'">
						    			</select>
									</td>
					    			<td>&nbsp;</td>
					    			<td width="60px">
					    				<a href="javascript:void(0)" class="easyui-linkbutton" style="width: 60px"
					    					onclick="searchMilkBoxList<?php echo ($_GET['clientid']); ?>Form()"><?php echo (L("query")); ?></a>
					    			</td>
					    			<td width="60px">
					    				<a href="javascript:void(0)" class="easyui-linkbutton" style="width: 60px"
					    					onclick="clearMilkBoxList<?php echo ($_GET['clientid']); ?>Form()"><?php echo (L("clear")); ?></a>
					    			</td>
					    		</tr>
					    	</table>
					    </form>  
					</div>
				</td>
			</tr> -->
			<tr>
				<td valign="top">
					<div class="easyui-layout" id="ccLayout_<?php echo ($_GET['clientid']); ?>" style="width:100%; height:100%;">
						<div data-options="region:'center',border:true">
						 	<table id="dgLogisticsBox3_<?php echo ($_GET['clientid']); ?>" class="easyui-datagrid" title="" style="padding-top:20px" width="100%" height="100%"
									data-options="rownumbers:true,
									toolbar: '#tbLogisticsBoxid<?php echo ($_GET['clientid']); ?>',
									singleSelect:true,
									pagination:true,
									url:'/wf/Admin/LogisticsBox/../LogisticsBox/ajaxLogisticsList?clientid=<?php echo ($_GET['clientid']); ?>',
									method:'get',border:false,
									onDblClickRow : function(index,row){
										
									}">
								<thead>
									<tr>
										<th data-options="field:'id', hidden:true">id</th>
										<th data-options="field:'no', width:100"><?php echo (L("box_number")); ?></th>
										<!-- <th data-options="field:'maxcapacity', width:100">容量</th>
										<th data-options="field:'productname', width:200">商品名称</th>  -->
										<th data-options="field:'status', width:100, formatter:formatBox2Status"><?php echo (L("usage_status")); ?></th>
										<th data-options="field:'norm', width:100, formatter:formatNormStatus"><?php echo (L("norm")); ?></th>
										<!-- <th data-options="field:'num',width:100,editor:'textbox',styler:cellStyler">当前数量</th>  -->
										<!-- <th data-options="field:'duonum',width:100,editor:'textbox',styler:cellDuoStyler">近效期数量</th>  -->
										
										<!--  <th data-options="field:'type',width:100,editor:'textbox',formatter:formatType">类型</th>  -->
										<!-- <th data-options="field:'storagestatus',width:100,editor:'textbox',formatter:formatStorageStatus">储藏状态</th>  -->
										<th data-options="field:'storagestatus',width:100,formatter:formatStorageStatus"><?php echo (L("box_storage_state")); ?></th>
										<th data-options="field:'isopen',width:100,formatter:formatBoxIsOpenStatus"><?php echo (L("s0008")); ?></th>
										
									</tr>
								</thead>
							</table>
							<div id="tbLogisticsBoxid<?php echo ($_GET['clientid']); ?>" style="height:auto">
								<a href="javascript:void(0)" style="width: 70px"
									class="easyui-linkbutton" data-options="iconCls:'icon-network-pc',plain:true" onclick="doStartLogisticsBox_<?php echo ($_GET['clientid']); ?>()">&nbsp;<?php echo (L("start")); ?></a>
								<a href="javascript:void(0)" style="width: 70px"
									class="easyui-linkbutton" data-options="iconCls:'icon-shut-down',plain:true" onclick="doStopLogisticsBox_<?php echo ($_GET['clientid']); ?>()">&nbsp;<?php echo (L("stop")); ?></a>
								<!-- 
								<a href="javascript:void(0)" 
									class="easyui-linkbutton" data-options="iconCls:'icon-lookup',plain:true" onclick="toLogisticsBoxDetail_<?php echo ($_GET['clientid']); ?>()">&nbsp;<?php echo (L("view")); ?></a>
								 -->
								 <a href="javascript:void(0)" 
									class="easyui-linkbutton" data-options="iconCls:'icon-unlock',plain:true" onclick="openBox<?php echo ($_GET['clientid']); ?>()">&nbsp;<?php echo (L("s0032")); ?></a>
								
							</div>
						</div>
					</div>
				</td>
			</tr>
		</table>
			
			<script type="text/javascript">
					function openBox<?php echo ($_GET['clientid']); ?>(){
						var row = $('#dgLogisticsBox3_<?php echo ($_GET['clientid']); ?>').datagrid('getSelected');
						if (row){
							if('1' == row.status ){
								
								$.messager.confirm('<?php echo (L("warning")); ?>', '<?php echo (L("are_you_sure")); ?>?', function(r){
									if (r){
										
									
								 var url = "/wf/Admin/LogisticsBox/../LogisticsBox/openBox";
								 var urldata = "clientid="+row.clientid+"&id="+row.id;
						
								 $.ajax({
										async:false,/* 同步通讯*/
										type:"POST",/*传输方式*/
										cache:false,/*下载最新数据（无缓存）*/
										url:url,/*请求地址*/
										data:urldata,
										success:function(obj){
											$.messager.progress('close');
											var obj = $.parseJSON(obj);
											if("ok" == obj.msg){
												$('#dgLogisticsBox3_<?php echo ($_GET['clientid']); ?>').datagrid('reload');
												$.messager.show({
													title:'<?php echo (L("warning")); ?>',
													msg:'<?php echo (L("operation_is_successful")); ?>!',
													showType:'show'
												});
											}else{
												$.messager.alert('<?php echo (L("warning")); ?>','<?php echo (L("the_failure_of_operation")); ?>!','error');
											}
										}
									});
								 
								 
									}
								});
								 
								 
								 
							}
							
							
							
							
						}else{
							$.messager.alert('<?php echo (L("warning")); ?>','<?php echo (L("please_select_a_data")); ?>');
						}
					}
			
					function searchMilkBoxList<?php echo ($_GET['clientid']); ?>Form(){
						 $('#dgLogisticsBox3_<?php echo ($_GET['clientid']); ?>').datagrid({
				                queryParams: {
				                	productid: $("#form_accessBoxList3_<?php echo ($_GET['clientid']); ?>_product").combobox('getValue')
				                }
				        });
					}
					function clearMilkBoxList<?php echo ($_GET['clientid']); ?>Form(){
						$('#ffLogisticsBox3_<?php echo ($_GET['clientid']); ?>').form('clear');
					}
			</script>
		
			<!-- <div data-options="region:'north'" style="height:80px">
				<div class="easyui-panel" style="margin-bottom:10px;margin-top:10px;padding-bottom:10px;padding-top:10px;"
						data-options="border:false" >
					
				    
			 	</div>
			</div> 
			
		</div>-->
		
		
		<script type="text/javascript">
		function cellStyler(value,row,index){
			if (value < 1){
				return 'background-color:#FFAAAA;color:red;';
			}
		}
		
		function cellDuoStyler(value,row,index){
			if (value > 0){
				return 'background-color:#FFAAAA;color:red;';
			}
		}
		
		function toLogisticsBoxDetail_<?php echo ($_GET['clientid']); ?>(){
			var row = $('#dgLogisticsBox3_<?php echo ($_GET['clientid']); ?>').datagrid('getSelected');
			if (row){
				$('#ccLayout_<?php echo ($_GET['clientid']); ?>').layout('remove', 'east');
				
				var options = {
					region: 'east',
					split:true,
					collapsed:false,
					width : 300,
					title:'货道详情',
				};
				$('#ccLayout_<?php echo ($_GET['clientid']); ?>').layout('add', options);
				
				var eastPanel = $("#ccLayout_<?php echo ($_GET['clientid']); ?>").layout("panel","east");
				var targetObj = $( "<table class=\"easyui-propertygrid\" "
						+"data-options=\""
						+"url: '/wf/Admin/LogisticsBox/../LogisticsBox/ajaxGetMilkBoxProductInfoById',"
						+"method: 'get',"
						+"queryParams: {"
							+"boxid: " +row.id
						+"},"
						+"showGroup: true,"
						+"fit: true,"
						+"border: false\""
						+" ></table>").appendTo(eastPanel);
				$.parser.parse(eastPanel);//渲染
				
				
				
			}else{
				$.messager.alert('提醒','请选择要查看的货道!');
			}
		}
		
		
		
		function doStopLogisticsBox_<?php echo ($_GET['clientid']); ?>(){
			var row = $('#dgLogisticsBox3_<?php echo ($_GET['clientid']); ?>').datagrid('getSelected');
			if (row){
				if('1' == row.status ){
					 var url = "/wf/Admin/LogisticsBox/../LogisticsBox/doClientBoxStop";
					 var urldata = "id="+row.id;
					 $.ajax({
							async:false,/* 同步通讯*/
							type:"POST",/*传输方式*/
							cache:false,/*下载最新数据（无缓存）*/
							url:url,/*请求地址*/
							data:urldata,
							success:function(obj){
								$.messager.progress('close');
								var obj = $.parseJSON(obj);
								if("ok" == obj.msg){
									$('#dgLogisticsBox3_<?php echo ($_GET['clientid']); ?>').datagrid('reload');
									$.messager.show({
										title:'提示',
										msg:'操作成功!',
										showType:'show'
									});
								}else{
									$.messager.alert('提示','操作失败!','error');
								}
							}
						});
				}else{
					$.messager.alert('提醒','该状态下，无法停用!');
				}
			}else{
				$.messager.alert('提醒','请选择一台客户机进行停用!');
			}
		}
		
		
		function doStartLogisticsBox_<?php echo ($_GET['clientid']); ?>(){
			var row = $('#dgLogisticsBox3_<?php echo ($_GET['clientid']); ?>').datagrid('getSelected');
			if (row){
				if('2' == row.status || '3' == row.status ){
					var url = "/wf/Admin/LogisticsBox/../LogisticsBox/doClientBoxStart";
					var urldata = "id="+row.id;
					 $.ajax({
							async:false,/* 同步通讯*/
							type:"POST",/*传输方式*/
							cache:false,/*下载最新数据（无缓存）*/
							url:url,/*请求地址*/
							data:urldata,
							success:function(obj){
								$.messager.progress('close');
								var obj = $.parseJSON(obj);
								if("ok" == obj.msg){
									$('#dgLogisticsBox3_<?php echo ($_GET['clientid']); ?>').datagrid('reload');
									$.messager.show({
										title:'提示',
										msg:'操作成功!',
										showType:'show'
									});
								}else{
									$.messager.alert('提示','操作失败!','error');
								}
							}
						});
				}else{
					$.messager.alert('提醒','该状态下，无法启用!');
				}
			}else{
				$.messager.alert('提醒','请选择一台客户机进行停用!');
			}
		}
		</script>
	</div>
</body>
</html>
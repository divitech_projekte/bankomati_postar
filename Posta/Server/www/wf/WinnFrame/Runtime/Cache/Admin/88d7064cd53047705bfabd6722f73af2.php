<?php if (!defined('THINK_PATH')) exit();?><style>
body { margin: 0; text-align: center; background: #FFFFFF; }

#login { display: block; width:1100px; margin: 0 auto; text-align: left;}
#login_header { display:block; padding-top:40px; height:80px;}
#login_footer { clear:both; display:block; margin-bottom:20px; padding:10px; border-top:solid 0px #e2e5e8; color:#666; text-align:center;}

.login_form { background-color: #ffffff; width: 400px; height: 210px; border-radius: 15px; padding-top: 20px; position: absolute; margin-left: 350px; margin-top: 100px;}
</style>

<script type="text/javascript">

	$(function(){
		$("#fflogin").form('disableValidation');
		
		document.onkeydown = function(e){
		    var ev = document.all ? window.event : e;
		    if(ev.keyCode == 13) {
		    	submitForm();
		   	}
		}
	});

</script>

<div style="background-image: url('/<?php echo (C("PROJECT")); ?>/Public/images/login_header.png'); height: 20px"></div>

<div id="login">
	<div id="login_header">
		<a href="#"><img style="padding: 1px 1px 1px 20px" src="/<?php echo (C("PROJECT")); ?>/Public/images/logo_b.png" /></a>
	</div>
	<div class="login_form">
		<form id="fflogin" name="fflogin" method="post" action="/wf/Admin/Index/../Index/doLogin">
			<table border="0" width="100%">
		  		<tr><td>
		  			<div style="margin-left: 80px; margin-bottom: 1px; font-family: Microsoft YaHei; font-size: medium;"><?php echo (L("account")); ?></div></td></tr>
		    	<tr><td align="center">
		    		<input class="easyui-textbox" type="text" name="username" data-options="required:true" style="width: 250px; height: 32px;"></input></td></tr>
		    	<tr><td>
		    		<div style="margin-left: 80px; margin-bottom: 1px; margin-top: 5px; font-family: Microsoft YaHei; font-size: medium;"><?php echo (L("password")); ?></div></td></tr>
		    	<tr><td align="center">
		    		<input class="easyui-validatebox easyui-textbox" type="password" name="password" validType="length[4,32]"
						data-options="required:true" style="width: 250px; height: 32px;"></input></td></tr>
				<tr><td align="center">
					<a href="javascript:void(0)" class="easyui-linkbutton" style="width: 150px; height: 40px; margin-top: 12px;" onclick="submitForm()">
						<span style="font-family: Microsoft YaHei; font-weight: bold; font-size: medium;"><?php echo (L("sign_in")); ?></span></a></td></tr>
			</table>
		</form>
	</div>
	<div>
		<img src="/<?php echo (C("PROJECT")); ?>/Public/images/login_bg.png" />
	</div>
</div>
<div id="login_footer">
	Copyright © 2016 - 2017 winnsen.com. All Rights Reserved.
</div>

<script type="text/javascript">
	function submitForm(){
		$("#fflogin").form('enableValidation');
		$('#fflogin').form('submit', {success:function(data){
			var obj = JSON.parse(data); 
	        if("ok" == obj.msg){
	        	window.location.href = "/wf/Admin/Index/../Index/index";
	        }else{
	        	$.messager.alert('<?php echo (L("warning")); ?>', '<?php echo (L("s0017")); ?>');
	        }
	    }}); 
	}
 
</script>
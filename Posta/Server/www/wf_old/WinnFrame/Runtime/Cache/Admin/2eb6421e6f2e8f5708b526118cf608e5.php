<?php if (!defined('THINK_PATH')) exit();?><script type="text/javascript">


/**
 * 删除数据操作
 */
function doDel(str ,controller){
	var row = $('#dg'+str).datagrid('getSelected');
	if (row){
		$.messager.confirm('<?php echo (L("warning")); ?>', '<?php echo (L("are_you_sure")); ?>?', function(r){
			if (r){
				 var map = new HashMap(); 
				 map.put("id",row.id); 
				 ajaxPost("/wf/admin/index/../"+controller+"/ajaxDel",map);
				 $('#dg'+str).datagrid('reload');
			}
		});
	}else{
		$.messager.alert('<?php echo (L("warning")); ?>','<?php echo (L("please_select_a_data")); ?>!');
	}
}

/**
 * 注销数据操作（改变状态）
 */
function doCancel(str ,controller){
	var row = $('#dg'+str).datagrid('getSelected');
	if (row){
		$.messager.confirm('<?php echo (L("warning")); ?>', '<?php echo (L("are_you_sure")); ?>?', function(r){
			if (r){
				 var map = new HashMap(); 
				 map.put("id",row.id); 
				 ajaxPost("/wf/admin/index/../"+controller+"/ajaxCancel",map);
				 $('#dg'+str).datagrid('reload');
			}
		});
	}else{
		$.messager.alert('<?php echo (L("warning")); ?>','<?php echo (L("please_select_a_data")); ?>!');
	}
}

/**
 * 打开添加数据界面
 */
function toAdd(str){
	$("#ffxAdd"+str).form('disableValidation');
	$("#ffxAdd"+str).form('clear');
	$('#dlgAdd'+str).dialog('open');
}


/**
 * 数据添加操作
 */
function doAdd(str,controller){
	$("#ffxAdd" + str).form('enableValidation');
	$("#ffxAdd" + str).form('submit', {
		url : '/wf/admin/index/../' + controller + '/ajaxAdd',
		success : function(d) {
			var json = $.parseJSON(d);
			if (json.msg == "ok") {
			    	$.messager.show({
			       title:'成功 ',
			       msg:json.msg
			     });
			     $('#dlgAdd'+str).dialog('close');
			     $('#dg'+str).datagrid('reload');    
			}else if (json.msg == "no") {
				 $.messager.alert('<?php echo (L("warning")); ?>',json.data,'error');   
			}else{
			     $.messager.alert('<?php echo (L("warning")); ?>','新增数据不成功 !','error');
			 }
		}
	});
}

/**
 * 打开编辑数据界面
 */
function toEdit(str,controller){
	$("#ffxEdit"+str).form('disableValidation');
	var row = $('#dg'+str).datagrid('getSelected');
	if (row){
		$("#ffxEdit"+str).form('clear');
		
		$('#ffxEdit'+str).form('load', '/wf/admin/index/../'+controller+'/ajaxGetDataById?id='+row.id);
		
		$('#dlgEdit'+str).dialog('open');
	}else{
		$.messager.alert('<?php echo (L("warning")); ?>','<?php echo (L("please_select_a_data")); ?>!');
	}
}

/**
 * 编辑数据操作
 */
function doEdit(str,controller){
	$("#ffxEdit" + str).form('enableValidation');
	$("#ffxEdit"+str).form('submit', {
		url : '/wf/admin/index/../'+controller+'/ajaxEdit',
		success : function(d) {
			var json = $.parseJSON(d);
			if (json.msg == "ok") {
			    	$.messager.show({
			       title:'成功 ',
			       msg:json.msg
			     });
			     $('#dlgEdit'+str).dialog('close');
			     $('#dg'+str).datagrid('reload');    
			}else if (json.msg == "no") {
				 $.messager.alert('<?php echo (L("warning")); ?>',json.data,'error');   
			}else{
			     $.messager.alert('<?php echo (L("warning")); ?>','编辑数据不成功 !','error');
			 }
		}
	});
}


function ajaxPost(url,map){
	var win = $.messager.progress({
		title:'请等待',
		msg:'保存中...'
	});
	var urldata = "";
	var keySet = map.keySet(); 
	for(var i in keySet){ 
	  if(0 != i)		  
		  urldata += "&";
	  urldata += keySet[i]+"="+map.get(keySet[i])
	}
	$.ajax({
		async:false,/* 同步通讯*/
		type:"POST",/*传输方式*/
		cache:false,/*下载最新数据（无缓存）*/
		url:url,/*请求地址*/
		data:urldata,
		success:function(obj){
			$.messager.progress('close');
			var obj = $.parseJSON(obj);
			if("ok" == obj.msg){
				$.messager.show({
					title:'<?php echo (L("warning")); ?>',
					msg:'<?php echo (L("operation_is_successful")); ?>!',
					showType:'show'
				});
			}else{
				$.messager.alert('<?php echo (L("warning")); ?>','<?php echo (L("the_failure_of_operation")); ?>!','error');
			}
		}
	});
}

function ajaxGet(url,map){
	var win = $.messager.progress({
		title:'请等待',
		msg:'保存中...'
	});
	var urldata = "";
	var keySet = map.keySet(); 
	for(var i in keySet){ 
	  if(0 != i)		  
		  urldata += "&";
	  urldata += keySet[i]+"="+map.get(keySet[i])
	}
	$.ajax({
		async:false,/* 同步通讯*/
		type:"GET",/*传输方式*/
		cache:false,/*下载最新数据（无缓存）*/
		url:url,/*请求地址*/
		data:urldata,
		success:function(obj){
			$.messager.progress('close');
			var obj = $.parseJSON(obj);
			if("ok" == obj.msg){
				$.messager.show({
					title:'<?php echo (L("warning")); ?>',
					msg:'操作成功!',
					showType:'show'
				});
			}else{
				$.messager.alert('<?php echo (L("warning")); ?>','操作失败!','error');
			}
		}
	});
}

</script>
<style>
<!--
#loading{position: absolute; z-index: 1000; top: 30px; left: 0px; width: 100%; height: 100%;  text-align: center;}
.tabs li a.tabs-inner { padding: 0 25px;}
-->

.double-from-lable {text-align: right; width: 18%; padding-right: 10px}
.double-from-text {width: 32%; padding-right: 5px}
.single-from-lable {text-align: right; width: 36%; padding-right: 10px}
.single-from-text {width: 64%; padding-right: 5px}

.search-table-td{float:left;padding:0 0 0 10px;display:block;height:28px;line-height:28px;}
</style>
<div id="loading" class="panel-body"></div>
	<div id="dlgEditPassword" class="easyui-dialog" title="&nbsp;<?php echo (L("modify_password")); ?>" style="width:400px; height:220px; padding: 10px" data-options="
		iconCls:'icon-edit',
		closed:true,
		modal:true,
		toolbar: '#dlgEditPassword_toolbar'">
		<div class="easyui-panel" style="padding: 5px">
			<table border="0" width="100%" height="100%">
				<tr>
		   			<td class="single-from-lable"><?php echo (L("original_password")); ?>：</td>
		   			<td class="single-from-text">
		   				<input validType="length[4,32]" class="easyui-validatebox easyui-textbox" type="password" id="editUserOldPassword2" name="oldpassword" data-options=""></input>
		   			</td>
		   		</tr>
				<tr>
		   			<td class="single-from-lable"><?php echo (L("new_password")); ?>：</td>
		   			<td class="single-from-text">
		   				<input validType="length[4,32]" class="easyui-validatebox easyui-textbox" type="password" id="editUserPassword2" name="password" data-options=""></input>
		   			</td>
		   		</tr>
		   		<tr>
		   			<td class="single-from-lable"><?php echo (L("repeat_password")); ?>：</td>
		   			<td class="single-from-text">
		   				<input validType="equalTo['#editUserPassword2']" type="password"  class="easyui-validatebox easyui-textbox"
							name="passverify"  invalidMessage="<?php echo (L("two_input_passwords_do_not_match")); ?>" data-options=""></input>
					</td>
		   		</tr>
	   		</table>
	   	</div>
   		<script type="text/javascript">
	 		function doEditPassword(){
	 			var map = new HashMap(); 
	 			map.put("id",<?php echo (getUserId($uid)); ?>); 
	 			map.put("oldpassword",$('#editUserOldPassword2').val()); 
	 			map.put("password",   $('#editUserPassword2').val()); 
	 			
	 			ajaxPost("/wf/admin/index/../SysUser/ajaxEditPassword",map);
	 			$('#dlgEditPassword').dialog('close');
	 		}
   		</script>
	</div>
	<div id="dlgEditPassword_toolbar" style="height: auto">
		<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-edit', plain:true"
			onclick="doEditPassword()">&nbsp;<?php echo (L("confirm")); ?></a>
	</div>
		
	<div data-options="region:'north',border:false" style="height:55px;background-color:#E6EEF8;overflow: hidden;">
		<!-- <img src="/wf/Public/images/logo.png" />
		<div style="float:right;width:400px;height:50px;text-align:center;line-height:50px;">欢迎“<?php echo (getUserInfo($uid)); ?>”登录&nbsp;&nbsp;
			<a href="/wf/admin/index/../Index/outLogin" class="easyui-linkbutton" data-options="iconCls:'icon-shutdown'">安全退出</a>
		</div> -->
		<div id="header">
			<div class="headerNav">
				<img style="padding: 1px 1px 1px 10px" src="/<?php echo (C("PROJECT")); ?>/Public/images/logo_w.png" />
				<div style="float: right; color: #FFFFFF; padding: 8px">
					<?php echo (L("welcome_to_you")); ?> <?php echo (getUserInfo($uid)); ?> | 
					<a href="#" style="color: #FFFFFF; text-decoration: none"><?php echo (L("back_to_home")); ?></a> |
					<a href="#" style="color: #FFFFFF; text-decoration: none" onclick="$('#dlgEditPassword').dialog('open');"><?php echo (L("change_password")); ?></a> |
					<a href="/wf/admin/index/../Index/outLogin" style="color: #FFFFFF; text-decoration: none"><?php echo (L("logging_out")); ?></a>
				</div>
			</div>
		</div>
	</div>
	<div data-options="region:'south',border:false" style="height:30px;background-color:#E6EEF8;">
		<div style="text-align: center; width: 100%; padding-top: 7px">Copyright © 2016 - 2017 <a href="http://www.winnsen.com" target="_blank" style="color: #000000; text-decoration: none">winnsen.com</a>. All Rights Reserved.</div></div>
		<div data-options="region:'west',split:true,title:'<?php echo (L("main_menu")); ?>'" style="width:300px;">
		<div class="easyui-accordion" style="height:700px;" data-options="fit:true,border:false">
			
			
			<?php if( isHasAuth('/Admin/Charge/menu') ) { ?>
		
		<div title="<?php echo (L("s0011")); ?>" data-options="iconCls:'icon-blank'" style="overflow: auto; padding: 5px 20px 5px 20px">
			<!-- <?php echo ($result["id"]); ?>--<?php echo ($result["data"]); ?>  -->
			<ul class="easyui-tree" data-options="animate:true, lines:true">
				<?php if(isHasAuth('/Admin/Charge/menuClient')): ?><li>
						<span><a class="panel-title"><?php echo (L("terminal_management")); ?></a></span>
						<ul>
							<?php if(isHasAuth('/Admin/ChargeClient/chargeClientList')): endif; ?>
								<li>
									<a href="#" onclick="addTab(this,'/wf/admin/index/../ChargeClient/chargeClientList')" style="text-decoration: none; color: #0E2D5F" title="<?php echo (L("terminal_information")); ?>"><?php echo (L("terminal_information")); ?></a>
								</li>
							
							<?php if(isHasAuth('/Admin/ChargeClient/chargeStatusList')): endif; ?>
								<li>
									<a href="#" onclick="addTab(this,'/wf/admin/index/../ChargeClient/chargeStatusList')" style="text-decoration: none; color: #0E2D5F" title="<?php echo (L("running_state")); ?>"><?php echo (L("running_state")); ?></a>
								</li>
							
						
						</ul>
					</li><?php endif; ?>
				
			</ul>
		</div>
	<?php } ?>

			<?php if( isHasAuth('/Admin/Vending/menu') ) { ?>
				<div title="<?php echo (L("automatic_container_platform")); ?>" data-options="iconCls:'icon-blank'" style="overflow: auto; padding: 5px 20px 5px 20px">
					<!-- <?php echo ($result["id"]); ?>--<?php echo ($result["data"]); ?>  -->
					<ul class="easyui-tree" data-options="animate:true,lines:true">
						<?php if(isHasAuth('/Admin/Vending/menuClient')): ?><li>
								<span><a class="panel-title"><?php echo (L("terminal_management")); ?></a></span>
								<ul>
									<?php if(isHasAuth('/Admin/SysClient/milkClientList')): ?><li>
											<a href="#" onclick="addTab(this,'/wf/admin/index/../SysClient/milkClientList')" style="text-decoration: none; color: #0E2D5F" title="<?php echo (L("terminal_configuration")); ?>"><?php echo (L("terminal_configuration")); ?></a>
										</li><?php endif; ?>
									<?php if(isHasAuth('/Admin/SysBox/milkBoxList')): ?><li>
											<a href="#" onclick="addTab(this,'/wf/admin/index/../SysBox/milkBoxList')" style="text-decoration: none; color: #0E2D5F"  title="<?php echo (L("box_configuration")); ?>"><?php echo (L("box_configuration")); ?></a>
										</li><?php endif; ?>
									<?php if(isHasAuth('/Admin/SysClient/milkStatusList')): ?><li>
											<a href="#" onclick="addTab(this,'/wf/admin/index/../SysClient/milkStatusList')" style="text-decoration: none; color: #0E2D5F"  title="<?php echo (L("running_state")); ?>"><?php echo (L("running_state")); ?></a>
										</li><?php endif; ?>
								</ul>
							</li><?php endif; ?>
						<?php if(isHasAuth('/Admin/Vending/menuProduct')): ?><li>
								<span><a class="panel-title"><?php echo (L("commodity_management")); ?></a></span>
								<ul>
									<?php if(isHasAuth('/Admin/VendingProduct/milkProductList')): ?><li>
											<a href="#" onclick="addTab(this,'/wf/admin/index/../VendingProduct/milkProductList')" style="text-decoration: none; color: #0E2D5F"  title="<?php echo (L("commodity_information")); ?>"><?php echo (L("commodity_information")); ?></a>
										</li><?php endif; ?>
									<?php if(isHasAuth('/Admin/VendingConsignment/milkConsignmentList')): ?><li>
											<a href="#" onclick="addTab(this,'/wf/admin/index/../VendingConsignment/milkConsignmentList')" style="text-decoration: none; color: #0E2D5F"  title="<?php echo (L("cargo_management")); ?>"><?php echo (L("cargo_management")); ?></a>
										</li><?php endif; ?>
									<?php if(isHasAuth('/Admin/VendingStorage/milkStorageRecord')): ?><li>
											<a href="#" onclick="addTab(this,'/wf/admin/index/../VendingStorage/milkStorageRecord')" style="text-decoration: none; color: #0E2D5F"  title="<?php echo (L("cargo_record")); ?>"><?php echo (L("cargo_record")); ?></a>
										</li><?php endif; ?>
									<?php if(isHasAuth('/Admin/VendingOrder/milkOrderList')): ?><li>
											<a href="#" onclick="addTab(this,'/wf/admin/index/../VendingOrder/milkOrderList')" style="text-decoration: none; color: #0E2D5F"  title="<?php echo (L("sales_data")); ?>"><?php echo (L("sales_data")); ?></a>
										</li><?php endif; ?>
								</ul>
							</li><?php endif; ?>
						<?php if(isHasAuth('/Admin/Vending/menuConsumer')): ?><li>
								<span><a class="panel-title"><?php echo (L("card_management")); ?></a></span>
								<ul>
									<?php if(isHasAuth('/Admin/VendingConsumer/milkConsumerList')): ?><li>
											<a href="#" onclick="addTab(this,'/wf/admin/index/../VendingConsumer/milkConsumerList')" style="text-decoration: none; color: #0E2D5F"  title="<?php echo (L("card_management")); ?>"><?php echo (L("card_management")); ?></a>
										</li><?php endif; ?>
									<?php if(isHasAuth('/Admin/VendingConsumer/milkRecharge')): ?><li>
											<a href="#" onclick="addTab(this,'/wf/admin/index/../VendingConsumer/milkRecharge')" style="text-decoration: none; color: #0E2D5F"  title="<?php echo (L("rush_card")); ?>"><?php echo (L("rush_card")); ?></a>
										</li><?php endif; ?>
									<?php if(isHasAuth('/Admin/VendingConsumerrecord/milkRechargeRecord')): ?><li>
											<a href="#" onclick="addTab(this,'/wf/admin/index/../VendingConsumerrecord/milkRechargeRecord')" style="text-decoration: none; color: #0E2D5F"  title="<?php echo (L("rush_record")); ?>"><?php echo (L("rush_record")); ?></a>
										</li><?php endif; ?>
									<?php if(isHasAuth('/Admin/VendingConsumerrecord/milkConsumingRecord')): ?><li>
											<a href="#" onclick="addTab(this,'/wf/admin/index/../VendingConsumerrecord/milkConsumingRecord')" style="text-decoration: none; color: #0E2D5F"  title="<?php echo (L("records_of_consumption")); ?>"><?php echo (L("records_of_consumption")); ?></a>
										</li><?php endif; ?>
								</ul>
							</li><?php endif; ?>
						<?php if(isHasAuth('/Admin/Vending/menuStatistics')): ?><li>
								<span><a class="panel-title"  ><?php echo (L("statistical_analysis")); ?></a></span>
								<ul>
									<?php if(isHasAuth('/Admin/VendingStorage/milkStatistics1')): ?><li>
											<a href="#" onclick="addTab(this,'/wf/admin/index/../VendingStorage/milkStatistics1')" style="text-decoration: none; color: #0E2D5F"  title="<?php echo (L("terminal_sales")); ?>"><?php echo (L("terminal_sales")); ?></a>
										</li><?php endif; ?>
									<?php if(isHasAuth('/Admin/VendingStorage/milkStatistics2')): ?><li>
											<a href="#" onclick="addTab(this,'/wf/admin/index/../VendingStorage/milkStatistics2')" style="text-decoration: none; color: #0E2D5F"  title="<?php echo (L("monthly_sales_situation")); ?>"><?php echo (L("monthly_sales_situation")); ?></a>
										</li><?php endif; ?>
									<?php if(isHasAuth('/Admin/VendingStorage/milkStatistics3')): ?><li>
											<a href="#" onclick="addTab(this,'/wf/admin/index/../VendingStorage/milkStatistics3')" style="text-decoration: none; color: #0E2D5F"  title="<?php echo (L("classification_of_sales_ratio")); ?>"><?php echo (L("classification_of_sales_ratio")); ?></a>
										</li><?php endif; ?>
								</ul>
							</li><?php endif; ?>
					</ul>
				</div>
			<?php } ?>
			<?php if( isHasAuth('/Admin/Certificate/menu') ) { ?>
				<div title="<?php echo (L("documents_print_ark_platform")); ?>" data-options="iconCls:'icon-blank'" style="overflow: auto; padding: 5px 20px 5px 20px">
					<!-- <?php echo ($result["id"]); ?>--<?php echo ($result["data"]); ?>  -->
					<ul class="easyui-tree" data-options="animate:true,lines:true">
						<?php if(isHasAuth('/Admin/Certificate/menuClient')): ?><li>
								<span><a class="panel-title"><?php echo (L("terminal_management")); ?></a></span>
								<ul>
									<?php if(isHasAuth('/Admin/SysClient/certificateClientList')): ?><li>
											<a href="#" onclick="addTab(this,'/wf/admin/index/../SysClient/certificateClientList')" style="text-decoration: none; color: #0E2D5F" title="<?php echo (L("terminal_information")); ?>"><?php echo (L("terminal_information")); ?></a>
										</li><?php endif; ?>
								</ul>
							</li><?php endif; ?>
						<?php if(isHasAuth('/Admin/Certificate/menuCertificate')): ?><li>
								<span><a class="panel-title"><?php echo (L("certificate_management")); ?></a></span>
								<ul>
									<?php if(isHasAuth('/Admin/CertificateInfo/certificateList')): ?><li>
											<a href="#" onclick="addTab(this,'/wf/admin/index/../CertificateInfo/certificateList')" style="text-decoration: none; color: #0E2D5F" title="<?php echo (L("certificate_information")); ?>"><?php echo (L("certificate_information")); ?></a>
										</li><?php endif; ?>
									<?php if(isHasAuth('/Admin/CertificateInfo/certificatePrintList')): ?><li>
											<a href="#" onclick="addTab(this,'/wf/admin/index/../CertificateInfo/certificatePrintList')" style="text-decoration: none; color: #0E2D5F" title="<?php echo (L("print_record")); ?>"><?php echo (L("print_record")); ?></a>
										</li><?php endif; ?>
								</ul>
							</li><?php endif; ?>
					</ul>
				</div>
			<?php } ?>
			<?php if( isHasAuth('/Admin/Access/menu') ) { ?>
				<div title="<?php echo (L("smart_wash_wardrobe_platform")); ?>" data-options="iconCls:'icon-blank'" style="overflow: auto; padding: 5px 20px 5px 20px">
					<!-- <?php echo ($result["id"]); ?>--<?php echo ($result["data"]); ?>  -->
					<ul class="easyui-tree" data-options="animate:true,lines:true">
						<?php if(isHasAuth('/Admin/Access/menuClient')): ?><li>
								<span><a class="panel-title"><?php echo (L("terminal_management")); ?></a></span>
								<ul>
									<?php if(isHasAuth('/Admin/SysClient/accessClientList')): ?><li>
											<a href="#" onclick="addTab(this,'/wf/admin/index/../AccessClient/accessClientList')" style="text-decoration: none; color: #0E2D5F" title="<?php echo (L("terminal_information")); ?>"><?php echo (L("terminal_information")); ?></a>
										</li><?php endif; ?>
									<?php if(isHasAuth('/Admin/SysClient/accessStatusList')): ?><li>
											<a href="#" onclick="addTab(this,'/wf/admin/index/../SysClient/accessStatusList')" style="text-decoration: none; color: #0E2D5F" title="<?php echo (L("running_state")); ?>"><?php echo (L("running_state")); ?></a>
										</li><?php endif; ?>
									<?php if(isHasAuth('/Admin/SysBoxrecord/accessHistoryList')): ?><li>
											<a href="#" onclick="addTab(this,'/wf/admin/index/../SysBoxrecord/accessHistoryList')" style="text-decoration: none; color: #0E2D5F" title="<?php echo (L("using_the_history")); ?>"><?php echo (L("using_the_history")); ?></a>
										</li><?php endif; ?>
								</ul>
							</li><?php endif; ?>
						<?php if(isHasAuth('/Admin/Access/menuCompany')): ?><li>
								<span><a class="panel-title"><?php echo (L("operator_management")); ?></a></span>
								<ul>
									<?php if(isHasAuth('/Admin/AccessOwner/toList')): ?><li>
											<a href="#" onclick="addTab(this,'/wf/admin/index/../AccessOwner/toList')" style="text-decoration: none; color: #0E2D5F" title="<?php echo (L("contractor_information")); ?>"><?php echo (L("contractor_information")); ?></a>
										</li><?php endif; ?>
									<?php if(isHasAuth('/Admin/AccessStore/toList')): ?><li>
											<a href="#" onclick="addTab(this,'/wf/admin/index/../AccessStore/toList')" style="text-decoration: none; color: #0E2D5F" title="<?php echo (L("cleaners_information")); ?>"><?php echo (L("cleaners_information")); ?></a>
										</li><?php endif; ?>
									<?php if(isHasAuth('/Admin/AccessOrder/accessOrderList')): ?><li>
											<a href="#" onclick="addTab(this,'/wf/admin/index/../AccessOrder/accessOrderList')" style="text-decoration: none; color: #0E2D5F" title="<?php echo (L("order_to_check")); ?>"><?php echo (L("order_to_check")); ?></a>
										</li><?php endif; ?>
									<?php if(isHasAuth('/Admin/SysConfig/accessConfigList')): ?><li>
											<a href="#" onclick="addTab(this,'/wf/admin/index/../SysConfig/accessConfigList')" style="text-decoration: none; color: #0E2D5F" title="<?php echo (L("parameter_configuration")); ?>"><?php echo (L("parameter_configuration")); ?></a>
										</li><?php endif; ?>
								</ul>
							</li><?php endif; ?>
						<?php if(isHasAuth('/Admin/Access/menuOwner')): ?><li>
								<span><a class="panel-title"><?php echo (L("contractor_management")); ?></a></span>
								<ul>
									<?php if(isHasAuth('/Admin/AccessStore/toListByOwner')): ?><li>
											<a href="#" onclick="addTab(this,'/wf/admin/index/../AccessStore/toListByOwner')" style="text-decoration: none; color: #0E2D5F" title="<?php echo (L("cleaners_control")); ?>"><?php echo (L("cleaners_control")); ?></a>
										</li><?php endif; ?>
									<?php if(isHasAuth('/Admin/AccessProduct/accessProductForOwnerList')): ?><li>
											<a href="#" onclick="addTab(this,'/wf/admin/index/../AccessProduct/accessProductForOwnerList')" style="text-decoration: none; color: #0E2D5F" title="<?php echo (L("price_controls")); ?>"><?php echo (L("price_controls")); ?></a>
										</li><?php endif; ?>
									<?php if(isHasAuth('/Admin/AccessOrder/accessOrderForOwnerList')): ?><li>
											<a href="#" onclick="addTab(this,'/wf/admin/index/../AccessOrder/accessOrderForOwnerList')" style="text-decoration: none; color: #0E2D5F" title="<?php echo (L("order_to_control")); ?>"><?php echo (L("order_to_control")); ?></a>
										</li><?php endif; ?>
									<?php if(isHasAuth('/Admin/SysClient/accessProfitConfigList')): ?><li>
											<a href="#" onclick="addTab(this,'/wf/admin/index/../SysClient/accessProfitConfigList')" style="text-decoration: none; color: #0E2D5F" title="<?php echo (L("manage_profit_rate")); ?>"><?php echo (L("manage_profit_rate")); ?></a>
										</li><?php endif; ?>
									<?php if(isHasAuth('/Admin/AccessOrder/accessPayList')): ?><li>
											<a href="#" onclick="addTab(this,'/wf/admin/index/../AccessOrder/accessPayList')" style="text-decoration: none; color: #0E2D5F" title="<?php echo (L("settlement_management")); ?>"><?php echo (L("settlement_management")); ?></a>
										</li><?php endif; ?>
								</ul>
							</li><?php endif; ?>
						<?php if(isHasAuth('/Admin/Access/menuStore')): ?><li>
								<span><a class="panel-title"><?php echo (L("cleaners_management")); ?></a></span>
								<ul>
									<?php if(isHasAuth('/Admin/AccessProduct/accessProductList')): ?><li>
											<a href="#" onclick="addTab(this,'/wf/admin/index/../AccessProduct/accessProductList')" style="text-decoration: none; color: #0E2D5F" title="<?php echo (L("price_setting")); ?>"><?php echo (L("price_setting")); ?></a>
										</li><?php endif; ?>
									<?php if(isHasAuth('/Admin/AccessOrder/accessOrderForStoreList')): ?><li>
											<a href="#" onclick="addTab(this,'/wf/admin/index/../AccessOrder/accessOrderForStoreList')" style="text-decoration: none; color: #0E2D5F" title="<?php echo (L("order_management")); ?>"><?php echo (L("order_management")); ?></a>
										</li><?php endif; ?>
								</ul>
							</li><?php endif; ?>
						<?php if(isHasAuth('/Admin/Access/menuSms')): ?><li>
								<span><a class="panel-title"><?php echo (L("message_center")); ?></a></span>
								<ul>
									<?php if(isHasAuth('/Admin/SysSmsstencil/accessSmsstencilList')): ?><li>
											<a href="#" onclick="addTab(this,'/wf/admin/index/../SysSmsstencil/accessSmsstencilList')" style="text-decoration: none; color: #0E2D5F" title="<?php echo (L("message_template")); ?>"><?php echo (L("message_template")); ?></a>
										</li><?php endif; ?>
									<?php if(isHasAuth('/Admin/SysSms/accessSmsList')): ?><li>
											<a href="#" onclick="addTab(this,'/wf/admin/index/../SysSms/accessSmsList')" style="text-decoration: none; color: #0E2D5F" title="<?php echo (L("message_history")); ?>"><?php echo (L("message_history")); ?></a>
										</li><?php endif; ?>
									<?php if(isHasAuth('/Admin/SysSms/accessSmsPromotionList')): ?><li>
											<a href="#" onclick="addTab(this,'/wf/admin/index/../SysSms/accessSmsPromotionList')" style="text-decoration: none; color: #0E2D5F" title="<?php echo (L("sms_to_promote")); ?>"><?php echo (L("sms_to_promote")); ?></a>
										</li><?php endif; ?>
								</ul>
							</li><?php endif; ?>
						<?php if(isHasAuth('/Admin/Access/menuStatistics')): ?><li>
								<span><a class="panel-title"><?php echo (L("statistical_analysis")); ?></a></span>
								<ul>
									<?php if(isHasAuth('/Admin/AccessOrder/accessStatistics1')): ?><li>
											<a href="#" onclick="addTab(this,'/wf/admin/index/../AccessOrder/accessStatistics1')" style="text-decoration: none; color: #0E2D5F" title="<?php echo (L("sales_report")); ?>"><?php echo (L("sales_report")); ?></a>
										</li><?php endif; ?>
									<?php if(isHasAuth('/Admin/AccessOrder/accessStatistics2')): ?><li>
											<a href="#" onclick="addTab(this,'/wf/admin/index/../AccessOrder/accessStatistics2')" style="text-decoration: none; color: #0E2D5F" title="<?php echo (L("terminal_report")); ?>"><?php echo (L("terminal_report")); ?></a>
										</li><?php endif; ?>
								</ul>
							</li><?php endif; ?>
					</ul>
				</div>
			<?php } ?>
			<?php if( isHasAuth('/Admin/Logistics/menu') ) { ?>
		<div title="<?php echo (L("intelligent_logistics_cabinet_management")); ?>" data-options="iconCls:'icon-blank'" style="overflow: auto; padding: 5px 20px 5px 20px">
			<!-- <?php echo ($result["id"]); ?>--<?php echo ($result["data"]); ?>  -->
			<ul class="easyui-tree" data-options="animate:true, lines:true">
				<?php if(isHasAuth('/Admin/Logistics/menuClient')): ?><li>
						<span><a class="panel-title"><?php echo (L("terminal_management")); ?></a></span>
						<ul>
							<?php if(isHasAuth('/Admin/LogisticsClient/logisticsClientList')): endif; ?>
								<li>
									<a href="#" onclick="addTab(this,'/wf/admin/index/../LogisticsClient/logisticsClientList')" style="text-decoration: none; color: #0E2D5F" title="<?php echo (L("terminal_information")); ?>"><?php echo (L("terminal_information")); ?></a>
								</li>
							
							<?php if(isHasAuth('/Admin/LogisticsClient/logisticsStatusList')): endif; ?>
								<li>
									<a href="#" onclick="addTab(this,'/wf/admin/index/../LogisticsClient/logisticsStatusList')" style="text-decoration: none; color: #0E2D5F" title="<?php echo (L("running_state")); ?>"><?php echo (L("running_state")); ?></a>
								</li>
							
							<?php if(isHasAuth('/Admin/LogisticsBoxrecord/logisticsHistoryList')): endif; ?>
								<li>
									<a href="#" onclick="addTab(this,'/wf/admin/index/../LogisticsBoxrecord/logisticsHistoryList')" style="text-decoration: none; color: #0E2D5F" title="<?php echo (L("using_the_history")); ?>"><?php echo (L("using_the_history")); ?></a>
								</li>
							<li><!-- Abnormal cupboard door -->
								<a href="#" onclick="addTab(this,'/wf/admin/index/../SysBox/toErrList')" style="text-decoration: none; color: #0E2D5F" title="<?php echo (L("s0029")); ?>"><?php echo (L("s0029")); ?></a>
							</li>
						</ul>
					</li><?php endif; ?>
				<?php if(isHasAuth('/Admin/Logistics/menuConfig')): ?><li>
						<span><a class="panel-title"><?php echo (L("system_configuration")); ?></a></span>
						<ul>
							<?php if(isHasAuth('/Admin/LogisticsCourier/toList')): ?><li>
									<a href="#" onclick="addTab(this,'/wf/admin/index/../LogisticsCourier/toList')" style="text-decoration: none; color: #0E2D5F" title="<?php echo (L("express_company")); ?>"><?php echo (L("express_company")); ?></a>
								</li><?php endif; ?>
							
							<?php if(isHasAuth('/Admin/LogisticsCardinfo/toList')): ?><li>
									<a href="#" onclick="addTab(this,'/wf/admin/index/../LogisticsCardinfo/toList')" style="text-decoration: none; color: #0E2D5F" title="<?php echo (L("courier")); ?>"><?php echo (L("courier")); ?></a>
								</li><?php endif; ?>
							
							<?php if(isHasAuth('/Admin/LogisticsConfig/logisticsConfigList')): ?><li>
									<a href="#" onclick="addTab(this,'/wf/admin/index/../LogisticsConfig/logisticsConfigList')" style="text-decoration: none; color: #0E2D5F" title="<?php echo (L("parameter_configuration")); ?>"><?php echo (L("parameter_configuration")); ?></a>
								</li><?php endif; ?>
						</ul>
					</li><?php endif; ?>
				<?php if(isHasAuth('/Admin/Logistics/menuOrder')): ?><li>
						<span><a class="panel-title"><?php echo (L("waybill_management")); ?></a></span>
						<ul>
							<?php if(isHasAuth('/Admin/LogisticsOrder/logisticsOrderList')): endif; ?>
								<li>
									<a href="#" onclick="addTabBase(this,'/wf/admin/index/../LogisticsOrder/toInfoList',true)" style="text-decoration: none; color: #0E2D5F" id="waybill_information" title="<?php echo (L("waybill_information")); ?>"><?php echo (L("waybill_information")); ?></a>
								</li>
								<!-- <li>
									<a href="#" onclick="addTab(this,'/wf/admin/index/../LogisticsOrder/toList')" style="text-decoration: none; color: #0E2D5F" title="运单信息test">运单信息test</a>
								</li> -->
							
							<?php if(isHasAuth('/Admin/LogisticsOrder/logisticsHistoryList')): endif; ?>
								<li>
									<a href="#" onclick="addTabBase(this,'/wf/admin/index/../LogisticsOrder/toHistoryList',true)" style="text-decoration: none; color: #0E2D5F" id="waybill_history" title="<?php echo (L("waybill_history")); ?>"><?php echo (L("waybill_history")); ?></a>
								</li>
								
								<li><!-- Abnormal cupboard door -->
								<a href="#" onclick="addTabBase(this,'/wf/admin/index/../LogisticsOrder/overdueList',true)" style="text-decoration: none; color: #0E2D5F" title="<?php echo (L("s0030")); ?>"><?php echo (L("s0030")); ?></a>
								</li>
								
							
						</ul>
					</li><?php endif; ?>
				<?php if(isHasAuth('/Admin/Logistics/menuSms')): ?><li>
						<span><a class="panel-title"><?php echo (L("message_center")); ?></a></span>
						<ul>
							<?php if(isHasAuth('/Admin/LogisticsSmsstencil/logisticsSmsstencilList')): endif; ?>
								<li>
									<a href="#" onclick="addTab(this,'/wf/admin/index/../LogisticsSmsstencil/logisticsSmsstencilList')" style="text-decoration: none; color: #0E2D5F" title="<?php echo (L("message_template")); ?>"><?php echo (L("message_template")); ?></a>
								</li>
							
							<?php if(isHasAuth('/Admin/LogisticsSms/logisticsSmsList')): endif; ?>
								<li>
									<a href="#" onclick="addTab(this,'/wf/admin/index/../LogisticsSms/logisticsSmsList')" style="text-decoration: none; color: #0E2D5F" title="<?php echo (L("message_history")); ?>"><?php echo (L("message_history")); ?></a>
								</li>
							
						</ul>
					</li><?php endif; ?>
				<?php if(isHasAuth('/Admin/Logistics/menuStatistics')): ?><li>
						<span><a class="panel-title"><?php echo (L("statistical_analysis")); ?></a></span>
						<ul>
							<?php if(isHasAuth('/Admin/LogisticsStatistics/logisticsStatistics1')): endif; ?>
								<li>
									<a href="#" onclick="addTab(this,'/wf/admin/index/../LogisticsStatistics/logisticsStatistics1')" style="text-decoration: none; color: #0E2D5F" title="<?php echo (L("terminal_usage")); ?>"><?php echo (L("terminal_usage")); ?></a>
								</li>
							
							<?php if(isHasAuth('/Admin/LogisticsStatistics/logisticsStatistics2')): endif; ?>
								<li>
									<a href="#" onclick="addTab(this,'/wf/admin/index/../LogisticsStatistics/logisticsStatistics2')" style="text-decoration: none; color: #0E2D5F" title="<?php echo (L("sales_statistics")); ?>"><?php echo (L("sales_statistics")); ?></a>
								</li>
							
						</ul>
					</li><?php endif; ?>
			</ul>
		</div>
	<?php } ?>
			<?php if( isHasAuth('/Admin/Test/menu') ) { ?>
				<div title="<?php echo (L("develop_test_platform")); ?>" data-options="iconCls:'icon-blank'" style="padding:10px;overflow:auto;">
					<!-- <?php echo ($result["id"]); ?>--<?php echo ($result["data"]); ?>  -->
					<ul class="easyui-tree">
						<?php if(isHasAuth('/Admin/SysConfig/toList')): ?><li>
								<span><a href="#" onclick="addTab(this,'/wf/admin/index/../SysConfig/toList')" class="panel-title" style="text-decoration:none;"  title="系统配置">系统配置</a></span>
							</li><?php endif; ?>
						<?php if(isHasAuth('/Admin/SysClient/toList')): ?><li>
								<span><a href="#" onclick="addTab(this,'/wf/admin/index/../SysClient/toList')" class="panel-title" style="text-decoration:none;"  title="<?php echo (L("terminal_information")); ?>"><?php echo (L("terminal_information")); ?></a></span>
							</li><?php endif; ?>
						<?php if(isHasAuth('/Admin/SysBox/toList')): ?><li>
								<span><a href="#" onclick="addTab(this,'/wf/admin/index/../SysBox/toList')" class="panel-title" style="text-decoration:none;"  title="柜箱信息">柜箱信息</a></span>
							</li><?php endif; ?>
						<?php if(isHasAuth('/Admin/SysBoxrecord/toList')): ?><li>
								<span><a href="#" onclick="addTab(this,'/wf/admin/index/../SysBoxrecord/toList')" class="panel-title" style="text-decoration:none;"  title="柜箱使用记录">柜箱使用记录</a></span>
							</li><?php endif; ?>
						<?php if(isHasAuth('/Admin/SysCommand/toList')): ?><li>
								<span><a href="#" onclick="addTab(this,'/wf/admin/index/../SysCommand/toList')" class="panel-title" style="text-decoration:none;"  title="下发命令">下发命令</a></span>
							</li><?php endif; ?>
						<?php if(isHasAuth('/Admin/SysClientconfig/toList')): ?><li>
								<span><a href="#" onclick="addTab(this,'/wf/admin/index/../SysClientconfig/toList')" class="panel-title" style="text-decoration:none;"  title="终端扩展配置">终端扩展配置</a></span>
							</li><?php endif; ?>
						<?php if(isHasAuth('/Admin/VendingOrder/toList')): ?><li >
								<span><a href="#" onclick="addTab(this,'/wf/admin/index/../VendingOrder/toList')" class="panel-title" style="text-decoration:none;"  title="售货机_订单管理">售货机_订单管理</a></span>
							</li><?php endif; ?>
						<?php if(isHasAuth('/Admin/VendingProduct/toList')): ?><li >
								<span><a href="#" onclick="addTab(this,'/wf/admin/index/../VendingProduct/toList')" class="panel-title" style="text-decoration:none;"  title="售货机_产品信息">售货机_产品信息</a></span>
							</li><?php endif; ?>
						<?php if(isHasAuth('/Admin/VendingStore/toList')): ?><li >
								<span><a href="#" onclick="addTab(this,'/wf/admin/index/../VendingStore/toList')" class="panel-title" style="text-decoration:none;"  title="售货机_店商信息">售货机_店商信息</a></span>
							</li><?php endif; ?>
						<?php if(isHasAuth('/Admin/AccessCardrecord/toList')): ?><li>
								<span><a href="#" onclick="addTab(this,'/wf/admin/index/../AccessCardrecord/toList')" class="panel-title" style="text-decoration:none;"  title="存取柜_用户卡使用记录">存取柜_用户卡使用记录</a></span>
							</li><?php endif; ?>
						<?php if(isHasAuth('/Admin/AccessOrderdetail/toList')): ?><li >
								<span><a href="#" onclick="addTab(this,'/wf/admin/index/../AccessOrderdetail/toList')" class="panel-title" style="text-decoration:none;"  title="存取柜_订单详情">存取柜_订单详情</a></span>
							</li><?php endif; ?>
						<?php if(isHasAuth('/Admin/AccessRate/toList')): ?><li >
								<span><a href="#" onclick="addTab(this,'/wf/admin/index/../AccessRate/toList')" class="panel-title" style="text-decoration:none;"  title="存取柜_费率管理">存取柜_费率管理</a></span>
							</li><?php endif; ?>
						
						<?php if(isHasAuth('/Admin/SysItem/toList')): ?><li >
									<span><a href="#" onclick="addTab(this,'/wf/admin/index/../SysItem/toList')" class="panel-title" style="text-decoration:none;"  title="代码字典">代码字典</a></span>
							</li><?php endif; ?>
						<?php if(isHasAuth('/Admin/SysLog/toList')): ?><li >
									<span><a href="#" onclick="addTab(this,'/wf/admin/index/../SysLog/toList')" class="panel-title" style="text-decoration:none;"  title="系统日志">系统日志</a></span>
							</li><?php endif; ?>
						<?php if(isHasAuth('/Admin/SysSms/toList')): ?><li>
								<span><a href="#" onclick="addTab(this,'/wf/admin/index/../SysSms/toList')" class="panel-title" style="text-decoration:none;"  title="发送数据">发送数据</a></span>
							</li><?php endif; ?>
						<?php if(isHasAuth('/Admin/SysSmsstencil/toList')): ?><li >
								<span><a href="#" onclick="addTab(this,'/wf/admin/index/../SysSmsstencil/toList')" class="panel-title" style="text-decoration:none;"  title="信息模板">信息模板</a></span>
							</li><?php endif; ?>
					    <?php if(isHasAuth('/Admin/AccessCardinfo/toList')): ?><li >
								<span><a href="#" onclick="addTab(this,'/wf/admin/index/../AccessCardinfo/toList')" class="panel-title" style="text-decoration:none;"  title="存取柜_用户卡列表">存取柜_用户卡列表</a></span>
							</li><?php endif; ?>
						<?php if(isHasAuth('/Admin/CertificateInfo/toList')): ?><li >
								<span><a href="#" onclick="addTab(this,'/wf/admin/index/../CertificateInfo/toList')" class="panel-title" style="text-decoration:none;"  title="证件打印">证件打印</a></span>
							</li><?php endif; ?>
						<?php if(isHasAuth('/Admin/CertificateAttachment/toList')): ?><li >
								<span><a href="#" onclick="addTab(this,'/wf/admin/index/../CertificateAttachment/toList')" class="panel-title" style="text-decoration:none;"  title="证件附件">证件附件</a></span>
							</li><?php endif; ?>
					</ul>
				</div>
			<?php } ?>
			<?php if( isHasAuth('/Admin/Admin/menu') ) { ?>
				<div title="<?php echo (L("system_management_platform")); ?>" data-options="iconCls:'icon-blank'" style="padding:10px;overflow:auto;">
					<!-- <?php echo ($result["id"]); ?>--<?php echo ($result["data"]); ?>  -->
					<ul class="easyui-tree">
						 <?php if(isHasAuth('/Admin/SysAuthorization/toList')): ?><li >
								<span><a href="#" onclick="addTab(this,'/wf/admin/index/../SysAuthorization/toList')" class="panel-title" style="text-decoration:none;" title="<?php echo (L("permissions_management")); ?>"><?php echo (L("permissions_management")); ?></a></span>
							</li><?php endif; ?>
						<?php if(isHasAuth('/Admin/SysAuthorization/toAllotAuthList')): ?><li >
								<span><a href="#" onclick="addTab(this,'/wf/admin/index/../SysAuthorization/toAllotAuthList')" class="panel-title" style="text-decoration:none;"  title="<?php echo (L("assign_permissions")); ?>"><?php echo (L("assign_permissions")); ?></a></span>
							</li><?php endif; ?>
						<?php if(isHasAuth('/Admin/SysRole/toList')): ?><li>
								<span><a href="#" onclick="addTab(this,'/wf/admin/index/../SysRole/toList')" class="panel-title"  style="text-decoration:none;"  title="<?php echo (L("role_management")); ?>"><?php echo (L("role_management")); ?></a></span>
							</li><?php endif; ?>
						<?php if(isHasAuth('/Admin/SysUser/toList')): ?><li>
								<span><a href="#" onclick="addTab(this,'/wf/admin/index/../SysUser/toList')" class="panel-title"  style="text-decoration:none;"  title="<?php echo (L("account_management")); ?>"><?php echo (L("account_management")); ?></a></span>
							</li><?php endif; ?>
						<?php if(isHasAuth('/Admin/SysUser/toAllotUserList')): ?><li>
								<span><a href="#" onclick="addTab(this,'/wf/admin/index/../SysUser/toAllotUserList')" class="panel-title"  style="text-decoration:none;"  title="<?php echo (L("role_assignment")); ?>"><?php echo (L("role_assignment")); ?></a></span>
							</li><?php endif; ?>
						
						<?php if(isHasAuth('/Admin/SysCompany/toList')): ?><li >
								<span><a href="#" onclick="addTab(this,'/wf/admin/index/../SysCompany/toList')" class="panel-title" style="text-decoration:none;"  title="公司信息">公司信息</a></span>
							</li><?php endif; ?>
						<?php if(isHasAuth('/Admin/SysCompany/toAllotCompanyList')): ?><li>
								<span><a href="#" onclick="addTab(this,'/wf/admin/index/../SysCompany/toAllotCompanyList')" class="panel-title"  style="text-decoration:none;"  title="公司账号">公司账号</a></span>
							</li><?php endif; ?>
						
						<?php if(isHasAuth('/Admin/SysClient/toAllotClientList')): ?><li>
								<span><a href="#" onclick="addTab(this,'/wf/admin/index/../SysClient/toAllotClientList')" class="panel-title"  style="text-decoration:none;"  title="公司终端">公司终端</a></span>
							</li><?php endif; ?>
						
						
					</ul>
					
				</div>
			<?php } ?>
			
			
		</div>
	</div>
	<!-- <div data-options="region:'east',split:true,collapsed:true,title:'East'" style="width:100px;padding:10px;">east region</div>  -->

	<div data-options="region:'center'" >
		<div id="tt" class="easyui-tabs" data-options="fit:true,border:false,plain:true" ><!-- ,tabWidth:120 -->
			<div title="<?php echo (L("platform_home_page")); ?>" style="padding: 10px; overflow-x: hidden;">
				<!-- 首页预留 -->
				<!-- 
				<button onclick="testxx() ">测试</button>
				<script type="text/javascript">
				function testxx(){
					
					$('#ffSysclient2').form('submit', {
						url: 'aaaaaaaa'
						
					});
				}
				</script>
				<?php echo (getLoginInfo($uid)); ?>
				 
				 <?php echo (THINK_VERSION); ?><br />
				 
				  
				 <?php echo (L("lan_define")); ?>-->
				<div class="easyui-panel" style="width: 100%; border: 0px;">
					
					    <?php if((C('SYSTEM_ID') == 'vending')): ?><!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
				<table border="0" width="99%" height="100%">
						<tr>
							<td rowspan="3" valign="top" width="55%" style="padding: 3px">
								<div id="mainclientinfo" class="easyui-panel" title="&nbsp;<?php echo (L("condition_monitoring")); ?>" style="padding: 10px; width: 100%;"
									data-options="
										href:'/wf/admin/index/../Index/getClientInfoPanel',
										tools:[{
											iconCls:'icon-reload',
											handler : function(){
												$('#mainclientinfo').panel('refresh', '/wf/admin/index/../Index/getClientInfoPanel');
											}
										}]
									">
								</div>
							</td>
							<td valign="top" style="padding: 3px" height="200px">
								<div id="mainnewsinfo" class="easyui-panel" title="&nbsp;<?php echo (L("latest_announcement")); ?>"style="padding: 10px; width: 100%; height: 100%;"
									data-options="
										href:'/wf/admin/index/../Index/getNewsInfoPanel',
										tools:[{
											iconCls:'icon-reload',
											handler : function(){
												$('#mainnewsinfo').panel('refresh', '/wf/admin/index/../Index/getNewsInfoPanel');
											}
										}]
									">
								</div>
							</td>
						</tr>
						<tr>
							<td valign="top" style="padding: 3px" height="300px">
								<div id="mainsalesinfo" class="easyui-panel" title="&nbsp;<?php echo (L("sales_statistics")); ?>"style="padding: 10px; width: 100%; height: 100%;"
									data-options="
										href:'/wf/admin/index/../Index/getSalesInfoPanel',
										tools:[{
											iconCls:'icon-reload',
											handler : function(){
												$('#mainsalesinfo').panel('refresh', '/wf/admin/index/../Index/getSalesInfoPanel');
											}
										}]
									">
								</div>
							</td>
						</tr>
						<tr>
							<td></td>
						</tr>
						
					</table>
</body>
</html>
					    <?php elseif((C('SYSTEM_ID') == 'certificate')): ?>
							<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<table border="0" width="99%" height="100%">
						<tr>
							<td rowspan="3" valign="top" width="55%" style="padding: 3px">
								<div id="mainclientinfo" class="easyui-panel" title="&nbsp;<?php echo (L("condition_monitoring")); ?>" style="padding: 10px; width: 100%;"
									data-options="
										href:'/wf/admin/index/../Index/getClientInfoPanel',
										tools:[{
											iconCls:'icon-reload',
											handler : function(){
												$('#mainclientinfo').panel('refresh', '/wf/admin/index/../Index/getClientInfoPanel');
											}
										}]
									">
								</div>
							</td>
							<td valign="top" style="padding: 3px" height="200px">
								<div id="mainnewsinfo" class="easyui-panel" title="&nbsp;<?php echo (L("latest_announcement")); ?>"style="padding: 10px; width: 100%; height: 100%;"
									data-options="
										href:'/wf/admin/index/../Index/getNewsInfoPanel',
										tools:[{
											iconCls:'icon-reload',
											handler : function(){
												$('#mainnewsinfo').panel('refresh', '/wf/admin/index/../Index/getNewsInfoPanel');
											}
										}]
									">
								</div>
							</td>
						</tr>
						<tr>
							<td valign="top" style="padding: 3px" height="300px">
							
							</td>
						</tr>
						<tr>
							<td></td>
						</tr>
						
					</table>
</body>
</html>
						<?php elseif((C('SYSTEM_ID') == 'access')): ?>
							<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
				<table border="0" width="99%" height="100%">
						<tr>
							<td rowspan="3" valign="top" width="55%" style="padding: 3px">
								<div id="mainclientinfo" class="easyui-panel" title="&nbsp;<?php echo (L("condition_monitoring")); ?>" style="padding: 10px; width: 100%;"
									data-options="
										href:'/wf/admin/index/../Index/getClientInfoPanelAccess',
										tools:[{
											iconCls:'icon-reload',
											handler : function(){
												$('#mainclientinfo').panel('refresh', '/wf/admin/index/../Index/getClientInfoPanelAccess');
											}
										}]
									">
								</div>
							</td>
							<td valign="top" style="padding: 3px" height="200px">
								<div id="mainnewsinfo" class="easyui-panel" title="&nbsp;<?php echo (L("the_latest_order")); ?>"style="padding: 10px; width: 100%; height: 100%;"
									data-options="
										href:'/wf/admin/index/../Index/getNewsInfoPanel',
										tools:[{
											iconCls:'icon-reload',
											handler : function(){
												$('#mainnewsinfo').panel('refresh', '/wf/admin/index/../Index/getNewsInfoPanel');
											}
										}]
									">
								</div>
							</td>
						</tr>
						<tr>
							<td valign="top" style="padding: 3px" height="300px">
								<div id="mainsalesinfo" class="easyui-panel" title="&nbsp;<?php echo (L("sales_statistics")); ?>"style="padding: 10px; width: 100%; height: 100%;"
									data-options="
										href:'/wf/admin/index/../Index/getSalesInfoPanel',
										tools:[{
											iconCls:'icon-reload',
											handler : function(){
												$('#mainsalesinfo').panel('refresh', '/wf/admin/index/../Index/getSalesInfoPanel');
											}
										}]
									">
								</div>
							</td>
						</tr>
						<tr>
							<td></td>
						</tr>
						
					</table>
</body>
</html>
					    <?php elseif((C('SYSTEM_ID') == 'logistics')): ?>
					    	<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
				<table border="0" width="99%" height="100%">
						<tr>
							<td rowspan="3" valign="top" width="55%" style="padding: 3px">
								<div id="mainclientinfo" class="easyui-panel" title="&nbsp;<?php echo (L("condition_monitoring")); ?>" style="padding: 10px; width: 100%;"
									data-options="
										href:'/wf/admin/index/../Logistics/getClientInfoPanelAccess',
										tools:[{
											iconCls:'icon-reload',
											handler : function(){
												$('#mainclientinfo').panel('refresh', '/wf/admin/index/../Logistics/getClientInfoPanelAccess');
											}
										}]
									">
								</div>
							</td>
							<td valign="top" style="padding: 3px" height="200px">
								<div id="mainnewsinfo" class="easyui-panel" title="&nbsp;<?php echo (L("the_latest_order")); ?>"style="padding: 10px; width: 100%; height: 100%;"
									data-options="
										href:'/wf/admin/index/../Logistics/getNewsInfoPanel',
										tools:[{
											iconCls:'icon-reload',
											handler : function(){
												$('#mainnewsinfo').panel('refresh', '/wf/admin/index/../Logistics/getNewsInfoPanel');
											}
										}]
									">
								</div>
							</td>
						</tr>
						<tr>
							<td valign="top" style="padding: 3px" height="300px">
								<div id="mainsalesinfo" class="easyui-panel" title="&nbsp;<?php echo (L("sales_statistics")); ?>"style="padding: 10px; width: 100%; height: 100%;"
									data-options="
										href:'/wf/admin/index/../Logistics/getSalesInfoPanel',
										tools:[{
											iconCls:'icon-reload',
											handler : function(){
												$('#mainsalesinfo').panel('refresh', '/wf/admin/index/../Logistics/getSalesInfoPanel');
											}
										}]
									">
								</div>
							</td>
						</tr>
						<tr>
							<td></td>
						</tr>
						
					</table>
</body>
</html>
					    <?php elseif((C('SYSTEM_ID') == 'charge')): ?>
					    	<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
				<table border="0" width="99%" height="100%">
						<tr>
							<td rowspan="3" valign="top" width="55%" style="padding: 3px">
								<div id="mainclientinfo" class="easyui-panel" title="&nbsp;<?php echo (L("condition_monitoring")); ?>" style="padding: 10px; width: 100%;"
									data-options="
										href:'/wf/admin/index/../Charge/getClientInfoPanel',
										tools:[{
											iconCls:'icon-reload',
											handler : function(){
												$('#mainclientinfo').panel('refresh', '/wf/admin/index/../Charge/getClientInfoPanel');
											}
										}]
									">
								</div>
							</td>
							<td valign="top" style="padding: 3px" height="200px">
								<div id="mainnewsinfo" class="easyui-panel" title="&nbsp;<?php echo (L("the_latest_order")); ?>"style="padding: 10px; width: 100%; height: 100%;"
									data-options="
										href:'/wf/admin/index/../Charge/getNewsInfoPanel',
										tools:[{
											iconCls:'icon-reload',
											handler : function(){
												$('#mainnewsinfo').panel('refresh', '/wf/admin/index/../Charge/getNewsInfoPanel');
											}
										}]
									">
								</div>
							</td>
						</tr>
						<tr>
							<td valign="top" style="padding: 3px" height="300px">
								<div id="mainsalesinfo" class="easyui-panel" title="&nbsp;<?php echo (L("sales_statistics")); ?>"style="padding: 10px; width: 100%; height: 100%;"
									data-options="
										href:'/wf/admin/index/../Charge/getSalesInfoPanel',
										tools:[{
											iconCls:'icon-reload',
											handler : function(){
												$('#mainsalesinfo').panel('refresh', '/wf/admin/index/../Charge/getSalesInfoPanel');
											}
										}]
									">
								</div>
							</td>
						</tr>
						<tr>
							<td></td>
						</tr>
						
					</table>
</body>
</html>
					    <?php else: ?>
					    
					    
					    aaaaaaa<?php endif; ?>
					
					
				</div>
			</div>
		</div>onLoadEasyUI
		<script type="text/javascript">
			function addTab(a_obj, url){
				addTabBase(a_obj, url,false);
			}
			function addTabBase(a_obj, url,bool){
				if ($('#tt').tabs('exists', a_obj.title)&&!bool){/* 如果该标签已经存在，则选择该标签 ,bool=true 关闭重新打开 */
					$('#tt').tabs('select', a_obj.title);
					var pp = $('#tt').tabs('getSelected');
					
					var objs = pp.find(".easyui-datagrid");
					for(var i = 0;i < objs.length; i++){
						$(objs[i]).datagrid('reload');
					}
					
					var objs = pp.find(".easyui-combobox");
					for(var i = 0;i < objs.length; i++){
						$(objs[i]).combobox('reload');
					}
					
				} else {/* 如果该标签不存在，这新建该标签 */
					if (bool && $('#tt').tabs('exists', a_obj.title)){
						$('#tt').tabs('close', a_obj.title);
					}
					$('#tt').tabs('add',{
						title:a_obj.title,
						href: url,
						bodyCls:'content-doc',
						mode:true,
						closable:true,
						extractor:function(data){
							data = $.fn.panel.defaults.extractor(data);
							var tt = "<script>function onLoadEasyUI(){}</sc"+"ript>";
							data = tt+data;
							return data;
						},
					});
				}
			}
			
			function delTab(a_obj){
				if ($('#tt').tabs('exists', a_obj.title)){/* 如果该标签已经存在，则选择该标签 */
					$('#tt').tabs('close', a_obj.title);
				}
			}
			
			function onLoadEasyUI(){
				setInterval(intervalRun,25000);
			}
			$.parser.onComplete= function(){/* 网页加载过程的蒙板  */
				$("#loading").remove();
				onLoadEasyUI();
		    }
			$.extend($.fn.validatebox.defaults.rules, {/* 密码框与重复密码框是否一致验证 */
	        	/* 必须和某个字段相等 */
	       		equalTo: { validator: function (value, param) { return $(param[0]).val() == value; }, message: '字段不匹配' }
	        });
			
			if($.fn.propertygrid)  
			
			
			function intervalRun(){
				$('#mainclientinfo').panel('refresh');/* 刷新主页面的客户端状态 */
			}
		</script>
	
	</div>
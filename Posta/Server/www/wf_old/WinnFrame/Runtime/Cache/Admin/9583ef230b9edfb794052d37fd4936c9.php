<?php if (!defined('THINK_PATH')) exit();?>function formatLogisticsCardinfoType(val,row){
	switch(val)
	{
	case "0":
		return "<font color='red'><?php echo (L("courier")); ?></font>";
		break;
	case "1":
		return "<font color='green'><?php echo (L("the_administrator")); ?></font>";
	  break;
	default:
		return "<?php echo (L("undefined")); ?>";
	}
}


	function formatLogisticsCardinfoStatus(val,row){
		switch(val)
		{
			case "0":
				return "<font color='red'><?php echo (L("stop")); ?></font>";
				break;
			case "1":
				return "<font color='#EE6911'><?php echo (L("register_to_be_audited")); ?></font>";
				break;
			case "2":
				return "<font color='green'><?php echo (L("approved")); ?></font>";
			  break;
			case "3":
				return "<font color='blue'><?php echo (L("reject")); ?></font>";
			  break;
			default:
				return "<?php echo (L("undefined")); ?>";
		}
	}
	
	
	function formatLogisticsOrderStatus(val,row){
		switch(val)
		{
			case "0":
				return "<font><?php echo (L("complete_the_waybill")); ?> </font>";
				break;
			case "1":
				return "<font color='green'><?php echo (L("in_the_air_waybill")); ?></font>";
				break;
			case "2":
				return "<font color='blue'><?php echo (L("receiving_waybill")); ?></font>";
			  break;
			 case "3":
				return "<font color='blue'><?php echo (L("abnormal_end")); ?></font>";
			  break;
			default:
				return "<?php echo (L("undefined")); ?>";
		}
	}
	
	function formatLogisticsOrderType(val,row){
		switch(val)
		{
			case "1":
				return "<font color='green'><?php echo (L("delivery_list")); ?></font>";
				break;
			case "2":
				return "<font color='blue'><?php echo (L("delivery_note")); ?></font>";
			  break;
			default:
				return "<?php echo (L("undefined")); ?>";
		}
	}
	
	
	function formatLogisticsOrderPaystatus(val,row){
		switch(val)
		{
			case "0":
				return "<font color='red'><?php echo (L("unpaid")); ?></font>";
				break;
			case "1":
				return "<font color='green'><?php echo (L("paid")); ?></font>";
			  break;
			default:
				return "<?php echo (L("undefined")); ?>";
		}
	}
	
function refishLogisticsOrderGoing(clientid){
	addTabBase($('#waybill_information').get(0),"/wf/admin/index/../LogisticsOrder/toInfoList?clientid="+clientid,true);
}

function refishLogisticsOrderDoed(clientid){
	addTabBase($('#waybill_history').get(0),"/wf/admin/index/../LogisticsOrder/toHistoryList?clientid="+clientid,true);
}
function formatSysClientOperate(val,row){
	return "<a href=\"javascript:refishLogisticsOrderGoing("+row.id+")\" ><?php echo (L("have_in_hand")); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;"
	+"<a  href=\"javascript:refishLogisticsOrderDoed("+row.id+")\" ><?php echo (L("has_been_completed")); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;"
	+"<a href='javascript:detailAccessBox()'><?php echo (L("box_state")); ?></a>";
}


function formatNormStatus(val,row){
	switch(val)
	{
		case "0":
			return "<font ><?php echo (L("large")); ?></font>";
			break;
		case "1":
			return "<font ><?php echo (L("medium")); ?></font>";
		  break;
		case "2":
			return "<font ><?php echo (L("small")); ?></font>";
		  break;
		default:
			return "<?php echo (L("undefined")); ?>";
	}
}



function formatLogisticsCourierStatus(val,row){
	switch(val)
	{
	case "0":
		return "<font color='red'><?php echo (L("stop")); ?></font>";
		break;
	case "1":
		return "<font color='red'><?php echo (L("useing")); ?></font>";
	  break;
	
	default:
		return "<?php echo (L("undefined")); ?>";
	}
}

function formatBoxIsOpenStatus(val,row){
	switch(val)
	{
	case "0":
		return "<font color='green'><?php echo (L("s0010")); ?></font>";
		break;
	case "1":
		return "<font color='red'><?php echo (L("s0009")); ?></font>";
	  break;
	
	default:
		return "<?php echo (L("undefined")); ?>";
	}
}
<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>运单信息</title>
</head>
<body>
	<div style="padding:10px;height:96%;overflow:hidden;">
		<div id="loading" class="panel-body"></div>
		<table border="0px" width="100%" height="100%">
			<tr>
				<td height="50px" valign="top">
					<div class="easyui-panel" style="padding:5px;">
						<form id="ffLogisticsOrderInfoList">
							<table  border="0" width="100%">
								<tr>
									<td class="search-table-td"><?php echo (L("terminal_serial_number")); ?>：</td>
		    						<td class="search-table-td">
										<select class="easyui-combobox" id="LogisticsOrderInfoListClientid"  editable="false" name="clientid" 
											data-options="
												valueField:'id',
												url:'/wf/admin/logistics_order/../LogisticsClient/ajaxSelectList?sysid=logistics',
												width:150">
												<option value="<?php echo ($_GET['clientid']); ?>">=========</option>
						    			</select>
									</td>
					    			<td class="search-table-td"><?php echo (L("operator")); ?>:</td>
					    			<td class="search-table-td"><input class="easyui-textbox" type="text" id="form_LogisticsOrderInfo_telphone" name="telphone" ></input></td>
									<td class="search-table-td"><?php echo (L("order_num")); ?>:</td>
					    			<td class="search-table-td"><input class="easyui-textbox" type="text" id="form_LogisticsOrderInfo_orders" name="orders" ></input></td>
									
									
									<td>&nbsp;</td>
					    			<td width="60px">
					    				<a href="javascript:void(0)" class="easyui-linkbutton" style="width: 60px"
					    					onclick="searchLogisticsOrderInfoListForm()"><?php echo (L("query")); ?></a>
					    			</td>
					    			<td width="60px">
					    				<a href="javascript:void(0)" class="easyui-linkbutton" style="width: 60px"
					    					onclick="clearLogisticsOrderInfoListForm()"><?php echo (L("clear")); ?></a>
					    			</td>
								</tr>
							</table>
							<script type="text/javascript">
							function searchLogisticsOrderInfoListForm(){
								 $('#dgLogisticsOrderInfoList').datagrid({
						                queryParams: {
						                	clientid: $("#LogisticsOrderInfoListClientid").combobox('getValue'),
						                	orderno: $("#form_LogisticsOrderInfo_orderno").val(),
						                	telphone: $("#form_LogisticsOrderInfo_telphone").val(),
						                	orders:$("#form_LogisticsOrderInfo_orders").val(),
						                }
						        });
							}
							function clearLogisticsOrderInfoListForm(){
								$('#ffLogisticsOrderInfoList').form('clear');
							}
							</script>
						</form>
					</div>
				</td>
			</tr>
			<tr>
				<td valign="top">
					<table id="dgLogisticsOrderInfoList"  class="easyui-datagrid" title="" style="padding-top:20px"
						data-options="
						rownumbers:true,
						fit: true,
						toolbar: '#tbLogisticsOrderInfoList',
						singleSelect:true,
						pagination:true,
						url:'/wf/admin/logistics_order/../LogisticsOrder/ajaxInfoList?clientid=<?php echo ($_GET['clientid']); ?>',
						method:'get'">
						<thead>
							<tr>
								<th data-options="field:'id',width:80,hidden:true">ID</th>
								<!-- <th data-options="field:'orderno',width:150"><?php echo (L("order_num")); ?></th> -->
								<!-- <th data-options="field:'orders',width:200"><?php echo (L("business_order")); ?></th> -->
								<th data-options="field:'orders',width:200"><?php echo (L("order_num")); ?></th>
								<th data-options="field:'inputtime',width:146,formatter:formatDateBoxFull"><?php echo (L("waybill_generation_time")); ?></th>
								<!-- <th data-options="field:'type',width:150,formatter:formatLogisticsOrderType"><?php echo (L("operation_property")); ?></th> -->
								
								<th data-options="field:'telphone',width:100"><?php echo (L("operator")); ?></th>
								
								<th data-options="field:'password',width:100"><?php echo (L("extract_password")); ?></th>
								
								<th data-options="field:'clientname',width:100"><?php echo (L("terminal_serial_number")); ?></th>
								<th data-options="field:'couriername',width:100"><?php echo (L("express_company")); ?></th>
								<th data-options="field:'status',width:100,formatter:formatLogisticsOrderStatus"><?php echo (L("state")); ?></th>
								
								<!-- <th data-options="field:'updatetime',width:146,formatter:formatDateBoxFull"><?php echo (L("the_last_update_time")); ?></th> -->
								<!-- <th data-options="field:'price',width:100"><?php echo (L("order_amount")); ?></th> 
								<th data-options="field:'retention',width:100"><?php echo (L("retention_money")); ?></th>
								<th data-options="field:'paystatus',width:60,formatter:formatLogisticsOrderPaystatus"><?php echo (L("payment_status")); ?></th>
								
								<th data-options="field:'paymoney',width:100"><?php echo (L("amount_paid")); ?></th> -->
							</tr>
						</thead>
					 </table>
					 <div id="tbLogisticsOrderInfoList" style="height:auto">
					<!-- 
							<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add-item', plain:true"
								onclick="toAdd('LogisticsOrderInfoList')">&nbsp;<?php echo (L("add")); ?></a> -->
							<!-- <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-edit', plain:true"
								onclick="toEdit('LogisticsOrderInfoList','LogisticsOrder')">&nbsp;<?php echo (L("edit")); ?></a>
								 -->
							<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-lookup', plain:true"
								onclick="toLogisticsOrderView('LogisticsOrderInfoList','LogisticsOrder')">&nbsp;<?php echo (L("view")); ?></a>
								
							<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-push-pin', plain:true"
								onclick="stopOrder('LogisticsOrderInfoList','LogisticsOrder')">&nbsp;<?php echo (L("s0031")); ?></a>
								
								<!-- 
							<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-delete-item', plain:true"
								onclick="doDel('LogisticsOrderInfoList','LogisticsOrder')">&nbsp;<?php echo (L("del")); ?></a> -->
								
					</div>
					<script type="text/javascript">
					
					function stopOrder(str,controller){
						var row = $('#dg'+str).datagrid('getSelected');
						if (row){
							$.messager.confirm('<?php echo (L("warning")); ?>', '你确定终止订单吗？', function(r){
								if (r){
								 var map = new HashMap(); 
								 map.put("id",row.id); 
								 ajaxPost("/wf/admin/logistics_order/../LogisticsOrder/doStop",map);
								 $('#dgLogisticsOrderInfoList').datagrid('reload');
								}
							});
						}else{
							$.messager.alert("<?php echo (L("warning")); ?>","<?php echo (L("please_select_a_data")); ?>");
						}
					}
					function toLogisticsOrderView(str,controller){
						$("#ffxView"+str).form('disableValidation');
						var row = $('#dg'+str).datagrid('getSelected');
						if (row){
							$("#ffxView"+str).form('clear');
							
							$('#ffxView'+str).form('load', '/wf/admin/logistics_order/../'+controller+'/ajaxGetDataById?id='+row.id);
							
							$('#dlgView'+str).dialog('open');
							
							
							
							
							$('#LogisticsOrderInfoList').datalist({
							    url: '/wf/admin/logistics_order/../LogisticsBoxrecord/ajaxLogisticsBoxrecordList?clientid='+row.clientid+"&orderid="+row.id
							});
							
						}else{
							$.messager.alert("<?php echo (L("warning")); ?>","<?php echo (L("please_select_a_data")); ?>");
						}
					}
					</script>
				</td>
			</tr>
		</table>
		
		
		<!-- view_start -->
		<div id="dlgViewLogisticsOrderInfoList" class="easyui-dialog" title="&nbsp;<?php echo (L("view")); ?>"
			style="width:900px; height:400px; padding:10px;"
			data-options="
				iconCls: 'icon-push-pin',
				modal: false,
				closed: true,
				inline: true">
			
			<div class="easyui-panel" style="padding: 20px 5px 20px 5px;">
				<form id="ffxViewLogisticsOrderInfoList" method="post">
					<input type="hidden" name="id" />
					<table border="0" width="100%">
							<tr>
								<td class="double-from-lable"><?php echo (L("order_num")); ?>：</td>
								<td class="double-from-text" >
									<input class="easyui-textbox" type="text" name="orders" data-options="required:true,readonly:true" ></input>
								</td>
								<td class="double-from-lable"><?php echo (L("type")); ?>：</td>
								<td class="double-from-text" >
									<select class="easyui-combobox"  name="type" style="width: 180px" data-options="required:true,readonly:true">
										<option value="1"><?php echo (L("delivery_list")); ?></option>
				    					<option value="2"><?php echo (L("delivery_note")); ?></option>
				    				</select>
								</td>
								
							</tr>
							
							<tr>
								<td class="double-from-lable"><?php echo (L("express_company")); ?>：</td>
								<td class="double-from-text">
									<select class="easyui-combobox"  editable="false" name="courierid" style="width: 120px"
										data-options="
													required:true,
													valueField:'id',
													textField:'text',
													readonly:true,
													url:'/wf/admin/logistics_order/../LogisticsCourier/ajaxSelectList',
													queryParams: {}">
				    				</select>
								</td>
								<td class="double-from-lable"><?php echo (L("terminal_serial_number")); ?>：</td>
								<td class="double-from-text">
									<select class="easyui-combobox"  editable="false" name="clientid" style="width: 120px"
										data-options="
													required:true,
													valueField:'id',
													textField:'text',
													readonly:true,
													url:'/wf/admin/logistics_order/../LogisticsClient/ajaxSelectList?sysid=logistics',
													">
				    				</select>
								</td>
							</tr>
							<tr>
								
								<td class="double-from-lable"><?php echo (L("operator")); ?>：</td>
								<td class="double-from-text">
									<input class="easyui-textbox" type="text" name="telphone" data-options="required:true,readonly:true"></input>
								</td>
								<!-- 
								<td class="double-from-lable"><?php echo (L("order_amount")); ?>：</td>
								<td class="double-from-text" >
									<input class="easyui-textbox" type="text" name="price" data-options="readonly:true" ></input>
								</td>  -->
							<!-- </tr>
							
							
							<tr>
								<td class="double-from-lable"><?php echo (L("retention_money")); ?>：</td>
								<td class="double-from-text" >
									<input class="easyui-textbox" type="text" name="retention" data-options="readonly:true" ></input>
								</td> 
								<td class="double-from-lable"><?php echo (L("payment_status")); ?>：</td>
								<td class="double-from-text" >
									<select class="easyui-combobox"  name="paystatus" style="width: 180px" data-options="required:true,readonly:true">
										<option value="0"><?php echo (L("unpaid")); ?></option>
				    					<option value="1"><?php echo (L("paid")); ?></option>
				    				</select>
								</td>
							</tr>
							
							<tr>-->
								<!-- <td class="double-from-lable"><?php echo (L("amount_paid")); ?>：</td>
								<td class="double-from-text">
									<input class="easyui-textbox" type="text" name="paymoney" data-options="required:true,readonly:true"></input>
								</td> -->
								<td class="double-from-lable"><?php echo (L("state")); ?>：</td>
								<td class="double-from-text" colspan="3">
									<select class="easyui-combobox" editable="false" name="status" data-options="readonly:true">
				    					<option value="0"><?php echo (L("complete_the_waybill")); ?></option>
				    					<option value="1"><?php echo (L("in_the_air_waybill")); ?></option>
				    					<option value="2"><?php echo (L("receiving_waybill")); ?></option>
				    					<option value="3"><?php echo (L("abnormal_end")); ?></option>
				    				</select>
								</td>
							</tr>
							
							
			    			<tr>
								<td class="double-from-lable"><?php echo (L("waybill_generation_time")); ?>：</td>
								<td class="double-from-text">
									<input class="easyui-textbox" type="text" name="inputtime" data-options="required:true,readonly:true"></input>
								</td>
								<td class="double-from-lable"><?php echo (L("the_last_update_time")); ?>：</td>
								<td class="double-from-text">
									<input class="easyui-textbox" type="text" name="pickuptime" data-options="required:true,readonly:true"></input>
								</td>
							</tr>
			    		

					</table>
				</form>
			</div>
			<div class="easyui-panel"  style="padding: 20px 5px 20px 5px;margin-top:10px;width:100%;height:135px;">
						<div id="LogisticsOrderInfoList" class="easyui-datalist">
						
						</div>
			</div>
		</div>
		<!-- view_end -->
	</div>
	
</body>
</html>
<?php
namespace Comm\Controller;

use Think\Model;

/**
 * 证件打印柜控制器类
 * Comm\Controller$CertificateController
 * Copyright (c) 2016. 云绅（张家港）精密工业有限公司 版权所有
 * 创建人：徐辰辰
 * 创建时间：2016-4-19 下午04:40:17
 */
class CertificateController extends BaseController
{
	/*
	 * 证件打印柜控制器类架构函数
	 */
	public function _initialize()
	{
		// TODO
		parent::_initialize();
	}
	
	/**
	 * 身份验证并获取证件信息
	 * certificateInfo
	 * @return return_type
	 * @throws
	 */
	public function certificateInfo()
	{
		//令牌号
		$token = I('get.token', '');
		
		//终端ID
		$clientid = $this->tokenToId($token);
		//获取柜子id
		$box = $this->clientidToBox($clientid);
		if($box != null) {
			$boxid = $box[0]['id'];
		}
		else {
			$boxid = '0';
		}
		
		//身份证号码
		$cardid = I('get.cardid', '');
		
		//证件号码
		$barcode = I('get.barcode', '');
		
		//终端未注册
		if($clientid == '0') {
			$this->retMsg(0, 425);
		}
		//身份证号码和证件号码都没有设定
		else if($cardid == '' && $barcode == '') {
			$this->retMsg(0, 427);
		}
		//入参无误
		else {
			$model = new Model();
			
			$sql = "select
					 a.cardid
					,a.name
					,a.sex
					,a.type
					,a.validity
					,a.code
					,a.valid
					,b.url
					from wf_certificate_info a, wf_certificate_attachment b
					where a.id = b.certificateid";
			
			if($cardid != '') {
				$sql = $sql . " and a.cardid = '$cardid'";
			}
			
			if($barcode != '') {
				$sql = $sql . " and a.code = '$barcode'";
			}
			
			$data = $model->query($sql);
			
			if($data) {
				if($data[0]['validity'] < time()) {
					$this->retMsg(1, 429);
				}
				else if($data[0]['valid'] == '0') {
					$this->retMsg(1, 430);
				}
				else {
					//获取证件信息
					$ret = array('name' => $data[0]['name'],
								 'sex' => $data[0]['sex'],
								 'type' => $data[0]['type'],
								 'validity' => $data[0]['validity'],
								 'code' => $data[0]['code'],
								 'url' => $data[0]['url']);
					$jsoninfo = json_encode($ret);
					//插入打印记录
					$model = M('SysBoxrecord');
					$model->createuser = $token;
					$model->createtime = time();
					$model->boxid = $boxid;
					$model->operateuser = $data[0]['cardid'];
					$model->operatetype = 30;
					$model->operatetime = time();
					$model->remark = '证件编号：' . $data[0]['code'] . '数据获取成功';
					$model->add();
					//发送客户端信息
					$this->retMsg(2, $jsoninfo);
				}
			}
			else {
				$this->retMsg(0, 428);
			}
		}
	}
}
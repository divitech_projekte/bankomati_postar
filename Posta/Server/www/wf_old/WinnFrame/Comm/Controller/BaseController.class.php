<?php
namespace Comm\Controller;
use Common\Controller\MyController;

/**
 * 通讯模块控制器基类
 * Comm\Controller$BaseController
 * Copyright (c) 2016. 云绅（张家港）精密工业有限公司 版权所有
 * 修改人：xucc
 * 修改时间：2016-3-3 下午04:08:04
 * 修改内容：
 */
class BaseController extends MyController
{
	/*
	 * 初始化通讯模块控制器基类架构函数
	 */
	public function _initialize()
	{
		// TODO
		parent::_initialize();
	}
	
	/**
	 * 通讯模块
	 * 返回通讯状态信息
	 * @param 状态 $status 1 成功(无数据) 0 失败  2成功（有数据）
	 * @param 信息代码 $msgCode
	 */
	public function retMsg($status, $msgCode)
	{
		$data = array('st'=>$status,'info'=>$msgCode);
		$this->ajaxReturn($data);
	}
	
	/**
	 * 验证token是否存在
	 * validateToken
	 * @param token $token
	 * @return 存在：false，不存在：true
	 * @throws
	 */
	public function validateToken($token)
	{
		if($token == '') {
			$token = '0';
		}
		$model = M('SysClient');
		$data = $model->where("token=$token and status = 1")->find();
		if($data) {
			return false;
		}
		else {
			return true;
		}
	}
	
	/**
	 * token转clientid
	 * tokenToId
	 * @param token $token
	 * @return clientid or 0
	 * @throws
	 */
	public function tokenToId($token)
	{
		if($token == '') {
			$token = '0';
		}
		$model = M('SysClient');
		$data = $model->where("token='$token' and status = 1")->find();
		if($data) {
			return $data['id'];
		}
		else {
			return '0';
		}
	}
	
	/**
	 * 根据终端id获取正常柜子信息
	 * clientidToBoxid
	 * @param unknown_type $clientid
	 * @return return_type
	 * @throws
	 */
	public function clientidToBox($clientid)
	{
		$model = M('SysBox');
		$data = $model->where("clientid=$clientid and status = 1")->select();
		if($data) {
			return $data;
		}
		else {
			null;
		}
	}
	
	/**
	 * 验证字段值是否存在
	 * validateField
	 * @param 表名 $table
	 * @param 字段名 $field
	 * @param 字段值 $value
	 * @return 存在：true，不存在：false
	 * @throws
	 */
	public function validateField($table, $field, $value)
	{
		$model = M($table);
		$data = $model->where("$field='$value'")->find();
		if($data) {
			return true;
		}
		else {
			return false;
		}
	}
	
	/**
	 * 根据字段符合条件获取表中某个字段的最大值
	 * @param unknown $table
	 * @param unknown $field
	 * @param unknown $value
	 * @param unknown $start
	 * @return unknown|boolean
	 */
	public function getMaxFieldByStr($table, $field, $value, $start)
	{
		$len = strlen($value);
		$model = M($table);
		$maxData = $model->where("substr($field, $start, $len) = '$value'")->max($field);
		
		if($maxData) {
			return $maxData;
		}
		else {
			return false;
		}
	}
	
	/**
	 * 根据柜子号码获取柜子id
	 * boxnoToId
	 * @param unknown_type $clientid
	 * @param unknown_type $boxno
	 * @return return_type
	 * @throws
	 */
	public function boxnoToId($clientid, $boxno)
	{
		$model = M("SysBox");
		$data = $model->where("clientid = $clientid and no = '$boxno'")->find();
		if($data) {
			return $data['id'];
		}
		else {
			return false;
		}
	}
	
	/**
	 * token转公司信息
	 * tokenToCompanyInfo
	 * @param unknown $token
	 * @return \Think\mixed
	 */
	public function tokenToCompanyInfo($token)
	{
		//获取公司id
		$clientId = $this->tokenToId($token);
		$modelTemp = M("SysCompanyclient");
		$dataTemp = $modelTemp->where("clientid = '$clientId'")->find();
		$companyId = $dataTemp['companyid'];
		//获取地区编号
		$modelCompany = M("SysCompany");
		$dataCompany = $modelCompany->where("id = $companyId")->find();
		return $dataCompany;
	}
	
	/**
	 * token转终端配置信息
	 * @param unknown $token
	 * @return \Think\mixed|boolean
	 */
	public function tokenToClientConfig($token)
	{
		$clientId = $this->tokenToId($token);
		$model = M("SysClientconfig");
		$data = $model->where("clientid = '$clientId'")->find();
		if($data) {
			return $data;
		}
		else {
			return false;
		}
	}
	
	/**
	 * 接口调用模块
	 * 发送调用信息
	 * @param 地址，参数
	 * @return 接口返回参数
	 */
	public function send_post($url, $post_data)
	{
		$postdata = http_build_query($post_data);
		$options = array(
				'http' => array(
						'method' => 'POST',//or GET
						'header' => 'Content-type:application/x-www-form-urlencoded',
						'content' => $postdata,
						'timeout' => 2 // 超时时间*2（单位:s）
				)
		);
		$context = stream_context_create($options);
		$doCnt = 0;
		$result = null;
		while ($result == null && $doCnt < 3) {
			$doCnt++;
			$result = file_get_contents($url, false, $context);
		}
		return $result;
	}
	
	/**
	 * 写入日志
	 * @param unknown $username
	 * @param unknown $module
	 * @param unknown $action
	 * @param unknown $info
	 * @return \Think\mixed|boolean
	 */
	public function setSysLog($username, $module, $action, $info)
	{
		$model = M("SysLog");
		$model->username = $username;
		$model->time = time();
		$model->module = $module;
		$model->action = $action;
		$model->info = $info;
		$id = $model->add();
		if($id) {
			return $id;
		}
		else {
			return false;
		}
	}
}
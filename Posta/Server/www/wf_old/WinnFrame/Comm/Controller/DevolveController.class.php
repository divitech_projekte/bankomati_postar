<?php
namespace Comm\Controller;

/**
 * 下发数据控制器类
 * Comm\Controller$DevolveController
 * Copyright (c) 2016. 云绅（张家港）精密工业有限公司 版权所有
 * 修改人：xucc
 * 修改时间：2016-3-4 上午09:00:42
 * 修改内容：
 */
class DevolveController extends BaseController
{
	/*
	 * 下发数据控制器类架构函数
	 */
	public function _initialize()
	{
		// TODO
		parent::_initialize();
	}
    
    /**
     * 下发数据控制器
     * 心跳访问的方法
     */
    public function getDevolve()
    {
    	//获取get过来的token信息
	 	$token = I("get.token");
	 	
	 	//更新心跳时间
	 	$modelClient = M("SysClient");
	 	$modelClient->isonline = time();
	 	$modelClient->where("token=$token and status <> 0")->save();
	 	
	 	//实例化下发表数据
		$sysCommand = M("SysCommand");
	 	$condition['token'] = $token;
	 	$condition['status'] = 0;
	 	
	 	$res = $sysCommand->where($condition)->field('id, createtime, serialno, otype, parameter, jsoninfo')
	 			->order(array('id'=>'asc'))->limit(1)->find();
		
	 	//分析数据，生成json
	 	if($res) {
	 		//操作实时性判断
	 		if($res['otype'] == 201 || $res['otype'] == 202 || $res['otype'] == 203 || $res['otype'] == 204) {
	 			$period = time() - (int)$res['createtime'];
	 			//****************** 15秒未执行属于逾期操作 ***************************
	 			if($period > 15) {
	 				$data['id'] = $res['id'];
	 				$data['updateuser'] = $token;
					$data['updatetime'] = time();
	 				$data['status'] = 2;                                 //过期未执行
	 				$res = $sysCommand->save($data);
	 				$jsonVal = array('st'=>1);
	 				$this->ajaxReturn($jsonVal);
	 			}
	 			else {
	 				$jsonVal = array('st'=>2,
			 						 'wo'=>(int)$res['serialno'],
			 						 'cb'=>(int)$res['otype'],
			 						 'cp'=>(int)$res['parameter'],
			 						 'co'=>$res['jsoninfo']);
	 				$this->ajaxReturn($jsonVal);
	 			}
	 		}
	 		else {
	 			$jsonVal = array('st'=>2,
		 						 'wo'=>(int)$res['serialno'],
		 						 'cb'=>(int)$res['otype'],
		 						 'cp'=>(int)$res['parameter'],
		 						 'co'=>$res['jsoninfo']);
	 			$this->ajaxReturn($jsonVal);
	 		}
	 	}
	 	else {
	 		$jsonVal = array('st'=>1);
	 		$this->ajaxReturn($jsonVal);
	 	}
    }
    
    /**
     * 下发数据控制器
     * 返回下发指令执行情况
     */
    public function retDevolve()
    {
    	//获取get信息
	 	$token = I("get.token");
	 	$serialno = I("get.wo");
	 	$otype = I("get.cb");
	 	
	 	//实例化下发表数据
		$sysCommand = M("SysCommand");
		$condition['token'] = $token;
		$condition['serialno'] = $serialno;
		$condition['otype'] = $otype;
		$condition['status'] = 0;
		
		//更新字段
		$sysCommand->updateuser = $token;
		$sysCommand->updatetime = time();
		$sysCommand->status = 1;
		$sysCommand->executetime = time();
		
		$res = $sysCommand->where($condition)->save();
		
		//分析数据，生成json
		if($res) {
			$this->retMsg(1, 200);
		}
		else {
			$this->retMsg(0, 401);
		}
    }
}
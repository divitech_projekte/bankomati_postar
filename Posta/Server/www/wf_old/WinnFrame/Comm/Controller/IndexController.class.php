<?php
namespace Comm\Controller;

/**
 * 通讯模块默认访问控制器类
 * Comm\Controller$IndexController
 * Copyright (c) 2016. 云绅（张家港）精密工业有限公司 版权所有
 * 修改人：xucc
 * 修改时间：2016-3-3 下午04:10:50
 * 修改内容：
 */
class IndexController extends BaseController
{
	/*
	 * 初始化通讯模块默认访问控制器架构函数
	 */
	public function _initialize()
	{
		// TODO
		parent::_initialize();
	}
	
	/*
	 * 默认方法
	 */
    public function index()
    {
    	// TODO
    	$this->show("welcome to use winnsen  communication module.<br />");
    }
}
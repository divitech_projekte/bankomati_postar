<?php
namespace Comm\Controller;

use Think\Model;
use Think\Think;

/**
 * 客户端基本操作控制器类
 * Comm\Controller$ClientBaseController
 * Copyright (c) 2016. 云绅（张家港）精密工业有限公司 版权所有
 * 修改人：xucc
 * 修改时间：2016-3-9 上午10:11:45
 * 修改内容：
 */
class ClientBaseController extends BaseController
{
	/*
	 * 客户端基本操作控制器类架构函数
	 */
	public function _initialize()
	{
		// TODO
		parent::_initialize();
	}
	
	/**
	 * 客户端基本操作控制器
	 * 客户端停用
	 */
	public function clientDisable()
	{
		//获取get信息
	 	$token = I("get.token");
	 	$type = I("get.type");
	 	$boxNo = I("get.boxno");
	 	
	 	//未get boxno或者boxno值未输入时，默认为0
	 	if($boxNo == null || $boxNo == '')
	 		$boxNo = 0;
	 	
	 	switch ($type)
	 	{
	 		//停用一个柜子
	 		case '1':
	 			$model = new Model();
	 			$sql = "select a.status as cstatus, b.id, b.status as bstatus, b.storagestatus
						from wf_sys_client a, wf_sys_box b
						where a.id = b.clientid
						and a.token = '$token'
						and b.no = $boxNo";
	 			$data = $model->query($sql);
	 			if($data) {
	 				//状态筛选，判断是否满足停用条件
		 			if($data[0]['cstatus'] <> 1) {
		 				$this->retMsg(0, 406);
		 			}
		 			else if($data[0]['bstatus'] <> 1) {
		 				$this->retMsg(0, 407);
		 			}
		 			else if($data[0]['storagestatus'] <> 0) {
		 				$this->retMsg(0, 408);
		 			}
		 			else {
		 				//更新停用信息
		 				$sysBox = M("SysBox");
		 				$sysBox->updateuser = $token;
		 				$sysBox->updatetime = time();
		 				$sysBox->status = 2;
		 				$res = $sysBox->where('id=' . $data[0]['id'])->save();
		 				
			 			if($res) {
		    				$this->retMsg(1, 200);
		    			}
		    			else {
		    				$this->retMsg(0, 502);
		    			}
		 			}
	 			}
	 			else {
	 				$this->retMsg(0, 409);
	 			}
	 			break;
	 		//停用客户端
	 		case '2':
	 			$sysClient = M("SysClient");
	 			$dataClient = $sysClient->where("token='$token'")->find();
	 			if($dataClient) {
	 				//状态筛选，判断是否满足停用条件
	 				if($dataClient['status'] <> 1) {
		 				$this->retMsg(0, 411);
		 			}
		 			else {
		 				$sysBox = M("SysBox");
		 				$res = $sysBox->where('clientid=' . $dataClient['id'])->select();
		 				$status  = true;
		 				for($i = 0; $i < count($res); $i++) {
		 					if($res[$i]['storagestatus'] <> 0) {
		 						$status = false;
		 						break;
		 					}
		 				}
		 				if($status) {
		 					//更新停用信息
		 					$sysClient->updateuser = $token;
		 					$sysClient->updatetime = time();
		 					$sysClient->status = 2;
		 					$res = $sysClient->where("token='$token'")->save();
		 				
				 			if($res) {
			    				$this->retMsg(1, 200);
			    			}
			    			else {
			    				$this->retMsg(0, 502);
			    			}
		 				}
		 				else {
		 					$this->retMsg(0, 412);
		 				}
		 			}
	 			}
	 			else {
	 				$this->retMsg(0, 410);
	 			}
	 			break;
	 		//客户端维护
	 		case '3':
	 			$sysClient = M("SysClient");
	 			$dataClient = $sysClient->where("token='$token'")->find();
	 			if($dataClient) {
	 				//状态筛选，判断是否满足维护条件
	 				if($dataClient['status'] <> 1) {
		 				$this->retMsg(0, 414);
		 			}
		 			else {
		 				$sysBox = M("SysBox");
		 				$res = $sysBox->where('clientid=' . $dataClient['id'])->select();
		 				$status  = true;
		 				for($i = 0; $i < count($res); $i++) {
		 					if($res[$i]['storagestatus'] <> 0) {
		 						$status = false;
		 						break;
		 					}
		 				}
		 				if($status) {
		 					//更新维护信息
		 					$sysClient->updateuser = $token;
		 					$sysClient->updatetime = time();
		 					$sysClient->status = 3;
		 					$res = $sysClient->where("token='$token'")->save();
		 				
				 			if($res) {
			    				$this->retMsg(1, 200);
			    			}
			    			else {
			    				$this->retMsg(0, 502);
			    			}
		 				}
		 				else {
		 					$this->retMsg(0, 415);
		 				}
		 			}
	 			}
	 			else {
	 				$this->retMsg(0, 413);
	 			}
	 			break;
	 		default:
	 			$this->retMsg(0, 400);
	 			break;
	 	}
	}
	
	/**
	 * 客户端基本操作控制器
	 * 客户端启用
	 */
	public function clientEnable()
	{
		//获取get信息
	 	$token = I("get.token");
	 	$type = I("get.type");
	 	$boxNo = I("get.boxno");
	 	
	 	//未get boxno或者boxno值未输入时，默认为0
	 	if($boxNo == null || $boxNo == '')
	 		$boxNo = 0;
	 	
	 	switch ($type)
	 	{
	 		//启用一个柜子
	 		case '1':
	 			$model = new Model();
	 			$sql = "select a.status as cstatus, b.id, b.status as bstatus, b.storagestatus
						from wf_sys_client a, wf_sys_box b
						where a.id = b.clientid
						and a.token = '$token'
						and b.no = $boxNo";
	 			$data = $model->query($sql);
	 			if($data) {
	 				//状态筛选，判断是否满足启用条件
		 			if($data[0]['cstatus'] <> 1) {
		 				$this->retMsg(0, 416);
		 			}
		 			else if($data[0]['bstatus'] <> 2 && $data[0]['bstatus'] <> 3) {
		 				$this->retMsg(0, 417);
		 			}
		 			else {
		 				//更新启用信息
		 				$sysBox = M("SysBox");
		 				$sysBox->updateuser = $token;
		 				$sysBox->updatetime = time();
		 				$sysBox->status = 1;
		 				$res = $sysBox->where('id=' . $data[0]['id'])->save();
		 				
			 			if($res) {
		    				$this->retMsg(1, 200);
		    			}
		    			else {
		    				$this->retMsg(0, 502);
		    			}
		 			}
	 			}
	 			else {
	 				$this->retMsg(0, 418);
	 			}
	 			break;
	 		//启用客户端
	 		case '2':
	 			$sysClient = M("SysClient");
	 			$dataClient = $sysClient->where("token='$token'")->find();
	 			if($dataClient) {
	 				//状态筛选，判断是否满足启用条件
	 				if($dataClient['status'] <> 2 && $dataClient['status'] <> 3) {
		 				$this->retMsg(0, 419);
		 			}
		 			else {
			 			//更新启用信息
			 			$sysClient->updateuser = $token;
			 			$sysClient->updatetime = time();
			 			$sysClient->status = 1;
			 			$res = $sysClient->where("token='$token'")->save();
			 				
						if($res) {
				    		$this->retMsg(1, 200);
				    	}
				    	else {
				    		$this->retMsg(0, 502);
				    	}
		 			}
	 			}
	 			else {
	 				$this->retMsg(0, 420);
	 			}
	 			break;
	 		default:
	 			$this->retMsg(0, 400);
	 			break;
	 	}
	}
	
	/**
	 * 客户端基本操作控制器
	 * 是否接受远程操作
	 */
	public function remoteControl()
	{
		//获取get信息
	 	$token = I("get.token");
	 	$type = I("get.type");
	 	switch ($type)
	 	{
	 		//接受远程控制
	 		case '1':
	 			$sysClient = M("SysClient");
	 			$sysClient->isremote = 1;
	 			$res = $sysClient->where("token='$token' and status <> 0")->save();
	 			if($res) {
				 	$this->retMsg(1, 200);
				}
				else {
				 	$this->retMsg(0, 502);
				}
	 			break;
	 		//拒绝远程控制
	 		case '0':
	 			$sysClient = M("SysClient");
	 			$sysClient->isremote = 0;
	 			$res = $sysClient->where("token='$token' and status <> 0")->save();
	 			if($res) {
				 	$this->retMsg(1, 200);
				}
				else {
				 	$this->retMsg(0, 502);
				}
	 			break;
	 		default:
	 			$this->retMsg(0, 400);
	 			break;
	 	}
	}
	
	/**
	 * 客户端基本操作控制器
	 * 柜门的开关状态检测
	 */
	public function boxOpenOrClose()
	{
		$token = I("get.token");
		$boxNo = I("get.boxno");
		$type = I("get.type");
		
		$clientId = $this->tokenToId($token);
		switch ($type)
		{
			case '1':
				$sysBox = M("SysBox");
				$sysBox->updateuser = 'boxOpenOrClose';
				$sysBox->updatetime = time();
				$sysBox->isopen = 1;
				$res = $sysBox->where("clientid = '$clientId' and no = '$boxNo'")->save();
				if($res) {
					$this->retMsg(1, 200);
				}
				else {
					$this->retMsg(0, 502);
				}
				break;
			case '0':
				$sysBox = M("SysBox");
				$sysBox->updateuser = 'boxOpenOrClose';
				$sysBox->updatetime = time();
				$sysBox->isopen = 0;
				$res = $sysBox->where("clientid = '$clientId' and no = '$boxNo'")->save();
				if($res) {
					$this->retMsg(1, 200);
				}
				else {
					$this->retMsg(0, 502);
				}
				break;
			default:
	 			$this->retMsg(0, 400);
	 			break;
		}
	}
	
	/**
	 * 客户端基本操作控制器
	 * 柜门异常数据上报
	 */
	public function boxError()
	{
		//获取get信息
		$token = I("get.token");
		$boxNo = I("get.boxno");
		$model = new Model();
		$sql = "select a.status as cstatus, b.id, b.status as bstatus, b.storagestatus
				from wf_sys_client a, wf_sys_box b
				where a.id = b.clientid
				and a.token = '$token'
				and b.no = $boxNo";
		$data = $model->query($sql);
		if($data) {
			//更新停用信息
			$sysBox = M("SysBox");
			$sysBox->updateuser = $token;
			$sysBox->updatetime = time();
			$sysBox->status = 0;
			$res = $sysBox->where('id=' . $data[0]['id'])->save();
			
			if($res) {
				$sysBoxrecord = M("SysBoxrecord");
				$sysBoxrecord->createuser = 'boxError';
				$sysBoxrecord->createtime = time();
				$sysBoxrecord->boxid = $data[0]['id'];
				$sysBoxrecord->operateuser = $token;
				$sysBoxrecord->operatetype = '14';
				$sysBoxrecord->operatetime = time();
				$sysBoxrecord->remark = 'open or close the door is error.';
				$sysBoxrecord->add();
				
				$this->retMsg(1, 200);
			}
			else {
				$this->retMsg(0, 502);
			}
		}
	}
	
	/**
	 * 客户端基本操作控制器
	 * 终端掉线数据采集
	 */
	public function getOfflineClient()
	{
		$model = new Model();
		$sql = "select
				 a.csn
				,a.token
				,a.isonline
				,b.companyid
				from wf_sys_client a, wf_sys_companyclient b, wf_sys_config c
				where a.id = b.clientid
				and b.companyid = c.companyid
				and c.key = 'dropped'
				and a.status = 1
				and unix_timestamp(now()) - a.isonline > c.value * 60
				and a.isonline is not null";
		$data = $model->query($sql);
		$modelOffline = M("SysOffline");
		foreach ($data as $row) {
			$token = $row['token'];
			$onlinetime = $row['isonline'];
			$dataOffline = $modelOffline->where("token = $token and onlinetime = $onlinetime")->find();
			if($dataOffline) {
				// TODO
			}
			else {
				$modelOffline->createuser = 'getOfflineClient';
				$modelOffline->createtime = time();
				$modelOffline->token = $token;
				$modelOffline->csn = $row['csn'];
				$modelOffline->onlinetime = $row['isonline'];
				$modelOffline->companyid = $row['companyid'];
				$modelOffline->ispost = '0';
				$modelOffline->add();
			}
		}
		
		$dataPost = $modelOffline->where("ispost = '0'")->select();
		foreach ($dataPost as $row) {
			$id = $row['id'];
			$companyid = $row['companyid'];
			$modelCompany = M("SysCompany");
			$dataCompany = $modelCompany->where("id = $companyid")->find();
			switch ($dataCompany['code']) {
				case 'TTX':
					//天天鲜调用第三方接口
					$url = $dataCompany['apiurl'] . 'offlineWarning';
					$param = array(
							'terminalSN' => $row['csn'],
							'onlinetime' => $row['onlinetime'],
					);
					$json = $this->send_post($url, $param);
					if($json) {
						$objjson = json_decode($json);
						$this->setSysLog('setOrderStatus', 'API', 'setBoxRecord', json_encode($param) . '|' . $json);
						if($objjson->st == '1') {
							$modelOffline->updateuser = 'getOfflineClient';
							$modelOffline->updatetime = time();
							$modelOffline->ispost = '1';
							$modelOffline->where("id = $id")->save();
						}
						else {
							$this->setSysLog('setOrderStatus', 'DEBUG', 'setBoxRecord', json_encode($param) . '|' . $json);
						}
					}
					else {
						$this->setSysLog('setOrderStatus', 'ERROR', 'setBoxRecord', json_encode($param));
					}
					break;
				default:
					$modelOffline->updateuser = 'getOfflineClient';
					$modelOffline->updatetime = time();
					$modelOffline->ispost = '2';
					$modelOffline->where("id = $id")->save();
			}
		}
	}
}
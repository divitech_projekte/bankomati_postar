<?php
return array(
		
		'the_user_into_the_item'=>'User put the item into the ',
		'the_item_after_info'=>' cabinet.',
		'courier_romove_out_before'=>'Courier remove the item from the ',
		'courier_romove_out_after'=>' cabinet.',
		'courier_put_into_before'=>'Courier put the item into the ',
		'courier_put_into_after'=>' cabinet.',
		'user_romove_out_before'=>'User remove the item from the ',
		'user_romove_out_after'=>' cabinet.',
		'courier_return_step'=>' cabinet(return step).',
		
		'user_put_into_before'=>'User put the clothes into the ',
		'user_put_into_after'=>' cabinet.',
		'supplier_romove_out_before'=>'Supplier remove the clothes from the ',
		'supplier_romove_out_after'=>' cabinet.',
		'supplier_put_into_before'=>'Supplier put the clothes into the ',
		'supplier_put_into_after'=>' cabinet.',
		'user_romove_out_before'=>'User remove the clothes from the ',
		'user_romove_out_after'=>' cabinet.',
);
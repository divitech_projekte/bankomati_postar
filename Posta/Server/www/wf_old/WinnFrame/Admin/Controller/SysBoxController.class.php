<?php
namespace Admin\Controller;

/**
 * 柜箱信息
 * Admin\Controller$SysBoxController
 * Copyright (c) 2016. 云绅（张家港）精密工业有限公司 版权所有
 * 修改人：jcccy
 * 修改时间：2016年3月7日 下午4:54:09
 * 修改内容：
 */
class SysBoxController extends BaseController {
	public $fields = 'status,clientid,no,type,storagestatus';

	public function _initialize(){
		parent::_initialize();
	}

	/**
	 * 加载界面
	 */
	public  function toList(){
		$this->views("list");
	}
	
	
	/**
	 * 加载界面
	 */
	public  function toErrList(){
		$this->views("errList");
	}
	/**
	 * ajax数据展示
	 */
	public  function ajaxList(){
		
		$model = D('SysBox');
		
		$data = array();
		
		$datar = array();
		$datar['total']  = $model->where($data)->count();
		$datar['rows'] =  $model->alias('a')
			->field("`a`.`id`,`a`.`status`,`a`.`clientid`,`a`.`no`,`a`.`type`,`a`.`storagestatus`,`b`.`name` as `clientname`,`b`.`csn` as `clientcsn`")
			->join('LEFT JOIN `wf_sys_client` AS `b` ON `b`.`id` =`a`.`clientid`')
			->where($data)->order(array('`a`.`clientid`'=>'asc','`a`.`no`'=>'asc'))
			->page(I('get.page').','.I('get.rows'))->select();
		
		
		$this->ajaxReturn($datar);
		

	}
	
	/**
	 * ajax数据展示
	 */
	public  function ajaxErrList(){
		$model = D('SysBox');
		$data = array();
		$data['`a`.`status`'] = array('EQ','0');
		$data['`b`.`sysid`'] =  C('SYSTEM_ID');
	
		$datar = array();
	
		$datar['total']  = $model->alias('a')
		->join('LEFT JOIN `wf_sys_client` AS `b` ON `b`.`id` =`a`.`clientid`')
		->join('LEFT JOIN `wf_sys_boxconfig` AS `c` ON `c`.`boxid` = `a`.`id`')->where($data)->count();
	
	
		$datar['rows'] =  $model->alias('a')
		->field("`a`.`id`,`a`.`status`,`a`.`norm`,`a`.`clientid`,`a`.`no`,`a`.`type`,`a`.`storagestatus`,
				`b`.`name` as `clientname`,`b`.`csn` as `clientcsn`
				") 
		->join('LEFT JOIN `wf_sys_client` AS `b` ON `b`.`id` =`a`.`clientid`')
		->join('LEFT JOIN `wf_sys_boxconfig` AS `c` ON `c`.`boxid` = `a`.`id`')
		->where($data)->order(array('`a`.`clientid`'=>'asc','`a`.`no`'=>'asc'))
		->page(I('get.page').','.I('get.rows'))->select();
		
		$datar['sql'] = $model->getLastSql();
		$this->ajaxReturn($datar);
	}
	
	
	public function doReset(){
		$datab = array('msg'=>"no");
		
		$model = D('SysBox');
		$obj = $model->alias('a')
		->field('`a`.`id`,`a`.`no`,`a`.`status`,`b`.`token`')
		->where('`a`.`id`='.I('post.id',0))
		->join('LEFT JOIN `wf_sys_client` AS `b` ON `b`.`id` =`a`.`clientid`')
		->find();
		
		if($obj){
			if( false !== sendCommand($obj['token'],'204','1',$obj['no'])){
				$model->create();
				$model->status = "1";
				if($model->field("id,status")->save()){
					$datab['msg'] = "ok";
				}
			}
		}
		$this->ajaxReturn($datab);
	}
	/**
	 * ajax删除ID指定的数据
	 */
	public  function ajaxDel(){
		$model = D('SysBox');
		$id = I('post.id',0);
		$datab = array();
		$model->delete($id);
		if(true){
			$datab['msg'] = "ok";
		}else{
			$datab['msg'] = "no";
		}
		$this->ajaxReturn($datab);
	}
	
	/**
	 * ajax添加一条数据
	 */
	public  function ajaxAdd(){
		
		$datab = array();
		
	
		
		
		$model = D('SysBox');
		if( false !== $model->field($this->fields)->addTo()){
			$datab['msg'] = "ok";
		}else{
			$datab['msg'] = "no";
		}
		$this->ajaxReturn($datab);
	}
	
	/**
	 * ajax修改一条数据
	 */
	public  function ajaxEdit(){
			$datab = array();
		
			
		
			$model = D('SysBox');
			if( false !== $model->field('id,'.$this->fields)->saveTo()){
				$id = I("post.id");
				
				$model2 = D('SysBoxrecord');
				$boxrecord = array();
				$boxrecord['boxid'] = $id;
				
				$boxrecord['operateuser'] = $model->getUsername();
				//$boxrecord['operatetype'] = $sysconfigobj['value'];
				$boxrecord['operatetime'] = time();
				$boxrecord['remark'] = '后台操作';
				
				$boxrecord['createuser'] = $model->getUsername();
				$boxrecord['createtime'] = time();
				$model2->add($boxrecord);
				
				$datab['msg'] = "ok";
			}else{
				$datab['msg'] = "no";
			}
			$this->ajaxReturn($datab);
	}
	
	/**
	 * ajax获取一条数据
	 */
	public  function ajaxGetDataById(){
		$datab = array();
		$model = D('SysBox');
		$id = I('get.id',0);
		$obj = $model->where('id='.$id)->field('id,'.$this->fields)->find();
	
		$this->ajaxReturn($obj);
	}
	
	/**
	 * 跳转到牛奶柜list页面
	 */
	public function milkBoxList(){
		$this->views("milkBoxList");
	}
	
	/**
	 * ajax List牛奶柜数据
	 */
	public  function ajaxMilkList(){
	
		$model = D('SysBox');
	
		$data = array();
	
	
		if(I('get.clientid')){
			$data['`a`.`clientid`'] = I('get.clientid');
		}
		if(I('get.productid')){
			$data['`c`.`productid`'] = I('get.productid');
		}
		
		$data['`b`.`status`'] = array('NEQ','0');
		$data['`b`.`sysid`'] =  'vending';
	
		$datar = array();
		
		$datar['total']  = $model->alias('a')
		->join('LEFT JOIN `wf_sys_client` AS `b` ON `b`.`id` =`a`.`clientid`')
		->join('LEFT JOIN `wf_sys_boxconfig` AS `c` ON `c`.`boxid` = `a`.`id`')->where($data)->count();
		
		
		$datar['rows'] =  $model->alias('a')
		->field("`a`.`id`,`a`.`status`,`a`.`clientid`,`a`.`no`,`a`.`type`,`a`.`storagestatus`,
				`b`.`name` as `clientname`,`b`.`csn` as `clientcsn`,
				`c`.`maxcapacity`,CONCAT(`d`.`name`,'（',`d`.`code`,'）') as `productname`,
				(select count(*) from `wf_vending_storage` as `f` where `f`.`boxid`=`a`.`id` and (`f`.`status` = 1) and `f`.`duodate` < unix_timestamp(now())) as `duonum`,
				(select count(*) from `wf_vending_storage` as `e` where `e`.`boxid`=`a`.`id` and (`e`.`status` = 1  or `e`.`status` = 2)) as `num`")
		->join('LEFT JOIN `wf_sys_client` AS `b` ON `b`.`id` =`a`.`clientid`')
		->join('LEFT JOIN `wf_sys_boxconfig` AS `c` ON `c`.`boxid` = `a`.`id`')
		->join('LEFT JOIN `wf_vending_product` AS `d` ON `d`.`id` = `c`.`productid`')
		->where($data)->order(array('`a`.`clientid`'=>'asc','`a`.`no`'=>'asc'))
		->page(I('get.page').','.I('get.rows'))->select();
	
	
		$this->ajaxReturn($datar);
	
	
	}
	
	/**
	 * ajax获取box的配置信息（用在牛奶机上）
	 */
	public function ajaxGetConfig(){
		$model = D('SysBox');
		$obj = $model->alias('a')
		->field('`a`.`id`,`a`.`status`,`a`.`clientid`,`a`.`no`,`a`.`type`,`a`.`storagestatus`,`b`.`maxcapacity`,`b`.`productid`')
		->join('LEFT JOIN `wf_sys_boxconfig` AS `b` ON `b`.`boxid` = `a`.`id`')
		->where('`a`.`id`='.I('get.id',0))
		->find();
		
		$this->ajaxReturn($obj);
	}
	
	/**
	 * 保存货道配置
	 */
	public function ajaxSaveConfig(){
		$datab = array();
		$boo = false;
		
		$model = D('SysBoxconfig');
		$obj = $model->getByBoxid(I('post.id'));
		
		$model1 = D('SysBox');
		$boxObj = $model1->getById(I('post.id'));
		
		$model2 = D('SysClient');
		$clientObj = $model2->getById($boxObj['clientid']);
		
		$model3 = D('VendingProduct');
		$productObj = $model3->getById(I('post.productid'));
		
		if(null == $obj){
			$model->create();
			$model->boxid = I('post.id');
			
			
			$model->field('id,createtime,createuser,boxid,productid,maxcapacity')->add();
			sendCommand($clientObj['token'],'408',1,"{\"bn\":".$boxObj['no'].",\"code\":\"".$productObj['code']."\",\"max\":\"".I('post.maxcapacity')."\"}");//货道配置新增命令
		}else{
			$model->create();
			$model->field('id,updatetime,updateuser,boxid,productid,maxcapacity')->save();
			
			sendCommand($clientObj['token'],'408',2,"{\"bn\":".$boxObj['no'].",\"code\":\"".$productObj['code']."\",\"max\":\"".I('post.maxcapacity')."\"}");//货道修改新增命令
		}
		
		
		
		
		
		$datab['msg'] = "ok";
		
		
		$this->ajaxReturn($datab);
	}
	
	
	/**
	 * ajax加载下拉数据
	 */
	public  function ajaxSelectList(){
	
		$model = D('SysBox');
		$data = array();
		$data['status'] = 1;
		if(I("post.clientid")){
			$data['clientid'] = I("post.clientid");
		}
		$datar = $model->alias('a')
		->field("`a`.`id`,CONCAT(`a`.`no`,'号柜') as `text`,'' as `desc`")
		->where($data)->order(array('`a`.`id`'=>'asc'))
		->select();
		$this->ajaxReturn($datar);
	
	}
	
	
	/**
	 * 跳转到牛奶机客户端货道运行状态界面
	 */
	public function milkBoxStatusList(){//milkBoxStatusList?clientid=4
		$this->views("milkBoxStatusList");
	}
	
	/**
	 * ajax获取一条数据
	 */
	public  function ajaxGetMilkBoxProductInfoById(){
		$datab = array();
		$model = D('SysBox');
		$i=0;

		
		
	
		//$datab['total'] = "3";
		$datab['rows'] = array();
		
		/*
		  $obj = $model->alias('a')
			->field('`a`.`no`,`d`.`name` as `productname`,`d`.`code` as `productcode` ')
			->join('LEFT JOIN `wf_sys_boxconfig` AS `c` ON `c`.`boxid` = `a`.`id`')
			->join('LEFT JOIN `wf_vending_product` AS `d` ON `d`.`id` = `c`.`productid`')
			->where('`a`.`id`='.I('get.boxid'))->find();
			
		foreach($obj as $key => $value) {
			switch ($key){
				case 'no':
					$datab['rows'][$i]['name']= '货道';
					break;
				case 'productname':
					$datab['rows'][$i]['name']= '商品名称';
					break;
				case 'productcode':
					$datab['rows'][$i]['name']= '商品编码';
					break;
				default:
					$datab['rows'][$i]['name']= '未知字段';
			}
			
			$datab['rows'][$i]['value']= $value;
			$datab['rows'][$i]['group']='商品详情';
			$datab['rows'][$i]['editor']='text';
			$i++;
		}*/
		
		
		
		
		$modelStorage = D('VendingStorage');
		
		$list = $modelStorage->alias('a')//`a`.`id`,`a`.`productid`,`a`.`boxid`,`a`.`status`,`a`.`orderid`,`a`.`saletime`,`a`.`outtime`,`a`.`listid`,
		->field("`a`.`ontime`,`a`.`duodate`,`a`.`status`,`d`.`name` as `productname`")
		->join('LEFT JOIN `wf_vending_product` AS `d` ON `d`.`id` = `a`.`productid`')
		->where('`a`.`boxid`='.I('get.boxid').' and  (`a`.`status`=1 or `a`.`status`=2)')
		->order(array('`a`.`ontime`'=>'asc'))->select();
		$j = 1;
		foreach($list as $obj) {
			$group = $j.'.'.$obj['productname']."-".(1 == $obj['status']?"待销售 ":"支付完成待出货");
			foreach($obj as $key => $value) {
				//$group .= $value;
				switch ($key){
					case 'duodate':
						$datab['rows'][$i]['name']= '到期日期';
						$datab['rows'][$i]['value']= date("Y-m-d H:i",$value);
						$datab['rows'][$i]['group']=''.$group;
						$datab['rows'][$i]['editor']='text';
						$i++;
						break;
					case 'ontime':
						$datab['rows'][$i]['name']= '上货时间';
						$datab['rows'][$i]['value']= date("Y-m-d H:i",$value);
						$datab['rows'][$i]['group']=''.$group;
						$datab['rows'][$i]['editor']='text';
						$i++;
						break;
					default:
						//$datab['rows'][$i]['name']= '未知字段';
						
				}
			}
			$j++;
		}
		
		
		
		$this->ajaxReturn($datab);
	}
	
	
	
	
	/**
	 * 下发停用客户端或者箱子的命令
	 */
	public function doClientBoxStop(){
		$datab = array();
		$datab['msg'] = "no";
		
		
		$model = D('SysBox');
		$obj = $model->alias('a')
		->field('`a`.`id`,`a`.`no`,`a`.`status`,`b`.`token`')
		->where('`a`.`id`='.I('post.id',0))
		->join('LEFT JOIN `wf_sys_client` AS `b` ON `b`.`id` =`a`.`clientid`')
		->find();
		
		if($obj&& ('1' === $obj['status'])){//当前状态为启动状态时才能停止
			
			if( false !== sendCommand($obj['token'],'203','1',$obj['no'])){//$token,$otype,$parameter = '1 停用一个箱子 2 停用客户端 3 客户端维护',$jsoninfo = ''
				$model->create();
				$model->status = "2";
				if( false !== $model->field('id,status')->save()){
					$datab['msg'] = "ok";
				}
			}
		}
		$this->ajaxReturn($datab);
	}
	
	/**
	 * 下发启用客户端或者箱子的命令
	 */
	public function doClientBoxStart(){
		$datab = array();
		$datab['msg'] = "no";
		
		$model = D('SysBox');
		$obj = $model->alias('a')
		->field('`a`.`id`,`a`.`no`,`a`.`status`,`b`.`token`')
		->where('`a`.`id`='.I('post.id',0))
		->join('LEFT JOIN `wf_sys_client` AS `b` ON `b`.`id` =`a`.`clientid`')
		->find();
		
		if($obj && ('2' === $obj['status'] || '3' === $obj['status'])){//当前状态为 2：停用 3：维护状态时才能启动
			if( false !== sendCommand($obj['token'],'204','1',$obj['no'])){//$token,$otype,$parameter = '1 启用一个箱子 2 启用客户端',$jsoninfo = ''
				$model->create();
				$model->status = "1";
				if( false !== $model->field('id,status')->save()){
					$datab['msg'] = "ok";
				}
			}
		}
	
		$this->ajaxReturn($datab);
	}
	
	
	/**
	 * 跳转到洗衣柜客户端货道运行状态界面
	 */
	public function accessBoxStatusList(){//milkBoxStatusList?clientid=4
		$this->views("accessBoxStatusList");
	}
	
	
	/**
	 * ajax List洗衣柜数据
	 */
	public  function ajaxAccessList(){
	
		$model = D('SysBox');
	
		$data = array();
	
	
		if(I('get.clientid')){
			$data['`a`.`clientid`'] = I('get.clientid');
		}
		if(I('get.productid')){
			$data['`c`.`productid`'] = I('get.productid');
		}
	
		$data['`b`.`status`'] = array('NEQ','0');
		$data['`b`.`sysid`'] =  'access';
	
		$datar = array();
	
		$datar['total']  = $model->alias('a')
		->join('LEFT JOIN `wf_sys_client` AS `b` ON `b`.`id` =`a`.`clientid`')
		->join('LEFT JOIN `wf_sys_boxconfig` AS `c` ON `c`.`boxid` = `a`.`id`')->where($data)->count();
	
	
		$datar['rows'] =  $model->alias('a')
		->field("`a`.`id`,`a`.`status`,`a`.`clientid`,`a`.`no`,`a`.`type`,`a`.`storagestatus`,
				`b`.`name` as `clientname`,`b`.`csn` as `clientcsn`,
				`c`.`maxcapacity`")
					->join('LEFT JOIN `wf_sys_client` AS `b` ON `b`.`id` =`a`.`clientid`')
					->join('LEFT JOIN `wf_sys_boxconfig` AS `c` ON `c`.`boxid` = `a`.`id`')
					->where($data)->order(array('`a`.`clientid`'=>'asc','`a`.`no`'=>'asc'))
					->page(I('get.page').','.I('get.rows'))->select();
	
	
					$this->ajaxReturn($datar);
	
	
	}
	
	
}
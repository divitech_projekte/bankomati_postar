<?php
namespace Admin\Controller;

/**
 * 用户卡控制器
 * Admin\Controller$VendingConsumerController
 * Copyright (c) 2016. 云绅（张家港）精密工业有限公司 版权所有
 * 创建人：徐辰辰
 * 创建时间：2016-3-28 上午08:10:37
 */
class VendingConsumerController extends BaseController {
	
	/*
	 * 上下货清单控制器类架构函数
	 */
	public function _initialize()
	{
		// TODO
		parent::_initialize();
	}
	
	/**
	 * 卡片管理页面载入
	 * milkConsumerList
	 * @return return_type
	 * @throws
	 */
	public function milkConsumerList()
	{
		$this->views('milkconsumerlist');
	}
	
	/**
	 * 加载卡片管理页面数据
	 * ajaxMilkConsumerList
	 * @return return_type
	 * @throws
	 */
	public function ajaxMilkConsumerList()
	{
		$cardno = I('get.cardno', '');
		$username = I('get.username', '');
		
		$sql = "select
				 a.id
				,a.status
				,a.cardno
				,(select value from wf_sys_item where code = 'consumer_status' and itemname = a.status) as statusname
				,a.username
				,a.idnumber
				,a.balance
				,(select value from wf_sys_item where code = 'consumer_type' and itemname = a.type) as typename
				,from_unixtime(a.createtime) as createtime
				from wf_vending_consumer a
				where a.createtime > '1458921600'";
		
		if($cardno != "") {
			$sql = $sql . " and a.cardno = '$name'";
		}
		if($username != "") {
			$sql = $sql . " and a.username like '%$username%'";
		}
		
		$sql = $sql . " order by a.createtime desc";
		
		$datar = array();
		$datar['total']  = $this->getPaginationDataSumCount($sql);
		$datar['rows'] = $this->getPaginationData($sql, I('get.page'), I('get.rows'));
		$this->ajaxReturn($datar);
	}
	
	/**
	 * 执行卡片开卡操作
	 * ajaxMilkConsumerAdd
	 * @return return_type
	 * @throws
	 */
	public function ajaxMilkConsumerAdd()
	{
		$model = D('VendingConsumer');
		
		if($model->addTo() !== false) {
			$this->jsonData['status'] = 200;
			$this->jsonData['msg'] = '添加数据成功！';
		}
		else {
			$this->jsonData['status'] = 400;
			$this->jsonData['msg'] = '添加数据失败！';
		}
		$this->ajaxReturn($this->jsonData);
	}
	
	/**
	 * ajax逻辑删除指定数据
	 * ajaxVoid
	 * @return return_type
	 * @throws
	 */
	public function ajaxVoid()
	{
		$model = D('VendingConsumer');
		$model->create();
		$model->status = 0;
		if($model->save() !== false) {
			$this->jsonData['status'] = 200;
			$this->jsonData['msg'] = '作废数据成功！';
		}
		else {
			$this->jsonData['status'] = 400;
			$this->jsonData['msg'] = '作废数据失败！';
		}
		$this->ajaxReturn($this->jsonData);
	}
	
	/**
	 * 挂失
	 * ajaxFreeze
	 * @return return_type
	 * @throws
	 */
	public function ajaxFreeze()
	{
		$model = D('VendingConsumer');
		$model->create();
		$model->status = 2;
		if($model->save() !== false) {
			$this->jsonData['status'] = 200;
			$this->jsonData['msg'] = '挂失成功！';
		}
		else {
			$this->jsonData['status'] = 400;
			$this->jsonData['msg'] = '挂失失败！';
		}
		$this->ajaxReturn($this->jsonData);
	}
	
	/**
	 * 解挂
	 * ajaxtThaw
	 * @return return_type
	 * @throws
	 */
	public function ajaxtThaw()
	{
		$model = D('VendingConsumer');
		$model->create();
		$model->status = 1;
		if($model->save() !== false) {
			$this->jsonData['status'] = 200;
			$this->jsonData['msg'] = '解挂成功！';
		}
		else {
			$this->jsonData['status'] = 400;
			$this->jsonData['msg'] = '解挂失败！';
		}
		$this->ajaxReturn($this->jsonData);
	}
	
	/**
	 * 卡片充值页面载入
	 * milkConsumerList
	 * @return return_type
	 * @throws
	 */
	public function milkRecharge()
	{
		$this->views('milkrecharge');
	}
	
	/**
	 * 加载卡片充值页面数据
	 * ajaxMilkRechargeList
	 * @return return_type
	 * @throws
	 */
	public function ajaxMilkRechargeList()
	{
		$cardno = I('get.cardno', '');
		$username = I('get.username', '');
		
		$sql = "select
				 a.id
				,a.status
				,a.cardno
				,(select value from wf_sys_item where code = 'consumer_status' and itemname = a.status) as statusname
				,a.username
				,a.idnumber
				,a.balance
				,(select value from wf_sys_item where code = 'consumer_type' and itemname = a.type) as typename
				,from_unixtime(a.createtime) as createtime
				from wf_vending_consumer a
				where a.status = 1";
		
		if($cardno != "") {
			$sql = $sql . " and a.cardno = '$name'";
		}
		if($username != "") {
			$sql = $sql . " and a.username like '%$username%'";
		}
		
		$sql = $sql . " order by a.createtime desc";
		
		$datar = array();
		$datar['total']  = $this->getPaginationDataSumCount($sql);
		$datar['rows'] = $this->getPaginationData($sql, I('get.page'), I('get.rows'));
		$this->ajaxReturn($datar);
	}
	
	/**
	 * ajax获取一条数据
	 * ajaxGetDataById
	 * @return return_type
	 * @throws
	 */
	public function ajaxGetDataById()
	{
		$model = D('VendingConsumer');
		$id = I('get.id', 0);
		$obj = $model->where("id=$id")->find();
		$this->ajaxReturn($obj);
	}
	
	/**
	 * 充值
	 * ajaxMilkRecharge
	 * @return return_type
	 * @throws
	 */
	public function ajaxMilkRecharge()
	{
		$recharge = I('post.recharge', 0);
		$balance = I('post.balance', 0);
		$cardno = I('post.cardno', '');
		$remark = I('post.remark', '');
		
		$model = D('VendingConsumer');
		$model->create();
		$model->balance = $balance + $recharge;
		
		if($model->save() !== false) {
			$this->jsonData['status'] = 200;
			$this->jsonData['msg'] = '充值成功！';
		}
		else {
			$this->jsonData['status'] = 400;
			$this->jsonData['msg'] = '充值失败！';
		}
		
		//插入充值记录
		$model = M('VendingConsumerrecord');
		$model->createuser = getUserInfo();
		$model->createtime = time();
		$model->cardno = $cardno;
		$model->type = 1;
		$model->amountchange = $recharge;
		$model->remark = $remark;
		$model->add();
		
		$this->ajaxReturn($this->jsonData);
	}
	
	/**
	 * 退款
	 * ajaxMilkRefund
	 * @return return_type
	 * @throws
	 */
	public function ajaxMilkRefund()
	{
		$refund = I('post.refund', 0);
		$balance = I('post.balance', 0);
		$cardno = I('post.cardno', '');
		$remark = I('post.remark', '');
		
		$model = D('VendingConsumer');
		$model->create();
		$model->balance = $balance - $refund;
		
		if($model->save() !== false) {
			$this->jsonData['status'] = 200;
			$this->jsonData['msg'] = '退款成功！';
		}
		else {
			$this->jsonData['status'] = 400;
			$this->jsonData['msg'] = '退款失败！';
		}
		
		//插入退款记录
		$model = M('VendingConsumerrecord');
		$model->createuser = getUserInfo();
		$model->createtime = time();
		$model->cardno = $cardno;
		$model->type = 6;
		$model->amountchange = $refund;
		$model->remark = $remark;
		$model->add();
		
		$this->ajaxReturn($this->jsonData);
	}
}
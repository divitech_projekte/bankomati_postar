<?php
namespace Admin\Controller;
use Think\Model;
/**
 * 
 * Admin\Controller$ChargeBoxrecordController
 * Copyright (c) 2016. 云绅（张家港）精密工业有限公司 版权所有
 * 修改人：jcccy
 * 修改时间：2016年6月16日 上午9:37:43
 * 修改内容：
 */
class ChargeBoxrecordController extends SysBoxrecordController {
	
	/*
	 * 店铺管理控制器类架构函数
	 */
	public function _initialize()
	{
		// TODO
		parent::_initialize();
	}
	
	public function chargeHistoryList(){
		$this->views("chargeHistoryList");
	}

	
	
	public function ajaxChargePrintList(){
		$clientid = I('get.clientid', '');
		$cardid = I('get.cardid', '');
		$orderno = I('get.orderno', '');
	
		
		$userid = getUserId();
		$where = "";
		if('1'!=$userid){
			$where = "AND `f`.`userid`=$userid ";
		}
		
		$sql = "select
				 c.id
				,a.csn as clientname
				,a.position
				,c.operateuser
				,FROM_UNIXTIME(c.operatetime) as operatetime
				,c.remark
				,d.orderno
				from wf_sys_client a, wf_sys_box b, wf_sys_boxrecord c,wf_charge_order d,wf_sys_companyclient AS e,wf_sys_companyuser AS f
				where a.id = b.clientid and a.id = e.clientid AND e.companyid = f.companyid $where
				and b.id = c.boxid
				and a.sysid = 'charge'
				and (c.operatetype = 21 or c.operatetype = 22 or c.operatetype = 23 or c.operatetype = 24 or c.operatetype = 25) -- 21：客户存入物品 22：快递员取出物品  23：快递员存入物品 24：客户取出物品 25: 退件
				and c.orderid = d.id
				";
	
	
		if($clientid != "") {
			$sql = $sql . " and a.id = '$clientid'";
		}
	
		if($orderno != "") {
			$sql = $sql . " and d.orderno like '%$orderno%'";
		}
	
		if($cardid != "") {
			$sql = $sql . " and c.operateuser = '$cardid'";
		}
	
		$sql = $sql . " order by c.createtime desc";
	
		$datar = array();
		$datar['total']  = $this->getPaginationDataSumCount($sql);
		$datar['rows'] = $this->getPaginationData($sql, I('get.page'), I('get.rows'));
		$model = new Model();
		$datar['sql'] = $model->getLastSql();
		$this->ajaxReturn($datar);
	}
	
	
	public function ajaxChargeBoxrecordList(){
		
		$model = D('SysBoxrecord');
		/*
		$data = array();
		
		if(I("get.clientid")){
			$data['`c`.`id`'] = I("get.clientid");
		}
		if(I("get.orderid")){
			$data['`a`.`orderid`'] = I("get.orderid");
		}
		*/
		$clientid = I("get.clientid", 0);
		$orderid = I("get.orderid", 0);
		$where = "c.id = $clientid and (a.orderid = $orderid";
		
		$modelOrder = M("ChargeOrder");
		$dataOrder = $modelOrder->where("id = $orderid")->find();
		if($dataOrder) {
			$orders = $dataOrder['orders'];
			if($orders != '') {
				$dataTemp = $modelOrder->where("orders = '$orders' and id != $orderid")->select();
				foreach ($dataTemp as $row) {
					$orderidTemp = $row['id'];
					$where = $where . " or a.orderid = $orderidTemp";
				}
			}
		}
		$where = $where . ")";
		
		$datar = array();
		$datar['rows']  = $model->alias('a')
		->field("CONCAT('".L('time')."：',from_unixtime(`a`.`operatetime`, '%Y-%m-%d %H:%i:%S'),'&nbsp;&nbsp;','".L('type')."：',IF(`a`.`operatetype`=10,'".L('remote_open')."',IF(`a`.`operatetype`=11,'".L('close_the_box')."',IF(`a`.`operatetype`=12,'".L('box_enabled')."',IF(`a`.`operatetype`=13,'".L('box_stop')."',IF(`a`.`operatetype`=21,'".L('the_client_deposited_in_the_item')."',IF(`a`.`operatetype`=22,'".L('courier_out_items')."',IF(`a`.`operatetype`=23,'".L('courier_deposit_articles')."',IF(`a`.`operatetype`=24,'".L('customer_take_out_items')."',IF(`a`.`operatetype`=25,'".L('courier_out_items_return')."',IF(`a`.`operatetype`=29,'送货员上货',IF(`a`.`operatetype`=30,'证件打印','未定意'))))))))))) ,'&nbsp;&nbsp;&nbsp;&nbsp;','".L('memo')."：',`a`.`remark`,'&nbsp;&nbsp;&nbsp;&nbsp;','".L('operator')."：',IFNULL(`a`.`operateuser`,''),'&nbsp;&nbsp;&nbsp;&nbsp;') as `text`")
		->join(array('LEFT JOIN `wf_sys_box` AS `b` ON `b`.`id` =`a`.`boxid`'
				,'LEFT JOIN `wf_sys_client` AS `c` ON `c`.`id` =`b`.`clientid`'))
		->where($where)->order(array('`a`.`id`'=>'asc'))->select();
		$datar['sql'] = $model->getLastSql();
		$this->ajaxReturn($datar);
	}
}
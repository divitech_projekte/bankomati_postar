<?php
namespace Admin\Controller;

use Think\Model;

/**
 * 快递公司信息
 * Admin\Controller$LogisticsCourierController
 * Copyright (c) 2016. 云绅（张家港）精密工业有限公司 版权所有
 * 修改人：jcccy
 * 修改时间：2016年6月16日 上午8:30:07
 * 修改内容：
 */
class LogisticsCourierController extends BaseController {
	public $fields = 'status,name,code,short,address,linkman,phone,email,regdate';
	
	public function _initialize(){
		parent::_initialize();
	}
	
	/**
	 * 加载界面
	 */
	public  function toList(){
		$this->views("list");
	}
	
	/**
	 * ajax加载分页数据
	 */
	public  function ajaxList(){
		$model = D('LogisticsCourier');
		$data = array();
		
		
		if("" != I('get.name',"")){
			$data['name'] = array('like','%'.I('get.name').'%');
		}
		
		if("" != I('get.code',"")){
			$data['code'] = array('like','%'.I('get.code').'%');
		}
		
		$companyids = getCompanyidsByUid(getUserId());
		$where = "1!=1";
		foreach($companyids as $vo){
			$where .= " or a.companyid=".$vo['companyid'];
		}
		
		$datar = array();
		$datar['total']  = $model->where($data)->count();
		$datar['rows'] =  $model->alias('a')
		->field("`a`.`id`,`a`.`status`,`a`.`name`,`a`.`code`,`a`.`short`,
				`a`.`address`,`a`.`linkman`,`a`.`phone`,`a`.`email`,`a`.`regdate`")
		//->join('LEFT JOIN `wf_sys_client` AS `b` ON `b`.`id` =`a`.`clientid`')
		//->join('LEFT JOIN `wf_vending_store` AS `c` ON `c`.`id` =`a`.`storeid`')
		->where($where)
		->where($data)->order(array('`a`.`id`'=>'asc'))
		->page(I('get.page').','.I('get.rows'))->select();
		$datar['sql'] = $companyids;
		
		$this->ajaxReturn($datar);
		
		
		
		
		

	}
	
	/**
	 * ajax删除ID指定的数据
	 */
	public  function ajaxDel(){
		$datab = array("msg"=>"no");
		$model = D('LogisticsCourier');
		$id = I('post.id',0);
		if($model->delete($id)){
			$datab['msg'] = "ok";
		}
		$this->ajaxReturn($datab);
	}
	
	/**
	 * ajax添加一条数据
	 */
	public  function ajaxAdd(){
		$datab = array("msg"=>"no");
		$companyids = getCompanyidsByUid(getUserId());
		
		$model = D('LogisticsCourier');
		$model->create();
		$model->companyid = $companyids[0]['companyid'];
		//$datab['sql'] = $model->companyid;
		$model->regdate = time();//intval(strtotime(I("post.regdate")));
		$id = $model->field('id,createtime,createuser,companyid,'.$this->fields)->add();
		if($id){
			
			$clientids = I("post.clientids");
			if($clientids){
				$model3 = D('LogisticsCourierclient');
				$model3->where('courierid='.$id)->delete();
				foreach ($clientids as $val){
					if(0 != intval($val)){
						$model3->courierid = $id;
						$model3->clientid = $val;
						$model3->field('courierid,clientid')->add();
					}
				}
			}
			
			
			
			$datab['msg'] = "ok";
		}
		$this->ajaxReturn($datab);
	}
	
	/**
	 * ajax修改一条数据
	 */
	public  function ajaxEdit(){
			$datab = array("msg"=>"no");
			$model = D('LogisticsCourier');
			$model->create();
			//$model->regdate = time();//intval(strtotime(I("post.regdate")));
			if($model->field('id,updatetime,updateuser,'.$this->fields)->save()){
				$id = I("post.id");
				$clientids = I("post.clientids");
				if($clientids){
					$model3 = D('LogisticsCourierclient');
					$model3->where('courierid='.$id)->delete();
					foreach ($clientids as $val){
						if(0 != intval($val)){
							$model3->courierid = $id;
							$model3->clientid = $val;
							$model3->field('courierid,clientid')->add();
						}
					}
				}
				
				$datab['msg'] = "ok";
			}
			$this->ajaxReturn($datab);
	}
	
	
	/**
	 * ajax获取一条数据
	 */
	public  function ajaxGetDataById(){
		$datab = array();
		$model = D('LogisticsCourier');
		$id = I('get.id',0);
		$obj = $model->alias('a')->field("`a`.`id`,`a`.`status`,`a`.`name`,`a`.`code`,`a`.`short`,`a`.`address`,`a`.`linkman`,`a`.`phone`,`a`.`email`,`a`.`regdate`,IFNULL(GROUP_CONCAT(`c`.`clientid`),'') AS `clientids[]`,IFNULL(GROUP_CONCAT(`d`.`name`,'[',`d`.`csn`,']'),'') AS `clientname[]`")
		->join('LEFT JOIN `wf_logistics_courierclient` AS `c` ON `c`.`courierid` = `a`.`id`')
		->join('LEFT JOIN `wf_sys_client` AS `d` ON `d`.`id` = `c`.`clientid`')
		->where('`a`.`id`='.$id)->find();
	
		
		
		
		
		
		$this->ajaxReturn($obj);
	}
	
	
	/**
	 * ajax加载下拉数据
	 */
	public  function ajaxSelectList(){
	
		$model = D('LogisticsCourier');
		$data = array();
	
		$companyids = getCompanyidsByUid(getUserId());
		$where = "1!=1";
		foreach($companyids as $vo){
			$where .= " or a.companyid=".$vo['companyid'];
		}
		
		
		
		$datar = $model->alias('a')
		
		->field("`a`.`id`,`a`.`name` as `text`,'' as `desc`")
		->where($where)
		->where($data)->order(array('`a`.`id`'=>'asc'))
		->select();
		$this->ajaxReturn($datar);
	
	}
	
	public function  ajaxSelectClientListByCourierid(){
		$datar = array();
		$courierid = I("get.courierid");
		
		if($courierid){
			$model = D ( 'LogisticsCourierclient' );
			$datar = $model->alias ( 'a' )
			->field ( "`d`.`id`,CONCAT(`d`.`name`,'-',`d`.`csn`) as `text`,'' as `desc`" )
			->join ( 'LEFT JOIN `wf_sys_client` AS `d` ON `d`.`id` =`a`.`clientid`' )
			->where ( "`a`.`courierid`=".$courierid )
			->select();
		}
		$this->ajaxReturn ( $datar );
	}
	
}
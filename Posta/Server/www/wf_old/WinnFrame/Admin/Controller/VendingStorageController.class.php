<?php
namespace Admin\Controller;

use Think\Model;

/**
 * 库存及销售数据控制器
 * Admin\Controller$VendingStorageController
 * Copyright (c) 2016. 云绅（张家港）精密工业有限公司 版权所有
 * 创建人：徐辰辰
 * 创建时间：2016-3-21 下午01:06:39
 */
class VendingStorageController extends BaseController {
	
	/*
	 * 上下货清单控制器类架构函数
	 */
	public function _initialize()
	{
		// TODO
		parent::_initialize();
	}
	
	/**
	 * 跳转生成上下货清单页面
	 * milkStorageList
	 * @return return_type
	 * @throws
	 */
	public function milkStorageList()
	{
		$type = I('get.type', '0');
		$listno = I('get.listno', '');
		
		$this->assign('type', $type);
		$this->assign('listno', $listno);
		$this->views('milkStorageList');
	}
	
	/**
	 * 生成上下货清单
	 * ajaxBuildStorageInfo
	 * @return return_type
	 * @throws
	 */
	public function ajaxBuildStorageInfo()
	{
		$clientid = I('post.clientid', '0');
		//$clientid = 8;
		
		//以客户端编号和上货状态为生成未上货判断是否存在未上货清单数据
		$model = D('VendingConsignment');
		$ret = $model->where("clientid = $clientid and status = 1")->find();
		if($ret != null) {
			$this->jsonData['status'] = 300;
			$this->jsonData['msg'] = '该终端存在待上下货清单，无法重复生成！如要重新生成该终端上下货清单，请先作废之前生成的清单，谢谢！';
			$this->ajaxReturn($this->jsonData);
		}
		else {
			$model = new Model();
			$sql = "select
					 a.id
					,a.clientid
					,a.no
					,a.status
					,b.maxcapacity
					,c.id as productid
					,c.shelflife
					from wf_sys_box a left join wf_sys_boxconfig b
					on a.id = b.boxid left join (select id, shelflife from wf_vending_product where valid = 1) c
					on b.productid = c.id
					where a.clientid = $clientid";
			//获取终端的柜门信息
			$dataClient = $model->query($sql);
			if($dataClient)
			{
				//获取最大上下货清单号码
				$model = D('VendingConsignment');
				$maxListno = $model->where("substr(listno, 1, 8) = date_format(now(), '%Y%m%d')")->max(listno);
				if($maxListno == null) {
					$maxListno = date('Ymd', time()) . '000';
				}
				
				$istrue = true;
				for($i = 0; $i < count($dataClient); $i++) {
					if($dataClient[$i]['maxcapacity'] == null || $dataClient[$i]['productid'] == null) {
						$istrue = false;
						break;
					}
				}
				if($istrue) {
					
					$createtime = time();
					
					//新增Consignment信息
					foreach($dataClient as $row) {
						$model = D('VendingStorage');
						$boxid = $row['id'];
						//库存数量
						$stockCount = count($model->where("status = 1 and boxid = $boxid")->select());
						//近效期数量
						$nearepCount = count($model->where("unix_timestamp(now()) > duodate and status = 1 and boxid = $boxid")->select());
						//判断货道状态，1为启用，2为停用
						if($row['status'] == 1) {
							//本次下架数量
							$takedownCount = $nearepCount;
							//本次上货数量
							$amount = $row['maxcapacity'] - ($stockCount - $takedownCount);
						}
						else {
							//本次下架数量
							$takedownCount = $stockCount;
							//本次上货数量
							$amount = 0;
						}
						
						$conModel = D('VendingConsignment');
						$conModel->createuser = getUserInfo();
						$conModel->createtime = $createtime;
						$conModel->clientid = $clientid;
						$conModel->listno = $maxListno + 1;
						$conModel->status = 1;
						$conModel->boxid = $boxid;
						$conModel->stock = $stockCount;
						$conModel->nearep = $nearepCount;
						$conModel->takedown = $takedownCount;
						$conModel->amount = $amount;
						
						$conModel->add();
					}
					$this->jsonData['status'] = 200;
					$this->jsonData['msg'] = $maxListno + 1 . '|生成上下货清单成功';
					$this->ajaxReturn($this->jsonData);
				}
				else {
					$this->jsonData['status'] = 400;
					$this->jsonData['msg'] = '货道配置信息错误，无法生成上下货清单，请检查货道配置信息';
					$this->ajaxReturn($this->jsonData);
				}
			}
			else {
				$this->jsonData['status'] = 400;
				$this->jsonData['msg'] = '终端配置信息错误，无法生成上下货清单，请检查终端配置信息';
				$this->ajaxReturn($this->jsonData);
			}
		}
	}
	
	/**
	 * 加载上下货清单数据
	 * ajaxMilkStorageList
	 * @return return_type
	 * @throws
	 */
	public function ajaxMilkStorageList()
	{
		$listno = I('get.listno', '0');
		if($listno == '') {
			$listno = '0';
		}
		$clientid = I('get.clientid');
		$sql = "select * from
				(select a.name as clientname, b.no as boxno, d.value as status, b.id as mboxid
				 from wf_sys_client a, wf_sys_box b,
						  (select distinct clientid from wf_vending_consignment where listno = $listno) c,
						  (select itemname, value from wf_sys_item where code = 'box_status') d
				 where a.id = b.clientid
				 and a.id = c.clientid
				 and b.status = d.itemname) m
				left join
				(select id, boxid as nboxid, listno, stock, takedown, amount from wf_vending_consignment where listno = $listno) n
				on m.mboxid = n.nboxid
				left join
				(select a.boxid as sboxid, b.name as productname, a.maxcapacity 
				 from wf_sys_boxconfig a, wf_vending_product b
				 where a.productid = b.id) s
				on m.mboxid = s.sboxid";
		
		$sql = $sql . " order by m.boxno";
		
		$datar = array();
		$datar['total']  = $this->getPaginationDataSumCount($sql);
		$datar['rows'] = $this->getPaginationData($sql, I('get.page'), I('get.rows'));
		$this->ajaxReturn($datar);
	}
	
	/**
	 * 跳转上下货记录页面
	 * milkStorageRecord
	 * @return return_type
	 * @throws
	 */
	public function milkStorageRecord()
	{
		$this->views("milkRecordList");
	}
	
	/**
	 * 加载上下货记录数据
	 * ajaxMilkRecordList
	 * @return return_type
	 * @throws
	 */
	public function ajaxMilkRecordList()
	{
		$clientid = I('get.clientid', '');
		
		$sql = "select
				 c.id
				,a.name as clientname
				,a.csn
				,b.no
				,c.operateuser
				,from_unixtime(c.operatetime) as operatetime
				,c.remark
				from wf_sys_client a, wf_sys_box b, wf_sys_boxrecord c
				where a.id = b.clientid
				and b.id = c.boxid
				and c.operatetype = 29";
		
		if($clientid != '') {
			$sql = $sql . " and a.id = $clientid";
		}
		
		$sql = $sql . " order by c.operatetime desc, b.no";
		
		$datar = array();
		$datar['total']  = $this->getPaginationDataSumCount($sql);
		$datar['rows'] = $this->getPaginationData($sql, I('get.page'), I('get.rows'));
		$this->ajaxReturn($datar);
	}
	
	public function milkStatistics1(){
		$this->views("milkStatistics1");
	}
	public function ajaxMilkStorage1(){
		$datar = array();
		$datar['tooltip'] = array();
		$datar['tooltip']['trigger'] = 'axis';
		$datar['tooltip']['axisPointer'] = array();
		$datar['tooltip']['axisPointer']['type'] = 'shadow';
		$datar['legend'] = array();
		$datar['legend']['data'] = array();
		//$datar['legend']['data'] = array('直接访问1','邮件营销2','联盟广告','视频广告','搜索引擎','百度','谷歌','必应','其他');
		
		$model = D('VendingProduct');
		$list = $model->alias('a')
		->field("`a`.`id`,`a`.`name`")
		->where("`a`.`valid`=1")->order(array('`a`.`id`'=>'asc'))
		->select();
		foreach ($list as $key=>$val)
		{
			array_push($datar['legend']['data'],$val['name']);
		}
		
		//array_push($datar['legend']['data'],'直接访问','邮件营销','联盟广告','视频广告','搜索引擎','百度','谷歌','必应','其他');
		/*
		$datar['legend']['data'][] = '直接访问';
		$datar['legend']['data'][] = '邮件营销';
		$datar['legend']['data'][] = '联盟广告';
		$datar['legend']['data'][] = '视频广告';
		$datar['legend']['data'][] = '搜索引擎';
		$datar['legend']['data'][] = '百度';
		$datar['legend']['data'][] = '谷歌';
		$datar['legend']['data'][] = '必应';
		$datar['legend']['data'][] = '其他';
		*/
		$datar['grid'] = array();
		$datar['grid']['left'] = '3%';
		$datar['grid']['right'] = '4%';
		$datar['grid']['bottom'] = '3%';
		$datar['grid']['containLabel'] = true;
		$datar['xAxis'] = array();
		$datar['xAxis'][0]['type'] = 'category';
		$datar['xAxis'][0]['data'] = array();
		//$datar['xAxis'][0]['data'] = array('周一', '周二', '周三', '周四', '周五', '周六', '周日');
		
		$wheresaletime = " ";
		if(I('post.saletime_start')){
			$wheresaletime .= " and `a`.`saletime` > ".strtotime(I('post.saletime_start',0));
		}
		if(I('post.saletime_end')){
			$wheresaletime .= " and `a`.`saletime` < ".strtotime(I('post.saletime_end',0) . ' 23:59:59.999');
		}
		
		
		
		$model = D('SysClient');
		$list2 = $model->alias('a')
		->field("`a`.`id`,`a`.`name`")
		->where("`a`.`status`=1 and `a`.`sysid`='vending'")->order(array('`a`.`id`'=>'asc'))
		->select();
		foreach ($list2 as $key=>$val)
		{
			array_push($datar['xAxis'][0]['data'],$val['name']);
		}
		
		
		$datar['yAxis'] = array();
		$datar['yAxis'][0]['type'] = 'value';
		
		$datar['series'] = array();
		
		foreach ($list as $key=>$val)//商品
		{
			$datar['series'][$key] = array();
			$datar['series'][$key]['name'] = $val['name'];//商品名称
			$datar['series'][$key]['type'] = 'bar';
			$datar['series'][$key]['data'] = array();
			//$datar['series'][$key]['data'] = array(320, 332, 301, 334);//该商品在各个终端的数量
			foreach ($list2 as $key2=>$val2)//终端
			{
				$model = D('VendingStorage');
				$count = $model->alias('a')
				->join('LEFT JOIN `wf_sys_box` AS `b` ON `b`.`id`=`a`.`boxid`')
				->where(" `a`.`status`=3 and `a`.`productid`=".$val['id']." and `b`.`clientid`=".$val2['id'].$wheresaletime)
				->count();
				array_push($datar['series'][$key]['data'],$count);
			}
		}
		
		
		$this->ajaxReturn($datar);
	}
	public function milkStatistics2(){
		$this->views("milkStatistics2");
	}
	public function ajaxMilkStorage2(){
		$datar = array();
		$num = 12;//12个月
		
		$year = date('Y');
		$month = date('m');
		
		if(I('post.year')){
			$year = I('post.year');
		}
		
		
		if(I('post.month')){
			$month = I('post.month') + 1;
		}
		
		$d=mktime(0, 0, 0, $month, 1, $year);//时，分，秒，月，日，年（向前推12个月份最晚时间点）
		
		
		$datar['title'] = array();
		$total_amount_of_sales =  L('total_amount_of_sales');//L('total_amount_of_sales');
		$datar['title']['text'] = L('monthly_sales_situation');//'月度别销售情况'; 
		
		$datar['tooltip'] = array();
		$datar['tooltip']['trigger'] = 'axis';
		
		$datar['legend'] = array();
		//$datar['legend']['data'] = array('邮件营销','联盟广告','视频广告','直接访问','搜索引擎');
		
		$datar['legend']['data'] = array($total_amount_of_sales);
		
		$datar['grid'] = array();
		$datar['grid']['left'] = '3%';
		$datar['grid']['right'] = '4%';
		$datar['grid']['bottom'] = '3%';
		$datar['grid']['containLabel'] = true;
		
		$datar['toolbox'] = array();
		$datar['toolbox']['feature'] =  array();
		$datar['toolbox']['feature']['saveAsImage'] = array();
		$datar['xAxis'] = array();
		$datar['xAxis']['type'] = 'category';
		$datar['xAxis']['boundaryGap'] = false;
		$type = "Ym";
		
		
		$datar['xAxis']['data'] = array();
		/*
		$datar['xAxis']['data'] = array(
				date($type, strtotime("-12 month")),
				date($type, strtotime("-11 month")),
				date($type, strtotime("-10 month")),
				date($type, strtotime("-9 month")),
				date($type, strtotime("-8 month")),
				date($type, strtotime("-7 month")),
				date($type, strtotime("-6 month")),
				date($type, strtotime("-5 month")),
				date($type, strtotime("-4 month")),
				date($type, strtotime("-3 month")),
				date($type, strtotime("-2 month")),
				date($type, strtotime("-1 month")),
		);
		*/
		for($i = 0;$i < $num;$i++){
			array_push($datar['xAxis']['data'],date($type, strtotime(($i-$num)." month",$d)));
		}
		
		$datar['yAxis'] = array();
		$datar['yAxis']['type'] = 'value';
		
		$datar['series'] = array();
		
		$datar['series'][0] = array();
		$datar['series'][0]['name'] = $total_amount_of_sales;//'销售总金额';
		$datar['series'][0]['type'] = 'line';
		$datar['series'][0]['stack'] = '总金额';
		$datar['series'][0]['data'] = array();
		//$datar['series'][0]['data'] = array(120, 132, 101, 134, 90, 230, 210, 101, 134, 90, 230, 210);
		for($i = 0;$i < $num;$i++){
			
			$starttime = strtotime(date('Y-m-01', strtotime(($i-$num)." month",$d)));//开始时间戳
			$endtime = strtotime(date('Y-m-01', strtotime(($i-$num+1)." month",$d)));//结束时间戳
			
			$model = D('VendingOrder');
			$list = $model->field('SUM(`a`.`paymoney`) as `allmoney`')->alias('a')->where('`a`.`status`=0 and `a`.`updatetime` >= '.$starttime.' and `a`.`updatetime` < '.$endtime)->select();
			array_push($datar['series'][0]['data'],$list[0]['allmoney']);
		}
		
		$this->ajaxReturn($datar);
	}
	
	public function milkStatistics3(){
		$this->views("milkStatistics3");
	}
	
	public function ajaxMilkStorage3(){
		$datar = array();
		$datar['title'] = array();
		$datar['title']['text'] = '销售占比统计';
		$datar['title']['subtext'] = '数量统计';
		$datar['title']['x'] = 'center';
		
		$datar['tooltip'] = array();
		$datar['tooltip']['trigger'] = 'item';
		$datar['tooltip']['formatter'] = "{a} <br/>{b} : {c} ({d}%)";
		
		$datar['legend'] = array();
		$datar['legend']['orient'] = 'vertical';
		$datar['legend']['left'] = 'left';
		$datar['legend']['data'] = array();
		//$datar['legend']['data'] = array('直接访问','邮件营销','联盟广告','视频广告','搜索引擎');
		
		$model = D('VendingProduct');
		$list = $model->alias('a')
		->field("`a`.`id`,`a`.`name`")
		->where("`a`.`valid`=1")->order(array('`a`.`id`'=>'asc'))
		->select();
		foreach ($list as $key=>$val)
		{
			array_push($datar['legend']['data'],$val['name']);
		}
		
		
		$datar['series'] = array();
		$datar['series'][0]['name'] = '访问来源';
		$datar['series'][0]['type'] = 'pie';
		$datar['series'][0]['radius'] = '55%';
		$datar['series'][0]['center'] = array('50%', '60%');
		$datar['series'][0]['data'] = array();
		
		$wheresaletime = " ";
		if(I('post.saletime_start')){
			$wheresaletime .= " and `a`.`saletime` > ".strtotime(I('post.saletime_start',0));
		}
		if(I('post.saletime_end')){
			$wheresaletime .= " and `a`.`saletime` < ".strtotime(I('post.saletime_end',0) . ' 23:59:59.999');
		}
		
		foreach ($list as $key=>$val)//遍历产品
		{
			$model = D('VendingStorage');
			$count = $model->alias('a')
			->join('LEFT JOIN `wf_sys_box` AS `b` ON `b`.`id`=`a`.`boxid`')
			->where(" `a`.`status`=3 and `a`.`productid`=".$val['id']." ".$wheresaletime)
			->count();
			
			$datar['series'][0]['data'][$key] = array();
			$datar['series'][0]['data'][$key]['value'] = $count;
			$datar['series'][0]['data'][$key]['name'] = $val['name'];
		}
		
		/*
		$datar['series'][0]['data'][1]['value'] = 310;
		$datar['series'][0]['data'][1]['name'] = '邮件营销';
		
		$datar['series'][0]['data'][2]['value'] = 234;
		$datar['series'][0]['data'][2]['name'] = '联盟广告';
		
		$datar['series'][0]['data'][3]['value'] = 135;
		$datar['series'][0]['data'][3]['name'] = '视频广告';
		
		$datar['series'][0]['data'][4]['value'] = 1548;
		$datar['series'][0]['data'][4]['name'] = '搜索引擎';
		*/
		$datar['series'][0]['itemStyle'] = array();
		$datar['series'][0]['itemStyle']['emphasis'] = array();
		$datar['series'][0]['itemStyle']['emphasis']['shadowBlur'] = 10;
		$datar['series'][0]['itemStyle']['emphasis']['shadowOffsetX'] = 0;
		$datar['series'][0]['itemStyle']['emphasis']['shadowColor'] = 'rgba(0, 0, 0, 0.5)';
		$this->ajaxReturn($datar);
	}
	
	
	/*
	 * 跳转到“上货单据”打印页面
	 */
	public function printMilkStorage(){
		
		$listno = I('get.listno', '0');
		if($listno == '') {
			$listno = '0';
		}

		$sql = "select
				 clientname
				,boxno
				,status
				,createtime
				,id
				,listno
				,ifnull(stock, '-') as stock
				,ifnull(takedown, '-') as takedown
				,ifnull(amount, '-') as amount
				,productname
				,maxcapacity
				from
				(select a.name as clientname, b.no as boxno, d.value as status, c.createtime, b.id as mboxid
				 from wf_sys_client a, wf_sys_box b,
				(select distinct clientid, from_unixtime(createtime) as createtime from wf_vending_consignment where listno = $listno) c,
				(select itemname, value from wf_sys_item where code = 'box_status') d
				 where a.id = b.clientid
				 and a.id = c.clientid
				 and b.status = d.itemname) m
				left join
				(select id, boxid as nboxid, listno, stock, takedown, amount from wf_vending_consignment where listno = $listno) n
				on m.mboxid = n.nboxid
				left join
				(select a.boxid as sboxid, b.name as productname, a.maxcapacity
				 from wf_sys_boxconfig a, wf_vending_product b
				 where a.productid = b.id) s
				on m.mboxid = s.sboxid";
		
		$sql = $sql . " order by m.boxno";
		
		
		$model = new Model();
		$list = $model->query($sql);
		$this->assign("storageList", $list);
		$this->assign("listno", $list[0]['listno']);
		$this->assign("clientname", $list[0]['clientname']);
		$this->assign("createtime", $list[0]['createtime']);
		$this->views("printMilkStorage");
	}
	
}









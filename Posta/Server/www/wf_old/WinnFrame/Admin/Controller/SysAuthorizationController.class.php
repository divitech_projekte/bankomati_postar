<?php
namespace Admin\Controller;

/**
 * 系统权限数据
 * Admin\Controller$SysAuthorizationController
 * Copyright (c) 2016. 云绅（张家港）精密工业有限公司 版权所有
 * 修改人：jcccy
 * 修改时间：2016年3月7日 下午4:54:02
 * 修改内容：
 */
class SysAuthorizationController extends BaseController {
	public $fields = 'status,pid,name,action,target,type,sort';
	
	public function _initialize(){
		parent::_initialize();
	}
	
	/**
	 * 加载界面
	 */
	public  function toList(){
		$this->views("list");
	}
	
	/**
	 * ajax加载分页数据
	 */
	public  function ajaxList(){
		
		$model = D('SysAuthorization');
		
		$data = array();
		if("" != I('get.name',"")){
			$data['name'] = array('like','%'.I('get.name').'%');
		}
		if("" != I('get.action',"")){
			$data['action'] = array('like','%'.I('get.action').'%');
		}
		
		$datar = array();
		$datar['total']  = $model->where($data)->count();
		$datar['rows'] =  $model->alias('a')
		->field("`a`.`id`,`a`.`status`,`a`.`pid`,`a`.`name`,`a`.`action`,`a`.`target`,`a`.`type`,`a`.`sort`")
		->where($data)->order(array('`a`.`sort`'=>'asc'))
		->page(I('get.page').','.I('get.rows'))->select();
		
		
		$this->ajaxReturn($datar);
		
	}
	
	/**
	 * ajax加载分页数据
	 */
	public  function ajaxListByRole(){
	
		$model = D('SysAuthorization');
		$roleid = I('get.roleid','');
		$data = array();
		if("" != I('get.name',"")){
			$data['name'] = array('like','%'.I('get.name').'%');
		}
		if("" != I('get.action',"")){
			$data['action'] = array('like','%'.I('get.action').'%');
		}
	
		$datar = array();
		$datar['total']  = $model->where($data)->count();
		if("" != $roleid){
			$datar['rows'] =  $model->alias('a')
			->field("`a`.`id`,`a`.`status`,`a`.`pid`,`a`.`name`,`a`.`action`,`a`.`target`,`a`.`type`,`a`.`sort`,(select (SELECT IF (0=(select count(*) from wf_sys_roleauthorise as b where b.menuid=a.id and b.roleid=".$roleid."),'','true') AS  `checked`)) AS  `checked`")
			->where($data)->order(array('`a`.`id`'=>'asc'))
			->page(I('get.page').','.I('get.rows'))->select();
		}else{
			$datar['rows'] =  $model->alias('a')
			->field("`a`.`id`,`a`.`status`,`a`.`pid`,`a`.`name`,`a`.`action`,`a`.`target`,`a`.`type`,`a`.`sort`")
			->where($data)->order(array('`a`.`sort`'=>'asc'))
			->page(I('get.page').','.I('get.rows'))->select();
			
		}
		$this->ajaxReturn($datar);
	}
	
	/**
	 * ajax删除ID指定的数据
	 */
	public  function ajaxDel(){
		$model = D('SysAuthorization');
		$id = I('post.id',0);
		$datab = array();
		$model->delete($id);
		if(true){
			$datab['msg'] = "ok";
		}else{
			$datab['msg'] = "no";
		}
		$this->ajaxReturn($datab);
	}
	
	/**
	 * ajax添加一条数据
	 */
	public  function ajaxAdd(){
		
		$model = D('SysAuthorization');
		if( false !== $model->field($this->fields)->addTo()){
			$datab['msg'] = "ok";
		}else{
			$datab['msg'] = "no";
		}
		$this->ajaxReturn($datab);
	}
	
	/**
	 * ajax修改一条数据
	 */
	public  function ajaxEdit(){
			$datab = array();
		
		
			$model = D('SysAuthorization');
			if( false !== $model->field('id,'.$this->fields)->saveTo()){
				$datab['msg'] = "ok";
			}else{
				$datab['msg'] = "no";
			}
			$this->ajaxReturn($datab);
	}
	
	/**
	 * ajax获取一条数据
	 */
	public  function ajaxGetDataById(){
		$datab = array();
		$model = D('SysAuthorization');
		$id = I('get.id',0);
		$obj = $model->where('id='.$id)->field('id,'.$this->fields)->find();
	
		$this->ajaxReturn($obj);
	}
	
	/**
	 * 跳转到分配权限页面
	 */
	public function toAllotAuthList(){
		$this->views("allotAuth");
	}
	
	
	
	
	
}
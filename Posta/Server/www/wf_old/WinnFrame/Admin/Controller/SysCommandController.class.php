<?php
namespace Admin\Controller;

/**
 * 系统下发命令
 * Admin\Controller$SysCommandController
 * Copyright (c) 2016. 云绅（张家港）精密工业有限公司 版权所有
 * 修改人：jcccy
 * 修改时间：2016年3月7日 下午4:54:26
 * 修改内容：
 */
class SysCommandController extends BaseController {
	public $fields = 'status,serialno,executetime,token,otype,parameter,jsoninfo';
	
	
	
	
	public function _initialize(){
		parent::_initialize();
	}

	/**
	 * 加载界面
	 */
	public  function toList(){
		$this->views("list");
	}
	
	/**
	 * ajax加载分页数据
	 */
	public  function ajaxList(){
		
		$model = D('SysCommand');
		$data = array();
		
		$datar = array();
		$datar['total']  = $model->where($data)->count();
		
		$datar['rows'] =  $model->alias('a')
			->field("`a`.`id`,`a`.`status`,`a`.`serialno`,`a`.`executetime`,`a`.`token`,`a`.`otype`,`a`.`parameter`,`a`.`jsoninfo`,`b`.`name` as `clientname`,`b`.`csn` as `clientcsn`")
			->join('LEFT JOIN `wf_sys_client` AS `b` ON `b`.`token` =`a`.`token`')
			->where($data)->order(array('`a`.`id`'=>'asc'))
			->page(I('get.page').','.I('get.rows'))->select();
		
		
		$this->ajaxReturn($datar);
	}
	
	/**
	 * ajax删除ID指定的数据
	 */
	public  function ajaxDel(){
		$model = D('SysCommand');
		$id = I('post.id',0);
		$datab = array();
		$model->delete($id);
		if(true){
			$datab['msg'] = "ok";
		}else{
			$datab['msg'] = "no";
		}
		$this->ajaxReturn($datab);
	}
	
	/**
	 * ajax添加一条数据
	 */
	public  function ajaxAdd(){
		
		$datab = array();
		
	
		
		
		$model = D('SysCommand');
		if( false !== $model->field($this->fields)->addTo()){
			$datab['msg'] = "ok";
		}else{
			$datab['msg'] = "no";
		}
		$this->ajaxReturn($datab);
	}
	
	/**
	 * ajax修改一条数据
	 */
	public  function ajaxEdit(){
			$datab = array();
		
			
		
			$model = D('SysCommand');
			if( false !== $model->field('id,'.$this->fields)->saveTo()){
				$datab['msg'] = "ok";
			}else{
				$datab['msg'] = "no";
			}
			$this->ajaxReturn($datab);
	}
}
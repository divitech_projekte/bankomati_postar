<?php
namespace Admin\Controller;

/**
 * 
 * Admin\Controller$LogisticsClientController
 * Copyright (c) 2016. 云绅（张家港）精密工业有限公司 版权所有
 * 修改人：jcccy
 * 修改时间：2016年6月16日 上午9:37:43
 * 修改内容：
 */
class LogisticsSmsstencilController extends SysSmsstencilController {
	
	/*
	 * 店铺管理控制器类架构函数
	 */
	public function _initialize()
	{
		// TODO
		parent::_initialize();
	}
	
	public function logisticsSmsstencilList(){
		$this->views("logisticsSmsstencilList");
	}
	
	/**
	 * ajax短信模板加载分页数据
	 */
	public  function ajaxSmsstencilList(){
		$model = D('SysSmsstencil');
	
		$data = array();
	
	
		if("" != I('get.info',"")){
			$data['info'] = array('like','%'.I('get.info').'%');
		}
	
	
		if("" != I('get.code',"")){
			$data['code'] = I('get.code');
		}
	
		if("" != I('get.type',"")){
			$data['type'] = I('get.type');
		}
	
		if("" != I('get.status',"")){
			$data['status'] = I('get.status');
		}
	
	
	
	
		$data['sysid'] = C('SYSTEM_ID');
		
		$companyids = getCompanyidsByUid(getUserId());
		$where = "1!=1";
		foreach($companyids as $vo){
			$where .= " or a.companyid=".$vo['companyid'];
		}
	
		$datar = array();
		$datar['total']  = $model->where($data)->count();
		$datar['rows'] =  $model->alias('a')
		->field("`a`.`id`,`a`.`info`,`a`.`code`,`a`.`type`,`a`.`status`")
		->where($where)
		->where($data)->order(array('`a`.`id`'=>'asc'))
		->page(I('get.page').','.I('get.rows'))->select();
	
	
		$this->ajaxReturn($datar);
	}
	/**
	 * ajax添加一条数据
	 */
	public  function ajaxAdd(){
		$datab = array("msg"=>"no");
		$model = D('SysSmsstencil');
	
		$type = I('post.type');
		$code = I('post.code');
		$status = I('post.status');
		$sysid = C('SYSTEM_ID');
		$companyids = getCompanyidsByUid(getUserId());
		$companyid = $companyids[0]['companyid'];
		$obj = $model->where("type='$type' and code='$code' and status='$status' and companyid=$companyid")->count();
		if(0 == $obj  || '0' == I('post.status')){
			$model->create();
			$model->sysid =  $sysid;
			$model->companyid = $companyid;
			if( false !== $model->field("id,createtime,createuser,sysid,companyid,".$this->fields)->add()){
				$datab['msg'] = "ok";
			}
		}else{
			$datab['data'] = "已经含有相同的模板类型和业务类型的模板被启用！";
		}
	
		$this->ajaxReturn($datab);
	}
	

	/**
	 * ajax修改一条数据
	 */
	public  function ajaxEdit(){
		$datab = array("msg"=>"no");
		$model = D('SysSmsstencil');
	
		$objx = $model->getById(I('post.id',0));
		$type = $objx['type'];
		$code = $objx['code'];
		$companyids = getCompanyidsByUid(getUserId());
		$companyid = $companyids[0]['companyid'];
		$obj = $model->where("type='$type' and code='$code' and status='1' and companyid=$companyid")->find();
		if(null === $obj || '0' == I('post.status') || $objx === $obj){
			$model->create();
			if( false !== $model->field('id,updatetime,updateuser,info,status')->save()){
				$datab['msg'] = "ok";
			}
		}else{
			$datab['data'] = "已经含有相同的模板类型和业务类型的模板被启用！";
		}
		$this->ajaxReturn($datab);
	}
}
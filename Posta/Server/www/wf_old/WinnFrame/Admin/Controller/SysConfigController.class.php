<?php
namespace Admin\Controller;

/**
 * 系统配置信息
 * Admin\Controller$SysConfigController
 * Copyright (c) 2016. 云绅（张家港）精密工业有限公司 版权所有
 * 修改人：jcccy
 * 修改时间：2016年3月7日 下午4:54:40
 * 修改内容：
 */
class SysConfigController extends BaseController {
	
	public function _initialize(){
		parent::_initialize();
	}

	/**
	 * 加载界面
	 */
	public  function toList(){
		$this->views("list");
	}
	
	/**
	 * ajax加载分页数据
	 */
	public  function ajaxList(){

		$model = D('SysConfig');
		$data = array();
		
		
		if("" != I('get.key',"")){
			$data['`a`.`key`'] = array('like','%'.I('get.key').'%');
		}
		
		if("" != I('get.value',"")){
			$data['`a`.`value`'] = array('like','%'.I('get.value').'%');
		}
		
		
		if("" != I('get.sysid',"")){
			$data['`a`.`sysid`'] = I('get.sysid');
		}
		
		$datar = array();
		$datar['total']  = $model->alias('a')->where($data)->count();
		
		$datar['rows'] =  $model->alias('a')
		->field("`a`.`id`,`a`.`key`,`a`.`sysid`,`a`.`status`,`a`.`value`,`a`.`remark`")
		->where($data)->order(array('`a`.`id`'=>'asc'))
		->page(I('get.page').','.I('get.rows'))->select();
		
		$this->ajaxReturn($datar);
		

	}
	/**
	 * 加载界面
	 */
	public  function accessConfigList(){
		$this->views("accessConfigList");
	}
	
	
	
	/**
	 * ajax删除ID指定的数据
	 */
	public  function ajaxDel(){
		$datab = array("msg"=>"no");
		/*
		$id = I('post.id',0);
		$model = D('SysConfig');
		if( false !== $model->delete($id))
		{
		$datab['msg'] = "ok";
		}
		*/
		$this->ajaxReturn($datab);
	}
	
	/**
	 * ajax添加一条数据
	 */
	public  function ajaxAdd(){
		$datab = array("msg"=>"no");
		
		$model = D('SysConfig');
		$model->create();
		
		$model->status = "1";
		
		$ownerid = $model->field('id,status,sysid,key,value,remark')->add();
		if( false !== $ownerid){
			$datab['msg'] = "ok";
		}
			
		$this->ajaxReturn($datab);
	}
	
	/**
	 * ajax修改一条数据
	 */
	public  function ajaxEdit(){
			$datab = array("msg"=>"no");
			
			$model = D('SysConfig');
			$model->create();
			if( false !== $model->field('id,updatetime,updateuser,status,sysid,key,value,remark')->save()){
				$obj = $model->getById(I("post.id",0));
				
					
				$model = D('SysClient');
				$clientlist = $model->alias('a')
				->field('`a`.`token`')
				->where("`a`.`status`!=0  and `a`.`sysid` = 'access'")->select();
				
				$jsoninfo = json_encode(array('key' => $obj['key'],'value' => I("post.value")),JSON_UNESCAPED_UNICODE);
				foreach($clientlist as $val){
					sendCommand($val['token'],'412',2,$jsoninfo);//系统参数设置下发接口
				}
				
				
				$datab['msg'] = "ok";
			}
			$this->ajaxReturn($datab);
	}
	
	
	/**
	 * ajax获取一条数据
	 */
	public  function ajaxGetDataById(){
		$model = D('SysConfig');
		$id = I('get.id',0);
		$obj = $model->getById($id);
	
		$this->ajaxReturn($obj);
	}
	
	
	
	/**
	 * ajax加载分页数据
	 */
	public  function ajaxAccessConfigList(){
	
		$model = D('SysConfig');
		$data = array();
	
	
		if("" != I('get.key',"")){
			$data['`a`.`key`'] = array('like','%'.I('get.key').'%');
		}
	
		if("" != I('get.value',"")){
			$data['`a`.`value`'] = array('like','%'.I('get.value').'%');
		}
	
	
		$data['`a`.`sysid`'] = "access";
	
		$datar = array();
		$datar['total']  = $model->alias('a')->where($data)->count();
	
		$datar['rows'] =  $model->alias('a')
		->field("`a`.`id`,`a`.`key`,`a`.`sysid`,`a`.`status`,`a`.`value`,`a`.`remark`")
		->where($data)->order(array('`a`.`id`'=>'asc'))
		->page(I('get.page').','.I('get.rows'))->select();
	
		$this->ajaxReturn($datar);
	
	
	}
}
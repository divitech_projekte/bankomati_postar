<?php

namespace Admin\Controller;

use Think\Model;

/**
 * Admin\Controller$LogisticsClientController
 * Copyright (c) 2016.
 * 云绅（张家港）精密工业有限公司 版权所有
 * 修改人：jcccy
 * 修改时间：2016年6月16日 上午9:37:43
 * 修改内容：
 */
class LogisticsClientController extends SysClientController {
	
	/*
	 * 店铺管理控制器类架构函数
	 */
	public function _initialize() {
		// TODO
		parent::_initialize ();
	}
	
	/**
	 * ajax加载分页数据
	 */
	public function ajaxSelectList() {
		$model = D ( 'SysClient' );
		$data = array ();
		$data ['status'] = 1;
		if (I ( 'get.sysid' )) {
			$data ['sysid'] = I ( 'get.sysid' );
		}
		
		$companyids = getCompanyidsByUid ( getUserId () );
		$where = "1!=1";
		foreach ( $companyids as $vo ) {
			$where .= " or d.companyid=" . $vo ['companyid'];
		}
		$where .= "";
		
		$datar = $model->alias ( 'a' )->field ( "`a`.`id`,CONCAT(`a`.`csn`) as `text`,'' as `desc`" )->join ( 'LEFT JOIN `wf_sys_companyclient` AS `d` ON `d`.`clientid` =`a`.`id`' )->where ( $where )->where ( $data )->order ( array (
				'`a`.`id`' => 'asc' 
		) )->select ();
		// $datar['aaa']=$model->getLastSql();
		$this->ajaxReturn ( $datar );
	}
	public function logisticsClientList() {
		$this->views ( "logisticsClientList" );
	}
	public function ajaxLogisticsClientList() {
		// $model = D('SysClient');
		$model = new Model ();
		$data = array ();
		
		if ("" != I ( 'get.name', "" )) {
			$data ['`a`.`name`'] = array (
					'like',
					'%' . I ( 'get.name' ) . '%' 
			);
		}
		if ("" != I ( 'get.csn', "" )) {
			$data ['`a`.`csn`'] = array (
					'like',
					'%' . I ( 'get.csn' ) . '%' 
			);
		}
		
		$data ['`a`.`sysid`'] = 'logistics';
		
		$userid = getUserId ();
		$where = "";
		if ('1' != $userid) {
			$where = "AND `f`.`userid`=$userid ";
		}
		
		$datar = array ();
		$datar ['total'] = intval ( $model->table ( 'wf_sys_client as `a`,wf_sys_clientconfig AS `b`,wf_sys_companyclient AS `e`,wf_sys_companyuser AS `f` ' )->where ( "`b`.`clientid` = `a`.`id`  and `a`.`id` = `e`.`clientid` AND `e`.`companyid` = `f`.`companyid` $where" )->where ( $data )->count () );
		$datar ['rows'] = $model->table ( 'wf_sys_client as `a`,wf_sys_clientconfig AS `b`,wf_sys_companyclient AS `e`,wf_sys_companyuser AS `f` ' )->field ( "`a`.`id`,`a`.`longitude`,`a`.`latitude`,`a`.`status`,`a`.`csn`,`a`.`token`,`a`.`name`,`a`.`position`,`a`.`boxnum`,`a`.`type`,`a`.`isonline`,`a`.`isremote`,`b`.`emergency`,`b`.`isremotepower`" )->where ( "`b`.`clientid` = `a`.`id`  and `a`.`id` = `e`.`clientid` AND `e`.`companyid` = `f`.`companyid` $where" )->where ( $data )->order ( array (
				'`a`.`id`' => 'asc' 
		) )->page ( I ( 'get.page' ) . ',' . I ( 'get.rows' ) )->select ();
		
		$datar ['total'] = $datar ['total'] + intval ( $model->table ( 'wf_sys_client as `a`,wf_sys_companyclient AS `e`,wf_sys_companyuser AS `f` ' )->where ( "`a`.`status`=4 and `a`.`id` = `e`.`clientid` AND `e`.`companyid` = `f`.`companyid` $where" )->where ( $data )->count () );
		$datar ['rows'] = array_merge ( $datar ['rows'], $model->table ( 'wf_sys_client as `a`,wf_sys_companyclient AS `e`,wf_sys_companyuser AS `f` ' )->field ( "`a`.`id`,`a`.`longitude`,`a`.`latitude`,`a`.`status`,`a`.`csn`,`a`.`token`,`a`.`name`,`a`.`position`,`a`.`boxnum`,`a`.`type`,`a`.`isonline`,`a`.`isremote`" )->where ( "`a`.`status`=4 and `a`.`id` = `e`.`clientid` AND `e`.`companyid` = `f`.`companyid` $where" )->where ( $data )->order ( array (
				'`a`.`id`' => 'asc' 
		) )->page ( I ( 'get.page' ) . ',' . I ( 'get.rows' ) )->select () );
		
		$this->ajaxReturn ( $datar );
	}
	
	/**
	 * 跳转到智能洗衣柜运行状态界面
	 */
	public function logisticsStatusList() { //
		$this->views ( "logisticsStatusList" );
	}
	public function ajaxLogisticsClientStatusList() {
		$model = D ( 'SysClient' );
		
		$data = array ();
		
		if ("" != I ( 'get.name', "" )) {
			$data ['name'] = array (
					'like',
					'%' . I ( 'get.name' ) . '%' 
			);
		}
		if ("" != I ( 'get.csn', "" )) {
			$data ['csn'] = array (
					'like',
					'%' . I ( 'get.csn' ) . '%' 
			);
		}
		
		$data ['status'] = array (
				'NEQ',
				'0' 
		);
		
		$data ['sysid'] = 'logistics';
		
		$userid = getUserId ();
		$where = "";
		if ('1' != $userid) {
			$where = "AND `f`.`userid`=$userid ";
		}
		
		$datar = array ();
		$datar ['total'] = $model->table ( 'wf_sys_client as `a`,wf_sys_clientconfig AS `b`,wf_sys_companyclient AS `e`,wf_sys_companyuser AS `f` ' )->where ( "`b`.`clientid` = `a`.`id` and `a`.`id` = `e`.`clientid` AND `e`.`companyid` = `f`.`companyid` $where" )->where ( $data )->count ();
		$datar ['rows'] = $model->table ( 'wf_sys_client as `a`,wf_sys_clientconfig AS `b`,wf_sys_companyclient AS `e`,wf_sys_companyuser AS `f` ' )->field ( "`a`.`id`,`a`.`status`,`a`.`csn`,`a`.`token`,`a`.`name`,`a`.`position`,`a`.`boxnum`,`a`.`type`,`a`.`isonline`,`a`.`isremote`,`b`.`emergency`,`b`.`isremotepower`" )->where ( "`b`.`clientid` = `a`.`id` and `a`.`id` = `e`.`clientid` AND `e`.`companyid` = `f`.`companyid` $where" )->where ( $data )->order ( array (
				'`a`.`id`' => 'asc' 
		) )->page ( I ( 'get.page' ) . ',' . I ( 'get.rows' ) )->select ();
		
		$this->ajaxReturn ( $datar );
	}
	
	/**
	 * 保存牛奶机客户端的配置信息
	 */
	public function ajaxSaveConfig() {
		$datab = array ();
		$datab ['msg'] = "no";
		
		$unit = I('post.unit', 0);
		
		$boo = false;
		$model = D ( 'SysClient' );
		$clientid = I ( 'post.id' );
		$obj = $model->getById ( $clientid );
		if ($obj && ('1' === $obj ['status'] || '2' === $obj ['status'] || '3' === $obj ['status'])) { // 状态只能是1：启用（可使用状态） 2：停用 3：维护 被配置
			$model2 = D ( 'SysClientconfig' );
			$obj2 = $model2->getByClientid ( $clientid );
			if (null == $obj2) {
				$model2->create ();
				$model2->createuser = getUserInfo ();
				$model2->createtime = time ();
				$model2->clientid = $clientid;
				$model2->unit = $unit;
				$model2->field ( "createtime,createuser,clientid,emergency,unit" )->add ();
			} else {
				$model2->create ();
				$model2->updateuser = getUserInfo ();
				$model2->updatetime = time ();
				$model2->id = $obj2 ['id'];
				$model2->unit = $unit;
				$model2->field ( 'id,updatetime,updateuser,emergency,unit' )->save ();
			}
			$cardids = I ( "post.cardids" );
			
			if($cardids){
			
				$model3 = D ( 'LogisticsCardclient' );
				$model3->where ( 'clientid=' . $clientid )->delete ();
				foreach ( $cardids as $val ) {
					if (0 != intval ( $val )) {
						$model3->cardid = $val;
						$model3->clientid = $clientid;
						$model3->field ( 'cardid,clientid' )->add ();
					}
				}
			}
			
			
			
			$model4 = D ( "LogisticsCardclient" );
			$courier = $model4->alias ( 'a' )->field ( "`c`.`id`,`c`.`name`,`c`.`phone`" )->join ( "LEFT JOIN `wf_logistics_cardinfo` AS `b` ON `b`.`id`=`a`.`cardid`" )->join ( "LEFT JOIN `wf_logistics_courier` AS `c` ON `c`.`id`=`b`.`courierid`" )->where ( "`a`.`clientid`=" . $clientid )->group ( "`c`.`id`" )->select ();
			
			$jsoninfo = json_encode ( array (
					'emergency' => I ( 'post.emergency' ), // 求助电话
					'unit' => $unit, // 费率
					'courier' => $courier // 快递公司信息下发
			) );
			
			sendCommand ( $obj ['token'], '413', 2, $jsoninfo );
			
			
			
			
			$datab ['msg'] = "ok";
		}
		
		$this->ajaxReturn ( $datab );
	}
	
	/**
	 * ajax点击配置获取数据
	 */
	public function ajaxGetConfigDataById() {
		$datab = array ();
		$model = D ( 'SysClient' );
		$id = I ( 'get.id', 0 );
		$obj = $model->alias ( 'a' )->field ( "GROUP_CONCAT(IFNULL(`c`.`cardid`,'')) AS `cardids[]`,GROUP_CONCAT(`d`.`cardno`,'[',`e`.`name`,']') AS `cardnames[]`,`a`.`id`,`a`.`status`,`a`.`csn`,`a`.`token`,`a`.`name`,`a`.`position`,`a`.`boxnum`,`a`.`type`,`a`.`isonline`,`a`.`isremote`,`b`.`emergency`,`b`.`isremotepower`,`b`.`unit`,`b`.`freetime`,`b`.`interval`,`b`.`fee`,`b`.`owner`,`b`.`supplier`" )->join ( 'LEFT JOIN `wf_sys_clientconfig` AS `b` ON `b`.`clientid` = `a`.`id`' )->

		join ( 'LEFT JOIN `wf_logistics_cardclient` AS `c` ON `c`.`clientid` = `a`.`id`' )->join ( 'LEFT JOIN `wf_logistics_cardinfo` AS `d` ON `d`.`id` = `c`.`cardid`' )->join ( 'LEFT JOIN `wf_logistics_courier` AS `e` ON `e`.`id` = `d`.`courierid`' )->where ( '`a`.`id`=' . $id )->find ();
		
		$this->ajaxReturn ( $obj );
	}
	public function ajaxVerify() {
		$datab = array ();
		
		$model = D ( 'SysClient' );
		$clientid = I ( 'post.id' );
		$csn = I ( 'post.csn' );
		
		$num = strpos ( $csn, "-" );
		if ($num) {
			$csn = substr ( $csn, 0, $num ); // csn取-前面字符串
		}
		
		$obj = $model->getById ( $clientid ); // where(array('id'=>I('post.id')))->find();
		
		$boo = false;
		
		if (4 == $obj ['status']) { // 注册待审核状态
			$sysbox = D ( 'SysBox' );
			$sysconfig = D ( 'SysConfig' ); // 系统配置表里提取boxtype值
			$sysconfigobj = $sysconfig->getByKey ( 'boxtype' );
			$objnorm = $sysconfig->where ( "`sysid`='logistics' and `status`=1 and `key`='norm' and `value`='$csn'" )->find ();
			$remark = $objnorm ['remark'];
			
			$remark = str_replace ( "[", "", $remark );
			$remark = str_replace ( "]", "", $remark );
			
			$remarkarr = explode ( ",", $remark );
			
			// 创建柜门信息
			for($i = 0; $i < $obj ['boxnum']; $i ++) {
				$box = array ();
				$box ['clientid'] = $obj ['id'];
				$box ['no'] = $i + 1;
				$box ['type'] = $sysconfigobj ['value'];
				$box ['status'] = 1;
				$box ['storagestatus'] = 0;
				$box ['norm'] = $remarkarr [$i];
				$box ['createuser'] = $obj ['createuser'];
				$box ['createtime'] = time ();
				$sysbox->add ( $box );
			}
			
			// 审核操作
			$model->create ();
			
			$model->isremote = 1; // 默认开启远程
			$model->status = '1';
			if ($model->field ( 'id,status,name,position,type,isonline,isremote,longitude,latitude,updatetime,updateuser' )->save ()) {
				$model2 = D ( 'SysClientconfig' );
				$obj2 = $model2->getByClientid ( $clientid );
				if (null == $obj2) {
					$model2->create ();
					$model2->createuser = getUserInfo ();
					$model2->createtime = time ();
					$model2->clientid = $clientid;
					$model2->field ( "createtime,createuser,clientid" )->add ();
				}
				
				// ================第三方通讯====
				$companys = getCompanyidsByClientid ( $clientid );
				if (1 == count ( $companys )) {
					// $datab['test1'] = "1111";
					$companyid = $companys [0] ['companyid'];
					$model3 = D ( "SysCompany" );
					$obj3 = $model3->getById ( $companyid );
					// $datab['test2'] = "222222:".$companyid.":".$obj3['isapi'];
					// $datab['test2x'] = $companys;
					// $datab['test2x2'] = $companys[0];
					if ($obj3 && '1' === $obj3 ['isapi']) {
						// $datab['test3'] = "33333";
						if ('NDC' === $obj3 ['code']) { // 无锡市耐思洗涤服务有限公司
							
							$url = $obj3 ['apiurl'] . "SynchronizeCabinetInfo";
							
							/*
							 * $post = array();
							 * $post['Token'] = $obj['token'];
							 * $post['CabinetNo'] = $obj['csn'];
							 * $post['CabinetName'] = I('post.name');
							 * $post['CabinetArea'] = I('post.position');
							 * $post['CabinetNum'] = $obj['boxnum'];
							 * $door = array();
							 * $model4 = D("SysBox");
							 * $boxs = $model4->where("`clientid`=$clientid")->select();
							 * foreach ($boxs as $k=>$v){
							 * $boxobj = $v;
							 * $door[$k]= array("DoorNorm"=>$v['norm'],"DoorNo"=>$v['no'],"DoorState"=>0);
							 * $datab['door'.$k] = $door[$k];
							 * }
							 *
							 *
							 * $post['CabinetDoor']= json_encode($door);
							 *
							 * $post['X']= I('post.longitude');// $obj3['longitude'];
							 * $post['Y']= I('post.latitude');//$obj3['latitude'];
							 * $post['State']= 0;//新增
							 * $post['Tid']= getSerNo();
							 */
							$post2 = array ();
							$bb = array ();
							$bb ['Parameters'] = array ();
							$bb ['Parameters'] ['Token'] = $obj ['token'];
							$bb ['Parameters'] ['CabinetNo'] = $obj ['csn'];
							
							$bb ['Parameters'] ['CabinetName'] = I ( 'post.name' );
							$bb ['Parameters'] ['CabinetArea'] = I ( 'post.position' );
							$bb ['Parameters'] ['CabinetNum'] = $obj ['boxnum'];
							
							$door = array ();
							$model4 = D ( "SysBox" );
							$boxs = $model4->where ( "`clientid`=$clientid" )->select ();
							foreach ( $boxs as $k => $v ) {
								$boxobj = $v;
								$door [$k] = array (
										"DoorNorm" => $v ['norm'],
										"DoorNo" => $v ['no'],
										"DoorState" => 0 
								);
								$datab ['door' . $k] = $door [$k];
							}
							
							// $bb['Parameters']['CabinetDoor']= json_encode($door);
							$bb ['Parameters'] ['CabinetDoor'] = $door;
							$bb ['Parameters'] ['X'] = I ( 'post.longitude' );
							$bb ['Parameters'] ['Y'] = I ( 'post.latitude' );
							
							$bb ['Parameters'] ['State'] = 0; // 新增
							$bb ['Parameters'] ['Tid'] = getSerNo ();
							
							$post2 ['pRequest'] = json_encode ( $bb );
							
							request_post ( $url, $post2 );
						}
					}
				}
				// ===================
				
				$boo = true;
			}
		}
		
		if ($boo) {
			$datab ['msg'] = "ok";
		} else {
			$datab ['msg'] = "no";
		}
		$this->ajaxReturn ( $datab );
	}
	
	/**
	 * ajax修改一条数据
	 */
	public function ajaxEdit() {
		$datab = array ();
		$datab ['msg'] = "no";
		$model = D ( 'SysClient' );
		$clientid = I ( 'post.id' );
		$obj = $model->getById ( $clientid );
		if ($obj && ('1' === $obj ['status'] || '2' === $obj ['status'] || '3' === $obj ['status'])) { // 状态只能是1：启用（可使用状态） 2：停用 3：维护 被修改
			$model->create ();
			if (I ( "post.isremotedef" )) { // 表单isremote字段生效标示isremotedef
				if (! I ( "post.isremote" )) { // 未传值I("post.isremote") = false，则赋0
					$model->isremote = 0; // 是否接受远程
					                      // 下发“是否远程”命令
					sendCommand ( $obj ['token'], '205', 2 ); // 拒绝
				} else {
					// 下发“是否远程”命令
					sendCommand ( $obj ['token'], '205', 1 ); // 接受
				}
			}
			if (false !== $model->field ( 'id,name,position,type,isremote,updatetime,updateuser,longitude,latitude' )->save ()) {
				// ================第三方通讯====
				$companys = getCompanyidsByClientid ( $clientid );
				if (1 == count ( $companys )) {
					// $datab['test1'] = "1111";
					$companyid = $companys [0] ['companyid'];
					$model3 = D ( "SysCompany" );
					$obj3 = $model3->getById ( $companyid );
					// $datab['test2'] = "222222:".$companyid.":".$obj3['isapi'];
					// $datab['test2x'] = $companys;
					// $datab['test2x2'] = $companys[0];
					if ($obj3 && '1' === $obj3 ['isapi']) {
						// $datab['test3'] = "33333";
						if ('NDC' === $obj3 ['code']) {  // 无锡市耐思洗涤服务有限公司
							$url = $obj3 ['apiurl'] . "SynchronizeCabinetInfo";
							
							/*
							 * $post = array();
							 * $post['Token'] = $obj['token'];
							 * $post['CabinetNo'] = $obj['csn'];
							 * $post['CabinetName'] = I('post.name');
							 * $post['CabinetArea'] = I('post.position');
							 * $post['CabinetNum'] = $obj['boxnum'];
							 * $door = array();
							 * $model4 = D("SysBox");
							 * $boxs = $model4->where("`clientid`=$clientid")->select();
							 * foreach ($boxs as $k=>$v){
							 * $boxobj = $v;
							 * $door[$k]= array("DoorNorm"=>$v['norm'],"DoorNo"=>$v['no'],"DoorState"=>0);
							 * $datab['door'.$k] = $door[$k];
							 * }
							 *
							 *
							 * $post['CabinetDoor']= json_encode($door);
							 *
							 * $post['X']= I('post.longitude');// $obj3['longitude'];
							 * $post['Y']= I('post.latitude');//$obj3['latitude'];
							 * $post['State']= 1;//编辑
							 * $post['Tid']= getSerNo();
							 */
							$post2 = array ();
							$bb = array ();
							$bb ['Parameters'] = array ();
							$bb ['Parameters'] ['Token'] = $obj ['token'];
							$bb ['Parameters'] ['CabinetNo'] = $obj ['csn'];
							
							$bb ['Parameters'] ['CabinetName'] = I ( 'post.name' );
							$bb ['Parameters'] ['CabinetArea'] = I ( 'post.position' );
							$bb ['Parameters'] ['CabinetNum'] = $obj ['boxnum'];
							
							$door = array ();
							$model4 = D ( "SysBox" );
							$boxs = $model4->where ( "`clientid`=$clientid" )->select ();
							foreach ( $boxs as $k => $v ) {
								$boxobj = $v;
								$door [$k] = array (
										"DoorNorm" => $v ['norm'],
										"DoorNo" => $v ['no'],
										"DoorState" => 0 
								);
								$datab ['door' . $k] = $door [$k];
							}
							
							// $bb['Parameters']['CabinetDoor']= json_encode($door);
							$bb ['Parameters'] ['CabinetDoor'] = $door;
							$bb ['Parameters'] ['X'] = I ( 'post.longitude' );
							$bb ['Parameters'] ['Y'] = I ( 'post.latitude' );
							
							$bb ['Parameters'] ['State'] = 1; // 编辑
							$bb ['Parameters'] ['Tid'] = getSerNo ();
							
							$post2 ['pRequest'] = json_encode ( $bb );
							
							request_post ( $url, $post2 );
						}
					}
				}
				// ===================
				
				$datab ['msg'] = "ok";
			}
		}
		$this->ajaxReturn ( $datab );
	}
}
<?php
namespace Admin\Controller;

/**
 * 
 * Admin\Controller$CertificateInfoController
 * Copyright (c) 2016. 云绅（张家港）精密工业有限公司 版权所有
 * 修改人：jcccy
 * 修改时间：2016年4月15日 下午3:19:53
 * 修改内容：
 */
class CertificateInfoController extends BaseController {
	public $fields = 'name,sex,type,validity,code,valid';//添加与修改的通用字段
	
	public function _initialize(){
		parent::_initialize();
	}

	/**
	 * 加载界面
	 */
	public  function toList(){
		$this->views("list");
	}
	
	/**
	 * ajax加载分页数据
	 */
	public  function ajaxList(){
		
		
		$model = D('CertificateInfo');
		
		$data = array();
		
		$datar = array();
		$datar['total']  = $model->where($data)->count();
		$datar['rows'] =  $model->alias('a')
		->field("`a`.`id`,`a`.`name`,`a`.`sex`,`a`.`type`,`a`.`validity`,`a`.`code`,`a`.`valid`")
		->where($data)->order(array('`a`.`id`'=>'asc'))
		->page(I('get.page').','.I('get.rows'))->select();
		$this->ajaxReturn($datar);
		
		
		
		

	}
	
	/**
	 * ajax删除ID指定的数据
	 */
	public  function ajaxDel(){
		$datab = array("msg"=>"no");
		
		$model = D('CertificateInfo');
		$id = I('post.id',0);
		
		$model->delete($id);
		
		$datab['msg'] = "ok";
			
		
		$this->ajaxReturn($datab);
	}
	
	/**
	 * ajax添加一条数据
	 */
	public  function ajaxAdd(){
		$datab = array("msg"=>"no");
		
		$model = D('CertificateInfo');
		$model->create();
		if( false !== $model->field("id,createtime,createuser,".$this->fields)->add()){
			$datab['msg'] = "ok";
		}
		$this->ajaxReturn($datab);
	}
	
	/**
	 * ajax修改一条数据
	 */
	public  function ajaxEdit(){
			$datab = array("msg"=>"no");
		
			
		
			$model = D('CertificateInfo');
			$model->create();
			if( false !== $model->field('id,updatetime,updateuser,'.$this->fields)->save()){
				$datab['msg'] = "ok";
			}
			$this->ajaxReturn($datab);
	}
	
	
	/**
	 * ajax获取一条数据
	 */
	public  function ajaxGetDataById(){
		$datab = array();
		$model = D('CertificateInfo');
		$id = I('get.id',0);
		$obj = $model->getById($id);//$model->where('id='.$id)->field('id,'.$this->fields)->find();
	
		$this->ajaxReturn($obj);
	}
	
	
	/**
	 * ajax加载分页数据
	 */
	public  function ajaxSelectList(){
	
		$model = D('CertificateInfo');
		$data = array();
		$data['status'] = 1;
	
		$datar = $model->alias('a')
		->field("`a`.`id`,CONCAT(`a`.`name`,'(',`a`.`code`,')') as `text`,'' as `desc`")
		->where($data)->order(array('`a`.`id`'=>'asc'))
		->select();
		$this->ajaxReturn($datar);
	
	}
	
	
	
	/**
	 * 跳转到证件终端信息
	 */
	public function certificateList(){
		$this->views("certificateList");
	}
	

	
	/**
	 * 加载证件信息列表数据
	 * 修改：徐辰辰
	 * 时间：2016-04-21
	 * ajaxCertificateInfoList
	 * @return return_type
	 * @throws
	 */
	public  function ajaxCertificateInfoList()
	{
		
		$name = I('get.name', '');
		$cardid = I('get.cardid', '');
		
		$sql = "select
				 a.id
				,a.cardid
				,a.name
				,a.sex
				,a.type
				,from_unixtime(a.validity, '%Y-%m-%d') as validity
				,a.code
				,a.valid
				,ifnull(b.type, 0) as filetype
				from wf_certificate_info a, wf_certificate_attachment b
				where a.id = b.certificateid";
		
		if($name != "") {
			$sql = $sql . " and a.name like '%$name%'";
		}
		if($cardid != "") {
			$sql = $sql . " and a.cardid = '$cardid'";
		}
		
		$sql = $sql . " order by a.createtime desc";
		
		$datar = array();
		$datar['total']  = $this->getPaginationDataSumCount($sql);
		$datar['rows'] = $this->getPaginationData($sql, I('get.page'), I('get.rows'));
		$this->ajaxReturn($datar);
	}
	
	
	
	
	/**
	 * 新增证件信息
	 * 修改：徐辰辰
	 * 时间：2016-04-21
	 * ajaxCertificateInfoAdd
	 * @return return_type
	 * @throws
	 */
	public  function ajaxCertificateInfoAdd()
	{

		$model = D('CertificateInfo');
		$model->createuser = getUserInfo();
		$model->createtime = time();
		$model->cardid = I('post.cardid');
		$model->name = I('post.name');
		$model->sex = I('post.sex');
		$model->type = I('post.type');
		$model->validity = strtotime(I('post.validity'));
		$model->code = I('post.code');
		$model->valid = 1;
		
		$id = $model->add();
		
		if( false !== $id){
			$fields_tmp = "";
			$model2 = D('CertificateAttachment');
			
			$model2->createuser = getUserInfo();
			$model2->createtime = time();
			$model2->certificateid = $id;
			$model2->type = 1;//照片
			$model2->title = I('post.cardid');
			$info = fileOneUpload('file1');//上传图片，返回上传图片的信息
			if($info) {//图片上传成功,则执行
				$fields_tmp = "url,";
				$model2->url = $info['savepath'] . $info['savename'];
			}
			if( false !== $model2->field("id,createuser,createtime," . $fields_tmp . "title,certificateid,type")->add() ) {
				$datab['msg'] = "ok";
				$datab['data'] = $id;
			}
		}
		$this->ajaxReturn($datab);
	}
	
	/**
	 * 跳转编辑证件信息页面
	 * 修改：徐辰辰
	 * 时间：2016-04-21
	 * ajaxCertificateInfoDataById
	 * @return return_type
	 * @throws
	 */
	public function ajaxCertificateInfoDataById()
	{
		
		$model = D('CertificateInfo');
		$id = I('get.id', 0);
		$obj = $model->where("id=$id")->find();
		
		$obj['validity'] = date("Y-m-d", $obj['validity']);
		
		$model2 = D('CertificateAttachment');
		$objAtta = $model2->getByCertificateid($id);
		$obj['url'] = $objAtta['url'];
		
		$this->ajaxReturn($obj);
	}
	
	
	/**
	 * 保存证件编辑数据
	 */
	public function ajaxCertificateInfoEdit()
	{
		$datab = array("msg"=>"no");
		
		$model = D('CertificateInfo');
		$model->id = I('post.id');
		$model->updateuser = getUserInfo();
		$model->updatetime = time();
		$model->cardid = I('post.cardid');
		$model->name = I('post.name');
		$model->sex = I('post.sex');
		$model->type = I('post.type');
		$model->validity = strtotime(I('post.validity'));
		$model->code = I('post.code');
		
		if( false !== $model->save()) {
		
			$model2 = D('CertificateAttachment');
			
			$obj = $model2->getByCertificateid(I('post.id'));
			if($obj){
				$fields_tmp = "";
				$model2->id = $obj['id'];
				$model2->updatetime = time();
				$model2->updateuser = getUserInfo();
				$info = fileOneUpload('file1');//上传图片，返回上传图片的信息
				if($info){//图片上传成功,则执行
					//删除之前的图片
					unlink("./Public/".$obj['url']);
					$fields_tmp = "url,";
					$model2->url = $info['savepath'].$info['savename'];
				}
				
				if( false !== $model2->field('id,updatetime,updateuser,'.$fields_tmp)->save()){
					$datab['msg'] = "ok";
				}
			}
			
		}
		$this->ajaxReturn($datab);
	}
	
	/**
	 * 证件信息注销操作
	 * ajaxVoid
	 * @return return_type
	 * @throws
	 */
	public function ajaxVoid()
	{
		$model = D('CertificateInfo');
		$model->create();
		$model->valid = 0;
		
		$count = $model->save();

		if($count !== false) {
			$this->jsonData['status'] = 200;
			$this->jsonData['msg'] = '注销证件信息成功！';
		}
		else {
			$this->jsonData['status'] = 400;
			$this->jsonData['msg'] = '注销证件信息失败！';
		}
		$this->ajaxReturn($this->jsonData);
	}
	

	
	/**
	 * 跳转到证件打印记录页面
	 */
	public function certificatePrintList(){
		$this->views("certificatePrintList");
		
	}
	
	
	/**
	 * ajax加载证件打印记录分页数据
	 */
	public  function ajaxCertificatePrintList()
	{
		
		$clientname = I('get.clientname', '');
		$cardid = I('get.cardid', '');
		
		$sql = "select
				 c.id
				,a.name as clientname
				,a.position
				,c.operateuser
				,FROM_UNIXTIME(c.operatetime) as operatetime
				,c.remark
				from wf_sys_client a, wf_sys_box b, wf_sys_boxrecord c
				where a.id = b.clientid
				and b.id = c.boxid
				and a.sysid = 'certificate'
				and c.operatetype = 30";
		
		if($clientname != "") {
			$sql = $sql . " and a.name like '%$clientname%'";
		}
		if($cardid != "") {
			$sql = $sql . " and c.operateuser = '$cardid'";
		}
		
		$sql = $sql . " order by c.createtime desc";
		
		$datar = array();
		$datar['total']  = $this->getPaginationDataSumCount($sql);
		$datar['rows'] = $this->getPaginationData($sql, I('get.page'), I('get.rows'));
		$this->ajaxReturn($datar);
		
	}
	
}
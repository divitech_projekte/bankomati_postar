<?php
namespace Admin\Controller;

/**
 * 终端信息
 * Admin\Controller$SysClientController
 * Copyright (c) 2016. 云绅（张家港）精密工业有限公司 版权所有
 * 修改人：jcccy
 * 修改时间：2016年3月7日 下午4:54:20
 * 修改内容：
 */
use Think\Model;

class SysClientController extends BaseController {
	public $fields = 'status,csn,token,name,position,boxnum,type,isonline,isremote,longitude,latitude';
	
	
	public function _initialize(){
		parent::_initialize();
	}

	/**
	 * 加载界面
	 */
	public  function toList(){
		$this->views("list");
	}
	
	/**
	 * ajax加载分页数据
	 */
	public  function ajaxList(){
		
		$model = D('SysClient');
		
		$data = array();
		
		if("" != I('get.name',"")){
			$data['name'] = array('like','%'.I('get.name').'%');
		}
		if("" != I('get.token',"")){
			$data['token'] = array('like','%'.I('get.token').'%');
		}
		$this->ajaxReturn($model->lists($data,I('get.page'),I('get.rows')));
	}
	
	/**
	 * ajax加载分页数据
	 */
	public  function ajaxSelectList(){
	
		$model = D('SysClient');
		$data = array();
		$data['status'] = 1;
		if(I('get.sysid')){
			$data['sysid'] = I('get.sysid');
		}
	
		$datar = $model->alias('a')
		->field("`a`.`id`,CONCAT(`a`.`name`,`a`.`csn`) as `text`,'' as `desc`")
		->where($data)->order(array('`a`.`id`'=>'asc'))
		->select();
		$this->ajaxReturn($datar);
	
	}
	
	
	/**
	 * ajax删除ID指定的数据
	 */
	public  function ajaxDel(){
		$model = D('SysClient');
		$id = I('post.id',0);
		$datab = array();
		$model->delete($id);
		if(true){
			$datab['msg'] = "ok";
		}else{
			$datab['msg'] = "no";
		}
		$this->ajaxReturn($datab);
	}
	
	/**
	 * ajax添加一条数据
	 */
	public  function ajaxAdd(){
		$datab = array();
		
	
		
		
		$model = D('SysClient');
		if( false !== $model->field($this->fields)->addTo()){
			$datab['msg'] = "ok";
		}else{
			$datab['msg'] = "no";
		}
		$this->ajaxReturn($datab);
	}
	
	/**
	 * ajax修改一条数据
	 */
	public  function ajaxEdit(){
		$datab = array();
		$datab['msg'] = "no";
		$model = D('SysClient');
		$obj = $model->getById(I('post.id'));
		if($obj && ('1' === $obj['status'] || '2' === $obj['status'] || '3' === $obj['status'])){//状态只能是1：启用（可使用状态） 2：停用 3：维护 被修改
			$model->create();
			if(I("post.isremotedef")){//表单isremote字段生效标示isremotedef
				if(!I("post.isremote")){//未传值I("post.isremote") = false，则赋0
					$model->isremote = 0;// 是否接受远程
					//下发“是否远程”命令
					sendCommand($obj['token'],'205',2);//拒绝
				}else{
					//下发“是否远程”命令
					sendCommand($obj['token'],'205',1);//接受
				}
			}
			if( false !== $model->field('id,name,position,type,isremote,updatetime,updateuser,longitude,latitude')->save()){
				$datab['msg'] = "ok";
			}
		}
		$this->ajaxReturn($datab);
	}
	
	
	
	/**
	 * ajax获取一条数据
	 */
	public  function ajaxGetDataById(){
		$datab = array();
		$model = D('SysClient');
		$id = I('get.id',0);
		$obj = $model->where('id='.$id)->field('id,'.$this->fields)->find(); 
		
		$this->ajaxReturn($obj);
	}
	
	
	
	/**
	 * ajaxVerify审核设备
	 */
	public  function ajaxVerify(){
		$datab = array();
		
		$model = D('SysClient');
		$clientid = I('post.id');
		$obj = $model->getById($clientid);//where(array('id'=>I('post.id')))->find();
	
		$boo = false;
		
		if(4 == $obj['status']){//注册待审核状态
			$sysbox = D('SysBox');
			$sysconfig = D('SysConfig');//系统配置表里提取boxtype值
			$sysconfigobj = $sysconfig->getByKey('boxtype');
	
			//创建柜门信息
			for($i = 0;$i < $obj['boxnum'];$i++){
				$box = array();
				$box['clientid'] = $obj['id'];
				$box['no'] = $i+1;
				$box['type'] = $sysconfigobj['value'];
				$box['status'] = 1;
				$box['storagestatus'] = 0;
	
				$box['createuser'] = $obj['createuser'];
				$box['createtime'] = time();
				$sysbox->add($box);
			}
			
			//审核操作
			$model->create();
			
			$model->isremote = 1;// 默认开启远程
			$model->status = '1';
			if($model->field('id,status,name,position,type,isonline,isremote,longitude,latitude,updatetime,updateuser')->save()){
				$model2 = D('SysClientconfig');
				$obj2 = $model2->getByClientid($clientid);
				if(null == $obj2){
					$model2->create();
					$model2->createuser = getUserInfo();
					$model2->createtime = time();
					$model2->clientid = $clientid;
					$model2->field("createtime,createuser,clientid")->add();
				}
				
				
				$boo = true;
			}
		}
		
		if($boo){
			$datab['msg'] = "ok";
		}else{
			$datab['msg'] = "no";
		}
		$this->ajaxReturn($datab);
	}
	
	
	
	/**
	 * 跳转到牛奶售货机终端配置
	 */
	public function milkClientList(){
		$this->views("milkClientList");
	}
	
	/**
	 * ajax牛奶售卖机加载分页数据
	 */
	public  function ajaxMilkClientList(){
		$model = D('SysClient');
		
		$data = array();
		
		
		if("" != I('get.name',"")){
			$data['name'] = array('like','%'.I('get.name').'%');
		}
		if("" != I('get.csn',"")){
			$data['csn'] = array('like','%'.I('get.csn').'%');
		}
		
		
		//$data['sysid'] = C('SYSTEM_ID');
		$data['sysid'] = 'vending';
		
		
		$datar = array();
		$datar['total']  = $model->where($data)->count();
		$datar['rows'] =  $model->alias('a')
		->field("`a`.`id`,`a`.`status`,`a`.`csn`,`a`.`token`,`a`.`name`,`a`.`position`,`a`.`boxnum`,`a`.`type`,`a`.`isonline`,`a`.`isremote`,`b`.`emergency`,`b`.`isremotepower`")
		->join('LEFT JOIN `wf_sys_clientconfig` AS `b` ON `b`.`clientid` = `a`.`id`')
		->where($data)->order(array('`a`.`id`'=>'asc'))
		->page(I('get.page').','.I('get.rows'))->select();
		
		
		$this->ajaxReturn($datar);
	}
	
	/**
	 * ajax牛奶售卖机加载运行状态分页数据
	 */
	public  function ajaxMilkClientStatusList(){
		$model = D('SysClient');
	
		$data = array();
	
	
		if("" != I('get.name',"")){
			$data['name'] = array('like','%'.I('get.name').'%');
		}
		if("" != I('get.csn',"")){
			$data['csn'] = array('like','%'.I('get.csn').'%');
		}
	
		$data['status'] = array('NEQ','0');
		
		//$data['sysid'] = C('SYSTEM_ID');
		$data['sysid'] = 'vending';
	
		$datar = array();
		$datar['total']  = $model->where($data)->count();
		$datar['rows'] =  $model->alias('a')
		->field("`a`.`id`,`a`.`status`,`a`.`csn`,`a`.`token`,`a`.`name`,`a`.`position`,`a`.`boxnum`,`a`.`type`,`a`.`isonline`,`a`.`isremote`,`b`.`emergency`,`b`.`isremotepower`")
		->join('LEFT JOIN `wf_sys_clientconfig` AS `b` ON `b`.`clientid` = `a`.`id`')
		->where($data)->order(array('`a`.`id`'=>'asc'))
		->page(I('get.page').','.I('get.rows'))->select();
	
	
		$this->ajaxReturn($datar);
	}
	
	
	/**
	 * ajax注销客户端操作
	 */
	public  function ajaxLogoff(){
		$datab['msg'] = "no";
		$datab = array();
		//*************************
		$model = D('SysClient');
		$obj = $model->getById(I('post.id'));
		if($obj && ('1' === $obj['status'] || '2' === $obj['status'] || '3' === $obj['status'])){//状态只能是1：启用（可使用状态） 2：停用 3：维护 被注销
			
			if( false !== sendCommand(I('post.token'),'101')){//发送注销命令
				
				$model = D('SysClient');
				$model->create();
				
				if( false !== $model->field('id,status')->save()){
					$datab['msg'] = "ok";
				}
			}
		}
		$this->ajaxReturn($datab);
	}
	
	
	
	/**
	 * ajax点击配置获取数据
	 */
	public  function ajaxGetConfigDataById(){
		$datab = array();
		$model = D('SysClient');
		$id = I('get.id',0);
		$obj = $model->alias('a')
		->field('`a`.`id`,`a`.`status`,`a`.`csn`,`a`.`token`,`a`.`name`,`a`.`position`,
				`a`.`boxnum`,`a`.`type`,`a`.`isonline`,`a`.`isremote`,`b`.`emergency`,`b`.`isremotepower`,`b`.`unit`,`b`.`freetime`,`b`.`interval`,`b`.`fee`,`b`.`owner`,`b`.`supplier`')
		->join('LEFT JOIN `wf_sys_clientconfig` AS `b` ON `b`.`clientid` = `a`.`id`')
		->where('`a`.`id`='.$id)
		->find();
	
		$this->ajaxReturn($obj);
	}
	
	/**
	 * 保存牛奶机客户端的配置信息
	 */
	public function ajaxSaveConfig(){
		$datab = array();
		$datab['msg'] = "no";
		$boo = false;
		$model = D('SysClient');
		$obj = $model->getById(I('post.id'));
		if($obj && ('1' === $obj['status'] || '2' === $obj['status'] || '3' === $obj['status'])){//状态只能是1：启用（可使用状态） 2：停用 3：维护 被配置
			$model2 = D('SysClientconfig');
			$obj2 = $model2->getByClientid(I('post.id'));
			if(null == $obj2){
				$model2->create();
				$model2->createuser = getUserInfo();
				$model2->createtime = time();
				$model2->clientid = I('post.id');
				$model2->field("createtime,createuser,clientid,emergency,owner,supplier")->add();
			}else{
				$model2->create();
				$model2->updateuser = getUserInfo();
				$model2->updatetime = time();
				$model2->id = $obj2['id'];
				$model2->field('id,updatetime,updateuser,emergency,owner,supplier')->save();
			}
			sendCommand($obj['token'],'301',2,I('post.emergency'));//变更下发求助电话
			/* 全部更改生效
			$list = $model->where('status=1')->select();
			foreach ($list as $a){
				sendCommand($obj['token'],'301',2,I('post.emergency'));//变更下发求助电话
			}
			*/
		
			$datab['msg'] = "ok";
		}
	
		$this->ajaxReturn($datab);
	}
	
	/**
	 * 跳转到牛奶机客户端运行状态界面
	 */
	public function milkStatusList(){//
		$this->views("milkClientStatusList");
	}
	
	/**
	 * 下发重新启动程序或系统的命令
	 */
	public function doClientReboot(){
		$datab = array();
		$datab['msg'] = "no";
		$model = D('SysClient');
		$clientobj = $model->getById(I("post.id"));
		if($clientobj && ('1' === $clientobj['status'])){//当前状态为启动状态时才能重新启动
			if( false !== sendCommand($clientobj['token'],'202',I("post.mode"))){//otype  $token,$otype,$parameter = '1：系统重启 2：程序重启',$jsoninfo = ''
				$model->create();
				$model->status = "1";
				if( false !== $model->field('id,status')->save()){
					$datab['msg'] = "ok";
				}
			}
		}
		
		$this->ajaxReturn($datab);
	}
	
	/**
	 * 下发停用客户端
	 */
	public function doClientStop(){
		$datab = array();
		$datab['msg'] = "no";
		$model = D('SysClient');
		$clientobj = $model->getById(I("post.id"));
		if($clientobj && ('1' === $clientobj['status'])){//当前状态为启动状态时才能停止
			if( false !== sendCommand($clientobj['token'],'203','2')){//$token,$otype,$parameter = '1 停用一个箱子 2 停用客户端 3 客户端维护',$jsoninfo = ''
				$model->create();
				$model->status = "2";
				if( false !== $model->field('id,status')->save()){
					$datab['msg'] = "ok";
				}
			}
		}
	
		$this->ajaxReturn($datab);
	}
	
	/**
	 * 下发启用客户端
	 */
	public function doClientStart(){
		$datab = array();
		$datab['msg'] = "no";
		$model = D('SysClient');
		$clientobj = $model->getById(I("post.id"));
		if($clientobj && ('2' === $clientobj['status'] || '3' === $clientobj['status'])){////当前状态为 2：停用 3：维护 状态时才能启动
			if( false !== sendCommand($clientobj['token'],'204','2')){//$token,$otype,$parameter = '1 启用一个箱子 2 启用客户端',$jsoninfo = ''
				$model->create();
				$model->status = "1";
				if( false !== $model->field('id,status')->save()){
					$datab['msg'] = "ok";
				}
			}
		}
	
		$this->ajaxReturn($datab);
	}
	

	/**
	 * 跳转到证件打印终端配置
	 */
	public function certificateClientList(){
		$this->views("certificateClientList");
	}
	
	/**
	 * ajax证件打印加载分页数据
	 */
	public  function ajaxCertificateClientList(){
		$model = D('SysClient');
		
		$data = array();
		
		
		if("" != I('get.name',"")){
			$data['name'] = array('like','%'.I('get.name').'%');
		}
		if("" != I('get.csn',"")){
			$data['csn'] = array('like','%'.I('get.csn').'%');
		}
		
		//$data['sysid'] = C('SYSTEM_ID');
		$data['sysid'] = 'certificate';
		
		
		
		$datar = array();
		$datar['total']  = $model->where($data)->count();
		$datar['rows'] =  $model->alias('a')
		->field("`a`.`id`,`a`.`status`,`a`.`csn`,`a`.`token`,`a`.`name`,`a`.`position`,`a`.`boxnum`,`a`.`type`,`a`.`isonline`,`a`.`isremote`,`b`.`emergency`,`b`.`isremotepower`")
		->join('LEFT JOIN `wf_sys_clientconfig` AS `b` ON `b`.`clientid` = `a`.`id`')
		->where($data)->order(array('`a`.`id`'=>'asc'))
		->page(I('get.page').','.I('get.rows'))->select();
		
		
		$this->ajaxReturn($datar);
	}
	
	
	
	 /**
	  * 跳转到智能洗衣柜终端配置
	  */
	 public function accessClientList(){
	 	$this->views("accessClientList");
	 }
	 
	 
	 /**
	  * ajax智能洗衣柜加载分页数据
	  */
	 public  function ajaxAccessClientList(){
	 	$model = D('SysClient');
	 
	 	$data = array();
	 
	 
	 	if("" != I('get.name',"")){
	 		$data['`a`.`name`'] = array('like','%'.I('get.name').'%');
	 	}
	 	if("" != I('get.csn',"")){
	 		$data['`a`.`csn`'] = array('like','%'.I('get.csn').'%');
	 	}
	 
	 
	 	//$data['sysid'] = C('SYSTEM_ID');
	 	$data['`a`.`sysid`'] = 'access';
	 
	 
	 	$datar = array();
	 	$datar['total']  = $model->alias('a')->where($data)->count();
	 	$datar['rows'] =  $model->alias('a')
	 	->field("`a`.`id`,`a`.`status`,`a`.`csn`,`a`.`token`,`a`.`name`,`a`.`position`,`a`.`boxnum`,`a`.`type`,`a`.`isonline`,`a`.`isremote`,`b`.`emergency`,`b`.`isremotepower`,CONCAT(`c`.`name`,'/',`c`.`code`,'(',`g`.`username`,')') as `suppliername`,CONCAT(`d`.`name`,'/',`d`.`code`,'(',`h`.`username`,')') as `ownername`")
	 	->join('LEFT JOIN `wf_sys_clientconfig` AS `b` ON `b`.`clientid` = `a`.`id`')
	 	->join('LEFT JOIN `wf_access_store` AS `c` ON `c`.`id` = `b`.`supplier`')
	 	->join('LEFT JOIN `wf_access_storeuser` AS `e` ON `e`.`storeid`=`c`.`id`')
	 	->join('LEFT JOIN `wf_sys_user` AS `g` ON `g`.`id`=`e`.`userid`')
	 	
	 	->join('LEFT JOIN `wf_access_owner` AS `d` ON `d`.`id` = `b`.`owner`')
	 	->join('LEFT JOIN `wf_access_owneruser` AS `f` ON `f`.`ownerid`=`d`.`id`')
	 	->join('LEFT JOIN `wf_sys_user` AS `h` ON `h`.`id`=`f`.`userid`')
	 	
	 	->where($data)->order(array('`a`.`id`'=>'asc'))
	 	->page(I('get.page').','.I('get.rows'))->select();
	 
	 
	 	$this->ajaxReturn($datar);
	 }
	 
	 /**
	  * 跳转到智能洗衣柜运行状态界面
	  */
	 public function accessStatusList(){//
	 	$this->views("accessStatusList");
	 }
	 
	/**
	 * 根据承包商信息加载终端数据
	 */
	public function ajaxSelectListForOwner(){
	
		$model = new Model();
		$sql = "select
				 a.id
				,CONCAT(a.name, a.csn) as text
				from wf_sys_client a, wf_sys_clientconfig b
				where a.id = b.clientid
				and a.status = 1
				and a.sysid = 'access'";
		
		$ownerId = $this->accessUsernameToRoleAndId(getUserInfo(), 'access_owner');
	 	
	 	if($ownerId != '-1') {
			$sql = $sql . " and b.owner = $ownerId";
		}
		else {
			//TODO
		}
		$data = $model->query($sql);
		$this->ajaxReturn($data);
	}
	
	/**
	 * 根据洗衣店信息加载终端数据
	 */
	public function ajaxSelectListForStore(){
	
		$model = new Model();
		$sql = "select
				 a.id
				,CONCAT(a.name, a.csn) as text
				from wf_sys_client a, wf_sys_clientconfig b
				where a.id = b.clientid
				and a.status = 1
				and a.sysid = 'access'";
		
		$storeId = $this->accessUsernameToRoleAndId(getUserInfo(), 'access_supplier');
		
		if($storeId != '-1') {
			$sql = $sql . " and b.supplier = $storeId";
		}
		else {
			//TODO
		}
		$data = $model->query($sql);		
		$this->ajaxReturn($data);
	}
	 
	 
	 /**
	  * ajax智能洗衣柜加载运行状态分页数据
	  */
	 public  function ajaxAccessClientStatusList(){
	 	$model = D('SysClient');
	 
	 	$data = array();
	 
	 
	 	if("" != I('get.name',"")){
	 		$data['name'] = array('like','%'.I('get.name').'%');
	 	}
	 	if("" != I('get.csn',"")){
	 		$data['csn'] = array('like','%'.I('get.csn').'%');
	 	}
	 
	 	$data['status'] = array('NEQ','0');
	 
	 	//$data['sysid'] = C('SYSTEM_ID');
	 	$data['sysid'] = 'access';
	 
	 	$datar = array();
	 	$datar['total']  = $model->where($data)->count();
	 	$datar['rows'] =  $model->alias('a')
	 	->field("`a`.`id`,`a`.`status`,`a`.`csn`,`a`.`token`,`a`.`name`,`a`.`position`,`a`.`boxnum`,`a`.`type`,`a`.`isonline`,`a`.`isremote`,`b`.`emergency`,`b`.`isremotepower`")
	 	->join('LEFT JOIN `wf_sys_clientconfig` AS `b` ON `b`.`clientid` = `a`.`id`')
	 	->where($data)->order(array('`a`.`id`'=>'asc'))
	 	->page(I('get.page').','.I('get.rows'))->select();
	 
	 
	 	$this->ajaxReturn($datar);
	 }
	 
	 /**
	  * 跳转利润率设置页面
	  * accessProfitConfigList
	  * @return return_type
	  * @throws
	  */
	 public function accessProfitConfigList()
	 {
	 	$this->views("accessProfitConfigList");
	 }
	 
	 /**
	  * 加载利润率设置页面数据
	  * ajaxAccessProfitConfigList
	  * @return return_type
	  * @throws
	  */
	 public function ajaxAccessProfitConfigList()
	 {
	 	$clientId = I('get.clientid', '');
	 	
	 	$model = new Model();
	 	$sql = "select
				 b.id
				,a.csn
				,a.name
				,a.position
				,b.emergency
				,a.status
				,a.isonline
				,c.name as storename
				,CONCAT(b.profit, '%') as profit
				from wf_sys_client a, wf_sys_clientconfig b, wf_access_store c
				where a.id = b.clientid
				and b.supplier = c.id
				and a.sysid = 'access'";
	 	
	 	$ownerId = $this->accessUsernameToRoleAndId(getUserInfo(), 'access_owner');
	 	
	 	if($ownerId != '-1') {
			$sql = $sql . " and b.owner = $ownerId";
		}
		else {
			//TODO
		}
	 	
	 	if($clientId != '') {
			$sql = $sql . " and b.clientid = $clientId";
		}
		
		$sql = $sql . " order by a.createtime desc";
		
		//分页绑定
		$datar = array();
		$datar['total']  = $this->getPaginationDataSumCount($sql);
		$datar['rows'] = $this->getPaginationData($sql, I('get.page'), I('get.rows'));
		$this->ajaxReturn($datar);
	 }
	 
	 /**
	  * 加载设置利润率页面数据
	  * accessGetProfitInfo
	  * @return return_type
	  * @throws
	  */
	 public function accessGetProfitInfo()
	 {
	 	$id = I('get.id', 0);
	 	$model = new Model();
	 	$sql = "select
				 b.id
				,a.name as clientname
				,d.name as ownername
				,c.name as supplier
				,b.profit
				from wf_sys_client a, wf_sys_clientconfig b, wf_access_store c, wf_access_owner d
				where a.id = b.clientid
				and b.owner = d.id
				and b.supplier = c.id
				and b.id = $id";
	 	$data = $model->query($sql);
	 	$this->ajaxReturn($data[0]);
	 }
	 
	 
	 /**
	  * 承包商创建服务商时绑定客户端获取客户端List
	  */
	 public function ajaxSelectListAccessByOwner(){
	 	$model = new Model();
	 	
	 	$data = array();
	 	
	 	$ownerid = $this->accessUsernameToRoleAndId(getUserInfo(), 'access_owner');
	 	if("-1" !== $ownerid){//等于零表示有多个角色，但是不是admin。等于-1表示admin,-2(无角色)
	 		$data['`b`.`owner`'] = $ownerid;
	 	}
	 	$datar = $model->table("wf_sys_client as `a`,wf_sys_clientconfig as `b`")
	 	->field("`a`.`id`,CONCAT(`a`.`name`, `a`.`csn`)  as `text`,'' as `desc`")
	 	->where($data)
	 	->where("`a`.`id`=`b`.`clientid` and `a`.`status`=1 and `a`.`sysid`='access' and `b`.`supplier` IS NULL")
	 	->select();
	 	$this->ajaxReturn($datar);
	 }
	 
	 /**
	  * 承包商修改服务商时绑定客户端获取客户端List
	  */
	 public function ajaxSelectListAccessEditByOwner(){
	 	$model = new Model();
	 	 
	 	$data = array();
	 	 
	 	$ownerid = $this->accessUsernameToRoleAndId(getUserInfo(), 'access_owner');
	 	if("-1" !== $ownerid){//等于零表示有多个角色，但是不是admin。等于-1表示admin,-2(无角色)
	 		$data['`b`.`owner`'] = $ownerid;
	 	}
	 	$datar = $model->table("wf_sys_client as `a`,wf_sys_clientconfig as `b`")
	 	->field("`a`.`id`,CONCAT(`a`.`name`, `a`.`csn`)  as `text`,'' as `desc`")
	 	->where($data)
	 	->where("`a`.`id`=`b`.`clientid` and `a`.`status`=1 and `a`.`sysid`='access'")
	 	->select();
	 	$this->ajaxReturn($datar);
	 }
	 
	 
	 public function toAllotClientList(){
	 	$this->views("allotClient");
	 } 
	 
	 
	 
	 
	 /**
	  * ajax加载分页数据
	  */
	 public  function ajaxListByCompany(){
	 
	 	$model = D('SysClient');
	 	$companyid = I('get.companyid','');
	 	$data = array();
	 	if("" != I('get.name',"")){
	 		$data['`a`.`name`'] = array('like','%'.I('get.name').'%');
	 	}
	 	if("" != I('get.csn',"")){
	 		$data['`a`.`csn`'] = array('like','%'.I('get.csn').'%');
	 	}
	 
	 	$datar = array();
	 	$datar['total'] = $model->alias('a')
	 	->where($data)->count();
	 
	 	if("" != $companyid){
	 		$datar['rows'] =  $model->alias('a')//group_concat('【',IFNULL(`c`.`username`,'无'),'】') as `usersname`,
	 		->field("`a`.`id`,`a`.`csn`,`a`.`name`,`a`.`status`,`a`.`token`,`a`.`position`,`a`.`boxnum`,`a`.`type`,(select (SELECT IF (0=(select count(*) from wf_sys_companyclient as b where b.clientid=a.id and b.companyid=".$companyid."),'','true') AS  `checked`)) AS  `checked`")
	 	//	->join("LEFT JOIN `wf_sys_companyuser` AS `b` ON `b`.`clientid` =`a`.`id`")
	 	//	->join("LEFT JOIN `wf_sys_user` AS `c` ON `c`.`id` =`b`.`userid`")
	 		->where($data)->order(array('`a`.`type`'=>'desc'))
	 		->page(I('get.page').','.I('get.rows'))->group('`a`.`id`')->select();
	 			
	 		$datar['sql'] = $model->getLastSql();
	 	}else{
	 		$datar['rows'] =  $model->alias('a')//group_concat('【',IFNULL(`c`.`username`,'无'),'】') as `usersname`,
	 		->field("`a`.`id`,`a`.`csn`,`a`.`name`,`a`.`status`,`a`.`token`,`a`.`position`,`a`.`boxnum`,`a`.`type`")
	 		//->join("LEFT JOIN `wf_sys_companyuser` AS `b` ON `b`.`companyid` =`a`.`id`")
	 		//->join("LEFT JOIN `wf_sys_user` AS `c` ON `c`.`id` =`b`.`userid`")
	 		->where($data)->order(array('`a`.`type`'=>'desc'))
	 		->page(I('get.page').','.I('get.rows'))->group('`a`.`id`')->select();
	 		$datar['sql2'] = $model->getLastSql();
	 
	 	}
	 	$this->ajaxReturn($datar);
	 }
	 
}







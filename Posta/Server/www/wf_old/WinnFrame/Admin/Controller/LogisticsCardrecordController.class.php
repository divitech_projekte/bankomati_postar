<?php
namespace Admin\Controller;

use Think\Model;

/**
 * 
 * Admin\Controller$LogisticsCardrecordController
 * Copyright (c) 2016. 云绅（张家港）精密工业有限公司 版权所有
 * 修改人：jcccy
 * 修改时间：2016年6月16日 上午8:30:07
 * 修改内容：
 */
class LogisticsCardrecordController extends BaseController {
	public $fields = 'cardid,orderid,operatetype,operatetime,changemoney,remark';
	
	public function _initialize(){
		parent::_initialize();
	}
	
	/**
	 * 加载界面
	 */
	public  function toList(){
		$this->views("list");
	}
	
	/**
	 * ajax加载分页数据
	 */
	public  function ajaxList(){
		$model = D('LogisticsCardrecord');
		$data = array();
		
		$datar = array();
		$datar['total']  = $model->where($data)->count();
		$datar['rows'] =  $model->alias('a')
		->field("`a`.`id`,`a`.`cardid`,`a`.`orderid`,`a`.`operatetype`,`a`.`operatetime`,
				`a`.`changemoney`,`a`.`remark`")
		->join('LEFT JOIN `wf_logistics_cardinfo` AS `b` ON `b`.`id` =`a`.`cardid`')
		->join('LEFT JOIN `wf_logistics_order` AS `c` ON `c`.`id` =`a`.`orderid`')
		->where($data)->order(array('`a`.`id`'=>'asc'))
		->page(I('get.page').','.I('get.rows'))->select();
		
		
		$this->ajaxReturn($datar);
		
		
		
		
		

	}
	
	/**
	 * ajax删除ID指定的数据
	 */
	public  function ajaxDel(){
		$datab = array("msg"=>"no");
		$model = D('LogisticsCardrecord');
		$id = I('post.id',0);
		if($model->delete($id)){
			$datab['msg'] = "ok";
		}
		$this->ajaxReturn($datab);
	}
	
	/**
	 * ajax添加一条数据
	 */
	public  function ajaxAdd(){
		$datab = array("msg"=>"no");
		$model = D('LogisticsCardrecord');
		$model->create();
		if($model->field('id,createtime,createuser,'.$this->fields)->add()){
			$datab['msg'] = "ok";
		}
		$this->ajaxReturn($datab);
	}
	
	/**
	 * ajax修改一条数据
	 */
	public  function ajaxEdit(){
			$datab = array("msg"=>"no");
			$model = D('LogisticsCardrecord');
			$model->create();
			if($model->field('id,updatetime,updateuser,'.$this->fields)->save()){
				$datab['msg'] = "ok";
			}
			$this->ajaxReturn($datab);
	}
	
	
	/**
	 * ajax获取一条数据
	 */
	public  function ajaxGetDataById(){
		$model = D('LogisticsCardrecord');
		$id = I('get.id',0);
		$obj = $model->where('id='.$id)->field('id,'.$this->fields)->find();
		$this->ajaxReturn($obj);
	}
	
	
	/**
	 * ajax加载下拉数据
	 */
	public  function ajaxSelectList(){
		$model = D('LogisticsCardrecord');
		$data = array();
		$datar = $model->alias('a')
		->field("`a`.`id`,`a`.`remark` as `text`,'' as `desc`")
		->join('LEFT JOIN `wf_logistics_cardinfo` AS `b` ON `b`.`id` =`a`.`cardid`')
		->join('LEFT JOIN `wf_logistics_order` AS `c` ON `c`.`id` =`a`.`orderid`')
		->where($data)->order(array('`a`.`id`'=>'asc'))
		->select();
		$this->ajaxReturn($datar);
	
	}
	
}
<?php
namespace Admin\Controller;

use Think\Model;
/**
 * 
 * Admin\Controller$AccessStoreController
 * Copyright (c) 2016. 云绅（张家港）精密工业有限公司 版权所有
 * 修改人：jcccy
 * 修改时间：2016年5月21日 上午11:34:28
 * 修改内容：
 */
class AccessStoreController extends BaseController {
	
	/*
	 * 店铺管理控制器类架构函数
	 */
	public function _initialize()
	{
		// TODO
		parent::_initialize();
	}
	
	/**
	 * 跳转店铺信息页面
	 * accessStoreList
	 * @return return_type
	 * @throws
	 */
	public function accessStoreList()
	{
		//$this->views('accessStoreList');
		$this->views('list');
	}
	
	
	/**
	 * 跳转店铺信息页面
	 * accessStoreList
	 * @return return_type
	 * @throws
	 */
	public function toList()
	{
		$this->views('list');
	}
	
	
	
	/**
	 * ajax加载分页数据
	 */
	public  function ajaxList(){
	
		$model = D('AccessStore');
		$data = array();
	
		if("" != I('get.name',"")){
			$data['`a`.`name`'] = array('like','%'.I('get.name').'%');
		}
	
		if("" != I('get.code',"")){
			$data['`a`.`code`'] = array('like','%'.I('get.code').'%');
		}
	
	
		$datar = array();
		$datar['total']  = $model->alias('a')
		->join('LEFT JOIN `wf_access_storeuser` AS `b` ON `b`.`storeid` =`a`.`id`')
		->join('LEFT JOIN `wf_sys_user` AS `c` ON `c`.`id` =`b`.`userid`')
		->where($data)->count();
	
		$datar['rows'] =  $model->alias('a')
		->field("`a`.`id`,`a`.`name`,`a`.`code`,`a`.`short`,`a`.`address`,`a`.`linkman`,
				`a`.`phone`,`a`.`email`,`a`.`regdate`,`a`.`status`,`c`.`username`,`c`.`cardno`")
		->join('LEFT JOIN `wf_access_storeuser` AS `b` ON `b`.`storeid` =`a`.`id`')
		->join('LEFT JOIN `wf_sys_user` AS `c` ON `c`.`id` =`b`.`userid`')
		->where($data)->order(array('`a`.`id`'=>'asc'))
		->page(I('get.page').','.I('get.rows'))->select();
	
		$this->ajaxReturn($datar);
	}
	
	
	
	
	/**
	 * 跳转店铺信息页面
	 * accessStoreList
	 * @return return_type
	 * @throws
	 */
	public function toListByOwner()
	{
		$this->views('accessStoreByOwnerlist');
	}
	
	/**
	 * ajax加载分页数据
	 */
	public  function ajaxListByOwner(){
		$model = new Model();
		$data = array();
	
		
		
		
		
		$ownerid = $this->accessUsernameToRoleAndId(getUserInfo(), 'access_owner');
		if(0 < intval($ownerid)){//等于零表示有多个角色，但是不是admin。等于-1表示admin,-2(无角色)
			$data['`d`.`owner`'] = $ownerid;
		}
		
		
		
		if("" != I('get.name',"")){
			$data['`a`.`name`'] = array('like','%'.I('get.name').'%');
		}
	
		if("" != I('get.code',"")){
			$data['`a`.`code`'] = array('like','%'.I('get.code').'%');
		}
	
	
		$datar = array();
		$datar['total']  = $model->table('wf_access_store as `a`,wf_sys_clientconfig as `d`,wf_access_storeuser as `b`,wf_sys_user AS `c`')
		->where("`a`.`id`=`d`.`supplier` and `b`.`storeid`=`a`.`id` and `b`.`userid`=`c`.`id`")
		->where($data)
		->count();
	
		$datar['rows'] =  $model->table('wf_access_store as `a`,wf_sys_clientconfig as `d`,wf_access_storeuser as `b`,wf_sys_user AS `c`')
		->field("`a`.`id`,`a`.`name`,`a`.`code`,`a`.`short`,`a`.`address`,`a`.`linkman`,group_concat(IFNULL((select CONCAT(`name`,`csn`) from `wf_sys_client`  where `d`.`clientid`=`id`),''),'') as `clientnames`,
				`a`.`phone`,`a`.`email`,`a`.`regdate`,`a`.`status`,`c`.`username`,`c`.`cardno`")
		->where("`a`.`id`=`d`.`supplier` and `b`.`storeid`=`a`.`id` and `b`.`userid`=`c`.`id`")
		->where($data)
		->group('`a`.`id`')
		->order(array('`a`.`id`'=>'asc'))
		->page(I('get.page').','.I('get.rows'))->select();
		$datar['sql'] = $model->getLastSql();
		$this->ajaxReturn($datar);
	}
	
	
	/**
	 * ajax添加一条数据
	 */
	public  function ajaxAdd(){
		$datab = array("msg"=>"no");
	
		$model3 = D("SysUser");
		$userobj = $model3->getByUsername(I('post.username',0));
		if(!$userobj){
			$model = D('AccessStore');
			$model->create();
			$model->createtime = time();
			$model->createuser = getUserInfo();
			$model->regdate = time();
			$model->status = "1";
		
			$storeid = $model->field('id,createtime,createuser,status,name,code,short,address,linkman,phone,email,regdate')->add();
			//$datab['data'] = $model->getLastSql();
			if( false !== $storeid){
				if("" !== I('post.username',"")){
					
					$model3->username = I('post.username');
					$model3->password =  md5(md5("000000").C('mark'));
					$model3->status = 1;
					$model3->createuser = getUserInfo();
					$model3->createtime = time();
					$model3->regdate = time();
					$model3->email = I("post.email");
					$model3->phone = I("post.phone");
					$model3->cardno = I("post.cardno");
					$model3->display = I("post.name");
					$model3->type = 1;
					$userid = $model3->add();
					if( false !== $userid){
						//账号与角色绑定
						$model5 = M("SysRole");
						$obj = $model5->getByKey("access_supplier");//供应商
						$model4 = M("SysUserrole");
						$model4->userid = $userid;
						$model4->roleid = $obj['id'];
						$model4->add();
						
						//服务商与账号绑定
						$model2 = M("AccessStoreuser");
						$model2->storeid = $storeid;
						$model2->userid = $userid;
						$model2->add();
						
						//===将服务商绑定到客户端==
						$clientids = I("post.clientids");
						$datab['data'] = $clientids;
						$model6 = D('SysClientconfig');
						foreach ($clientids as $clientid){
							$model6->supplier = $storeid;
							$model6->field('supplier')->where("clientid=$clientid")->save();
						}
						
						
						//===下发命令给客户端_start===
						$model7 = D("SysClient");
						$clientlist = $model7->alias('a')
						->field('`a`.`token`,`b`.`owner`,`b`.`emergency`')
						->join('LEFT JOIN `wf_sys_clientconfig` AS `b` ON `b`.`clientid` =`a`.`id`')
						->where("`b`.`supplier`=$storeid and `a`.`status`!=0  and `a`.`sysid` = 'access'")->select();
						
						
						$model3 = D('AccessStore');
						$store = $model3->getById($storeid);
						
						foreach($clientlist as $val){
							
							$jsoninfo = json_encode(array('ownerid' => $val['owner'],'storeid' => $storeid,'emergency' => $val['emergency'],'phone' =>$store['phone']), JSON_UNESCAPED_UNICODE);
							sendCommand($val['token'],'409',2,$jsoninfo);//终端变更下发
						}
						//===下发命令给客户端_end===
					}
					
					$datab['msg'] = "ok";
				}
			}
		}
		$this->ajaxReturn($datab);
	}
	
	
	/**
	 * ajax获取一条数据
	 */
	public  function ajaxGetDataById(){
		$datab = array();
		$model = new Model();//D('AccessStore');
		$id = I('get.id',0);
		//$obj = $model->getById($id);
		$obj = $model->table('wf_access_store as `a`')// 
		->field("`a`.`id`,`a`.`name`,`a`.`code`,`a`.`short`,`a`.`address`,`a`.`linkman`,`a`.`phone`,`a`.`email`,`a`.`regdate`,`a`.`status`,`c`.`username`,`c`.`cardno`,
				group_concat(IFNULL(`d`.`clientid`,''),'') as `clientids[]`,
				group_concat(IFNULL(`e`.`name`,''),IFNULL(`e`.`csn`,'')) as `clientids`")
		->join('LEFT JOIN `wf_access_storeuser` AS `b` ON `b`.`storeid` =`a`.`id`')
		->join('LEFT JOIN `wf_sys_user` AS `c` ON `c`.`id` =`b`.`userid`')
		->join('LEFT JOIN `wf_sys_clientconfig` AS `d` ON `d`.`supplier` =`a`.`id`')
		->join('LEFT JOIN `wf_sys_client` AS `e` ON `e`.`id` =`d`.`clientid`')
		
		->where("`a`.`id`=$id and `e`.`sysid`='access'")->find();
		
		$obj['sql'] = $model->getLastSql();
		$this->ajaxReturn($obj);
	}
	
	
	
	/**
	 * ajax获取一条数据
	 */
	public  function ajaxGetDataByIdNoClient(){
		$datab = array();
		$model = new Model();//D('AccessStore');
		$id = I('get.id',0);
		//$obj = $model->getById($id);
		$obj = $model->table('wf_access_store as `a`')//
		->field("`a`.`id`,`a`.`name`,`a`.`code`,`a`.`short`,`a`.`address`,`a`.`linkman`,`a`.`phone`,`a`.`email`,`a`.`regdate`,`a`.`status`,`c`.`username`,`c`.`cardno`")
					->join('LEFT JOIN `wf_access_storeuser` AS `b` ON `b`.`storeid` =`a`.`id`')
					->join('LEFT JOIN `wf_sys_user` AS `c` ON `c`.`id` =`b`.`userid`')
					->where("`a`.`id`=$id")->find();
	
					$obj['sql'] = $model->getLastSql();
					$this->ajaxReturn($obj);
	}
	
	/**
	 * ajax删除ID指定的数据
	 */
	public  function ajaxDel(){
		$datab = array("msg"=>"no");
		$id = I('post.id',0);
		
		$model0 = M("SysClientconfig");
		$obj2 = $model0->getBySupplier($id);
		if(!$obj2){
			$model = D("AccessStore");
			$model->id = $id;
			$model->status = 0;
			if( false !== $model->field('id,status')->save()){
				$datab['msg'] = "ok";
			}
		}
		/*
		$id = I('post.id',0);
		
		$model0 = M("AccessStoreuser");
		$obj2 = $model0->getByStoreid($id);
		$userid = $obj2['userid'];
		$model0->where("storeid=$id")->delete();
		
		
		$model5 = M("SysRole");
		$obj = $model5->getByKey("access_supplier");//供应商
		$roleid = $obj['id'];
		
		$model4 = M("SysUserrole");
		$model4->where("roleid=$roleid and userid=$userid")->delete();
		
		
		
		$model = D('AccessStore');
		if( false !== $model->delete($id))
		{
			$datab['msg'] = "ok";
		}
		*/
		$this->ajaxReturn($datab);
	}
	
	
	
	/**
	 * ajax修改一条数据
	 */
	public  function ajaxEdit(){
		$datab = array("msg"=>"no");
	
		$model = D('AccessStore');
		$model->create();
		if( false !== $model->field('id,updatetime,updateuser,status,name,code,short,address,linkman,phone,email,regdate')->save()){
			$storeid = I("post.id");
			
			$username = I("post.username",0);
			$cardno = I('post.cardno');
			
			$model3 = D('SysUser');
			$userobj = $model3->getByUsername($username);
			if($userobj){
				$model3->id = $userobj['id'];
				$model3->cardno = $cardno;
				$model3->field("id,cardno")->save();
			}
			
			/*
			if($userid){
				$model2 = M("AccessStoreuser");
				$model2->where("storeid=$storeid")->delete();
				
				
				$model2->storeid = $storeid;
				$model2->userid = $userid;
				$model2->add();
			}*/
			
			//===将服务商绑定到客户端==
			/*
			$clientids = I("post.clientids");
			$datab['data'] = $clientids;
			$model6 = D('SysClientconfig');
			//$model6->supplier=NULL;
			//$model6->field('supplier')->where("owner=10")->save();
			foreach ($clientids as $clientid){
				$model6->supplier = $storeid;
				$model6->field('supplier')->where("clientid=$clientid")->save();
			}*/
			
			//===下发命令给客户端_start===
			$model7 = D("SysClient");
			$clientlist = $model7->alias('a')
			->field('`a`.`token`,`b`.`owner`,`b`.`emergency`')
			->join('LEFT JOIN `wf_sys_clientconfig` AS `b` ON `b`.`clientid` =`a`.`id`')
			->where("`b`.`supplier`=$storeid and `a`.`status`!=0  and `a`.`sysid` = 'access'")->select();
				
				
			$store = I("post.phone");
				
			foreach($clientlist as $val){
			
				$jsoninfo = json_encode(array('ownerid' => $val['owner'],'storeid' => $storeid,'emergency' => $val['emergency'],'phone' =>$store), JSON_UNESCAPED_UNICODE);
				sendCommand($val['token'],'409',2,$jsoninfo);//终端变更下发
			}
			//===下发命令给客户端_end===
			
			$datab['msg'] = "ok";
		}
		$this->ajaxReturn($datab);
	}
	
	
	
	/**
	 * 分页加载店铺信息列表数据
	 * ajaxAccessStoreList
	 * @return return_type
	 * @throws
	 */
	public function ajaxAccessStoreList()
	{
		//查询条件
		$storeName = I('get.name', '');
		$storeCode = I('get.code', '');
		
		//查询sql
		$sql = "select
				 a.id
				,a.name
				,a.code
				,a.short
				,a.address
				,from_unixtime(a.regdate) as regdate
				,a.status
				,b.value as statusname
				from wf_access_store a, (select itemname, value from wf_sys_item where code = 'store_status') b
				where a.status = b.itemname";
		
		if($storeName != '') {
			$sql = $sql . " and a.name like '%$storeName%'";
		}
		
		if($storeCode != '') {
			$sql = $sql . " and a.code = '$storeCode'";
		}
		
		$sql = $sql . " order by a.regdate desc";
		
		//分页绑定
		$datar = array();
		$datar['total']  = $this->getPaginationDataSumCount($sql);
		$datar['rows'] = $this->getPaginationData($sql, I('get.page'), I('get.rows'));
		$this->ajaxReturn($datar);
	}
	
	/**
	 * 执行添加店铺信息操作
	 * ajaxAccessStoreAdd
	 * @return return_type
	 * @throws
	 */
	public function ajaxAccessStoreAdd()
	{
		$model = D('AccessStore');
		$model->create();
		$model->createuser = getUserInfo();
		$model->createtime = time();
		$model->regdate = time();
		$storeId = $model->add();
		
		if($storeid !== false) {
			//插入下发数据
			$model = D('SysClient');
			$dataClients = $model->where("status <> 0 and sysid = 'access'")->select();
			foreach ($dataClients as $row) {
				$token = $row['token'];
				$otype = 409;
				$parameter = 1;
				$info = array(  'storeid' => $storeId,
								'name' => I('post.short'),
								'code' => I('post.code'));
				$jsoninfo = json_encode($info, JSON_UNESCAPED_UNICODE);
				sendCommand($token, $otype, $parameter, $jsoninfo);
			}
			
			$this->jsonData['status'] = 200;
			$this->jsonData['msg'] = '添加数据成功！';
		}
		else {
			$this->jsonData['status'] = 400;
			$this->jsonData['msg'] = '添加数据失败！';
		}
		$this->ajaxReturn($this->jsonData);
	}
	
	
	/**
	 * 执行编辑店铺信息操作
	 * ajaxAccessStoreEdit
	 * @return return_type
	 * @throws
	 */
	public function ajaxAccessStoreEdit()
	{
		$model = D('AccessStore');
		$model->create();
		$model->updateuser = getUserInfo();
		$model->updatetime = time();
		
		if($model->save() !== false) {
			//插入下发数据
			$model = D('SysClient');
			$dataClients = $model->where("status <> 0 and sysid = 'access'")->select();
			foreach ($dataClients as $row) {
				$token = $row['token'];
				$otype = 409;
				$parameter = 2;
				$info = array(  'storeid' => I('post.id'),
								'name' => I('post.short'),
								'code' => I('post.code'));
				$jsoninfo = json_encode($info, JSON_UNESCAPED_UNICODE);
				sendCommand($token, $otype, $parameter, $jsoninfo);
			}
			
			$this->jsonData['status'] = 200;
			$this->jsonData['msg'] = '编辑数据成功！';
		}
		else {
			$this->jsonData['status'] = 400;
			$this->jsonData['msg'] = '编辑数据失败！';
		}
		$this->ajaxReturn($this->jsonData);
	}
	
	/**
	 * 执行注销店铺信息操作
	 * ajaxVoid
	 * @return return_type
	 * @throws
	 */
	public function ajaxVoid()
	{
		$storeId = I('post.id', '0');
		
		//验证注销条件
		$tempModel = new Model();
		$sql = "select * 
				from wf_access_order a, wf_access_store b
				where a.storeid = b.id
				and a.status = 1
				and b.id = $storeId";
		$tempData = $tempModel->query($sql);
		
		if(count($tempData) != 0) {
			$this->jsonData['status'] = 400;
			$this->jsonData['msg'] = '该店铺存在未完成的订单，无法注销！';
		}
		else {
			$model = D('AccessStore');
			$model->create();
			$model->status = 0;
			
			$count = $model->save();
	
			if($count !== false) {
				
				//插入下发数据
				$model = D('SysClient');
				$dataClients = $model->where("status <> 0 and sysid = 'access'")->select();
				
				foreach ($dataClients as $row) {
					$token = $row['token'];
					$otype = 409;
					$parameter = 3;
					$info = array('storeid' => $storeId);
					$jsoninfo = json_encode($info, JSON_UNESCAPED_UNICODE);
					sendCommand($token, $otype, $parameter, $jsoninfo);
				}
				
				$this->jsonData['status'] = 200;
				$this->jsonData['msg'] = '注销数据成功！';
			}
			else {
				$this->jsonData['status'] = 400;
				$this->jsonData['msg'] = '注销数据失败！';
			}
		}
		$this->ajaxReturn($this->jsonData);
	}
	
	/**
	 * 加载下拉框选项
	 * ajaxSelectList
	 * @return return_type
	 * @throws
	 */
	/*
	public function ajaxSelectList()
	{
		$model = D('AccessStore');
		$data = array();
		$data['status'] = 1;
	
		$datar = $model->alias('a')
		->field("a.id, a.short as text")
		->where($data)->order(array('a.id' => 'asc'))
		->select();
		$this->ajaxReturn($datar);
	}
	*/
	
	
	
	
	
	/**
	 * ajax加载下拉数据
	 */
	public  function ajaxSelectList(){
		$model = new Model();//D('AccessStore');
		$data = array();
		$data['`a`.`status`'] = 1;
		$datar = $model->table("wf_access_store as `a`,wf_access_storeuser as `b`,wf_sys_user as `c`")
		->field("`a`.`id`,CONCAT(`a`.`name`,'/',`a`.`code`,'(',`c`.`username`,')') as `text`,'' as `desc`")
		->where("`a`.`id`=`b`.`storeid` and `b`.`userid`=`c`.`id`")
		->where($data)->order(array('`a`.`id`'=>'asc'))
		->group("`a`.`id`")
		->select();
		
		$this->ajaxReturn($datar);
	}
	
	/**
	 * 根据承包商信息加载下拉数据
	 * ajaxSelectListForOwner
	 * @return return_type
	 * @throws
	 */
	public function ajaxSelectListForOwner()
	{
		$model = new Model();
		$sql = "select a.id, CONCAT(a.name, '/', a.code) as text
				from wf_access_store a, (select distinct owner, supplier from wf_sys_clientconfig) b
				where a.id = b.supplier";
	
		$ownerId = $this->accessUsernameToRoleAndId(getUserInfo(), 'access_owner');
		
		if($ownerId != '-1') {
			$sql = $sql . " and b.owner = $ownerId";
		}
		else {
			//TODO
			$sql = "select a.id, CONCAT(a.name, '/', a.code) as text
					from wf_access_store a";
		}
		$data = $model->query($sql);
		$this->ajaxReturn($data);
	}
	
	/**
	 * 根据洗衣店信息加载下拉数据
	 * ajaxSelectListForOwner
	 * @return return_type
	 * @throws
	 */
	public function ajaxSelectListForStore()
	{
		$model = new Model();
		$sql = "select a.id, CONCAT(a.name, '/', a.code) as text
				from wf_access_store a, (select distinct supplier from wf_sys_clientconfig) b
				where a.id = b.supplier";
	
		$storeId = $this->accessUsernameToRoleAndId(getUserInfo(), 'access_supplier');
	
		if($storeId != '-1') {
			$sql = $sql . " and b.supplier = $storeId";
		}
		else {
			//TODO
			$sql = "select a.id, CONCAT(a.name, '/', a.code) as text
					from wf_access_store a";
		}
		$data = $model->query($sql);
		$this->ajaxReturn($data);
	}
	
}
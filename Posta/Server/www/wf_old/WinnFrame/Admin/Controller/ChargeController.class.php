<?php
namespace Admin\Controller;
use Think\Model;
/**
 * 
 * Admin\Controller$ChargeController
 * Copyright (c) 2016. 云绅（张家港）精密工业有限公司 版权所有
 * 修改人：jcccy
 * 修改时间：2016年6月16日 上午9:37:43
 * 修改内容：
 */
class ChargeController extends BaseController {
	
	/*
	 * 店铺管理控制器类架构函数
	 */
	public function _initialize()
	{
		// TODO
		parent::_initialize();
	}
	public function getClientInfoPanel(){
		$mode = new Model();
		$time = time();
		$sysid = C('SYSTEM_ID');
		
		$userid = getUserId();
		$where = "";
		if('1'!=$userid){
			$where = "AND `f`.`userid`=$userid ";
		}
		
		$sql = "SELECT
		a.clientid
		,c.name AS clientname
		,c.isonline
		,CASE WHEN c.status = 2 THEN 4 ELSE (CASE WHEN c.status = 3 THEN 6 ELSE 0 END) END AS STATUS -- 2 停用返4  3 维护返6 1正常返0
		FROM
		(SELECT `x`.`id`, `x`.`name`, `x`.`status`,( CASE WHEN $time > IFNULL(`x`.isonline, 0) + 30 THEN 0 ELSE 1 END) AS isonline ,`d`.`owner`,`d`.`supplier`
		FROM wf_sys_client `x`,wf_sys_clientconfig `d`,`wf_sys_companyclient` `e`,`wf_sys_companyuser` `f` WHERE  `x`.`id`=`d`.`clientid` and `x`.`id` = `e`.`clientid` AND `e`.`companyid` = `f`.`companyid` $where   AND (`x`.`STATUS` = 1 OR `x`.`STATUS` = 2 OR `x`.`STATUS` = 3) AND `x`.`sysid` = '$sysid') c
		INNER JOIN
		(SELECT clientid, id FROM wf_sys_box WHERE STATUS = 1) a
		ON a.clientid = c.id
		GROUP BY a.clientid
		";
		
		$data = $mode->query($sql);
		
		
		$this->assign("list",$data);
		
		
		$this->views("mainclientinfo");
	}

	/**
	 * 首页显示销售数据图表
	 * getSalesInfoPanel
	 * @return return_type
	 * @throws
	 */
	public function getSalesInfoPanel()
	{
		$this->views("mainsalesinfo");
	}
	
	/**
	 * 首页显示最新公告
	 * getNewsInfoPanel
	 * @return return_type
	 * @throws
	 */
	public function getNewsInfoPanel()
	{
		$this->views("mainnewsinfo");
	}
}
<?php
namespace Admin\Controller;
use Think\Model;
/**
 * 
 * Admin\Controller$LogisticsStatisticsController
 * Copyright (c) 2016. 云绅（张家港）精密工业有限公司 版权所有
 * 修改人：jcccy
 * 修改时间：2016年6月16日 上午9:37:43
 * 修改内容：
 */
class LogisticsStatisticsController extends BaseController {
	
	/*
	 * 店铺管理控制器类架构函数
	 */
	public function _initialize()
	{
		// TODO
		parent::_initialize();
	}
	
	public function logisticsStatistics1(){
		$this->views("logisticsStatistics1");
	}
	
	public function logisticsStatistics2(){
		$this->views("logisticsStatistics2");
	}

	
	public function ajaxStorage1(){
		/*
		$model = D('LogisticsOrder');
		$data = array();
		$data['`a`.`status`'] = array('EQ',0);
		if("" != I('get.orderno',"")){
			$data['`a`.`orderno`'] = array('like','%'.I('get.orderno').'%');
		}
		if("" != I('get.telphone',"")){
			$data['`a`.`telphone`'] = array('like','%'.I('get.telphone').'%');
		}
		
		
		$datar = array();
		$datar['total']  = $model->alias('a')
		->join('LEFT JOIN `wf_sys_client` AS `b` ON `b`.`id` =`a`.`clientid`')
		->join('LEFT JOIN `wf_logistics_courier` AS `c` ON `c`.`id` =`a`.`courierid`')
		->where($data)->count();
	
		$datar['rows'] =  $model->alias('a')
		->field("`a`.`id`,`a`.`status`,`a`.`orderno`,`a`.`courierid`,`a`.`clientid`,
				`a`.`type`,`a`.`telphone`,`a`.`paystatus`,`a`.`paymoney`,`a`.`retention`,`a`.`price`")
					->join('LEFT JOIN `wf_sys_client` AS `b` ON `b`.`id` =`a`.`clientid`')
					->join('LEFT JOIN `wf_logistics_courier` AS `c` ON `c`.`id` =`a`.`courierid`')
					->where($data)->order(array('`a`.`id`'=>'asc'))
					->page(I('get.page').','.I('get.rows'))->select();
	*/
					//$this->ajaxReturn($datar);
					
		
		
		$model = D('SysClient');
		$data = array();
		if("" != I('get.name',"")){
			$data['`a`.`name`'] = array('like','%'.I('get.name').'%');
		}
		if("" != I('get.csn',"")){
			$data['`a`.`csn`'] = array('like','%'.I('get.csn').'%');
		}
		$data['`a`.`sysid`'] = 'logistics';
		
		
		$wheresaletime = " ";
		if(I('post.saletime_start')){
			$wheresaletime .= " and `c`.`createtime` > ".strtotime(I('post.saletime_start',0));
		}
		if(I('post.saletime_end')){
			$wheresaletime .= " and `c`.`createtime` < ".strtotime(I('post.saletime_end',0) . ' 23:59:59.999');
		}
		
		$companyids = getCompanyidsByUid(getUserId());
		$where = "1!=1";
		foreach($companyids as $vo){
			$where .= " or d.companyid=".$vo['companyid'];
		}
		
		/*
		$datar = array();
		$datar['total']  = $model->alias('a')
		->join('LEFT JOIN `wf_sys_clientconfig` AS `b` ON `b`.`clientid` = `a`.`id`')
		->join("LEFT JOIN `wf_logistics_order` AS `c` ON (`c`.`clientid` = `a`.`id` $wheresaletime)")
		->join('LEFT JOIN `wf_sys_companyclient` AS `d` ON `d`.`clientid` =`a`.`id`')
		->where($where)
		->where($data)
		->count();
		*/
		
		$datar['rows'] =  $model->alias('a')
		->field("count(`c`.`id`) as `num`,`a`.`id`,`a`.`status`,`a`.`csn`,`a`.`token`,`a`.`name`,`a`.`position`,`a`.`boxnum`,`a`.`type`,`a`.`isonline`,`a`.`isremote`,`b`.`emergency`,`b`.`isremotepower`")
		->join('LEFT JOIN `wf_sys_clientconfig` AS `b` ON `b`.`clientid` = `a`.`id`')
		->join("LEFT JOIN `wf_logistics_order` AS `c` ON (`c`.`clientid` = `a`.`id` $wheresaletime)")
		->join('LEFT JOIN `wf_sys_companyclient` AS `d` ON `d`.`clientid` =`a`.`id`')
		->where($where)
		->where($data)
		->group("`a`.`id`,`a`.`status`,`a`.`csn`,`a`.`token`,`a`.`name`,`a`.`position`,`a`.`boxnum`,`a`.`type`,`a`.`isonline`,`a`.`isremote`,`b`.`emergency`,`b`.`isremotepower`")
		->order(array('`a`.`id`'=>'asc'))
		->select();
		
		$sql = $model->getLastSql();
		//echo $sql;
		//$this->ajaxReturn($datar);
		//$titiles = '"使用次数"';
		//$datas = '"周一","周二","周三","周四","周五","周六","周日"';
		//$series = '320, 332, 301, 334, 390, 330, 320';
		
		
		$titiles = '"'.L('use_the_number').'"';
		$datas = '';
		$series = '';
		
		/*
		$model = D('LogisticsOrder');
		$data = array();
		$data['`a`.`status`'] = array('EQ',0);
		if("" != I('get.orderno',"")){
			$data['`a`.`orderno`'] = array('like','%'.I('get.orderno').'%');
		}
		if("" != I('get.telphone',"")){
			$data['`a`.`telphone`'] = array('like','%'.I('get.telphone').'%');
		}
		*/
		
		
		$count = count($datar['rows'])-1;
		foreach ($datar['rows'] as $key => $vo){
			$datas .= '"' . $vo['name'] . '"';
			//$titiles.='"'.$vo['name'].'"';
			//$data['`a`.`clientid`']=$vo['id'];
			
			$series .= $vo['num'];//$model->alias('a')->where($data)->count();
			if($key != $count){
				$datas .= ',';
				$series .= ',';
				//$titiles .= ',';
			}
		}
		/*
		$count = count($datar['rows'])-1;
		foreach ($datar['rows'] as $key=>$vo){
			$titiles .= '"'.$vo['name'].'"';
			
			$series .= '{
			            "name":"使用次数",
			            "type":"bar",
			            "data":[320, 332, 301, 334, 390, 330, 320]
			        }';
			if($key!=$count){
				$titiles .= ',';
				$series .= ',';
			}
		}*/
		
		echo '{
		    "tooltip" : {
		        "trigger": "axis",
		        "axisPointer" : {            
		            "type" : "shadow"        
		        }
		    },
		    "legend": {
		        "data":['.$titiles.']
		    },
		    "grid": {
		        "left": "3%",
		        "right": "4%",
		        "bottom": "3%",
		        "containLabel": "true"
		    },
		    "xAxis" : [
		        {
		            "type" : "category",
		            "data" : ['.$datas.']
		        }
		    ],
		    "yAxis" : [
		        {
		            "type" : "value"
		        }
		    ],
		    "series" : [
		        {
		            "name":"'.L('use_the_number').'",
		            "type":"bar",
		            "data":['.$series.']
		        }
		    ]
		}';
	}
	
	
	public function ajaxStorage2(){
		//$datar = array();
		
		//$this->ajaxReturn($datar);
		$type = "Ym";
		$num = 12;//12个月
		
		$year = date('Y');
		$month = date('m');
		
		if(I('post.year')){
			$year = I('post.year');
		}
		
		
		if(I('post.month')){
			$month = I('post.month') + 1;
		}
		
		$d=mktime(0, 0, 0, $month, 1, $year);//时，分，秒，月，日，年（向前推12个月份最晚时间点）
		
		
		
		
		
		//$datas = '"201507","201508","201509","201510","201511","201512","201601","201602","201603","201604","201605","201606"';
		//$mon = 'null,null,null,null,null,null,null,null,null,null,"756.30",null';
		
		$datas = '';
		$mon = '';
		$mon2 = '';
		
		$companyids = getCompanyidsByUid(getUserId());
		$where = "(1!=1";
		foreach($companyids as $vo){
			$where .= " or d.companyid=".$vo['companyid'];
		}
		$where .= ")";
		$model = new Model();//D('LogisticsOrder');
		for($i = 0;$i < $num;$i++){
			$datas .= date($type, strtotime(($i-$num)." month",$d));
			
			$starttime = strtotime(date('Y-m-01', strtotime(($i-$num)." month",$d)));//开始时间戳
			$endtime = strtotime(date('Y-m-01', strtotime(($i-$num+1)." month",$d)));//结束时间戳
			
			
			$mon .= $model->table('wf_logistics_order a')
			->join('LEFT JOIN `wf_sys_companyclient` AS `d` ON `d`.`clientid` =`a`.`clientid`')
			->where($where.' and `a`.`status`=0 and `a`.`updatetime` >= '.$starttime.' and `a`.`updatetime` < '.$endtime)->count();
			//$mon2 .= $model->alias('a')->where('`a`.`status`=3 and `a`.`createtime` >= '.$starttime.' and `a`.`createtime` < '.$endtime)->count();
			
			if($i!=($num-1)){
				$datas .= ',';
				$mon .= ',';
				//$mon2 .= ',';
			}
		}
		//echo $model->getLastSql();
		
		echo '{"title":
				{
				"text":"'.L(monthly_usage).'"},
				"tooltip":{"trigger":"axis"},
				"legend":{
					"data":["'.L(number_of_completed_orders).'"]
				},
				"grid":{
					"left":"3%",
					"right":"4%",
					"bottom":"3%",
					"containLabel":true
				},
				"toolbox":{
					"feature":{
					"saveAsImage":[]}
				},
				"xAxis":{
					"type":"category",
					"boundaryGap":false,
					"data":['.$datas.']
				},
				"yAxis":{
					"type":"value"
				},
				"series":[
					{
						"name":"'.L(number_of_completed_orders).'",
						"type":"line",
						"stack":"订单数",
						"data":['.$mon.']
					}
				]
			}';
	}
}
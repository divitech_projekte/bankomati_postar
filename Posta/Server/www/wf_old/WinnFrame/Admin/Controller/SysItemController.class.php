<?php
namespace Admin\Controller;

/**
 * 代码字典
 * Admin\Controller$SysItemController
 * Copyright (c) 2016. 云绅（张家港）精密工业有限公司 版权所有
 * 修改人：jcccy
 * 修改时间：2016年3月7日 下午4:54:46
 * 修改内容：
 */
class SysItemController extends BaseController {
	public $fields = 'name,code,itemname,value,sort';
	
	public function _initialize(){
		parent::_initialize();
	}

	/**
	 * 加载界面
	 */
	public  function toList(){
		$this->views("list");
	}
	
	/**
	 * ajax加载分页数据
	 */
	public  function ajaxList(){
		
		
		
		$model = D('SysItem');
		$data = array();
		
		$datar = array();
		$datar['total']  = $model->where($data)->count();
		$datar['rows'] =  $model->alias('a')
		->field("`a`.`id`,`a`.`name`,`a`.`code`,`a`.`itemname`,`a`.`value`,`a`.`sort`")
		->where($data)->order(array('`a`.`id`'=>'asc'))
		->page(I('get.page').','.I('get.rows'))->select();
		
		
		$this->ajaxReturn($datar);
		
		

	}
	
	/**
	 * ajax删除ID指定的数据
	 */
	public  function ajaxDel(){
		$model = D('SysItem');
		$id = I('post.id',0);
		$datab = array();
		$model->delete($id);
		if(true){
			$datab['msg'] = "ok";
		}else{
			$datab['msg'] = "no";
		}
		$this->ajaxReturn($datab);
	}
	
	/**
	 * ajax添加一条数据
	 */
	public  function ajaxAdd(){
		
		$datab = array();
		
	
		
		
		$model = D('SysItem');
		if( false !== $model->field($this->fields)->addTo()){
			$datab['msg'] = "ok";
		}else{
			$datab['msg'] = "no";
		}
		$this->ajaxReturn($datab);
	}
	
	/**
	 * ajax修改一条数据
	 */
	public  function ajaxEdit(){
			$datab = array();
		
			
		
			$model = D('SysItem');
			if( false !== $model->field('id,'.$this->fields)->saveTo()){
				$datab['msg'] = "ok";
			}else{
				$datab['msg'] = "no";
			}
			$this->ajaxReturn($datab);
	}
	
	
	/**
	 * 加载代码字典下拉框选项
	 * ajaxSelectList
	 * @return return_type
	 * @throws
	 */
	public  function ajaxSelectList(){
		
		$model = D('SysItem');
		$where = array();
		$where['code'] = I('post.code');
		
		$datar = $model
			->field('itemname as id, value as text')
			->where($where)
			->order('sort asc')
			->select();
		
		$this->ajaxReturn($datar);
	}
	
	
	/**
	 * ajax获取一条数据
	 */
	public  function ajaxGetDataById(){
		$datab = array();
		$model = D('SysItem');
		$id = I('get.id',0);
	
		$obj = $model->where('id='.$id)->field('id,name,code,itemname,value,sort')->find();
	
		$this->ajaxReturn($obj);
	}
}
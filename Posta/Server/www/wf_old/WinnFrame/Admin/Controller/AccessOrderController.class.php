<?php
namespace Admin\Controller;

use Think\Model;

/**
 * 订单信息
 * Admin\Controller$AccessOrderController
 * Copyright (c) 2016. 云绅（张家港）精密工业有限公司 版权所有
 * 创建人：徐辰辰
 * 创建时间：2016-5-9 下午02:30:37
 */
class AccessOrderController extends BaseController {
	
	/*
	 * 订单信息控制器类架构函数
	 */
	public function _initialize()
	{
		// TODO
		parent::_initialize();
	}
	
	/**
	 * 跳转订单查看页面
	 * accessOrderList
	 * @return return_type
	 * @throws
	 */
	public function accessOrderList()
	{
		$this->views('accessOrderList');
	}
	
	/**
	 * 加载订单进行列表数据
	 * ajaxAccessOrderList
	 * @return return_type
	 * @throws
	 */
	public function ajaxAccessOrderList()
	{
		$clientId = I('get.clientid', '');
		$status = I('get.status', '');
		$fromDate = I('get.fromdate', '');
		$toDate = I('get.todate', '');
		
		$model = new Model();
		$sql = "select
				 a.id
				,b.name as clientname
				,a.orderno
				,a.status
				,a.telphone
				,ifnull(a.price, '-') as price
				,ifnull(a.extra, '-') as extra
				,ifnull(a.retention, '-') as retention
				,ifnull(a.actualprice, '-') as actualprice
				,ifnull(a.paymoney, '-') as paymoney
				,a.paystatus
				,a.balance
				from wf_access_order a, wf_sys_client b
				where a.clientid = b.id";
		
		if($clientId != '') {
			$sql = $sql . " and a.clientid = $clientId";
		}
		
		if($status != '') {
			$sql = $sql . " and a.status = '$status'";
		}
		
		if($fromDate != '' && $toDate != '') {
			$fromDate = strtotime($fromDate . '00:00:00');
			$toDate = strtotime($toDate . '23:59:59');
			$sql = $sql . " and a.createtime between $fromDate and $toDate";
		}
		
		$sql = $sql . " order by a.createtime desc";
		
		//分页绑定
		$datar = array();
		$datar['total']  = $this->getPaginationDataSumCount($sql);
		$datar['rows'] = $this->getPaginationData($sql, I('get.page'), I('get.rows'));
		$this->ajaxReturn($datar);
	}
	
	/**
	 * 根据id获取数据
	 * ajaxGetDataById
	 * @return return_type
	 * @throws
	 */
	public function ajaxGetDataById()
	{
		$id = I('get.id', 0);
		
		$model = new Model();
		$sql = "select
				 a.orderno
				,a.status
				,a.telphone
				,a.price
				,a.actualprice
				,a.paystatus
				,a.paytype
				,a.paymoney
				,a.balance
				,b.name as clientname
				,d.name as owner
				,e.name as supplier
				from wf_access_order a, wf_sys_client b, wf_sys_clientconfig c, wf_access_owner d, wf_access_store e
				where a.clientid = b.id
				and b.id = c.clientid
				and c.owner = d.id
				and c.supplier = e.id
				and a.id = $id";
		$obj = $model->query($sql);
		$this->ajaxReturn($obj[0]);
	}
	
	/**
	 * 根据订单id获取订单详细信息
	 * ajaxGetOrderDetailByOrderId
	 * @return return_type
	 * @throws
	 */
	public function ajaxGetOrderDetailByOrderId()
	{
		$orderId = I('get.orderid', 0);
		$model = D("AccessOrderdetail");
		$data = $model->where("orderid = $orderId")->select();
		$this->ajaxReturn($data);
	}
	
	/**
	 * 跳转订单管控页面
	 * accessOrderForOwnerList
	 * @return return_type
	 * @throws
	 */
	public function accessOrderForOwnerList()
	{
		$this->views('accessOrderForOwnerList');
	}
	
	/**
	 * 加载订单管控列表数据
	 * ajaxAccessOrderForOwnerList
	 * @return return_type
	 * @throws
	 */
	public function ajaxAccessOrderForOwnerList()
	{
		$clientId = I('get.clientid', '');
		$status = I('get.status', '');
		$fromDate = I('get.fromdate', '');
		$toDate = I('get.todate', '');
		
		$model = new Model();
		$sql = "select
				 a.id
				,b.name as clientname
				,a.orderno
				,a.status
				,a.telphone
				,ifnull(a.price, '-') as price
				,ifnull(a.extra, '-') as extra
				,ifnull(a.retention, '-') as retention
				,ifnull(a.actualprice, '-') as actualprice
				,ifnull(a.paymoney, '-') as paymoney
				,a.paystatus
				,a.balance
				from wf_access_order a, wf_sys_client b, wf_sys_clientconfig c
				where a.clientid = b.id
				and b.id = c.clientid";
		
		$ownerId = $this->accessUsernameToRoleAndId(getUserInfo(), 'access_owner');
		
		if($ownerId != '-1') {
			$sql = $sql . " and c.owner = $ownerId";
		}
		else {
			//TODO
		}
		
		if($clientId != '') {
			$sql = $sql . " and a.clientid = $clientId";
		}
		
		if($status != '') {
			$sql = $sql . " and a.status = '$status'";
		}
		
		if($fromDate != '' && $toDate != '') {
			$fromDate = strtotime($fromDate . '00:00:00');
			$toDate = strtotime($toDate . '23:59:59');
			$sql = $sql . " and a.createtime between $fromDate and $toDate";
		}
		
		$sql = $sql . " order by a.createtime desc";
		
		//分页绑定
		$datar = array();
		$datar['total']  = $this->getPaginationDataSumCount($sql);
		$datar['rows'] = $this->getPaginationData($sql, I('get.page'), I('get.rows'));
		$this->ajaxReturn($datar);
	}
	
	/**
	 * 跳转订单管理页面
	 * accessOrderForStoreList
	 * @return return_type
	 * @throws
	 */
	public function accessOrderForStoreList()
	{
		$this->views('accessOrderForStoreList');
	}
	
	/**
	 * 加载订单管理列表数据
	 * ajaxAccessOrderForStoreList
	 * @return return_type
	 * @throws
	 */
	public function ajaxAccessOrderForStoreList()
	{
		$clientId = I('get.clientid', '');
		$status = I('get.status', '');
		$fromDate = I('get.fromdate', '');
		$toDate = I('get.todate', '');
		
		$model = new Model();
		$sql = "select
				 a.id
				,b.name as clientname
				,a.orderno
				,a.status
				,a.telphone
				,ifnull(a.price, '-') as price
				,ifnull(a.extra, '-') as extra
				,ifnull(a.price, 0) + ifnull(a.extra, 0) as allprice
				,a.paystatus
				,a.balance
				from wf_access_order a, wf_sys_client b, wf_sys_clientconfig c
				where a.clientid = b.id
				and b.id = c.clientid";
		
		$storeId = $this->accessUsernameToRoleAndId(getUserInfo(), 'access_supplier');
		
		if($storeId != '-1') {
			$sql = $sql . " and c.supplier = $storeId";
		}
		else {
			//TODO
		}
		
		if($clientId != '') {
			$sql = $sql . " and a.clientid = $clientId";
		}
		
		if($status != '') {
			$sql = $sql . " and a.status = '$status'";
		}
		
		if($fromDate != '' && $toDate != '') {
			$fromDate = strtotime($fromDate . '00:00:00');
			$toDate = strtotime($toDate . '23:59:59');
			$sql = $sql . " and a.createtime between $fromDate and $toDate";
		}
		
		$sql = $sql . " order by a.createtime desc";
		
		//分页绑定
		$datar = array();
		$datar['total']  = $this->getPaginationDataSumCount($sql);
		$datar['rows'] = $this->getPaginationData($sql, I('get.page'), I('get.rows'));
		$this->ajaxReturn($datar);
	}
	
	/**
	 * 获取订单信息(首页最新订单)
	 * ajaxGetSixOrderList
	 * @return ajax
	 * @throws
	 */
	public function ajaxGetSixOrderList()
	{
		
		$model = D('AccessOrder');
		$data = array();
		
		
		$ownerid = $this->accessUsernameToRoleAndId(getUserInfo(), 'access_owner');
		if(0 < intval($ownerid)){//等于零表示有多个角色，但是不是admin。等于-1表示admin,-2(无角色)
			$data['`b`.`owner`'] = $ownerid;
		}
		
		$supplierid = $this->accessUsernameToRoleAndId(getUserInfo(), 'access_supplier');
		if(0 < intval($supplierid)){//等于零表示有多个角色，但是不是admin。等于-1表示admin,-2(无角色)
			$data['`b`.`supplier`'] =  $supplierid;
		}
		
		
		$datar = array();
		$datar['total']  = $model->alias('a')
		->join('LEFT JOIN `wf_sys_clientconfig` AS `b` ON `b`.`clientid` =`a`.`clientid`')
		->join('LEFT JOIN `wf_sys_client` AS `c` ON `c`.`id` =`a`.`clientid`')
		->where($data)->count();
		
		$datar['rows'] =  $model->alias('a')
		->field("CONCAT(' ".L('order_num')."： ',`a`.`orderno`,'&nbsp;&nbsp;&nbsp;&nbsp;".L('terminal')."：',`c`.`name`,'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',from_unixtime(`a`.`createtime`, '%Y-%m-%d')) as `text`")
		->join('LEFT JOIN `wf_sys_clientconfig` AS `b` ON `b`.`clientid` =`a`.`clientid`')
		->join('LEFT JOIN `wf_sys_client` AS `c` ON `c`.`id` =`a`.`clientid`')
		->where($data)->order(array('`a`.`id`'=>'desc'))
		->page(I('get.page').','.I('get.rows'))->select();
		$datar['sql'] = $model->getLastSql();
		$this->ajaxReturn($datar);
	}
	
	/**
	 * 跳转结算管理页面
	 * accessPayList
	 * @return return_type
	 * @throws
	 */
	public function accessPayList()
	{
		$this->views('accessPayList');
	}
	
	/**
	 * 加载结算数据
	 * ajaxAccessPayList
	 * @return return_type
	 * @throws
	 */
	public function ajaxAccessPayList()
	{
		$fromDate = I('get.fromdate', '');
		$toDate = I('get.todate', '');
		$balanceNo = I('get.balanceno', '');
		$storeId = I('get.storeid', '');
		$balance = I('get.balance', '');
		
		$model = new Model();
		$sql = "select
				 a.id
				,c.name as supplier
				,a.orderno
				,a.status as orderstatus
				,a.price + a.extra as orderprice
				,a.paystatus
				,a.balance
				,a.balanceno
				from wf_access_order a, wf_sys_clientconfig b, wf_access_store c
				where a.clientid = b.clientid
				and b.supplier = c.id
				and a.status = 0
				and a.paystatus = 1";
		
		$footerSql = "select
					 'Totle：' as supplier
					,count(a.id) as orderno
					,sum(a.price) + sum(a.extra) as orderprice
					from wf_access_order a, wf_sys_clientconfig b, wf_access_store c
					where a.clientid = b.clientid
					and b.supplier = c.id
					and a.status = 0
					and a.paystatus = 1";
		
		$ownerId = $this->accessUsernameToRoleAndId(getUserInfo(), 'access_owner');
		
		if($ownerId != '-1') {
			$sql = $sql . " and b.owner = $ownerId";
			$footerSql = $footerSql . " and b.owner = $ownerId";
		}
		else {
			//TODO
		}
		
		if($fromDate != '' && $toDate != '') {
			$fromDate = strtotime($fromDate . '00:00:00');
			$toDate = strtotime($toDate . '23:59:59');
			$sql = $sql . " and a.createtime between $fromDate and $toDate";
			$footerSql = $footerSql . " and a.createtime between $fromDate and $toDate";
		}
			
		if($balanceNo != '') {
			$sql = $sql . " and a.balanceno = '$balanceNo'";
			$footerSql = $footerSql . " and a.balanceno = '$balanceNo'";
		}
		
		if($storeId != '') {
			$sql = $sql . " and b.supplier = $storeId";
			$footerSql = $footerSql . " and b.supplier = $storeId";
		}
		
		if($balance != '') {
			$sql = $sql . " and a.balance = $balance";
			$footerSql = $footerSql . " and a.balance = $balance";
		}
		
		$sql = $sql . " order by a.createtime desc";
		
		//统计合计绑定数据
		$datar = array();
		$datar['total']  = $this->getFooterTotal($sql);
		$datar['rows'] = $this->getFooterRows($sql);
		$datar['footer'] = $this->getFooterInfo($footerSql);
		$this->ajaxReturn($datar);
	}
	
	/**
	 * 执行结算操作
	 * ajaxBalanceAccessPay
	 * @return return_type
	 * @throws
	 */
	public function ajaxBalanceAccessPay()
	{
		$storeId = I('post.storeid', '');
		$fromdate = I('post.fromdate', '');
		$todate = I('post.todate', '');
		
		$model = new Model();
		$sql = "select a.id, a.orderno
				from wf_access_order a, wf_sys_clientconfig b, wf_access_store c
				where a.clientid = b.clientid
				and b.supplier = c.id
				and a.status = 0
				and a.paystatus = 1
				and a.balance = 0";
		
		$ownerId = $this->accessUsernameToRoleAndId(getUserInfo(), 'access_owner');
		
		if($ownerId != '-1') {
			$sql = $sql . " and b.owner = $ownerId";
		}
		else {
			//TODO
		}
		
		if($storeId != '') {
			$sql = $sql . " and b.supplier = $storeId";
		}
		
		if($fromDate != '' && $toDate != '') {
			$fromDate = strtotime($fromDate . '00:00:00');
			$toDate = strtotime($toDate . '23:59:59');
			$sql = $sql . " and a.createtime between $fromDate and $toDate";
		}
		
		$data = $model->query($sql);
		
		if(count($data) > 0) {
			$nowDate = date('Ymd', time());
			$modelOrder = D('AccessOrder');
			$dataBalanceNo = $modelOrder->field('balanceno')->where("left(balanceno, 8) = $nowDate")->order('balanceno desc')->select();
			if(count($dataBalanceNo) > 0) {
				$balanceNo = (int)$dataBalanceNo[0]['balanceno'] + 1;
			}
			else {
				$balanceNo = $nowDate . '001';
			}
			foreach ($data as $row) {
				
				$modelOrder->id = $row['id'];
				$modelOrder->updateuser = getUserInfo();
				$modelOrder->updatetime = time();
				$modelOrder->balance = 1;
				$modelOrder->balanceno = $balanceNo;
				$modelOrder->save();
			}
			
			$this->jsonData['status'] = 200;
			$this->jsonData['msg'] = '结算成功！本次结算的结算单号为：' . $balanceNo;
		}
		else {
			$this->jsonData['status'] = 300;
			$this->jsonData['msg'] = '无需要结算的数据，请核实结算信息！';
		}
		
		$this->ajaxReturn($this->jsonData);
	}
	
	
	
	
	
	/**
	 * 跳转销售报表
	 * accessStatistics1
	 * @throws
	 */
	public function accessStatistics1()
	{
		$this->views("accessStatistics1");
	}
	
	/**
	 * 销售报表
	 */
	public function ajaxOrderStorage1(){
		/*
		$datar = array();
		$datar['title'] = array();
		$datar['title']['text'] = "前十二个月订单销售";
		$datar['title']['subtext'] = "";
		
		$datar['tooltip'] = array();
		$datar['tooltip']['trigger'] = 'axis';
		
		$datar['legend'] = array();
		$datar['legend']['data'] = array("销售曲线");
		
		$datar['toolbox'] = array();
		$datar['toolbox']['show'] = true;
		$datar['toolbox']['feature'] = array();
		$datar['toolbox']['feature']['dataView']=array('readOnly'=>false);
		$datar['toolbox']['feature']['restore']=array();
		$datar['toolbox']['feature']['saveAsImage']=array();
		
		
		
		
		$datar['xAxis'] = array();
		$datar['xAxis'][0] = array();
		$datar['xAxis'][0]['type'] = 'category';
		$datar['xAxis'][0]['boundaryGap'] = true;
		$datar['xAxis'][0]['data'] = array('21041','21043','21045','21047','21049','21051','21053','21055','21057','21059','21067','21079');
		
		
		$datar['yAxis'] = array();
		$datar['yAxis'][0] = array();
		$datar['yAxis'][0]['type'] = 'value';
		$datar['yAxis'][0]['scale'] = true;
		$datar['yAxis'][0]['name'] = '销售额';
		$datar['yAxis'][0]['max'] = 20;
		$datar['yAxis'][0]['min'] = 0;
		$datar['yAxis'][0]['boundaryGap'] = array(0.2, 0.2);
		
		
		
		$datar['series'] = array();
		
		
		$datar['series'][0] = array();
		$datar['series'][0]['name'] = '最新成交价';
		$datar['series'][0]['type'] = 'line';
		$datar['series'][0]['data'] = array(14.4,10.6,6.3,8.3,14.8,11.1,8.4,6.4,10.4,13.8,6.3,8.3);
		
		*/
		$year = date('Y');
		$month = date('m');
		if(I('post.year')){
			$year = I('post.year');
		}
		if(I('post.month')){
			$month = I('post.month') + 1;
		}
		$num = 12;//12个月
		$type = "Ym";
		$d=mktime(0, 0, 0, $month, 1, $year);//时，分，秒，月，日，年（向前推12个月份最晚时间点）
		
		$datar = array();
		$datar['title'] = array();
		$datar['title']['text'] = L("twelve_months_ago_order_sales");//"前十二个月订单销售";
		$datar['title']['subtext'] = "";
		
		$datar['tooltip'] = array();
		$datar['tooltip']['trigger'] = 'axis';
		
		$datar['legend'] = array();
		$datar['legend']['data'] = array(L("sales_curve"));
		
		$datar['toolbox'] = array();
		$datar['toolbox']['show'] = true;
		$datar['toolbox']['feature'] = array();
		$datar['toolbox']['feature']['dataView']=array('readOnly'=>false);
		$datar['toolbox']['feature']['restore']=array();
		$datar['toolbox']['feature']['saveAsImage']=array();
		
		
		
		
		$datar['xAxis'] = array();
		$datar['xAxis'][0] = array();
		$datar['xAxis'][0]['type'] = 'category';
		$datar['xAxis'][0]['boundaryGap'] = true;
	//	$datar['xAxis'][0]['data'] = array('21041','21043','21045','21047','21049','21051','21053','21055','21057','21059','21067','21079');
		for($i = 0;$i < $num;$i++){
			$datar['xAxis'][0]['data'][] = date($type, strtotime(($i-$num)." month",$d));
		}
		
	
		
		$datar['series'] = array();
		
		
		$datar['series'][0] = array();
		$datar['series'][0]['name'] = L('latest_price');//'最新成交价';
		$datar['series'][0]['type'] = 'line';
		//$datar['series'][0]['data'] = array(14.4,10.6,6.3,8.3,14.8,11.1,8.4,6.4,10.4,13.8,6.3,8.3);
		$datar['series'][0]['data'] = array();
		
		
		$where = "";
		$ownerid = $this->accessUsernameToRoleAndId(getUserInfo(), 'access_owner');//等于零表示有多个角色，但是不是admin。等于-1表示admin,-2(无对应的角色)
		$supplierid = $this->accessUsernameToRoleAndId(getUserInfo(), 'access_supplier');//等于零表示有多个角色，但是不是admin。等于-1表示admin,-2(无对应的角色)
		if("-1" !== $ownerid || "-1" !== $supplierid){
			$where01 = "OR 1!=1 ";
			$roles = getRoleKeysByUid(getUserId());
			if(in_array("assess_company", $roles)){
				$where01 = "OR 1=1 ";
			}
			$where .= "AND  ( `c`.`owner`=$ownerid OR `c`.`supplier`=$supplierid $where01)";
		}
		$datar['sql']=array();
		for($i = 0;$i < $num;$i++){
			$starttime = strtotime(date('Y-m-01', strtotime(($i-$num)." month",$d)));//开始时间戳
			$endtime = strtotime(date('Y-m-01', strtotime(($i-$num+1)." month",$d)));//结束时间戳
			$model = D('AccessOrder');
			$list = $model->alias('b')
			->field('SUM(`b`.`paymoney`) as `allmoney`')
			->where('`b`.`status`=0 and `b`.`createtime` >= '.$starttime.' and `b`.`createtime` < '.$endtime.' '.$where)
			->join('LEFT JOIN `wf_sys_clientconfig` AS `c` ON `c`.`clientid` =`b`.`clientid`')
			->find();
			$datar['sql'][] = $model->getLastSql();
			$datar['series'][0]['data'][] = (null==$list['allmoney']?'0':$list['allmoney']);
		}
		
		
		$datar['yAxis'] = array();
		$datar['yAxis'][0] = array();
		$datar['yAxis'][0]['type'] = 'value';
		$datar['yAxis'][0]['scale'] = true;
		$datar['yAxis'][0]['name'] = L('sales');//'销售额';
		
		$a = $datar['series'][0]['data'];
		
		$pos = array_search(max($a), $a);
		$datar['yAxis'][0]['max'] = intval(intval($a[$pos])*1.1);
		
		$datar['yAxis'][0]['min'] = 0;
		$datar['yAxis'][0]['boundaryGap'] = array(0.2, 0.2);
		
		
		$this->ajaxReturn($datar);
	}
	
	/**
	 * 跳转终端报表
	 * accessStatistics1
	 * @throws
	 */
	public function accessStatistics2()
	{
		$this->views("accessStatistics2");
	}
	
	/**
	 * 终端报表
	 */
	public function ajaxOrderStorage2(){
		/*
		$datar = array();
		$datar['title'] = array();
		$datar['title']['text'] = "终端销售情况";
		$datar['title']['subtext'] = "";
		
		$datar['tooltip'] = array();
		$datar['tooltip']['trigger'] = 'axis';
		
		$datar['legend'] = array();
		$datar['legend']['data'] = array("销售柱状图");
		
		$datar['toolbox'] = array();
		$datar['toolbox']['show'] = true;
		$datar['toolbox']['feature'] = array();
		$datar['toolbox']['feature']['dataView']=array('readOnly'=>false);
		$datar['toolbox']['feature']['restore']=array();
		$datar['toolbox']['feature']['saveAsImage']=array();
		
		
		$datar['dataZoom'] = array();
		$datar['dataZoom']['show'] = false;
		$datar['dataZoom']['start'] = 0;
		$datar['dataZoom']['end'] = 100;
		
		$datar['xAxis'] = array();
		$datar['xAxis'][0] = array();
		$datar['xAxis'][0]['type'] = 'category';
		$datar['xAxis'][0]['boundaryGap'] = true;
		$datar['xAxis'][0]['data'] = array('21041','21043','21045','21047','21049','21051','21053','21055','21057','21059');
		
		
		$datar['yAxis'] = array();
		$datar['yAxis'][0] = array();
		$datar['yAxis'][0]['type'] = 'value';
		$datar['yAxis'][0]['scale'] = true;
		$datar['yAxis'][0]['name'] = '销售额';
		$datar['yAxis'][0]['max'] = 20;
		$datar['yAxis'][0]['min'] = 0;
		$datar['yAxis'][0]['boundaryGap'] = array(0.2, 0.2);
		
		
		
		$datar['series'] = array();
		
		
		$datar['series'][0] = array();
		$datar['series'][0]['name'] = '最新成交价';
		$datar['series'][0]['type'] = 'bar';
		$datar['series'][0]['data'] = array(14.4,10.6,6.3,8.3,14.8,11.1,8.4,6.4,10.4,13.8);
		
		
		*/
		$mode = new Model();
		
		$datar = array();
		$datar['title'] = array();
		$datar['title']['text'] = L('terminal_sales');//"终端销售情况";
		$datar['title']['subtext'] = "";
		
		$datar['tooltip'] = array();
		$datar['tooltip']['trigger'] = 'axis';
		
		$datar['legend'] = array();
		$datar['legend']['data'] = array(L('sales_histogram'));//"销售柱状图"
		
		$datar['toolbox'] = array();
		$datar['toolbox']['show'] = true;
		$datar['toolbox']['feature'] = array();
		$datar['toolbox']['feature']['dataView']=array('readOnly'=>false);
		$datar['toolbox']['feature']['restore']=array();
		$datar['toolbox']['feature']['saveAsImage']=array();
		
		
		$datar['dataZoom'] = array();
		$datar['dataZoom']['show'] = false;
		$datar['dataZoom']['start'] = 0;
		$datar['dataZoom']['end'] = 100;
		
		$datar['xAxis'] = array();
		$datar['xAxis'][0] = array();
		$datar['xAxis'][0]['type'] = 'category';
		$datar['xAxis'][0]['boundaryGap'] = true;
		//$datar['xAxis'][0]['data'] = array('21041','21043','21045','21047','21049','21051','21053','21055','21057','21059');
		$datar['xAxis'][0]['data'] = array();
		
		{
			$wheresaletime = " ";
			if(I('post.saletime_start')){
				$wheresaletime .= " and `b`.`createtime` > ".strtotime(I('post.saletime_start',0));
			}
			if(I('post.saletime_end')){
				$wheresaletime .= " and `b`.`createtime` < ".strtotime(I('post.saletime_end',0) . ' 23:59:59.999');
			}
			
			
			
			$sysid = C('SYSTEM_ID');
			$where = "";
			$ownerid = $this->accessUsernameToRoleAndId(getUserInfo(), 'access_owner');//等于零表示有多个角色，但是不是admin。等于-1表示admin,-2(无对应的角色)
			$supplierid = $this->accessUsernameToRoleAndId(getUserInfo(), 'access_supplier');//等于零表示有多个角色，但是不是admin。等于-1表示admin,-2(无对应的角色)
			if("-1" !== $ownerid || "-1" !== $supplierid){
				$where01 = "OR 1!=1 ";
				$roles = getRoleKeysByUid(getUserId());
				if(in_array("assess_company", $roles)){
					$where01 = "OR 1=1 ";
				}
				$where .= "AND  ( `c`.`owner`=$ownerid OR `c`.`supplier`=$supplierid $where01)";
			}
			$sql = "SELECT `a`.`id`, `a`.`name`,sum(`b`.`paymoney`) as `allpaymoney`
				FROM wf_sys_client `a`
				left join wf_access_order `b` on `a`.`id`=`b`.`clientid`
				left join wf_sys_clientconfig `c` on `a`.`id` = `c`.`clientid`
				WHERE   (`a`.`STATUS` = 1 OR `a`.`STATUS` = 2 OR `a`.`STATUS` = 3) $where $wheresaletime AND `a`.`sysid` = '$sysid' and `b`.`status`=0 GROUP BY `a`.`id`
			";
			
			$data = $mode->query($sql);
		}
		
		$datar['sql'] = $mode->getLastSql();
		
		
		
		
		
		
		$datar['series'] = array();
		
		$datar['series'][0] = array();
		$datar['series'][0]['name'] = L('latest_price');//'最新成交价';
		$datar['series'][0]['type'] = 'bar';
		//$datar['series'][0]['data'] = array(14.4,10.6,6.3);
		foreach ($data as $val){
			$datar['xAxis'][0]['data'][]=$val['name'];
			$datar['series'][0]['data'][]=$val['allpaymoney'];
		}
		
		
		
		
		
		
		
		$datar['yAxis'] = array();
		$datar['yAxis'][0] = array();
		$datar['yAxis'][0]['type'] = 'value';
		$datar['yAxis'][0]['scale'] = true;
		$datar['yAxis'][0]['name'] = L('sales');//'销售额';
		
		$a = $datar['series'][0]['data'];
		
		$pos = array_search(max($a), $a);
		$datar['yAxis'][0]['max'] = intval(intval($a[$pos])*1.1);
		
		$datar['yAxis'][0]['min'] = 0;
		
		$datar['yAxis'][0]['boundaryGap'] = array(0.2, 0.2);
		
		
		
		
		
		$this->ajaxReturn($datar);
	}
	
	
}

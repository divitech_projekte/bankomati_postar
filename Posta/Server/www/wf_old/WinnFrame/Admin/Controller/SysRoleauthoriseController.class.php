<?php
namespace Admin\Controller;

/**
 * 系统权限角色关系
 * Admin\Controller$SysRoleauthoriseController
 * Copyright (c) 2016. 云绅（张家港）精密工业有限公司 版权所有
 * 修改人：jcccy
 * 修改时间：2016年3月7日 下午4:54:02
 * 修改内容：
 */
class SysRoleauthoriseController extends BaseController {
	public $fields = 'status,pid,name,action,target,type,sort';
	
	public function _initialize(){
		parent::_initialize();
	}
	
	
	
}
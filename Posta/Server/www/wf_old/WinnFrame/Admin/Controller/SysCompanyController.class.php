<?php
namespace Admin\Controller;

/**
 * 公司信息
 * Admin\Controller$SysCompanyController
 * Copyright (c) 2016. 云绅（张家港）精密工业有限公司 版权所有
 * 修改人：jcccy
 * 修改时间：2016年3月7日 下午4:54:31
 * 修改内容：
 */
class SysCompanyController extends BaseController {
	public $fields = 'status,name,shortname,code,address,linkman,phone,email,type';
	
	public function _initialize(){
		parent::_initialize();
	}

	/**
	 * 加载界面
	 */
	public  function toList(){
		$this->views("list");
	}
	
	/**
	 * ajax加载分页数据
	 */
	public  function ajaxList(){
		
		
		$model = D('SysCompany');
		$data = array();
		$datar = array();
		$datar['total']  = $model->where($data)->count();
		$datar['rows'] =  $model->alias('a')
		->field("`a`.`id`,`a`.`status`,`a`.`name`,`a`.`shortname`,`a`.`code`,`a`.`address`,`a`.`linkman`,`a`.`phone`,`a`.`email`,`a`.`type`,`a`.`regdate`,`a`.`cc`")
		->where($data)->order(array('`a`.`id`'=>'asc'))
		->page(I('get.page').','.I('get.rows'))->select();
		$this->ajaxReturn($datar);
		
		
		
		
	}
	
	/**
	 * ajax删除ID指定的数据
	 */
	public  function ajaxDel(){
		$datab = array("msg"=>"no");
		$model = D('SysCompany');
		$id = I('post.id',0);
		$datab = array();
		if($model->delete($id)){
			$datab['msg'] = "ok";
		}
		$this->ajaxReturn($datab);
	}
	
	/**
	 * ajax添加一条数据
	 */
	public  function ajaxAdd(){
		$datab = array("msg"=>"no");
		$model = D('SysCompany');
		$model->create();
		$model->regdate = time();
		//$model->apikey = md5(time());
		if($model->field('id,createtime,createuser,regdate,apikey,cc,issms,isapi,apikey,apitype,apiurl,'.$this->fields)->add()){
			$datab['msg'] = "ok";
		}
		$this->ajaxReturn($datab);
	}
	
	/**
	 * ajax修改一条数据
	 */
	public  function ajaxEdit(){
			$datab = array("msg"=>"no");
			$model = D('SysCompany');
			$model->create();
			if($model->field('id,updatetime,updateuser,cc,issms,isapi,apikey,apitype,apiurl,'.$this->fields)->save()){
				$datab['msg'] = "ok";
			}
			$this->ajaxReturn($datab);
	}
	
	
	/**
	 * ajax获取一条数据
	 */
	public  function ajaxGetDataById(){
		$datab = array();
		$model = D('SysCompany');
		$id = I('get.id',0);

		$obj = $model->where('id='.$id)->field('id,status,name,shortname,code,address,linkman,phone,email,type,cc,issms,isapi,apikey,apitype,apiurl,FROM_UNIXTIME(regdate,\'%Y-%m-%d\') as regdate')->find();
	
		$this->ajaxReturn($obj);
	}
	
	
	/**
	 * ajax加载分页数据
	 */
	public  function ajaxSelectList(){
	
		$model = D('SysCompany');
		$data = array();
	
	
		$datar = $model->alias('a')
		->field("`a`.`id`,CONCAT(`a`.`name`,'/',`a`.`code`) as `text`,'' as `desc`")
		->where($data)->order(array('`a`.`id`'=>'asc'))
		->select();
		$this->ajaxReturn($datar);
	
	}
	
	
	
	/**
	 * ajax加载分页数据
	 */
	public  function ajaxListByUser(){
	
		$model = D('SysCompany');
		$userid = I('get.userid','');
		$data = array();
		if("" != I('get.name',"")){
			$data['name'] = array('like','%'.I('get.name').'%');
		}
	
	
		$datar = array();
		$datar['total']  = $model->where($data)->count();
		if("" != $userid){
			$datar['rows'] =  $model->alias('a')
			//->field("`a`.`id`,`a`.`status`,`a`.`name`,IF ((select count(*) from wf_sysroleauthorise as b where b.menuid=".$authid." and b.roleid=a.id) = 0),'false','true') as  `checked`")
			->field("`a`.`id`,`a`.`status`,`a`.`name`,(select (SELECT IF (0=(select count(*) from wf_sys_companyuser as b where b.userid=".$userid." and b.companyid=a.id),'','true') AS  `checked`)) AS  `checked`")
			//->field("`a`.`id`,`a`.`status`,`a`.`name`,cast((select count(*) from wf_sysroleauthorise as b where b.menuid=".$authid." and b.roleid=a.id) as char) as `num`")
			->where($data)->order(array('`a`.`id`'=>'asc'))
			->page(I('get.page').','.I('get.rows'))->select();
		}else{
			$datar['rows'] =  $model->alias('a')
			->field("`a`.`id`,`a`.`status`,`a`.`name`")
			->where($data)->order(array('`a`.`id`'=>'asc'))
			->page(I('get.page').','.I('get.rows'))->select();
		}
		$this->ajaxReturn($datar);
	}
	
	
	
	/**
	 * 跳转到分配页面
	 */
	public function toAllotCompanyList(){
		$this->views("allotCompany");
	}
	
	
	
	/**
	 * ajax添加一条关系数据
	 */
	public  function ajaxAddUser(){
		$datab = array("msg"=>"no");
		$model = D('SysCompanyuser');
		$data = array();
		$model->create();
		$model->companyid = I("post.companyid",'');
		$model->userid = I("post.userid",'');
	
		if($model->field("companyid,userid")->add()){
			$datab['msg'] = "ok";
		}
		$this->ajaxReturn($datab);
	}
	
	
	
	public  function ajaxDelUser(){
		$datab = array("msg"=>"no");
		$model = D('SysCompanyuser');
	
		if($model->where('companyid='.I("post.companyid",'0').' AND userid='.I("post.userid",'0'))->delete()){
			$datab['msg'] = "ok";
		}
		$this->ajaxReturn($datab);
	}
	
	
	
	public function ajaxListByClient(){

		$model = D('SysCompany');
		$clientid = I('get.clientid','');
		$data = array();
		if("" != I('get.name',"")){
			$data['name'] = array('like','%'.I('get.name').'%');
		}
		
		
		$datar = array();
		$datar['total']  = $model->where($data)->count();
		if("" != $clientid){
			$datar['rows'] =  $model->alias('a')
			//->field("`a`.`id`,`a`.`status`,`a`.`name`,IF ((select count(*) from wf_sysroleauthorise as b where b.menuid=".$authid." and b.roleid=a.id) = 0),'false','true') as  `checked`")
			->field("`a`.`id`,`a`.`status`,`a`.`name`,(select (SELECT IF (0=(select count(*) from wf_sys_companyclient as b where b.clientid=".$clientid." and b.companyid=a.id),'','true') AS  `checked`)) AS  `checked`")
			//->field("`a`.`id`,`a`.`status`,`a`.`name`,cast((select count(*) from wf_sysroleauthorise as b where b.menuid=".$authid." and b.roleid=a.id) as char) as `num`")
			->where($data)->order(array('`a`.`id`'=>'asc'))
			->page(I('get.page').','.I('get.rows'))->select();
		}else{
			$datar['rows'] =  $model->alias('a')
			->field("`a`.`id`,`a`.`status`,`a`.`name`")
			->where($data)->order(array('`a`.`id`'=>'asc'))
			->page(I('get.page').','.I('get.rows'))->select();
		}
		$this->ajaxReturn($datar);
	} 
	
	
	/**
	 * ajax添加一条关系数据
	 */
	public  function ajaxAddClient(){
		$datab = array("msg"=>"no");
		$model = D('SysCompanyclient');
		$data = array();
		$model->create();
		$model->companyid = I("post.companyid",'');
		$model->clientid = I("post.clientid",'');
	
		if($model->field("companyid,clientid")->add()){
			$datab['msg'] = "ok";
		}
		$this->ajaxReturn($datab);
	}
	
	
	
	public  function ajaxDelClient(){
		$datab = array("msg"=>"no");
		$model = D('SysCompanyclient');
	
		if($model->where('companyid='.I("post.companyid",'0').' AND clientid='.I("post.clientid",'0'))->delete()){
			$datab['msg'] = "ok";
		}
		$this->ajaxReturn($datab);
	}
	
}
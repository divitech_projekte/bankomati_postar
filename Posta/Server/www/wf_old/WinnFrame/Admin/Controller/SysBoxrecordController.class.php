<?php
namespace Admin\Controller;

/**
 * 柜箱使用记录
 * Admin\Controller$SysBoxrecordController
 * Copyright (c) 2016. 云绅（张家港）精密工业有限公司 版权所有
 * 修改人：jcccy
 * 修改时间：2016年3月7日 下午4:54:14
 * 修改内容：
 */
class SysBoxrecordController extends BaseController {
	public $fields = 'boxid,orderid,operateuser,operatetype,operatetime,remark';
	
	public function _initialize(){
		parent::_initialize();
	}

	/**
	 * 加载界面
	 */
	public  function toList(){
		$this->views("list");
	}
	
	/**
	 * ajax加载分页数据
	 */
	public  function ajaxList(){
		
		$model = D('SysBoxrecord');
		
		$data = array();
		
		
		$datar = array();
		$datar['total']  = $model->where($data)->count();
		$datar['rows'] =  $model->alias('a')
		->field("`a`.`id`,`a`.`boxid`,`a`.`orderid`,`a`.`operateuser`,`a`.`operatetype`,`a`.`operatetime`,`a`.`remark`,`b`.`no` as `boxno`,`c`.`csn`")
		->join(array('LEFT JOIN `wf_sys_box` AS `b` ON `b`.`id` =`a`.`boxid`','LEFT JOIN `wf_sys_client` AS `c` ON `c`.`id` =`b`.`clientid`'))
		->where($data)->order(array('`a`.`id`'=>'asc'))
		->page(I('get.page').','.I('get.rows'))->select();
		
		
		$this->ajaxReturn($datar);
		

	}
	
	/**
	 * ajax删除ID指定的数据
	 */
	public  function ajaxDel(){
		$model = D('SysBoxrecord');
		$id = I('post.id',0);
		$datab = array();
		$model->delete($id);
		if(true){
			$datab['msg'] = "ok";
		}else{
			$datab['msg'] = "no";
		}
		$this->ajaxReturn($datab);
	}
	
	/**
	 * ajax添加一条数据
	 */
	public  function ajaxAdd(){
		
		$datab = array();
		
	
		
		
		$model = D('SysBoxrecord');
		if( false !== $model->field($this->fields)->addTo()){
			$datab['msg'] = "ok";
		}else{
			$datab['msg'] = "no";
		}
		$this->ajaxReturn($datab);
	}
	
	/**
	 * ajax修改一条数据
	 */
	public  function ajaxEdit(){
			$datab = array();
		
			
		
			$model = D('SysBoxrecord');
			if( false !== $model->field('id,'.$this->fields)->saveTo()){
				$datab['msg'] = "ok";
			}else{
				$datab['msg'] = "no";
			}
			$this->ajaxReturn($datab);
	}
	
	/*
	 * 跳转到 上下货记录 打印页面
	 */
	public function printMilkRecord(){
		$model = D('SysBoxrecord');
		$data = $model->alias('a')
		->field("`a`.`id`,
				`a`.`operateuser`,
				`a`.`operatetype`,
				from_unixtime(`a`.`operatetime`) as `operatetime`,
				`a`.`remark`,
				`b`.`no` as `boxno`,
				`c`.`csn`,
				`c`.`name` as `clientname`")
		->join(array('LEFT JOIN `wf_sys_box` AS `b` ON `b`.`id` =`a`.`boxid`','LEFT JOIN `wf_sys_client` AS `c` ON `c`.`id` =`b`.`clientid`'))
		->where("`a`.`operatetype` = 29 and `a`.`id`=".I('get.id'))->order(array('a.operatetime'=>'desc','b.no'=>'asc'))->find();
	
		$this->assign("recordObj",$data);
		$this->views("printMilkRecord");
	}
	
	
	
	public function accessHistoryList(){
		$this->views("accessHistoryList");
	}
	
	public function ajaxAccessPrintList(){
		$clientid = I('get.clientid', '');
		$cardid = I('get.cardid', '');
		$orderno = I('get.orderno', '');
		
		$sql = "select
				 c.id
				,a.name as clientname
				,a.position
				,c.operateuser
				,FROM_UNIXTIME(c.operatetime) as operatetime
				,c.remark
				,d.orderno
				from wf_sys_client a, wf_sys_box b, wf_sys_boxrecord c,wf_access_order d
				where a.id = b.clientid
				and b.id = c.boxid
				and a.sysid = 'access'
				and (c.operatetype = 21 or c.operatetype = 22 or c.operatetype = 23 or c.operatetype = 24) -- 21：客户存入物品 22：快递员取出物品  23：快递员存入物品 24：客户取出物品
				and c.orderid = d.id 
				";
		
		
		if($clientid != "") {
			$sql = $sql . " and a.id = '$clientid'";
		}
		
		if($orderno != "") {
			$sql = $sql . " and d.orderno like '%$orderno%'";
		}
		
		if($cardid != "") {
			$sql = $sql . " and c.operateuser = '$cardid'";
		}
		
		$sql = $sql . " order by c.createtime desc";
		
		$datar = array();
		$datar['total']  = $this->getPaginationDataSumCount($sql);
		$datar['rows'] = $this->getPaginationData($sql, I('get.page'), I('get.rows'));
		$this->ajaxReturn($datar);
	}
}
<?php
namespace Admin\Model;

/**
 * 系统操作日志
 * Admin\Model$SysLogModel
 * Copyright (c) 2016. 云绅（张家港）精密工业有限公司 版权所有
 * 修改人：jcccy
 * 修改时间：2016年3月7日 下午4:57:40
 * 修改内容：
 */
class SysLogModel extends BaseModel{
	// 定义自动验证   
	
	
	public function lists($where,$page,$rows){
	
	
	
		$data = array();
		$data['total']  = $this->where($where)->count();
		$data['rows'] =  $this->where($where)->order(array('id'=>'asc'))->page($page.','.$rows)->select();
	
	
		return $data;
	}
	
}
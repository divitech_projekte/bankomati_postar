<?php
return array(
	//'配置项'=>'配置值'
	'theme'=>'default',//当前主题
	
	'LANG_SWITCH_ON' => true,// 开启语言包功能
	'LANG_AUTO_DETECT' => false, // 自动侦测语言 开启多语言功能后有效
	'DEFAULT_LANG' => 'en-us', // 默认语言（关闭自动侦查才有用）
	'LANG_LIST' => 'en-us,zh-cn', // 允许切换的语言列表 用逗号分隔
	'VAR_LANGUAGE'     => 'language', // 默认语言切换变量
	
	'DEFAULT_AJAX_RETURN' => 'COMP',
	'FILE_UPLOAD_TYPE'    =>  'Local',    // 文件上传方式
);
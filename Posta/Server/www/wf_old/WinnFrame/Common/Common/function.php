<?php


function getTest2($uid){
	
	return 'AAAAAAAAAAAAAAAA';
}



/**
 *
 * @param 接收人 $sendee
 * @param 模板类型 $type 1：短息 2：邮件 3：微信
 * @param 业务类型 $code
 * @param 传入参数 $arr
 * @param 流水号 $serNo
 * @param 关联订单 $order
 * @param 发送人 $sender
 * @return number //1发送成功   2传入参数不全,发送失败   3启用模板未找到,发送失败    4调用第三方短信接口，发送失败   0未知异常，发送失败
 */
function sendSMS($sendee,$type,$code,$arr = array(),$serNo,$order=0,$sender = ""){
	$boo = 0;//失败
	$sysId = C('SYSTEM_ID');
	$model2 = M('SysSmsstencil');
	$obj = $model2->where("type='$type' and code='$code' and status='1' and sysid='$sysId'")->find();
	if(null !== $obj){
			$info = $obj['info'];
			$num = preg_match_all('/{%[^%]+%}/',$info,$match);
			
			foreach ($match[0] as $value)//模板中的变量,例如：$value = {%orderno%}
			{
				$val = substr($value, 2,strlen($value)-4);//{%orderno%} => orderno
				if(isset($arr[$val])){
					$info = str_replace($value,$arr[$val],$info);
				}else{
					$boo = 2;// 2传入参数不全,发送失败
					return $boo;
				}
			}
			
			$sendMsgBool = false;//调用第三方接口成功
			//echo "开始发生短信{---";
			//type 1：短息 2：邮件 3：微信
			
			if('1' === $type){
				$key = C('SMS.Key');
				$Secret = C('SMS.Secret');
				$sendee = C('SMS.CC') . $sendee;
				$info = C('SMS.Sign') . $info;
				$infox = urlencode($info);
				$url="http://api.synhey.com/json?key=$key&secret=$Secret&to=$sendee&text=$infox";
				
				$json = file_get_contents($url);
				$objjson = json_decode($json);
				switch ($objjson->status){
					case '0':
						$sendMsgBool = true;
						break;
					default :
				}
				
				/*
				$json = "";
				$sendMsgBool = true;
				*/
				
				$log = D('SysLog');
				$log->username = 'system';
				$log->time = time();
				$log->ip = 'localhost';
				$log->module = "sms";
				$log->action = $url;
				$log->info = $json;
				$log->add();
			}
			
				
			
			
			//echo "---}";
			if($sendMsgBool){
				$model = M('SysSms');
				$model->create();
				$model->stencilid = $obj['id'];
				$model->orderno = $order;
				$model->sendee = $sendee;
				$model->content = $info;
				$model->sender = $sender;
				$model->sendtime = time();//发送时间
				$model->resendtimes = 1;//发送次数
				$model->status = 1;
				$model->tid = $serNo;//getSerNo();
				if( false !== $model->field("stencilid,orderno,sendee,content,sender,sendtime,resendtimes,status,tid")->add()){
					$boo = 1;//1发送成功
				}
			}else{
				$boo = 4;//4 调用第三方短信接口，发生失败 
			}
	
		}else{
			$boo = 3;//3启用模板未找到
		}
	
	return $boo;
}

/**
 *
 * @param 接收人 $sendee
 * @param 传入参数 $arr
 * @param 流水号 $serNo
 * @param 关联订单 $order
 * @param 发送人 $sender
 * @param 模板id
 * @return number //1发送成功   2传入参数不全,发送失败   3启用模板未找到,发送失败    4调用第三方短信接口，发送失败   0未知异常，发送失败
 */
function sendSMS_New($sendee, $arr = array(), $serNo, $order=0, $sender = "", $smsstencilId){
	$boo = 0;//失败
	$sysId = C('SYSTEM_ID');
	$model2 = M('SysSmsstencil');
	$obj = $model2->getById($smsstencilId);
	if(null !== $obj){
		$info = $obj['info'];
		$num = preg_match_all('/{%[^%]+%}/',$info,$match);
			
		//dump($match[0]);
		//dump($arr);
		foreach ($match[0] as $value)//模板中的变量,例如：$value = {%orderno%}
		{
			$val = substr($value, 2,strlen($value)-4);//{%orderno%} => orderno
			if(isset($arr[$val])){
				$info = str_replace($value,$arr[$val],$info);
			}else{
				$boo = 2;// 2传入参数不全,发送失败
				return $boo;
			}
		}
			
		$sendMsgBool = false;//调用第三方接口成功
		//echo "开始发生短信{---";
		//type 1：短息 2：邮件 3：微信
			
		if('1' === $obj['type']){
			$key = C('SMS.Key');
			$Secret = C('SMS.Secret');
			$sendee = $sendee;
			$info = C('SMS.Sign') . $info;
			$infox = urlencode($info);
			$url="http://api.synhey.com/json?key=$key&secret=$Secret&to=$sendee&text=$infox";

			$json = file_get_contents($url);
			$objjson = json_decode($json);
			switch ($objjson->status){
				case '0':
					$sendMsgBool = true;
					break;
				default :
			}

			/*
				$json = "";
				$sendMsgBool = true;
				*/

			$log = D('SysLog');
			$log->username = 'system';
			$log->time = time();
			$log->ip = 'localhost';
			$log->module = "sms";
			$log->action = $url;
			$log->info = $json;
			$log->add();
		}
		else {
			//TODO
		}
			

			
			
		//echo "---}";
		if($sendMsgBool){
			$model = M('SysSms');
			$model->create();
			$model->stencilid = $smsstencilId;
			$model->orderno = $order;
			$model->sendee = $sendee;
			$model->content = $info;
			$model->sender = $sender;
			$model->sendtime = time();//发送时间
			$model->resendtimes = 1;//发送次数
			$model->status = 1;
			$model->tid = $serNo;//getSerNo();
			if( false !== $model->field("stencilid,orderno,sendee,content,sender,sendtime,resendtimes,status,tid")->add()){
				$boo = 1;//1发送成功
			}
		}else{
			$model = M('SysSms');
			$model->create();
			$model->stencilid = $smsstencilId;
			$model->orderno = $order;
			$model->sendee = $sendee;
			$model->content = $info;
			$model->sender = $sender;
			$model->sendtime = time();//发送时间
			$model->resendtimes = 1;//发送次数
			$model->status = 0;
			$model->tid = $serNo;//getSerNo();
			if( false !== $model->field("stencilid,orderno,sendee,content,sender,sendtime,resendtimes,status,tid")->add()){
				$boo = 4;//4 调用第三方短信接口，发生失败
			}
		}

	}else{
		$boo = 3;//3启用模板未找到
	}

	return $boo;
}

/**
 * 获取验证session
 * @return mixed|boolean
 */
function getSession()
{
	$url = "http://goldgroup.gomostratus.com/campaign/3.4/api";
	
	$username = C('GoMo.User');
	$password = C('GoMo.Password');
	
	$param = array(
			'action' => 'getSession',
			'username' => $username,
			'password' => $password,
	);
	$json = send_post($url, $param);
	if($json) {
		return json_decode($json);
	}
	else {
		return false;
	}
}

/**
 * 发送短信
 * @param 验证信息 $sessionid
 * @param 手机号码 $mobile
 * @param 短信内容 $message
 * @return mixed|boolean
 */
function sendSMS_GoMo($sessionid, $mobile, $message)
{
	$url = "http://goldgroup.gomostratus.com/campaign/3.4/api";
	
	$cid = C('GoMo.CID');
	
	$param = array(
			'action' => 'sendSMS',
			'sessionid' => $sessionid,
			'mobile' => $mobile,
			'message' => $message,
			'cid' => $cid,
	);
	$json = send_post($url, $param);
	if($json) {
		return json_decode($json);
	}
	else {
		return false;
	}
}

/**
 * 发送短信
 * @param 手机号码 $mobile
 * @param 短信内容 $message
 * @return mixed|boolean
 */
function sendSMS_SynHey($mobile, $message)
{
	$url = "http://api.synhey.com/json";
	
	$key = C('SMS.Key');
	$secret = C('SMS.Secret');
	$mobile = C('SMS.CC') . $mobile;
	
	$param = array(
			'key' => $key,
			'secret' => $secret,
			'to' => $mobile,
			'text' => $message,
	);
	$json = send_post($url, $param);
	if($json) {
		return json_decode($json);
	}
	else {
		return false;
	}
}

/**
 * 
 * @param 承包商apiKey $ownApiKey
 * @param 承包商apiPassword $ownApiPassword
 * @param 用户姓名 $userName
 * @param 用户卡号 $userCardNumber
 * @param 用户卡逾期月 $userCardExpiryMonth
 * @param 用户卡逾期年 $userCardExpiryYear
 * @param 交易金额 $money
 * @return 交易号|boolean
 * payByEway('F9802CuqDrDWKwyHiFNz6KjhI+9Z1V1sEMk90/yZsKTEDoSY2PHsDc97V0cCyyxvdbETw6','o1eYrKsL','John Smith','4444333322221111','12','25','1000')
 */
function payByEway(
					$ownApiKey,
					$ownApiPassword,
					$userName,
					$userCardNumber,
					$userCardExpiryMonth,
					$userCardExpiryYear,
					$money){
	require_once './Public/eway-rapid-php-master/include_eway.php';
	
	$apiEndpoint = \Eway\Rapid\Client::MODE_SANDBOX; // Use \Eway\Rapid\Client::MODE_PRODUCTION when you go live
	$client = \Eway\Rapid::createClient($ownApiKey, $ownApiPassword, $apiEndpoint);
	
	$transaction = [
			'Customer' => [
					'CardDetails' => [
							"Name"=> $userName, //必须
							"Number"=> $userCardNumber, //必须
							"ExpiryMonth"=> $userCardExpiryMonth, //必须
							"ExpiryYear"=> $userCardExpiryYear, //必须
					]
			],
			'Payment' => [
					'TotalAmount' => $money,
			],
			'TransactionType' => \Eway\Rapid\Enum\TransactionType::MOTO,
	];
	
	//兼容php5.3写法
	/**
	$transaction = array(
			'Customer'=>array(
					'CardDetails'=>array(
							'Name'=>$userName,
							'Number'=>$userCardNumber,
							'ExpiryMonth'=>$userCardExpiryMonth,
							'ExpiryYear'=>$userCardExpiryYear,
					),
			),
			'Payment'=>array(
					'TotalAmount'=>$money,
			),
			'TransactionType'=>\Eway\Rapid\Enum\TransactionType::MOTO,
	);
	**/
	
	$response = $client->createTransaction(\Eway\Rapid\Enum\ApiMethod::DIRECT, $transaction);
	
	$log = D('SysLog');
	$log->username = 'system';
	$log->time = time();
	$log->ip = 'localhost';
	$log->module = "pay";
	$log->action = 'payByEway';
	$accessCode = $response->AccessCode;
	
	if ($response->TransactionStatus) {
		$transactionid = $response->TransactionID;
		
		$log->info = $transactionid;
		$log->add();
		return $transactionid;
	}else{
		$info = "";
		foreach ($response->getErrors() as $error) {
			$info .="Error: ".\Eway\Rapid::getMessage($error)."<br>";
		}
		$log->info = $info;
		$log->add();
		return false;
	}
	
}

/**
 * 接口调用模块
 * 发送调用信息
 * @param 地址，参数
 * @return 接口返回参数
 */
function send_post($url, $post_data)
{
	$postdata = http_build_query($post_data);
	$options = array(
			'http' => array(
					'method' => 'POST',//or GET
					'header' => 'Content-type:application/x-www-form-urlencoded',
					'content' => $postdata,
					'timeout' => 2 // 超时时间*2（单位:s）
			)
	);
	$context = stream_context_create($options);
	$doCnt = 0;
	$result = null;
	while ($result == null && $doCnt < 3) {
		$doCnt++;
		$result = file_get_contents($url, false, $context);
	}
	return $result;
}

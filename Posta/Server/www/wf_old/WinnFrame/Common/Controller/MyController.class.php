<?php
namespace Common\Controller;
use Think\Controller;

/**
 * 公共控制器类
 * Common\Controller$MyController
 * Copyright (c) 2016. 云绅（张家港）精密工业有限公司 版权所有
 * 修改人：xucc
 * 修改时间：2016-3-3 下午03:59:56
 * 修改内容：
 */
class MyController extends Controller
{
	/*
	 * 初始化父类架构函数
	 */
	public function _initialize()
	{
		// TODO
	}
	
	/**
	 * 获取流水号
	 * 返回 1-9999
	 */
	public function getSerialNo()
	{
		$sysconfig = M("SysConfig");
		
		$condition = array();
		$condition['status'] = 1;
		$condition['key'] = 'serialno';
        $data = $sysconfig->where($condition)->field('value')->find();
        $serialNo = (int)$data['value'];
        
       	//更新流水号
       	$newSerialNo = 1;
        if($serialNo > 0 && $serialNo < 9999) {
        	$newSerialNo = $serialNo + 1;
        }
        $sysconfig->value = $newSerialNo;
      	$sysconfig->where($condition)->save();
        
        return $serialNo;
	}
	
	/**
	 * 根据token判读客户端是否存在
	 * 返回客户端信息 or false
	 * @param 令牌号 $token
	 */
	public function havingClient($token)
	{
		$sysClient = M("SysClient");
    	$condition['token'] = $token;
    	$dataClient = $sysClient->where($condition)->find();
    	return $dataClient;
	}
	
	/**
     * 根据clientid判断是否满足注销条件
     * 返回true or false
     * @param 令牌号 $token
     */
    public function cancelCondition($clientId)
    {
    	//判断是否存在存有物品的柜子
    	$sysBox = M("SysBox");
    	$condition['clientid'] = $clientId;
    	$condition['storagestatus'] = 1;
    	$dataBox = $sysBox->where($condition)->select();
    	if($dataBox) {
    		return false;
    	}
    	return true;
    }
}